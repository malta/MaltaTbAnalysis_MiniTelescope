#define TBCorrelation_cxx
#include "TBCorrelation.h"


#include <TStyle.h>
#include <TGraphAsymmErrors.h>
#include <TMarker.h>
#include <math.h>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <dirent.h>

#include "TLorentzVector.h"

using namespace std;

bool debug=false;
std::string outPutName="a.root";
float pitch=36.4; //or 4???
float distanceCut=500; //500;
float corrX=0;
float corrY=0;
int advanceN=0;
int maxEntries=0;
int startP=0;
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
void TBCorrelation::Loop()
{
  if (fEvent == 0) return;
  
  Long64_t nentries = fEvent->GetEntries();

  vector<TH1D*> histos1;
  vector<TH2D*> histos2;

  TH1D* prec_X =new TH1D("prec_X","prec_Y"  ,30, 0 ,60); histos1.push_back(prec_X);
  TH1D* prec_Y =new TH1D("prec_Y","prec_Y"  ,30, 0, 60); histos1.push_back(prec_Y);
  prec_X -> SetTitle("; intercept res X [#mum]; entries;");
  prec_Y -> SetTitle("; intercept res Y [#mum]; entries;");

  TH1D* res_X =new TH1D("res_X","res_X"  ,100, -500, 500); histos1.push_back(res_X);
  TH1D* res_Y =new TH1D("res_Y","res_Y"  ,100, -500, 500); histos1.push_back(res_Y);
  res_X -> SetTitle("; clu-trk #DeltaX [#mum]; entries;");
  res_Y -> SetTitle("; clu-trk #DeltaY [#mum]; entries;");
  TH1D* res_2D     =new TH1D("res_2D","res_2D",100,     0, 1000); histos1.push_back(res_2D);
  TH1D* res_2Dlarge=new TH1D("res_2Dlarge","res_2Dlarge",100,     0, 5000); histos1.push_back(res_2Dlarge);
  res_2D -> SetTitle("; clu-trk #sqrt(#DeltaX^2+#DeltaY^2) [#mum]; entries;");
  res_2Dlarge -> SetTitle("; clu-trk #sqrt(#DeltaX^2+#DeltaY^2) [#mum]; entries;");

  TH1D* fram_A =new TH1D("fram_A","fram_A"  ,(int)nentries/500,0,nentries); histos1.push_back(fram_A);
  TH1D* fram_P =new TH1D("fram_P","fram_P"  ,(int)nentries/500,0,nentries); histos1.push_back(fram_P);
  TH1D* fram_E =new TH1D("fram_E","fram_E"  ,(int)nentries/500,0,nentries); histos1.push_back(fram_E);
  TH1D* time_P =new TH1D("time_P","time_P"  ,128,0,400); histos1.push_back(time_P);
  TH1D* time_F =new TH1D("time_F","time_F"  ,128,0,400); histos1.push_back(time_F);

  for (unsigned int i=0; i<histos1.size(); i++) {
    histos1[i]->Sumw2();
    histos1[i]->SetLineWidth(3);
  }

  int nBins=200;
  
  TH2D* CORR_X =new TH2D("CORR_X" ,"CORR_X" ,nBins,0,1100,nBins, 0,1100);  histos2.push_back(CORR_X);
  TH2D* CORR_Y =new TH2D("CORR_Y" ,"CORR_Y" ,nBins,0, 576,nBins, 0, 576);  histos2.push_back(CORR_Y);

  //TH2D* CORR_X =new TH2D("CORR_X" ,"CORR_X" ,nBins,0,1100,nBins/5,0, 80);  histos2.push_back(CORR_X);
  //TH2D* CORR_Y =new TH2D("CORR_Y" ,"CORR_Y" ,nBins,0, 576,nBins  ,0,330);  histos2.push_back(CORR_Y);

  /*
  TH2D* CORR2_X =new TH2D("CORR2_X" ,"CORR2_X" ,nBins,0,1100,nBins,0,512);  histos2.push_back(CORR2_X);
  TH2D* CORR2_Y =new TH2D("CORR2_Y" ,"CORR2_Y" ,nBins,0, 576,nBins,0,512);  histos2.push_back(CORR2_Y);

  TH2D* CORR3_X =new TH2D("CORR3_X" ,"CORR3_X" ,nBins,0,1100,nBins,0,512);  histos2.push_back(CORR3_X);
  TH2D* CORR3_Y =new TH2D("CORR3_Y" ,"CORR3_Y" ,nBins,0, 576,nBins,0,512);  histos2.push_back(CORR3_Y);
  */

  nBins=80;
  TH2D* CORR2_X =new TH2D("CORR2_X" ,"CORR2_X" ,nBins,0, 80,nBins*2,0,512);  histos2.push_back(CORR2_X);
  TH2D* CORR2_Y =new TH2D("CORR2_Y" ,"CORR2_Y" ,nBins,0,330,nBins*2,0,512);  histos2.push_back(CORR2_Y);

  TH2D* CORR3_X =new TH2D("CORR3_X" ,"CORR3_X" ,nBins,0, 80,nBins*2,0,512);  histos2.push_back(CORR3_X);
  TH2D* CORR3_Y =new TH2D("CORR3_Y" ,"CORR3_Y" ,nBins,0,330,nBins*2,0,512);  histos2.push_back(CORR3_Y);

  for (unsigned int i=0; i<histos2.size(); i++) {
    histos2[i]->Sumw2();
  }

  //nentries=2000;
  if (maxEntries!=0) {
    nentries=maxEntries;
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //TCanvas* myC=new TCanvas("Malta","Malta",800,600);
  TH2D* myH=new TH2D("Malta","Malta",256,0,256,512,0,512);
  myH->SetMaximum(400); //1600);
  myH->SetTitle(";pos X;pos Y;");
  
  ULong64_t all   =0; //decided to drop the 0.5
  ULong64_t passed=0;
  ULong64_t notEmpty=0;
  

  //nentries=60e3;
  Long64_t nbytes = 0, nb = 0;
  for (Long64_t jentry=startP; jentry<nentries;jentry++) {
    //counter++;
    Long64_t advance= advanceN;

    Long64_t ientry = LoadTree(jentry,advance);
    if (ientry < 0) break;
    nb = GetEntry(jentry,advance);   nbytes += nb;
    
    //if (NHits_FE!=1) continue;

    for (int h1=0; h1<NHits_M1; h1++) {
      bool matched=false;
      //cout << "HIT "<< h1 << " in Mimosa plane3: " << PixX_M1[h1] << " , " << PixY_M1[h1] << endl; 
      for (int h2=0; h2<NHits_M2; h2++) { 
	float Delta=sqrt( pow(PixX_M1[h1]-PixX_M2[h2],2) + pow(PixY_M1[h1]-PixY_M2[h2],2) );
	if (Delta<2) {
	  matched=true;
	  break;
	} 
	//cout << "   HIT " << h2 << " in Mimosa plane2: " << PixX_M2[h2] << " , " << PixY_M2[h2] << "  ||| distance : " << Delta << endl;
      }
      
      
      //if (not matched) continue;

      for (int hF=0; hF<NHits_FE; hF++) { 
	//float Delta=sqrt( pow(PixX_M1[h1]-PixX_M2[h2],2) + pow(PixY_M1[h1]-PixY_M2[h2],2) );
	//if (Delta>2) continue; 
	CORR_X->Fill( PixX_M1[h1] , PixX_FE[hF]);	
	CORR_Y->Fill( PixY_M1[h1] , PixY_FE[hF]);
	//cout << "   HIT " << h2 << " in Mimosa plane2: " << PixX_M2[h2] << " , " << PixY_M2[h2] << "  ||| distance : " << Delta << endl;
      }
     

      for (int hMA=0; hMA<NHits_MA; hMA++) { 
	if ( PixX_MA[hMA]>256 ) continue;
	if ( PixY_MA[hMA]>256 ) continue;
	//if ( PixY_M1[h1]>256 ) continue;
	CORR2_X->Fill( PixX_M1[h1] , PixY_MA[hMA]);	
	CORR2_Y->Fill( PixY_M1[h1] , PixY_MA[hMA]);

	CORR3_X->Fill( PixX_M1[h1] , PixX_MA[hMA]);	
	CORR3_Y->Fill( PixY_M1[h1] , PixX_MA[hMA]);
      }

    }
  }


  //myC->Divide(2,1);
  //myC->cd(1);
  ///CORR2_X->Draw("COLZ");
  //myC->cd(2);
  ///CORR3_Y->Draw("COLZ");
  //myC->Print("test.pdf");
  //system("open test.pdf");

  cout << " CORR_X: " << CORR2_X -> GetCorrelationFactor()*100;
  if ( CORR2_X -> GetCorrelationFactor()<-0.1 and CORR3_Y -> GetCorrelationFactor()>0 ) cout << " !!!!!!!!!!!!!!!!!!" << endl;
  else                                               cout << endl;
  cout << " CORR_Y: " << CORR3_Y -> GetCorrelationFactor()*100 << endl;


  TFile* output=new TFile(outPutName.c_str(),"recreate");
  for (unsigned int i=0; i<histos1.size(); i++) histos1[i]->Write();
  for (unsigned int i=0; i<histos2.size(); i++) histos2[i]->Write();
  

  output->Close();
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
std::vector<std::string> fileToStrVec(string fileList) {
  static const int MAX_LINE_LENGTH = 15000;
  char m_lineBuffer[MAX_LINE_LENGTH];

  std::ifstream m_file;
  m_file.open(fileList.c_str());
  if(!m_file) {
    std::cerr << "Error: could not open " << fileList << std::endl;
    exit(-1);
  }
  
  /////////////////////////////////////////////
  std::vector<std::string> fileContents;
  string line;
  while(!m_file.eof() ) {
    getline(m_file,line);
    //m_file.getline(line);
    if ( line.find("root")!=string::npos ) 
      fileContents.push_back(line);
  }
  
  std::sort(fileContents.begin(),fileContents.end());
  return fileContents;
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[]){  
  //SetAtlasStyle(); 
  //gROOT->ProcessLine("#include <vector>");
  //gROOT->ProcessLine(".L myFile.C+");
  gStyle->SetPalette(1);
  gStyle->SetOptStat(0);

  bool usePseudo=false;

  string path="";
  string fileList="";
  string mcString="";
  bool special=false;
  if (argc>=2) {
    cout << endl;
    cout << " Reading input from fileList: " << fileList << endl;
    fileList=string(argv[1]);
  } else {
    cout << "NOT enough arguments" << endl;
    exit(-1);
  }
  if (argc>=3) {
    outPutName=string(argv[2]);
  }
  if (argc>=4) {
    advanceN=stoi(string(argv[3]));
  }
  if (argc>=5) {
    startP=stoi(string(argv[4]));
  }  
  if (argc>=6) {
    maxEntries=stoi(string(argv[5]));
  }  

  vector<string> listFiles;
  
  if (fileList=="") {
    cout << "WTF file are you giving me!!!  ... BYE" << endl;
    exit(-1);
  } else if (fileList.find("root")!=string::npos) {
    cout << "Reading file: " << fileList << endl;
    listFiles.push_back(fileList);
  } else if (fileList.find("txt")!=string::npos) {    
    cout << "Reading fileList from file: " << fileList << endl;
    std::vector<std::string> files;
    files=fileToStrVec(fileList);
    for (unsigned iF=0; iF<files.size(); iF++) listFiles.push_back(files[iF]);
  } else {
    cout << "Input is a directory: reading all the files " << endl;
    DIR*     dir;
    dirent*  pdir;
    dir = opendir( fileList.c_str() );     // open current directory
    while ( (pdir = readdir(dir)) )  {
      string foldName=pdir->d_name;
      if (foldName.find("root")!=string::npos) {
	listFiles.push_back( (fileList+"/"+foldName).c_str() );
	continue;
      }
      if (foldName.find("mc")==string::npos) continue;
      cout << pdir->d_name << endl;
      DIR*     dir2;
      dirent*  pdir2;
      dir2 = opendir( (fileList+"/"+foldName).c_str() );  // open current directory
      while ( (pdir2 = readdir(dir2)) ) {
	string fName=pdir2->d_name;
	if (fName.find("root")==string::npos) continue;
	listFiles.push_back( (fileList+"/"+foldName+"/"+fName).c_str() );
      }
    }
  }
  std::sort(listFiles.begin(),listFiles.end());
  
  TChain* fE =new TChain("Event");
  TChain* fF =new TChain("Plane6/Hits"); // WAS 6
  TChain* fM1=new TChain("Plane6/Hits"); // WAS PLANE 3
  TChain* fM2=new TChain("Plane4/Hits");
  TChain* fMA=new TChain("Plane7/Hits");

  /*
  fE ->Add("synced4135.root"); //ref4096.root");
  fF ->Add("synced4135.root"); //anchor4096.root"); //real FEI4
  fM1->Add("synced4135.root");//ref4096.root");
  fM2->Add("synced4135.root");
  fMA->Add("malta_4135.root"); //malta_4096.root");
  */

  for (unsigned iF=0; iF<listFiles.size(); iF++) {
    cout << " ... adding: " << listFiles[iF] << endl;
    fE->Add( listFiles[iF].c_str() );
    fF->Add( listFiles[iF].c_str() );
    fM1->Add( listFiles[iF].c_str() );
    fM2->Add( listFiles[iF].c_str() );
    fMA->Add( listFiles[iF].c_str() );
  }
  cout << endl << "TOTAL NUMBER OF EVENTS IS: " << fE->GetEntries() << endl << endl;

  TBCorrelation mainClass;
  mainClass.Init(fE,fF,fM1,fM2,fMA);
  mainClass.Loop();
}

