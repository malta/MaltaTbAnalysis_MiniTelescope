#include "TROOT.h"
#include "TFile.h"
#include "TDirectory.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH2D.h"

#include <iomanip>
#include <cmath>
#include <string>
#include <vector>
#include <array>
#include <iostream>
#include <fstream>
#include <bitset>

#include "isNoisyPixel.C"

#include "chronos.C"

int offset=0;
int TOTwords=0;
int TOTdoublewords=0;

void setRealID(const ULong64_t maltaL1ID,
	       const ULong64_t prevMaltaL1ID,
	       ULong64_t &maltaL1IDreal,
	       ULong64_t &prevMaltaL1IDreal,
	       ULong64_t &nCycles){
  prevMaltaL1IDreal = prevMaltaL1ID + nCycles * 4096;
  if(maltaL1ID < prevMaltaL1ID) nCycles++;
  maltaL1IDreal = maltaL1ID + nCycles * 4096;
  return ;
}

int maltaToProteus_V(const char *inPath, 
		     const char *outFilePath,
		     int RunNum,
		     bool timeCut         =false,
		     bool aggressiveCut   =false,
		     bool isH6            =false,
		     int  chip            =0) {
  
  const bool BCIDoverflow = true;
  bool veto=false;
  
  const unsigned int nMax = 0;
  const bool debug = false;
  
  std::cout << " Input file: " << inPath      << std::endl;
  //std::cout << "Output file: " << outFilePath << std::endl;

  //open input file and link tree
  ///std::cout << "Linking input file..." << std::endl;
  TFile * inFile = new TFile(inPath,"READ");
  if(!inFile->IsOpen()){
    std::cout << "Could not open input file! (ROOT TFile failed)" << std::endl;
    return -1;
  }

  TTree * maltaHits = (TTree *)inFile->Get("MALTA");
  if(!maltaHits){
    std::cout << "Could not open malte tree!" << std::endl;
    return -1;
  }
  UInt_t maltaPixel;
  UInt_t maltaGroup;
  UInt_t maltaParity;
  UInt_t maltaDelay;
  UInt_t maltaDColumn;
  UInt_t maltaChipBCID;
  UInt_t maltaChipID  =0;
  UInt_t maltaPhase;
  UInt_t maltaWinID;
  UInt_t maltaBCID;
  UInt_t maltaL1ID;
  UInt_t maltaIsDuplicate;
  UInt_t maltaIThres;
  UInt_t maltaVLow; 

  if(maltaHits->SetBranchAddress("pixel",&maltaPixel) != 0){ //pix x,y
    std::cout << "Branch not found, stopping script..." << std::endl;
    return -1;
  }
  if(maltaHits->SetBranchAddress("delay",&maltaDelay) != 0){ //pix x,y
    std::cout << "Branch not found, stopping script..." << std::endl;
    return -1;
  }
  if(maltaHits->SetBranchAddress("group",&maltaGroup) != 0){ //pix x,y
    std::cout << "Branch not found, stopping script..." << std::endl;
    return -1;
  }
  if(maltaHits->SetBranchAddress("parity",&maltaParity) != 0){ //pix x,y
    std::cout << "Branch not found, stopping script..." << std::endl;
    return -1;
  }
  if(maltaHits->SetBranchAddress("dcolumn",&maltaDColumn) != 0){ //pix x,y
    std::cout << "Branch not found, stopping script..." << std::endl;
    return -1;
  }
  if(maltaHits->SetBranchAddress("chipbcid",&maltaChipBCID) != 0){ //used for high rate measurements in the future, ignore now
    std::cout << "Branch not found, stopping script..." << std::endl;
    return -1;
  }
  /*
  if(maltaHits->SetBranchAddress("chipid",&maltaChipID) != 0){ //maybe keep for future multi chip configurations and generat malta planes based on this value
    std::cout << "Branch not found, stopping script..." << std::endl;
    return -1;
  }
  */
  if(maltaHits->SetBranchAddress("phase",&maltaPhase) != 0){ //for time rel to L1A, use to cut noise from the malta data
    std::cout << "Branch not found, stopping script..." << std::endl;
    return -1;
  }
  if(maltaHits->SetBranchAddress("winid",&maltaWinID) != 0){ //for time rel to L1A, use to cut noise from the malta data
    std::cout << "Branch not found, stopping script..." << std::endl;
    return -1;
  }
  if(maltaHits->SetBranchAddress("bcid",&maltaBCID) != 0){ //for time rel to L1A, use to cut noise from the malta data
    std::cout << "Branch not found, stopping script..." << std::endl;
    return -1;
  }
  if(maltaHits->SetBranchAddress("l1id",&maltaL1ID) != 0){ //use to separate malta data stream into events
    std::cout << "Branch not found, stopping script..." << std::endl;
    return -1;
  }
  if(maltaHits->SetBranchAddress("isDuplicate",&maltaIsDuplicate) != 0){ //use to remove double hits from malta data stream
    std::cout << "Branch not found, stopping script..." << std::endl;
    return -1;
  }

  //set up output file
  //std::cout << std::endl;
  //std::cout << "Setting up output file ..." << std::endl;

  TFile * outFile = NULL;
  outFile = new TFile(outFilePath,"RECREATE");
  if(!outFile){
    std::cout << __PRETTY_FUNCTION__ << " :: Output file could not be opened!" << std::endl;
    return -1;
  }

  TTree * outputEvents = new TTree("Event","Event Information");
  if(!outputEvents){
    std::cout << "Could not open output event tree!" << std::endl;
    return -1;
  }

  //std::cout << "Linking output event branches... " << std::endl;
  ULong64_t frameNumber;
  ULong64_t timestamp;
  ULong64_t triggerTime;
  Int_t triggerInfo;
  Int_t triggerOffset;
  UInt_t triggerL1ID;
  UInt_t triggerBCID;
  Bool_t invalid;

  outputEvents->Branch("FrameNumber"  , &frameNumber  , "FrameNumber/l"  );
  outputEvents->Branch("TimeStamp"    , &timestamp    , "TimeStamp/l"    );
  outputEvents->Branch("TriggerTime"  , &triggerTime  , "TriggerTime/l"  );
  outputEvents->Branch("TriggerInfo"  , &triggerInfo  , "TriggerInfo/I"  );
  outputEvents->Branch("TriggerOffset", &triggerOffset, "TriggerOffset/I");
  outputEvents->Branch("TriggerL1ID"  , &triggerL1ID  , "TriggerL1ID/i"  );
  outputEvents->Branch("TriggerBCID"  , &triggerBCID  , "TriggerBCID/i"  );
  outputEvents->Branch("Invalid"      , &invalid      , "Invalid/O"      );

  //check how many planes can be found in the data here
  UInt_t nPlanes = 1;
  //USELESS at the moment
  //std::cout << "Detected " << nPlanes << " malta chip(s) in the data." << std::endl;

  //define variables to link to input file hit tree branches
  const Int_t maxHits = 200;
  std::map<std::string,Int_t> runNumber;
  std::map<std::string,Int_t> maltaNHits;
  std::map<std::string,std::array<Int_t,maxHits> > maltaHitPixX;
  std::map<std::string,std::array<Int_t,maxHits> > maltaHitPixY;
  std::map<std::string,std::array<Double_t,maxHits> > maltaHitValue;
  std::map<std::string,std::array<Double_t,maxHits> > maltaHitTiming;
  std::map<std::string,std::array<Int_t   ,maxHits> > maltaHitInCluster;

  //link hit tree branches to variables
  //std::cout << "Linking output hit branches..." << std::endl;
  std::map<std::string,TDirectory *> outputPlaneDirs;
  std::map<std::string,TTree *> outputHitTrees;
  for(UInt_t k = 0; k < nPlanes; k++){
    std::string planeName = "Plane" + std::to_string(k+offset);
    outputPlaneDirs[planeName] = outFile->mkdir(planeName.c_str());
    outputPlaneDirs[planeName]->cd();
    TTree * hits = new TTree("Hits","Hits");
    if(!hits){
      std::cout << "Could not create hits tree for plane " << planeName << "!" << std::endl;
      return -1;
    }
    //std::cout << "CREATED PLANE: " << planeName << std::endl;
    outputHitTrees[planeName] = hits;
    hits->Branch("runNumber"   , &runNumber[planeName]        , "runNumber/I");
    hits->Branch("NHits"       , &maltaNHits[planeName]       , "NHits/I");
    hits->Branch("Value"       , &maltaHitValue[planeName]    , "Value[NHits]/D"); 
    hits->Branch("Timing"      , &maltaHitTiming[planeName]   , "Timing[NHits]/D");
    hits->Branch("PixX"        , &maltaHitPixX[planeName]     , "PixX[NHits]/I");
    hits->Branch("PixY"        , &maltaHitPixY[planeName]     , "PixY[NHits]/I");
    hits->Branch("HitInCluster", &maltaHitInCluster[planeName], "HitInCluster[NHits]/I");
  }

  //loop through malta entries and build proteus events
  std::cout << "Processing file with " << maltaHits->GetEntries() << " EVENTS " << std::endl;
  Long64_t maltaFileEntry = 0;
  ULong64_t maltaEventCount = 0;
  ULong64_t nCycles = 0;
  maltaHits->GetEntry(maltaFileEntry);

  // VD: assumption is that I will always start from 0
  ULong64_t maltaL1IDreal     = 0;
  ULong64_t maltaL1IDrealPrev = 0;
  ULong64_t overflow      = 0;
  ULong64_t events        = 0;
  ULong64_t prevMaltaL1ID = 10000;

  //RESET
  for(UInt_t k = 0; k < nPlanes; k++){
    std::string planeName = "Plane" + std::to_string(k+offset);
    maltaNHits[planeName] = 0;
    maltaHitPixX[planeName].fill(0);
    maltaHitPixY[planeName].fill(0);
    maltaHitValue[planeName].fill(0.0);
    maltaHitTiming[planeName].fill(0.0);
    maltaHitInCluster[planeName].fill(0);
  }
  UInt_t nPixels = 0;

  bool firstGood=true;
  frameNumber   = 1;
  triggerL1ID   = 1;
  for(UInt_t k = 0; k < nPlanes; k++){
    std::string planeName = "Plane" + std::to_string(k+offset);
    maltaNHits[planeName] = nPixels;   
  }
  //if (chip==2) outputEvents->Fill();
  for(UInt_t iPlane = 0; iPlane < nPlanes; iPlane++){
    std::string planeName = "Plane" + std::to_string(iPlane+offset);
    //if (chip==2) outputHitTrees[planeName]->Fill();
  }

  UInt_t prevBCID=0;
  for (events=0; events<maltaHits->GetEntries(); events++) {
    //if (events>200000) break;
    if (events%50000==0) std::cout << "Processing event: " << events << std::endl;  
    maltaHits->GetEntry(events);

    //VD experimental: 
    if ( maltaBCID>=16 and events!=0) continue;
    if ( maltaL1ID==0  and events==0) continue;

    if ( maltaL1ID!=prevMaltaL1ID ) {
      //VD: temporary protection against L1jumps
      if ( maltaL1ID==0 and maltaBCID==0 ) continue; //this is likely a corrupted word
      if ( maltaL1ID==0 and events==0 ) continue; //VD last
      if ( prevMaltaL1ID>maltaL1ID and  maltaL1ID >2) {
	if ( (prevMaltaL1ID-maltaL1ID)<3500 and prevMaltaL1ID!=10000 ) {
	  cout << " SKIPPING1: " << prevMaltaL1ID << " --> " << maltaL1ID << " --- " <<  maltaL1IDrealPrev+1 << endl; 
	  continue;   //this is likely not a valid crossing
	}
      } 
      if ( prevMaltaL1ID<maltaL1ID ) {
	if ( (maltaL1ID-prevMaltaL1ID)>3800 and prevMaltaL1ID!=10000) { //VD on 13-05-18: was 100,   VD on 15-12-19 was 500 ... from 4000 to 3800
	  cout << " SKIPPING2: " << prevMaltaL1ID << " --> " << maltaL1ID << " --- " <<  maltaL1IDrealPrev+1 << endl;
	  continue;   //this is likely a bad jump ahead that could lead to a bad crossing in the next
	}
      }

      //save previous event if it's not the first
      frameNumber   = maltaL1IDrealPrev+1;
      timestamp     = 0;
      triggerTime   = 0;
      triggerBCID   = 0;
      triggerL1ID   = prevMaltaL1ID;
      triggerInfo   = -1;
      triggerOffset = -1;
      invalid       = 0;     
  
      //write out event
      for(UInt_t k = 0; k < nPlanes; k++){
	std::string planeName = "Plane" + std::to_string(k+offset);
	maltaNHits[planeName] = nPixels;   
      }
      if ( maltaL1IDrealPrev>=1 ) {
	if (chip==222) {
	  if (frameNumber!=2) outputEvents->Fill();
	} else                            outputEvents->Fill();
	for(UInt_t iPlane = 0; iPlane < nPlanes; iPlane++){
	  std::string planeName = "Plane" + std::to_string(iPlane+offset);
	  if (chip==222) {
	    if (frameNumber!=2) outputHitTrees[planeName]->Fill();
	  } else outputHitTrees[planeName]->Fill();
	}
      }
   
      //now reset
      for(UInt_t k = 0; k < nPlanes; k++){
	std::string planeName = "Plane" + std::to_string(k+offset);
	runNumber[planeName]  = RunNum;
	maltaNHits[planeName] = 0;
	maltaHitPixX[planeName].fill(0);
	maltaHitPixY[planeName].fill(0);
	maltaHitValue[planeName].fill(0.0);
	maltaHitTiming[planeName].fill(0.0);
	maltaHitInCluster[planeName].fill(0);
      }   
      nPixels = 0;

      //if ( maltaL1ID<prevMaltaL1ID and events!=0  ) overflow++;
      if ( maltaL1ID<prevMaltaL1ID and firstGood==false  ) overflow++;
      maltaL1IDreal=maltaL1ID+overflow*4096;

      //if (events==0) maltaL1IDrealPrev=0;//vdLast
      if (firstGood) {
	maltaL1IDrealPrev=0;//vdLast
	firstGood=false;
      }

      if ( (maltaL1IDreal-maltaL1IDrealPrev)!=1 ) { //and events!=0) {
	if ( (maltaL1IDreal-maltaL1IDrealPrev)>200 ) cout << "   adding: " << (maltaL1IDreal-maltaL1IDrealPrev) << " , because of: " << maltaL1ID << " --> " << prevMaltaL1ID << endl;
	for (int c=0; c<(maltaL1IDreal-maltaL1IDrealPrev)-1; c++) {
	  if (debug) std::cout << "          missing L1: " << maltaL1IDrealPrev+c+1 << std::endl;
	  frameNumber   = maltaL1IDrealPrev+c+1;
	  triggerL1ID   = prevMaltaL1ID+c+1;
	  if (triggerL1ID>=4096) triggerL1ID=triggerL1ID-4096; 
	  outputEvents->Fill();
	  for(UInt_t iPlane = 0; iPlane < nPlanes; iPlane++){
	    std::string planeName = "Plane" + std::to_string(iPlane+offset);
	    outputHitTrees[planeName]->Fill();
	  }
	}
      } else {
	//consecutive L1id no need to do anything else
	/*
	if (RunNum==1181 and chip==2 and maltaL1IDreal==60000 ) {
	  outputEvents->Fill();
	  for(UInt_t iPlane = 0; iPlane < nPlanes; iPlane++){
	    std::string planeName = "Plane" + std::to_string(iPlane+offset);
	    outputHitTrees[planeName]->Fill();
	  }
	}
	*/
	if (RunNum==1431 and chip==2 and maltaL1IDreal==48 ) {
	  outputEvents->Fill();
	  for(UInt_t iPlane = 0; iPlane < nPlanes; iPlane++){
	    std::string planeName = "Plane" + std::to_string(iPlane+offset);
	    outputHitTrees[planeName]->Fill();
	  }
	}
      }

      if (debug) {
	std::cout << "new l1id: "      << maltaL1ID 
		  << "  overflow: "    << overflow 
		  << "  progressive: " << maltaL1ID+overflow*4096 << std::endl;
      }
      prevMaltaL1ID=maltaL1ID;
      maltaL1IDrealPrev= maltaL1ID+overflow*4096; 
      prevBCID=0;
      veto=false;
    }

    //skipping first event since it contains many many hits!!!
    if ( !(maltaL1ID==0 and overflow==0) ) {
      //start filling hits
      std::string planeName = "Plane" + std::to_string(maltaChipID+offset);

      UInt_t tmpPixel=maltaPixel;
      if ( RunNum>=6784 and RunNum<=6819 ) {
	if (tmpPixel!=8192 and chip==1) {
	  if ( (tmpPixel&0x2000)!=0 ) {
	    tmpPixel= (maltaPixel&0xDFFF);
	    cout << "Correcting: " << maltaPixel << " to " << tmpPixel << endl;
	  }
	}
      }	

      if ( RunNum>=1503 and RunNum<=1516 ) {
	//if (chip==1) cout << "Correcting: " << bitset<16>(maltaPixel) << endl;
	if (tmpPixel!=4 and chip==1) {
	  if ( (tmpPixel&0x4)!=0 ) {
	    tmpPixel= (maltaPixel&0xFFFB);
	    cout << "Correcting: " << maltaPixel << " to " << tmpPixel << endl;
	  }
	}
      }	
      
      if ( RunNum>=1526 and RunNum<=1533 ) {
	//if (chip==1) cout << "Correcting: " << bitset<16>(maltaPixel) << endl;
	if (tmpPixel!=4 and chip==1) {
	  if ( (tmpPixel&0x4)!=0 ) {
	    tmpPixel= (maltaPixel&0xFFFB);
	    cout << "Correcting: " << maltaPixel << " to " << tmpPixel << endl;
	  }
	}
      }	
      
      TOTwords+=1;
      int tPix=0;
      for(UInt_t i=0; i<16;i++){
	if (((tmpPixel>>i)&0x1)==0)  continue;
	tPix++;
	//if (maltaIsDuplicate)        continue; ///could be dropped for testing
	if (nPixels>maxHits-1)       continue;

	//if (veto and not BCIDoverflow) continue;
	float timing= maltaBCID*25.0+maltaWinID*3.125+maltaPhase*0.390625; // it was 0.3906
	int Xcoord= maltaDColumn*2+i/8;
	int Ycoord= maltaGroup*16+maltaParity*8+i%8;

	//if (RunNum>=77777 and chip==0) {
	//  if (maltaChipBCID!=0) Xcoord=Xcoord-512-53;
	//}

// In block below (for Milou):
// chip = plane 
// maltaDelay = chip id on quad board
// maltaChipBCID = chip id on dual board
// 

	/*
	if (RunNum>=80000) {
	  if (chip==0) {
	    if (maltaDelay==0)      Xcoord=Xcoord+512+53;
	    else if (maltaDelay==1) Xcoord=Xcoord ;
	    else if (maltaDelay==2) Xcoord=Xcoord-512-53;
	    else if (maltaDelay==3) Xcoord=Xcoord-512-53-512-53;
	    //else                Xcoord=-999999;
	    //if (Xcoord==-999999) continue;
	  } else if (chip!=3) {
	    if (maltaChipBCID!=0) Xcoord=Xcoord-512-53;
	  }
	}
	*/

	if (RunNum<400 or RunNum==4097 or RunNum==4098 or RunNum==2112 or RunNum==1099) {
	  if (maltaDelay==0)      Xcoord=Xcoord+512+53+512+53+512+53;
	  else if (maltaDelay==1) Xcoord=Xcoord+512+53+512+53;
	  else if (maltaDelay==2) Xcoord=Xcoord+512+53;
	  else if (maltaDelay==3) Xcoord=Xcoord;
	}

	////if (Xcoord>255) continue; /// it was ON IN ELSA
        int min_cut_tel_lab = 120;
        int max_cut_tel_lab = 250;
        int min_RunNum_lab = 1100;
        int max_RunNum_lab = 2000;
	
	if ((RunNum > 10 && RunNum <400 && chip != 1)) {
/*
          int margin = 100;
          if(RunNum > 10 && RunNum < 21) margin = 200;
          //for telescope quad
	  if (timing<150) continue; 
	  if (timing>350) continue;
          //exclude left and right 100 columns on each chip and gap between chips
          //chip 1
          if(Xcoord < margin) continue;
          //chip 1 to chip 2
          if(Xcoord >= (512-margin) && Xcoord < (512+53+margin)) continue;
          //chip 2 to chip 3
          if(Xcoord >= (512+53+512-margin) && Xcoord < (512+53+512+53+margin)) continue;
          //chip 3 to chip 4
          if(Xcoord >= (512+53+512+53+512-margin) && Xcoord < (512+53+512+53+512+53+margin)) continue;
          //chip 4
          if(Xcoord >= (512+53+512+53+512+53+512-margin)) continue;

          //exclude top and bottom 100 rows on each chip
          if(Ycoord < margin) continue;
          if(Ycoord >= (512-margin)) continue; 
*/
	} else if ( RunNum>min_RunNum_lab and RunNum<max_RunNum_lab and RunNum<77777 ) {
	  if (timing<130) continue; //120 //VD on 13-10-2020
	  if (timing>200) continue; //200
	} else if ( RunNum==1227) {
	  if (timing<140) continue; //120 //VD on 13-10-2020
	  if (timing>220) continue; //300
	} else {
	  if (timing<170) continue; //120 //VD on 13-10-2020
	  if (timing>240) continue; //300
	}

	if (chip==1) {
          //if ((RunNum > min_RunNum_lab and RunNum <min_RunNum_lab) and (timing < min_cut_tel_lab or timing > max_cut_tel_lab )) continue;
	  //if (timing<130) continue; //120 //VD on 13-10-2020
	  //if (timing>200) continue; //300
	  if (RunNum>=7051 and RunNum<=7056) {
	  
            if (Xcoord==151 and Ycoord==482 ) continue;
	    if (Xcoord==151 and Ycoord==167 ) continue;
	  }

	  if (RunNum>=797 and RunNum<=807) {
	    if (Xcoord==327 and Ycoord==426 ) continue;
	    if (Xcoord==342 and Ycoord==265 ) continue;
	    if (Xcoord==276 and Ycoord==451 ) continue;
	  }

          if ( (RunNum>=810 and RunNum<=822) or
	       (RunNum>=867 and RunNum<=880) ) {
            if (Xcoord==205 and Ycoord==205 ) continue;
            if (Xcoord==350 and Ycoord==299 ) continue;
            if (Xcoord==21  and Ycoord==77  ) continue;
            if (Xcoord==10  and Ycoord==303 ) continue;
            if (Xcoord==298 and Ycoord==310 ) continue;
            if (Xcoord==331 and Ycoord==156 ) continue;
            if (Xcoord==263 and Ycoord==7   ) continue;

          }
	  if ( (RunNum>=884 and RunNum<=914) ){
            if (Xcoord==279 and Ycoord==62 ) continue;
            if (Xcoord==334 and Ycoord==414 ) continue;
            if (Xcoord==298 and Ycoord==310 ) continue;
            if (Xcoord==179 and Ycoord==270 ) continue;
          }
	  if ( (RunNum>=999990 and RunNum<=999999) ){
            if (Xcoord==205 and Ycoord==205 ) continue;
            if (Xcoord==279 and Ycoord==62  ) continue;
            if (Xcoord==334 and Ycoord==414 ) continue;
            if (Xcoord==298 and Ycoord==310 ) continue;
            if (Xcoord==179 and Ycoord==270 ) continue;
          }

	  if ( (RunNum>=1288 and RunNum<=1319) ){ //W7R0 in the lab
	    if (Xcoord== 92 and Ycoord==447 ) continue;
	    if (Xcoord==249 and Ycoord==310 ) continue;
	    if (Xcoord==345 and Ycoord==284 ) continue;
	    if (Xcoord==306 and Ycoord==331 ) continue;
	    if (Xcoord==131 and Ycoord==229 ) continue;
	    if (Xcoord==151 and Ycoord==440 ) continue;
	    if (Xcoord==193 and Ycoord==202 ) continue;
	    if (Xcoord==309 and Ycoord==507 ) continue;
	    if (Xcoord==145 and Ycoord==229 ) continue;
	    if (Xcoord==165 and Ycoord==243 ) continue;
	    if (Xcoord==276 and Ycoord==289 ) continue;
	    if (Xcoord==237 and Ycoord==284 ) continue;
	    if (Xcoord==148 and Ycoord==234 ) continue;
	    if (Xcoord== 84 and Ycoord==128 ) continue;
	    if (Xcoord== 99 and Ycoord==100 ) continue;
	    if (Xcoord==331 and Ycoord==237 ) continue;
	    if (Xcoord==278 and Ycoord==378 ) continue;
	    if (Xcoord==293 and Ycoord==210 ) continue;
	    if (Xcoord==337 and Ycoord==256 ) continue;
	    if (Xcoord==144 and Ycoord==101 ) continue;
	    if (Xcoord==290 and Ycoord==392 ) continue;
	    if (Xcoord==285 and Ycoord==247 ) continue;
	    if (Xcoord==159 and Ycoord==172 ) continue;
	    if (Xcoord==342 and Ycoord==354 ) continue;
	  }
	  if ( (RunNum>=1320 and RunNum<=1338) ){ //W7R5 in the lab
	    if (Xcoord==199 and Ycoord==101 ) continue;
	    if (Xcoord==306 and Ycoord==184 ) continue;
	    if (Xcoord== 98 and Ycoord==176 ) continue;
	    if (Xcoord==269 and Ycoord== 49 ) continue;
	    if (Xcoord==284 and Ycoord== 79 ) continue;
	    if (Xcoord==192 and Ycoord==199 ) continue;
	    if (Xcoord==259 and Ycoord== 76 ) continue;
	    if (Xcoord==133 and Ycoord==323 ) continue;
	    if (Xcoord== 81 and Ycoord==280 ) continue;
	    if (Xcoord==262 and Ycoord==153 ) continue;
	    if (Xcoord==262 and Ycoord==  0 ) continue;
	    if (Xcoord==284 and Ycoord==298 ) continue;
	    if (Xcoord==291 and Ycoord==199 ) continue;
	    if (Xcoord==331 and Ycoord==312 ) continue;
	    if (Xcoord==289 and Ycoord==402 ) continue;
	    if (Xcoord==345 and Ycoord== 18 ) continue;
	    if (Xcoord==263 and Ycoord==175 ) continue;
	    if (Xcoord==335 and Ycoord==367 ) continue;
	    if (Xcoord==301 and Ycoord==158 ) continue;
	    if (Xcoord== 84 and Ycoord==222 ) continue;
	    if (Xcoord==256 and Ycoord==103 ) continue;
	    if (Xcoord==327 and Ycoord== 47 ) continue;
	    if (Xcoord==290 and Ycoord==120 ) continue;
	    if (Xcoord==272 and Ycoord==453 ) continue;
	    if (Xcoord==340 and Ycoord==452 ) continue;
	    if (Xcoord==312 and Ycoord==309 ) continue;
	    if (Xcoord==284 and Ycoord==251 ) continue;
	  }
        if ( (RunNum>=1339 and RunNum<=1352) ){ //W9R5 in the lab
            if (Xcoord== 146 and Ycoord== 221 ) continue;
            if (Xcoord== 145 and Ycoord== 224 ) continue;
            if (Xcoord== 241 and Ycoord== 102 ) continue;
            if (Xcoord== 203 and Ycoord==  98 ) continue;
            if (Xcoord== 220 and Ycoord== 237 ) continue;
            if (Xcoord== 276 and Ycoord== 489 ) continue;
            if (Xcoord== 198 and Ycoord== 318 ) continue;
            if (Xcoord== 116 and Ycoord== 127 ) continue;
            if (Xcoord==  46 and Ycoord== 285 ) continue;
            if (Xcoord== 148 and Ycoord== 177 ) continue;
            if (Xcoord== 332 and Ycoord== 176 ) continue;
            if (Xcoord==  84 and Ycoord== 181 ) continue;
            if (Xcoord== 142 and Ycoord== 354 ) continue;
            if (Xcoord== 317 and Ycoord== 253 ) continue;
            if (Xcoord== 291 and Ycoord== 110 ) continue;
            if (Xcoord== 271 and Ycoord==  24 ) continue;
            if (Xcoord== 343 and Ycoord== 122 ) continue;
            if (Xcoord== 315 and Ycoord==  25 ) continue;
            if (Xcoord== 340 and Ycoord== 335 ) continue;
            if (Xcoord== 279 and Ycoord== 329 ) continue;
            if (Xcoord== 141 and Ycoord== 266 ) continue;
            if (Xcoord== 297 and Ycoord== 192 ) continue;
            if (Xcoord==  89 and Ycoord==  10 ) continue;
            if (Xcoord== 287 and Ycoord==  38 ) continue;
            if (Xcoord== 201 and Ycoord== 285 ) continue;
            if (Xcoord== 296 and Ycoord==  72 ) continue;
            if (Xcoord== 122 and Ycoord==   4 ) continue;
            if (Xcoord== 307 and Ycoord==  19 ) continue;
            if (Xcoord== 245 and Ycoord== 175 ) continue;
            if (Xcoord== 108 and Ycoord== 270 ) continue;
            if (Xcoord== 164 and Ycoord== 322 ) continue;
            if (Xcoord== 342 and Ycoord== 360 ) continue;
            if (Xcoord== 104 and Ycoord== 128 ) continue;
            if (Xcoord== 181 and Ycoord==  25 ) continue;
            if (Xcoord== 332 and Ycoord==  13 ) continue;
	}
        if ( ( RunNum>=1360 and RunNum <= 1375) ){
            if (Xcoord== 244 and Ycoord== 440  ) continue;
            if (Xcoord== 339 and Ycoord== 367  ) continue;
            if (Xcoord== 265 and Ycoord== 247  ) continue;
            if (Xcoord==  79 and Ycoord== 123  ) continue;
            if (Xcoord== 212 and Ycoord== 338  ) continue;
            if (Xcoord== 217 and Ycoord==  25  ) continue;
            if (Xcoord== 323 and Ycoord== 187  ) continue;
            if (Xcoord== 269 and Ycoord== 312  ) continue;
            if (Xcoord== 249 and Ycoord== 278  ) continue;
            if (Xcoord== 287 and Ycoord== 226  ) continue;
            if (Xcoord== 294 and Ycoord== 312  ) continue;
            if (Xcoord==  74 and Ycoord==  50  ) continue;
            if (Xcoord== 297 and Ycoord== 318  ) continue;
            if (Xcoord==  93 and Ycoord== 107  ) continue;
            if (Xcoord== 261 and Ycoord== 151  ) continue;
            if (Xcoord==  71 and Ycoord==  41  ) continue;
            if (Xcoord== 297 and Ycoord== 420  ) continue;
            if (Xcoord== 270 and Ycoord== 147  ) continue;
            if (Xcoord== 332 and Ycoord== 204  ) continue;
            if (Xcoord== 223 and Ycoord==  10  ) continue;
        }


        if ( ( RunNum>=1409 and RunNum <=1436) ){//W11R5 in the  lab
            if (Xcoord== 160 and Ycoord== 493  ) continue;
            if (Xcoord== 233 and Ycoord== 317  ) continue;
            if (Xcoord== 259 and Ycoord== 432  ) continue;
            if (Xcoord== 160 and Ycoord== 492  ) continue;
            if (Xcoord== 286 and Ycoord== 494  ) continue;
            if (Xcoord== 300 and Ycoord== 218   ) continue;
            if (Xcoord== 309 and Ycoord== 250  ) continue;
            if (Xcoord== 57  and Ycoord== 285  ) continue;
            if (Xcoord== 245 and Ycoord== 389  ) continue;
            if (Xcoord== 370 and Ycoord== 14  ) continue;
        }
        if ( ( RunNum>=1551 and RunNum <1552) ){
            if (Xcoord== 375  and Ycoord== 236  ) continue;
            if (Xcoord== 3  and Ycoord== 204  ) continue;
            if (Xcoord== 201  and Ycoord== 214  ) continue;
            if (Xcoord== 364  and Ycoord== 141  ) continue;
            if (Xcoord== 416  and Ycoord== 103  ) continue;
            if (Xcoord== 449  and Ycoord== 449  ) continue;
            if (Xcoord== 379  and Ycoord== 233  ) continue;
        }
        
	if ( ( RunNum>=1517 and RunNum <= 1525) ){ ///W11R11 2021
            if (Xcoord== 205 and Ycoord== 205  ) continue;
            if (Xcoord== 334 and Ycoord== 414  ) continue;
	}
	if ( ( RunNum>=1377 and RunNum <= 1400) ){
	  if (Xcoord==256 and Ycoord==  495 ) continue;
          if (Xcoord==125 and Ycoord==  164 ) continue;
	  if (Xcoord== 96 and Ycoord==  409 ) continue;
	  if (Xcoord==162 and Ycoord==  379 ) continue;
	  if (Xcoord==170 and Ycoord==   14 ) continue;
	  if (Xcoord==197 and Ycoord==  202 ) continue;
	  if (Xcoord== 64 and Ycoord==   12 ) continue;
	  if (Xcoord==203 and Ycoord==   87 ) continue;
	  if (Xcoord==323 and Ycoord==  366 ) continue;
	  if (Xcoord==233 and Ycoord==  317 ) continue;
	  if (Xcoord==259 and Ycoord==  432 ) continue;
	  if (Xcoord==309 and Ycoord==  250 ) continue;
	  if (Xcoord==160 and Ycoord==  493 ) continue;  
	  if (Xcoord==279 and Ycoord==  279 ) continue;
	  if (Xcoord==209 and Ycoord==  494 ) continue;
	  if (Xcoord==186 and Ycoord==  85  ) continue;
	  if (Xcoord==123 and Ycoord==  370 ) continue;
	  if (Xcoord==122 and Ycoord==  185 ) continue;
	  if (Xcoord==182 and Ycoord==  2   ) continue; 
          if (Xcoord==101 and Ycoord== 226  ) continue;
	  if (Xcoord==181 and Ycoord== 390  ) continue;
	  if (Xcoord==223 and Ycoord== 420  ) continue;
	  if (Xcoord==163 and Ycoord== 72   ) continue;
	  if (Xcoord==46  and Ycoord== 234  ) continue;
	  if (Xcoord==96  and Ycoord== 251  ) continue;
	  if (Xcoord==100 and Ycoord== 9    ) continue; 
          if (Xcoord==173 and Ycoord== 256  ) continue;
	  if (Xcoord==203 and Ycoord== 82   ) continue;
	  if (Xcoord==188 and Ycoord== 144  ) continue;
	  if (Xcoord==186 and Ycoord== 336  ) continue;
	  if (Xcoord==71  and Ycoord== 32   ) continue;
	  if (Xcoord==160 and Ycoord==492   ) continue;
	  if (Xcoord==245 and Ycoord==389   ) continue; 
          if (Xcoord==149 and Ycoord==229   ) continue;
	  if (Xcoord==228 and Ycoord==120   ) continue;
	  if (Xcoord==130 and Ycoord==26    ) continue;
	  if (Xcoord==203 and Ycoord==20    ) continue;
	  if (Xcoord==124 and Ycoord==31    ) continue;
	  if (Xcoord==159 and Ycoord==115   ) continue;
	  if (Xcoord==53  and Ycoord==106   ) continue; 
          if (Xcoord==98  and Ycoord==402   ) continue;
	  if (Xcoord==190 and Ycoord==241   ) continue;
	  if (Xcoord==167 and Ycoord==313   ) continue;
	  if (Xcoord==53  and Ycoord==270   ) continue;
	  if (Xcoord==122 and Ycoord==306   ) continue;
	  if (Xcoord==166 and Ycoord==241   ) continue;
	  if (Xcoord==186 and Ycoord==242   ) continue; 
          if (Xcoord==56  and Ycoord==143   ) continue;

        }
        if (RunNum==1774 ){ /// for milou
            if (Xcoord==255 and Ycoord==321   ) continue;
        }
	} else if (chip==2) {
          //if ((RunNum > min_RunNum_lab and RunNum <min_RunNum_lab) and (timing < min_cut_tel_lab or timing > max_cut_tel_lab )) continue;
	  //if (timing<120) continue; //VD on 13-10-2020
	  //if (timing>300) continue;
	  if (Xcoord== 249 and Ycoord==235 ) continue;
	  if (Xcoord== 254 and Ycoord==335 ) continue;
	  if (Xcoord== 158 and Ycoord==183 ) continue;
	} else if (chip==3) {
	  //if (timing<120) continue; //VD on 13-10-2020
	  //if (timing>300) continue;
	  if (Xcoord==  255 and Ycoord==321 ) continue;
	  if (Xcoord==  263 and Ycoord==301 ) continue;
	  if (Xcoord==  291 and Ycoord==309 ) continue;
	  if (Xcoord==  364 and Ycoord==168 ) continue;
	  if (Xcoord==  390 and Ycoord==213 ) continue;
	  if (Xcoord==   98 and Ycoord==485 ) continue;
	} else {
          //if ((RunNum > min_RunNum_lab and RunNum <min_RunNum_lab) and (timing < min_cut_tel_lab or timing > max_cut_tel_lab )) continue;
	  //if (timing<120) continue; //VD on 13-10-2020
	  //if (timing>300) continue;
	}


	if (nPixels!=0) {
	  //loop in existing PixX and PixY and if there is already a hit with the same coordinate and timing within 5  ... DO NOT ENTER THE PIXEL IN THE VECTOR 
	  bool found=false;
	  for (int ip=0; ip<nPixels;ip++) {
	    if (Xcoord!=maltaHitPixX[planeName][ip])             continue;
	    if (Ycoord!=maltaHitPixY[planeName][ip])             continue;
	    if ( fabs(timing-maltaHitTiming[planeName][ip])>3.5) continue;
	    found=true;
	    //cout << " FOUND" << endl;
	    break;
	  }
	  if (found) continue;
	}

	if (tPix!=1) TOTdoublewords++;
	//std::cout << " " << (maltaGroup*16+maltaParity*8+i%8) << " , " <<   maltaDColumn*2+i/8 << std::endl;
	maltaHitPixX[planeName][nPixels]      = Xcoord;
	maltaHitPixY[planeName][nPixels]      = Ycoord; 
	maltaHitTiming[planeName][nPixels]    = timing;
	maltaHitValue[planeName][nPixels]     =  1; //maltaL1ID+overflow*4096; ///(Double_t)maltaChipBCID;
	maltaHitInCluster[planeName][nPixels] = -1;
	nPixels++;
      }
    }
  }
 
  std::cout << std::endl;
  std::cout << "Finished writing file for a total of L1id: " << maltaL1IDrealPrev << std::endl << std::endl;
  outFile->Write();
  std::map<std::string, TTree*>::iterator it;
  for (auto const& pair : outputHitTrees) {
    auto key = pair.second;
    delete key;
  }
  //for (it = outputHitTrees.begin(); it!=outputHitTrees.end(); it++) {TTree* t =  (it->second()); t->Print();};
  //exit(0);
  delete outputEvents;
  delete maltaHits; 
  outFile->Close();
  inFile->Close();
  delete outFile;
  delete inFile;
  std::cout << "Pico info merging" << std::endl;
  ////mergePico(outFilePath);
  
  cout << "VALERIO SAYS: " 
       << TOTwords << " , " 
       << TOTdoublewords <<  " : " 
       << ((float)TOTdoublewords)/((float)TOTwords) << endl;

  return 0;
}
