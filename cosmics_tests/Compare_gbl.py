#!/usr/bin/env python
import sys
import os
import os.path
import glob
import ROOT
from array import *
import math

#gROOT.LoadMacro("~/AtlasStyleBase/AtlasStyle.C")
#gROOT.LoadMacro("~/AtlasStyleBase/AtlasUtils.C")
#SetAtlasStyle()
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPalette(1)
ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetEndErrorSize(10)

###############################################################################

f1=ROOT.TFile("run_000666/ana000666.root")
#f1=ROOT.TFile("../FinalPlot_DESY_Oct19/W7R12Cz/run__W7R12Cz__IDB70_ITH030_SUB30_PWELL06/run__W7R12Cz__IDB70_ITH030_SUB30_PWELL06.root")
f1.ls()
f2=ROOT.TFile("run_000666/ana_gbl_000666.root")

posX =f1.Get("res_X")
posX2=f2.Get("res_X_1")
posY =f1.Get("res_Y")
posY2=f2.Get("res_Y_1")
posX3=posX.Clone("x1")
posX3.Add(posX2,-1)

posX.Scale(1./posX.Integral())
posX2.Scale(1./posX2.Integral())
posX3.Scale(1./posX3.Integral())
posY.Scale(1./posY.Integral())
posY2.Scale(1./posY2.Integral())

#if posY.GetMaximum()>posY2.GetMaximum(): posY2.SetMaximum(posY.GetMaximum()*1.2)
#if posX.GetMaximum()>posX2.GetMaximum(): posX2.SetMaximum(posX.GetMaximum()*1.2)

posX.SetLineColor(4)
posY.SetLineColor(4)
posX2.SetLineColor(2)
posY2.SetLineColor(2)
posX3.SetLineColor(8)

posX.SetMarkerColor(4)
posY.SetMarkerColor(4)
posX2.SetMarkerColor(2)
posY2.SetMarkerColor(2)
posX3.SetMarkerColor(8)

posX.Rebin(2)
posY.Rebin(2)
posX2.Rebin(2)
posY2.Rebin(2)
posX3.Rebin(2)


fit =ROOT.TF1("fitG","gaus",-100,100)
fit2=ROOT.TF1("fitG","gaus",-100,100)
posX2.Fit(fit,"RE0")
SX2=fit.GetParameter(2)
posX.Fit(fit,"RE0")
SX=fit.GetParameter(2)
posY2.Fit(fit2,"RE0")
SY2=fit2.GetParameter(2)
posY.Fit(fit2,"RE0")
SY=fit2.GetParameter(2)



can=ROOT.TCanvas("CRes","Residuals_Average",1200,600)
can.Divide(2,1)
can.cd(1)
posX.GetYaxis().SetTitle("a.u.")
posX.GetXaxis().SetRangeUser(-80,120)
posX.Draw("E")
fit.Draw("SAME")
#posX2.Draw("SAMEE")
#posX3.Draw("SAMEE")

legend4=ROOT.TLegend(0.6,0.60,0.90,0.80)
legend4.SetTextFont(42)
legend4.SetTextSize(0.038)
legend4.SetFillColor(0)
legend4.SetLineColor(0)
legend4.SetFillStyle(0)
legend4.SetBorderSize(0)
print SX
legend4.AddEntry(posX , "#sigma = "+str(int(SX))+" #mum", "LPE")
#legend4.AddEntry(posX2, "SUB -30V", "LPE")
legend4.Draw()


can.cd(2)
posY.GetYaxis().SetTitle("a.u.")
posY.GetXaxis().SetRangeUser(-80,120)
#posY2.GetXaxis().SetRangeUser0,10000)
posY.Draw("E")
fit2.Draw("SAME")
legend5=ROOT.TLegend(0.6,0.60,0.90,0.80)
legend5.SetTextFont(42)
legend5.SetTextSize(0.038)
legend5.SetFillColor(0)
legend5.SetLineColor(0)
legend5.SetFillStyle(0)
legend5.SetBorderSize(0)
legend5.AddEntry(posY , "#sigma = "+str(int(SY))+" #mum", "LPE")
legend5.Draw()
#posY2.Draw("SAMEE")

can.Print("Comparison.pdf")
os.system("evince Comparison.pdf &")

sys.exit()

