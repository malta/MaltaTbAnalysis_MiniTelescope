#!/usr/bin/env python
import os
import os.path
import sys
import pexpect
import getpass
import argparse
import glob
import base64
import ROOT
import autoDQ


path     ="/home/sbmuser/MaltaSW/MaltaTbAnalysis/"
webfolder="/home/sbmuser/DESY_Sr90/web/"
os.system("mkdir -p "+webfolder)
adecmospassword = base64.b64decode("S2lnZUJlbGUxOQ==")#getpass.getpass("Enter adecmos password:")

parser = argparse.ArgumentParser()
parser.add_argument("-r", "--run"     , help="Run number"              , type=int           , required=True)
parser.add_argument("-a", "--analysis", help="analysis only Run number", action="store_true")
args=parser.parse_args()

run      =args.run
aONLY    =args.analysis

pathRun="%s/cosmics_tests/run_%06i/"%(path,run)
os.system("mkdir -p %s/cosmics_tests/run_%06i/"%(path,run))

testbeam="testbeam"
##copy files


raw_Malta=pathRun+("run_%06i_1.root.root"%run)
#raw_Mini =pathRun+("run_%06i_0.root.root"%run)
try:
    glob.glob(raw_Malta)[0]
except:
    print "Cannot find Malta/MiniMalta files on this machine ..... let me copy them for you ..... "
    query = "rsync -avzhre ssh --progress sbmuser@pcatlidps04:~/MaltaSW/MaltaDAQ/run_*"+str(run)+"*.root "+pathRun
    os.system(query)
    print "___Copied MaltaRawData from ps04 "
maltasR=[]
maltasR=glob.glob( pathRun+"run_00*.root")
maltasR.sort()
print maltasR
if len(maltasR) == 0:
    print "No files found, aborting..."
    sys.exit()

ROOT.gROOT.LoadMacro( "%s/maltaToProteus_V.C+" % path)
ROOT.gROOT.LoadMacro( "%s/miniMaltaToProteus_V.C+" % path)

if run!=666:
    count=-1
    for m in maltasR:
        count+=1
        outM=pathRun+("proc_%06i_"%run)+m.split("_")[-1]
        print outM
        #outM=m.replace("merged_folder+"/"+m.split("/")[-1].replace("run_","runConv_")
        if aONLY: continue
        if count<7: 
            ROOT.maltaToProteus_V(m,outM,args.run,True,True,True,count)
        else: 
            ROOT.miniMaltaToProteus_V(m,outM,args.run,True)

#proc_Malta=pathRun+("proc_%06i_1.root.root"%run)
#print proc_Malta
#if not aONLY: ROOT.maltaToProteus_V(raw_Malta,proc_Malta,run)#,True,True,True,count)

#proc_Mini =pathRun+("proc_%06i_0.root.root"%run)
#print proc_Mini
#if not aONLY: ROOT.miniMaltaToProteus_V(raw_Mini,proc_Mini,run)

maltasR=[]
maltasR=glob.glob( pathRun+"proc_00*.root")
maltasR.sort()
#print maltasR
#if len(maltasR)!=4: 
#    print "ERROR: I was expecting 6 planes and I got: "+str(len(maltasR))

######################################################################
print "CIAO "
print maltasR
ROOT.gROOT.LoadMacro("%s/Merger_MTel.C+" % path)
local_merged=pathRun+"Merged-%06i.root" % (run)
if not aONLY and run!=666: 
    if (len(maltasR)==4):
        ROOT.Merger_MTel(local_merged,
                         maltasR[0],
                         maltasR[1],
                         maltasR[2],
			 maltasR[3])
    else:
        ROOT.Merger_MTel(local_merged,
                         maltasR[0],
                         maltasR[1],
                         maltasR[2],
                         maltasR[3],
                         maltasR[4],
                         maltasR[5],
                         maltasR[6],
                         maltasR[7])


######################################################################
baseFolder=path+"/cosmics_tests/config/"

#alignFile="config/geometry_Sr90.toml"
alignFile="config/geometry_Sr90_short.toml"

#if run>=7140 and run<=7168:
#    alignFile="/home/sbmuser/MaltaSW/MaltaTbAnalysis/DESY_oct2019/run_007150/FULL-007150-geo.toml"


commandBase ="pt-align -d "+baseFolder+"device.toml "

commandBase+=" -c "+baseFolder+"analysis.toml "

print " "

print " "
myCommand=commandBase+" -u tel_fine -n 1000000 "
#myCommand+="-g "+pathRun+("TELcoarse-%06i-geo.toml" % (run))
myCommand+=" -g "+baseFolder+"geometry_cosmics_betterAlign.toml "
##myCommand+=" -g /home/sbmuser/MaltaSW/MaltaTbAnalysis//cosmics_tests/run_000821/FULL-000821-geo.toml"
myCommand+=" "+local_merged+" "
myCommand+=pathRun+("TELfine-%06i" % (run))
print myCommand
if not aONLY: os.system(myCommand)


print " "
myCommand=commandBase+" -u tel_fine3 -n 1000000 "
myCommand+="-g "+pathRun+("TELfine-%06i-geo.toml" % (run))
myCommand+=" "+local_merged+" "
myCommand+=pathRun+("TELfine3-%06i" % (run))
print myCommand
if not aONLY: os.system(myCommand)



print " "
myCommand=commandBase+" -u dut_fine -n 1000000 "
#myCommand+="-g "+pathRun+("DUTcoarse-%06i-geo.toml" % (run))
myCommand+="-g "+pathRun+("TELfine3-%06i-geo.toml" % (run))
myCommand+=" "+local_merged+" "
myCommand+=pathRun+("FULL-%06i" % (run))
print myCommand
if not aONLY: os.system(myCommand)



##################################################################################

print "############ RUN TRACKING #############"

local_track_Straight   = pathRun+"Tracked-%06i" % (run)
command="pt-track -d "+baseFolder+"device.toml -c "+baseFolder+"analysis.toml "
#command+=" -g "+pathRun+("DUTcoarse-%06i-geo.toml" % (run))+" -u straight "+local_merged+" "+local_track_Straight
#command+=" -g "+baseFolder+"geometry_cosmics_betterAlign.toml"+" -u straight "+local_merged+" "+local_track_Straight
command+=" -g "+pathRun+("FULL-%06i-geo.toml" % (run))+" -u straight "+local_merged+" "+local_track_Straight
defaultAlign="/home/sbmuser/MaltaSW/MaltaTbAnalysis//cosmics_tests/run_000821/FULL-000821-geo.toml"
#command+=" -g "+defaultAlign+" -u straight "+local_merged+" "+local_track_Straight
print command
if not aONLY: os.system(command)


local_track_gblTracking   = pathRun+"Tracked-gbl-%06i" % (run)
command2="pt-track -d "+baseFolder+"device.toml -c "+baseFolder+"analysis.toml "
command2+=" -g "+pathRun+("FULL-%06i-geo.toml" % (run))+" -u gbl "+local_merged+" "+local_track_gblTracking
#command2+=" -g "+baseFolder+"geometry_cosmics_betterAlign.toml"+" -u gbl "+local_merged+" "+local_track_gblTracking

print command2
if not aONLY: os.system(command2)



######################################################################
local_tracked_straight=local_track_Straight+"-data.root"
local_tracked_gbl=local_track_gblTracking+"-data.root"
print "---Running TBAnalysis"
local_tbana_straight="%s/ana%06i.root" % (pathRun,run)
local_tbana_gbl     ="%s/ana_gbl_%06i.root" % (pathRun,run)

#if os.path.exists(local_tracked_straight):
#    cmd="TBAnalysis %s %s %i" % (local_tracked_straight,local_tbana_straight,run)
#    print cmd
#    os.system(cmd)
#else:
#    pass
#    ###sys.exit()

if os.path.exists(local_tracked_gbl):
    cmd="TBAnalysis %s %s %i" % (local_tracked_gbl,local_tbana_gbl,run)
    print cmd
    os.system(cmd)
else:
    pass
    ###sys.exit()



print "------------------------------------------------------------------------------------------------------"
print "---Running autoDQ"
local_dqana_straight="%s/DQ_%06i.txt" % (pathRun,run)
autoDQ.autoDQ(local_tbana_straight,local_dqana_straight,0.10)

#local_dqana_gbl="%s/DQ_gbl_%06i.txt" % (pathRun,run)
#autoDQ.autoDQ(local_tbana_gbl,local_dqana_gbl,0.10)
#local_dqana_2=""
#if local_tbana_2!="":
#    local_dqana_2="%s/DQ_%06i_c2.txt" % (local_proc,args.run)
#    autoDQ.autoDQ(local_tbana_2,local_dqana_2,0.10)

######################################################################

#local_web = "%s/run_%06i" % (webfolder,run)
#os.system("mkdir -p %s" % local_web)
#os.system("cp %s %s/." % (local_dqana_straight,local_web))
#os.system("cp %s %s/." % (local_tbana_gbl,local_web))
#os.system("chmod -R 777 "+local_web)

#fuck it this is too slow
'''
eosPath="/eos/user/a/adecmos/www/ade-pixel-testbeam/files/"
query = "scp -r %s adecmos@lxplus.cern.ch:%s/%s" % (local_web, eosPath, "ELSA_Apr2019")
print query
try:
    child = pexpect.spawn(query)
    child.expect(["Password: "])
    child.sendline(adecmospassword)
    child.logfile = sys.stdout
    child.expect(pexpect.EOF, timeout=5)
    child.interact()
except:
    print "MOTHERFUCKER I wasn't able to copy the file .... "
    os.system(query)
'''
