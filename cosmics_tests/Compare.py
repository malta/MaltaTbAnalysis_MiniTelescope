##!/usr/bin/env python
import sys
import os
import os.path
import glob
import ROOT
from array import *
import math

#gROOT.LoadMacro("~/AtlasStyleBase/AtlasStyle.C")
#gROOT.LoadMacro("~/AtlasStyleBase/AtlasUtils.C")
#SetAtlasStyle()
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPalette(1)
ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetEndErrorSize(10)

###############################################################################

f2=ROOT.TFile("run_000667/ana000667.root")
#f1=ROOT.TFile("../FinalPlot_DESY_Oct19/W7R12Cz/run__W7R12Cz__IDB70_ITH030_SUB30_PWELL06/run__W7R12Cz__IDB70_ITH030_SUB30_PWELL06.root")
f1=ROOT.TFile("run_000667/ana000667.root")

#f2=ROOT.TFile("run_000667/ana000667.root")
#f1=ROOT.TFile("run_001227/ana001227.root")

#f2=ROOT.TFile("run_000555/ana000555.root")
#f1=ROOT.TFile("run_001288/ana001288_old.root")
#f1=ROOT.TFile("run_001289/ana001289.root")
##f1.ls()

f1=ROOT.TFile("run_000825/ana000825_prev.root")
f2=ROOT.TFile("run_000825/ana000825.root")

name1="old "
name2="new "

posX =f1.Get("res_X")
posX2=f2.Get("res_X")
posY =f1.Get("res_Y")
posY2=f2.Get("res_Y")
posX3=posX.Clone("x1")
#posX3.Add(posX2,-1)

'''
posX.Scale(1./posX.Integral())
posX2.Scale(1./posX2.Integral())
posX3.Scale(1./posX3.Integral())
posY.Scale(1./posY.Integral())
posY2.Scale(1./posY2.Integral())
'''

if posY.GetMaximum()>posY2.GetMaximum(): posY2.SetMaximum(posY.GetMaximum()*1.2)
if posX.GetMaximum()>posX2.GetMaximum(): posX2.SetMaximum(posX.GetMaximum()*1.2)

posX.SetLineColor(4)
posY.SetLineColor(4)
posX2.SetLineColor(2)
posY2.SetLineColor(2)
posX3.SetLineColor(8)

posX.SetMarkerColor(4)
posY.SetMarkerColor(4)
posX2.SetMarkerColor(2)
posY2.SetMarkerColor(2)
posX3.SetMarkerColor(8)

reb=4
posX.Rebin(reb)
posY.Rebin(reb)
posX2.Rebin(reb)
posY2.Rebin(reb)
posX3.Rebin(reb)


fit =ROOT.TF1("fitA","gaus",-100,100)
fit2=ROOT.TF1("fitB","gaus",-100,100)
posX2.Fit(fit2,"RE0")
SX2=fit2.GetParameter(2)
posX.Fit(fit,"RE0")
SX=fit.GetParameter(2)
fit.SetLineColor(7)
fit2.SetLineColor(6)


can=ROOT.TCanvas("CRes","Residuals_Average",1200,600)
can.Divide(2,1)
can.cd(1)
posX.GetYaxis().SetTitle("a.u.")
posX.GetXaxis().SetRangeUser(-80,120)
posX.Draw("E")
fit.Draw("SAME")
fit2.Draw("SAME")
posX2.Draw("SAMEE")
#posX3.Draw("SAMEE")

legend4=ROOT.TLegend(0.6,0.60,0.90,0.80)
legend4.SetTextFont(42)
legend4.SetTextSize(0.038)
legend4.SetFillColor(0)
legend4.SetLineColor(0)
legend4.SetFillStyle(0)
legend4.SetBorderSize(0)
print (SX)
legend4.AddEntry(posX , "#sigma_{1} = "+str(int(SX)) +" #mum", "LPE")
legend4.AddEntry(posX2, "#sigma_{2} = "+str(int(SX2))+" #mum", "LPE")
legend4.Draw()

##############################################################################################
##############################################################################################

fitB =ROOT.TF1("fitC","gaus",-100,100)
fitB2=ROOT.TF1("fitD","gaus",-100,100)
posY2.Fit(fitB2,"RE0")
SY2=fitB2.GetParameter(2)
posY.Fit(fitB,"RE0")
SY=fitB.GetParameter(2)
fitB.SetLineColor(7)
fitB2.SetLineColor(6)

can.cd(2)
posY.GetYaxis().SetTitle("a.u.")
posY.GetXaxis().SetRangeUser(-80,120)
#posY2.GetXaxis().SetRangeUser0,10000)
posY.Draw("E")
fitB.Draw("SAME")
fitB2.Draw("SAME")
legend5=ROOT.TLegend(0.6,0.60,0.90,0.80)
legend5.SetTextFont(42)
legend5.SetTextSize(0.038)
legend5.SetFillColor(0)
legend5.SetLineColor(0)
legend5.SetFillStyle(0)
legend5.SetBorderSize(0)
legend5.AddEntry(posY , "#sigma = "+str(int(SY)) +" #mum", "LPE")
legend5.AddEntry(posY2, "#sigma = "+str(int(SY2))+" #mum", "LPE")
legend5.Draw()
posY2.Draw("SAMEE")


can.Print("Comparison.pdf")
os.system("evince Comparison.pdf &")

sys.exit()

