#!/usr/bin/env python
import sys
import glob
import ROOT
from array import array
import math
import argparse
import os

#ROOT.gROOT.LoadMacro("~/AtlasStyleBase/AtlasStyle.C")
#ROOT.gROOT.LoadMacro("~/AtlasStyleBase/AtlasUtils.C")
#ROOT.SetAtlasStyle()

ROOT.gStyle.SetOptStat(0)
#ROOT.gStyle.SetPalette(1)
ROOT.gROOT.SetBatch(1)
#ROOT.gStyle.SetEndErrorSize(10)
#ROOT.gStyle.SetTitleOffset(1.25,"y")        

parser=argparse.ArgumentParser()
parser.add_argument("-c","--chip",nargs="+",type=str,default="W7R12")
parser.add_argument("-s2","--s2only",action='store_true')
parser.add_argument("-s3","--s3only",action='store_true')
args=parser.parse_args()



def func(elem):
    return int(elem.split('SUB')[1].split("_")[0])

os.system("mkdir -p SummaryPlots/")

plots=[
    #["W11R11T2021",[ROOT.kMagenta+1, ROOT.kMagenta]],
    #["W11R12T",[ROOT.kBlue+2, ROOT.kBlue+1, ROOT.kBlue]],
    #["W12R1T",[ROOT.kGreen+1, ROOT.kGreen]],
    #["W12R0T",[ROOT.kRed+1, ROOT.kRed]],

   # ["W7R0DESYNC" ,[ROOT.kRed,ROOT.kOrange] ],


    #["W7R12Cz",[ROOT.kBlack ] ],
    #["W7R12T" ,[ROOT.kRed+2,ROOT.kRed+1,ROOT.kRed,ROOT.kBlack,ROOT.kOrange] ],
    #["W7R13T" ,[ROOT.kGreen+3,ROOT.kGreen] ],
    #["W8R5T" ,[ROOT.kBlue,ROOT.kMagenta,ROOT.kAzure+10,ROOT.kRed,ROOT.kBlack,ROOT.kOrange] ],
    ["W5R10T" ,[ROOT.kBlue,ROOT.kMagenta,ROOT.kAzure+10,ROOT.kRed,ROOT.kBlack,ROOT.kOrange] ],
    #["W7R0TDESYNC" ,[ROOT.kMagenta,ROOT.kMagenta-3,ROOT.kOrange] ],
    #["run__W11R5T2021" ,[ROOT.kRed,ROOT.kGreen,ROOT.kAzure+10,ROOT.kRed,ROOT.kBlack,ROOT.kRed] ],
    #["W7R12T" ,[ROOT.kGreen,ROOT.kGreen+3,ROOT.kGreen+2,ROOT.kRed,ROOT.kBlack,ROOT.kOrange] ],
    #["W10R1T" ,[ROOT.kBlack,ROOT.kGray+2,ROOT.kAzure+10,ROOT.kRed,ROOT.kBlack,ROOT.kOrange] ],
    #["W9R11T2021" ,[ROOT.kRed+2, ROOT.kRed+1,ROOT.kRed] ],
    #["W9R14T" ,[ROOT.kAzure+1,ROOT.kAzure] ],
    #["W9R4T" ,[ROOT.kOrange+1,ROOT.kOrange] ],
    #["W9R1T" ,[ROOT.kAzure+1,ROOT.kAzure] ],
    #["W9R0T" ,[ROOT.kGreen+1,ROOT.kGreen] ],
    #["W11R11T2021",[ROOT.kMagenta+1, ROOT.kMagenta]],
    #["W9R5T2021" ,[ROOT.kRed,ROOT.kMagenta,ROOT.kAzure+10,ROOT.kRed,ROOT.kBlack,ROOT.kOrange] ],
    #["W9R5T" ,[ROOT.kBlue,ROOT.kAzure+10,ROOT.kRed,ROOT.kBlack,ROOT.kOrange] ],
    #["W11R5T2021" ,[ROOT.kBlue,ROOT.kMagenta,ROOT.kAzure+10,ROOT.kRed,ROOT.kBlack,ROOT.kOrange] ],
   # ["W4R12T" ,[ROOT.kYellow+2,ROOT.kYellow+3,ROOT.kRed,ROOT.kBlack,ROOT.kOrange] ],
    #["W11R5T20221" ,[ROOT.kRed,ROOT.kBlack,ROOT.kOrange] ],
    #["W11R0T" ,[ROOT.kBlack,ROOT.kGray+2,ROOT.kAzure+10,ROOT.kRed,ROOT.kBlack,ROOT.kOrange] ],
    #["W11R10T" ,[ROOT.kBlack,ROOT.kGray+2,ROOT.kAzure+10,ROOT.kRed,ROOT.kBlack,ROOT.kOrange] ],

  # ["W1R5T" ,[ROOT.kRed-1,ROOT.kRed+2]],
    #["W11R1T" ,[ROOT.kRed+2,ROOT.kRed+1]],
   #["W7R0T" ,[ROOT.kBlue,ROOT.kBlue-7]],
   #["W7R0T" ,[ROOT.kBlack,3,8,ROOT.kSpring+9,ROOT.kSpring-1,ROOT.kSpring+4] ],
    #["W9R5T" ,[ROOT.kBlue,ROOT.kMagenta,ROOT.kAzure+10,ROOT.kRed,ROOT.kBlack,ROOT.kOrange] ],
    #["W9R4T2021",[ROOT.kOrange+1]],
    #["W7R5T" ,[ROOT.kGray+1,ROOT.kMagenta,ROOT.kAzure+10,ROOT.kRed,ROOT.kBlack,ROOT.kOrange] ],
   # ["W7R0DESYNC" ,[ROOT.kBlue,ROOT.kMagenta,ROOT.kAzure+10,ROOT.kRed,ROOT.kBlack,ROOT.kOrange] ],
    #["W7R4Epi" ,[2]],
    #["W7R4T" ,[ROOT.kOrange+1]], ##ROOT.kAzure+10]],
    #["W11R0T"  ,[6]],
 #   ["W7R11T",[3,8,ROOT.kSpring+9,ROOT.kSpring-1,ROOT.kSpring+4]],
    #["W9R11Cz", [4]],
    #["W9R11T" , [ROOT.kAzure+10]],
    #["W11R11T" , [ROOT.kGreen+2]],
    #["W7R0", []],
]


base=ROOT.TH1D("vale",";SUB [V];Efficiency [%]",100,0,12)
base.SetMaximum(100)
base.SetMinimum( 10)##was 92

can=ROOT.TCanvas("cEffVsSub","EffVsSub",800,600)
can.SetGridy(1)
base.Draw("AXIS")

can2=ROOT.TCanvas("ccEffVsSub","cEffVsSub",800,600)
base2=ROOT.TH1D("vale",";Threshold [el];Noise [MHz]",100,200,600)
base2.SetMaximum(2)
base2.SetMinimum( 0)##was 92
base2.Draw("AXIS")
leg=ROOT.TLegend(0.20,0.70,0.50,0.90)
graphs=[]
for w in plots:
    print (" ")
    print (w[0])

    path =""
    #files=glob.glob(w[0]+"/*PWELL06*/Eff.txt")
    if "T" in w[0]:
       path="FinalPlot_Sr90/"
    else :
       path="/home/sbmuser/MaltaSW/MaltaTbAnalysis/FinalPlot_DESY_Oct19/"
   
    files=glob.glob(path+w[0]+"/*/Eff.txt")
    print(path,files)
    files = sorted(files, key=func)
    configs=[]
    for f in files:
        conf="IDB"+f.split("_IDB")[1].split("_SUB")[0]
        configs.append(conf)
    configs=list(set(configs))
    #configs = sorted(configs, key=func)
    configs.sort()
    print (configs)
    countC=-1
    for c in configs:
        countC+=1
        gS2=ROOT.TGraphErrors()
        gS3=ROOT.TGraphErrors()
        gS4=ROOT.TGraphErrors()
        graphs.append(gS2)
        graphs.append(gS3)
        gS2.SetLineWidth(2)
        gS3.SetLineWidth(2)
        gS4.SetLineWidth(2)
        gS2.SetLineColor(w[1][countC])
        gS3.SetLineColor(w[1][countC])
        gS3.SetLineStyle(2)
        gS2.SetMarkerColor(w[1][countC])
        gS3.SetMarkerColor(w[1][countC])
        gS2.SetMarkerSize(1.2)
        gS3.SetMarkerSize(1.2)
        gS4.SetMarkerSize(1.2)
        gS2.SetMarkerStyle(20)
        gS3.SetMarkerStyle(21)
        gS4.SetMarkerStyle(21)
        countF=-1
        for f in files:
            if c not in f: continue
            countF=countF+1
            sub=0;
            if "p2" in f or "p5" in f or "p7" in f:
                tmp=(f.split("SUB")[1].split("_PW")[0])
                tmp=tmp.replace("p",".")
                sub=float(tmp)+0.02*countC
            else:
                sub=float(f.split("SUB")[1].split("_PW")[0])+0.02*countC
            fr=open(f,"r")
            left =fr.readline().split(" ")
            right=fr.readline().split(" ")
            effL=left[1]
            effR=right[1]
            errL=left[3].split("\n")[0]
            errR=right[3].split("\n")[0]
            print ("SUB= "+str(sub)+" --> EFF_L: "+str(effL)+" +/- "+str(errL))
            gS2.SetPoint(countF,sub,float(effL))
            gS2.SetPointError(countF,0.01,float(errL))
            gS3.SetPoint(countF,sub,float(effR))
            gS3.SetPointError(countF,0.01,float(errR))
            fr.close()
            fr=open(f.replace("Eff","NoisePedestal"),"r")
            noise=fr.readline().split(" ")
            gS4.SetPoint(countF,sub,float(noise[0]))
        can.cd()
        leg.AddEntry(gS2,w[0]+" "+c,"l")
        print(w[0]+" "+c)
        leg.Draw("same")
        gS2.Draw("PL")
        gS3.Draw("PL")
        can2.cd()
        gS4.Draw("PL")
        print(c)
        gS4.Print("v")
#can.cd()
#g = ROOT.TGraph()
#g.SetPoint(0,50, 79.9) 
#g.SetMarkerStyle(29)
#g.SetMarkerSize(2)
#g.SetMarkerColor(ROOT.kAzure+10)
#g.SetLineColor(ROOT.kAzure+10)
#leg.AddEntry(g, "#mu @127-127 full chip average","p")
#
#gL = ROOT.TGraph()
#gL.SetPoint(0,50, 95.5) 
#gL.SetMarkerStyle(22)
#gL.SetMarkerColor(ROOT.kAzure+10)
#gL.SetLineColor(ROOT.kAzure+10)
#leg.AddEntry(gL, "#mu @127-127 left","p")
#
#gR = ROOT.TGraph()
#gR.SetPoint(0,50, 75.4) 
#gR.SetMarkerStyle(23)
#gR.SetMarkerColor(ROOT.kAzure+10)
#gR.SetLineColor(ROOT.kAzure+10)
#leg.AddEntry(gR, "#mu @127-127 right","p")
#
#g.Draw("pl,same")
#gL.Draw("pl,same")
#gR.Draw("pl,same")
#leg.Draw()
#
can.Update()
can.Print("SummaryPlots/Summary_"+plots[0][0]+".pdf")
can.Print("SummaryPlots/Summary_"+plots[0][0]+".root")
can2.Print("SummaryPlots/Summary_noise.pdf")








sys.exit()
###################################################################################################################################################################

files = []
for r, d, f in os.walk("/home/sbmuser/MaltaSW/MaltaTbAnalysis/Sr90_tests/FinalPlot_Sr90/"):
    for fil in f:
        if "Eff.txt" in fil:# and "overView" not in file:#and "SUB16" not in file:
            files.append(os.path.join(r, fil))
files = sorted(files)
print (files)
exit(0)
gS2={}
gS3={}
counter=0
#color=2
colors=[#["W4R0",2,"30 #mum EPI + n-gap 1E15 n_{eq}/cm^{2}"]
        ["W4R1",2,"30 #mum EPI + n-gap 1E15 n_{eq}/cm^{2}"],
        ["W4R4",3,"30 #mum EPI + n-gap 2E15 n_{eq}/cm^{2}"],
        ["W7R1Cz",4,"Cz Std 1E15 n_{eq}/cm^{2}"],
        ["W9R0Cz",1,"Cz n-gap 1E15 n_{eq}/cm^{2}"],
        ["W9R1Cz",6,"Cz n-gap 1E15 n_{eq}/cm^{2}"],
        ["W9R4Cz",7,"Cz n-gap 2E15 n_{eq}/cm^{2}"],
        ["W11R0Cz",8,"Cz xdpw 1E15 n_{eq}/cm^{2}"],
        ["W11R11Cz",9,"Cz xdpw  unirradiated"],
        ["W7R4",11,"30 #mum EPI Std unirradiated"],
]
configs={#"W4R0":[""],
         "W4R1":["IDB127_ITHR127"],
         "W4R4":["IDB127_ITHR3"],#"IDB127_ITHR127"],
         "W7R1Cz":["IDB127_ITHR127"],
         "W9R0Cz":["IDB127_ITHR127"],
         "W9R1Cz":["IDB127_ITH127"],#,"IDB100_ITH30"],
         "W9R4Cz":["IDB127_ITHR127"],#"IDB80_ITHR30"],
         "W11R0Cz":["IDB127_ITHR127"],#,"IDB100_ITHR30"],
         "W11R11Cz":["IDB127_ITH100_SUB06","IDB70_ITH30_SUB06","IDB100_ITH30_SUB06","IDB127_ITH30_SUB06"],
         "W11R5T2021":["IDB127_ITHR50"],
}
name=""

#for chip in args.chip:
for plot in range(len(plots)):
    cEffvsSub=ROOT.TCanvas("cEffVsSub","EffVsSub",600,600)
    ROOT.gPad.SetTicks()
    mg=ROOT.TMultiGraph()

    if len(plots[plot])==2:
        lg=ROOT.TLegend(0.446488-0.147993,0.191638,0.765886-0.147993,0.310105)
        lg2=ROOT.TLegend(0.436455-0.147993,0.191638,0.536789-0.147993,0.310105)
        tt=ROOT.TLatex(18.645,19.9535,"Sector 2")
        tt2=ROOT.TLatex(15.9,19.9535,"Sector 3")
    else:
        lg=ROOT.TLegend(0.446488,0.191638,0.765886,0.512195)
        lg2=ROOT.TLegend(0.436455,0.191638,0.536789,0.512195)
        tt=ROOT.TLatex(28.9,47,"Sector 2")
        tt2=ROOT.TLatex(26.2,47,"Sector 3")

    tt.SetTextAngle(90)
    tt.SetTextFont(42)
    tt.SetTextSize(0.04)
    tt2.SetTextAngle(90)
    tt2.SetTextFont(42)
    tt2.SetTextSize(0.04)

    print (len(plots[plot]))
    lg.SetTextFont(42)
    lg.SetTextSize(0.038)
    lg.SetFillColor(0)
    lg.SetLineColor(0)
    lg.SetFillStyle(0)
    lg.SetBorderSize(0)
    lg2.SetTextFont(42)
    lg2.SetTextSize(0.038)
    lg2.SetFillColor(0)
    lg2.SetLineColor(0)
    lg2.SetFillStyle(0)
    lg2.SetBorderSize(0)
    for chip in plots[plot]:
        print(plots)
        gS2[str(chip)]=ROOT.TGraph()
        gS3[str(chip)]=ROOT.TGraph()
        s=0
        while s < 65:
            if "_" in chip:chipN=chip.replace("_","")
            else:chipN=chip
            for file in files:
                #print(files)
                exit(0)
                okS2=True
                okS3=True
                if chip not in file:continue
                skip=True
                #if "W4R0_" and "SUB%i" in file: skip=False
                if "SUB%i_"%s not in file and skip==True:continue
                if "W11R0Cz" in chip and s>53:continue
                if "W11R11Cz" in chip and s>12 or "PWELL2" in file:continue
                #if "W4R4" in chip:
                #    if "ITHR3_" not in file: continue
                if chipN in configs:
                    if configs[chipN] != "" and chip!="W7R4":
                        idb=file.split("__")[2].split("_")[0]
                        ithr=file.split("__")[2].split("_")[1]
                        idb_ithr=idb+"_"+ithr
                        if idb_ithr not in configs[chipN]:continue
                        pass
                    pass
                fr=open(file,"r")
                effL=fr.readline().split(" ")[1]
                effR=fr.readline().split(" ")[1]
                baseName=fr.readline().strip()
                if "nan" in effL:okS2=False
                if "nan" in effR:okS3=False
                effL=float("%.4f"%float(effL))
                effR=float("%.4f"%float(effR))
                sub=float("%.3f"%float(baseName.split("SUB")[1].split("_")[0]))
                if "W7R1Cz" in chip and s==50 and effL<96.55:continue
                if "W7R1Cz" in chip and s==50 and effR<96.55:continue
                if okS2==True and effL >1:
                    gS2[chip].SetPoint(gS2[chip].GetN(),sub,effL)
                    pass
                if okS3==True and effR >1 and args.s2only==False:
                    gS3[chip].SetPoint(gS3[chip].GetN(),sub,effR)
                    pass
                #print effL,effR,baseName
                fr.close()
                pass
            s+=1
            """
            for color in colors:
                if chipN in color[0]:
                    gS2[chip].SetLineColor(color[1])
                    gS2[chip].SetMarkerStyle(21)
                    gS2[chip].SetMarkerColor(color[1])
                    gS2[chip].Draw("APL")
                    gS2[chip].GetYaxis().SetRangeUser(0,100)

                    gS3[chip].SetLineColor(color[1])
                    gS3[chip].SetMarkerStyle(22)
                    gS3[chip].SetMarkerColor(color[1])
                    gS3[chip].Draw("APL")
                    gS3[chip].GetYaxis().SetRangeUser(0,100)
                    pass
                pass
            """
            counter+=1
            pass

        for color in colors:
            if args.s2only and chipN in color:
                lg.AddEntry(gS2[chip],"%s"%color[2],"p")
                mg.Add(gS2[chip])
                pass
            elif args.s3only and chipN in color:
                lg.AddEntry(gS3[chip],"%s"%color[2],"p")
                mg.Add(gS3[chip])
                pass
            elif not args.s2only and not args.s3only and chipN in color:
                lg.AddEntry(gS2[chip],"%s"%color[2],"p")
                lg2.AddEntry(gS3[chip],"","p")
                mg.Add(gS2[chip])
                mg.Add(gS3[chip])
                pass
            pass
        name+="AAA"#chipN+"_"
        pass
        
    mg.SetTitle(";Substrate Voltage [V];Efficiency [%]")
    mg.Draw("APL")
    lg.Draw()
    lg2.Draw()
    if not args.s2only and not args.s3only:
        tt.Draw()
        tt2.Draw()
        pass
    mg.GetYaxis().SetRangeUser(0,100)
    cEffvsSub.Modified()
    cEffvsSub.Update()
    if args.s2only:name=name[:-1]+"_s2"
    if args.s3only:name=name[:-1]+"_s3"
    cEffvsSub.SaveAs("../SummaryPlots/%s_threshold.pdf"%name)
    wm=ROOT.TLatex(9.51306,4.39024,"TJ MALTA INTERNAL")
    wm.SetTextColorAlpha(17,0.476)
    wm.SetTextAngle(45.8384)
    wm.SetTextSize(0.097561)
    wm.Draw()
    cEffvsSub.Update()
    cEffvsSub.SaveAs("SummaryPlots/%s_Watermarked.png"%name)

    #raw_input("")
    pass
