#!/usr/bin/env python
import sys
import glob
import ROOT
from array import array
import math
import argparse
import os

#ROOT.gROOT.LoadMacro("~/AtlasStyleBase/AtlasStyle.C")
#ROOT.gROOT.LoadMacro("~/AtlasStyleBase/AtlasUtils.C")
#ROOT.SetAtlasStyle()

ROOT.gStyle.SetOptStat(0)
#ROOT.gStyle.SetPalette(1)
ROOT.gROOT.SetBatch(1)
#ROOT.gStyle.SetEndErrorSize(10)
#ROOT.gStyle.SetTitleOffset(1.25,"y")        

parser=argparse.ArgumentParser()
parser.add_argument("-c","--chip",nargs="+",type=str,default="W7R12")
parser.add_argument("-s2","--s2only",action='store_true')
parser.add_argument("-s3","--s3only",action='store_true')
args=parser.parse_args()

def func(elem):
    return int(elem.split('threshold')[1].split("_")[0])


os.system("mkdir -p SummaryPlots/")
base=ROOT.TH1D("vale",";Threshold [el]; <cl size>",100,200,600)

plots=[
    ["W7R12Cz",[ROOT.kBlack ] ],
    ["W7R12T" ,[ROOT.kGray+1] ],
#    ["W4R12Epi",[4]],
    ["W7R4Epi" ,[2]],
    ["W7R4T" ,[ROOT.kOrange+1]], ##ROOT.kAzure+10]],
   #["W7R4Epi" ,[2,ROOT.kOrange+1,6]],
    ["W11R0T"  ,[6]],
    ["W11R11Cz",[3,8,ROOT.kSpring+9,ROOT.kSpring-1,ROOT.kSpring+4]],
    ["W9R11Cz", [4]],
    ["W9R11T" , [ROOT.kAzure+10]],
    ["W11R11T" , [ROOT.kGreen+2]]
]
base.SetMaximum( 2.5)
base.SetMinimum( 1) #80
base.GetXaxis().SetRangeUser(0.5,3) #5


can=ROOT.TCanvas("cEffVsSub","EffVsSub",800,600)
can.SetGridy(1)
base.Draw("AXIS")
graphs=[]
for w in plots:
    print (" ")
    print (w[0])
    path=""
    ##files=glob.glob(w[0]+"/*PWELL06*/Eff.txt")
    if "T" in w[0]:
      path = "FinalPlot_Sr90/"
    else:
      path="/home/sbmuser/MaltaSW/MaltaTbAnalysis/FinalPlot_DESY_Oct19/"
    print(path)
    files=glob.glob(path+w[0]+"/*threshold*/Eff.txt")
    print (w[0]+"/*threshold*/Eff.txt")
    #files.sort()
    files = sorted(files, key=func)
    configs=[]
    for f in files:
        conf="IDB"+f.split("__IDB")[1].split("_SUB")[0]
        configs.append(conf)
    configs=list(set(configs))
    configs.sort()
    tmpC=[]
    for i in range(0,len(configs)):
        tmpC.append(configs[len(configs)-1-i])
    configs=tmpC
    print (configs)
    countC=-1
    for c in configs:
        print (c)
        countC+=1
        gS2=ROOT.TGraphErrors()
        gS3=ROOT.TGraphErrors()
        graphs.append(gS2)
        graphs.append(gS3)
        gS2.SetLineWidth(2)
        gS3.SetLineWidth(2)
        #gS2.SetLineColor(w[1][countC])
        #gS3.SetLineColor(w[1][countC])
        gS3.SetLineStyle(2)
        #gS2.SetMarkerColor(w[1][countC])
        #gS3.SetMarkerColor(w[1][countC])
        gS2.SetMarkerSize(1.4)
        gS3.SetMarkerSize(1.4)
        gS2.SetMarkerStyle(20)
        gS3.SetMarkerStyle(21)
        countF=-1
        for f in files:
            #if c not in f: continue
            print (f)
            countF+=1
            sub=0;
            if "p2" in f or "p5" in f or "p7" in f:
                tmp=(f.split("SUB")[1].split("_threshold")[0])
                tmp=tmp.replace("p",".")
                sub=float(tmp)+0.02*countC
            else:
                
                sub=float(f.split("threshold")[1].split("_")[0]) #+0.02*countC
                print(f.split("threshold")[1].split("_")[0])
            l2=glob.glob(f.replace("Eff.txt","run_*.root"))
            t2=ROOT.TFile(l2[0])
	    t2.Print()
            #t2.ls() #ClSize_match
            h2=t2.Get("ClSize_match")
            val=h2.GetMean()
            err=h2.GetRMS()
            gS2.SetPoint(gS2.GetN(),sub,val)
            gS2.SetPointError(gS2.GetN()-1,0.01,0.)
            t2.Close()
        gS2.Draw("PL")
        #gS3.Draw("PL")
gS2.Print()
can.Update()
can.Print("SummaryPlots/Njet_threshold.pdf")








sys.exit()
###################################################################################################################################################################
