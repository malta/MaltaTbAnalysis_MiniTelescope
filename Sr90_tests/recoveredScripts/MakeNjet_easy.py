#!/usr/bin/env python
import sys
import glob
import ROOT
from array import array
import math
import argparse
import os

#ROOT.gROOT.LoadMacro("~/AtlasStyleBase/AtlasStyle.C")
#ROOT.gROOT.LoadMacro("~/AtlasStyleBase/AtlasUtils.C")#
#ROOT.SetAtlasStyle()

#ROOT.gStyle.SetOptStat(0)
#ROOT.gStyle.SetPalette(1)
ROOT.gROOT.SetBatch(1)
#ROOT.gStyle.SetEndErrorSize(10)
#ROOT.gStyle.SetTitleOffset(1.25,"y")        

parser=argparse.ArgumentParser()
parser.add_argument("-c","--chip",nargs="+",type=str,default="W7R12")
parser.add_argument("-s2","--s2only",action='store_true')
parser.add_argument("-s3","--s3only",action='store_true')
args=parser.parse_args()

os.system("mkdir -p SummaryPlots/")
base=ROOT.TH1D("vale",";Substrate Voltage [V]; <cl size>",10,5,65)
base.SetMaximum( 2.5)
base.SetMinimum( 1) #80
base.GetXaxis().SetRangeUser(5,32) #5


config = {
         # "run_000759/ana_gbl_000759.root" : 6,
         # "run_000763/ana_gbl_000763.root" : 8,
         # "run_000762/ana_gbl_000762.root" : 10,
         # "run_000761/ana_gbl_000761.root" : 12,
         # "run_000764/ana_gbl_000764.root" : 16,
         # "run_000760/ana_gbl_000760.root" : 20,
         # "run_000765/ana_gbl_000765.root": 24,
         # "run_000766/ana_gbl_000766.root": 30,
          

  	  "run_000771/ana_gbl_000771.root" : 6,
          "run_000772/ana_gbl_000772.root" : 8,
          "run_000773/ana_gbl_000773.root" : 10,
          "run_000774/ana_gbl_000774.root" : 12,
          "run_000775/ana_gbl_000775.root" : 16,
          "run_000776/ana_gbl_000776.root" : 20,
          "run_000777/ana_gbl_000777.root": 24,
          "run_000778/ana_gbl_000778.root": 30,
	 }


can=ROOT.TCanvas("cEffVsSub","EffVsSub",800,600)
can.SetGridy(1)
base.Draw("AXIS")
graph=ROOT.TGraph()
point=0;
for f, sub in sorted(config.items(), key=lambda item: item[1]):
   
   t2=ROOT.TFile(f)
   #print(t2)
   h2=t2.Get("ClSize_match")
   #h2.Print()
   val=h2.GetMean()
   graph.SetPoint(point, sub, val)
   point+=1
   del h2
   del t2

graph.Print("v")
graph.SetMarkerSize(1.4)
graph.SetMarkerStyle(20)
graph.Draw("PL")
graph.SaveAs("g.root")
can.Update()
can.Print("SummaryPlots/Njet.pdf")

"""


for w in plots:
    print (" ")
    print (w[0])
    config=open("Eff.txt",'r')
    files = config.read().splitlines()
    #files=glob.glob("Eff.txt")
    #files=glob.glob(w[0]+"/*PWELL06*/Eff.txt")
    #files.sort()
    configs=[]
    for f in files:
        #conf="IDB"+f.split("__IDB")[1].split("_SUB")[0]
        #configs.append(conf)
        configs.append(f)
    configs=list(set(configs))
    #configs.sort()
    tmpC=[]
    #for i in range(0,len(configs)):
    #    tmpC.append(configs[len(configs)-1-i])
    #configs=tmpC
    print ("ANDREA")
    print (configs)
    countC=-1
    for c in configs:
        print (c)
        countC+=1
        gS2=ROOT.TGraphErrors()
        gS3=ROOT.TGraphErrors()
        graphs.append(gS2)
        graphs.append(gS3)
        gS2.SetLineWidth(2)
        gS3.SetLineWidth(2)
        gS2.SetLineColor(w[1][countC])
        gS3.SetLineColor(w[1][countC])
        gS3.SetLineStyle(2)
        gS2.SetMarkerColor(w[1][countC])
        gS3.SetMarkerColor(w[1][countC])
        gS2.SetMarkerSize(1.4)
        gS3.SetMarkerSize(1.4)
        gS2.SetMarkerStyle(20)
        gS3.SetMarkerStyle(21)
        countF=-1
        for f in files:
            if c not in f: continue
            print (f)
            countF+=1
            #sub=float(f.split("SUB")[1].split("_PW")[0])+0.02*countC
            #l2=glob.glob(f.replace("Eff.txt","run_*.root"))
            t2=ROOT.TFile(f)
            t2.Print()
            #t2.ls() #ClSize_match
            h2=t2.Get("ClSize_match")
            h2.Print()
            val=h2.GetMean()
            err=h2.GetRMS()
            gS2.SetPoint(gS2.GetN(),sub,val)
            gS2.SetPointError(gS2.GetN()-1,0.01,0.)
            t2.Close()
        gS2.Draw("PL")
        #gS3.Draw("PL")

can.Update()
can.Print("SummaryPlots/Njet.pdf")


"""





sys.exit()
###################################################################################################################################################################
