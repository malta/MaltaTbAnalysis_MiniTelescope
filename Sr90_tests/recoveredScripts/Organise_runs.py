import os
import sys
import glob
import ROOT
import math
import numpy as np

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(1)
ROOT.gStyle.SetOptFit(1)

badRuns=[]
baseFolder="/home/sbmuser/MaltaSW/MaltaTbAnalysis/Sr90_tests/"
outFolder ="/home/sbmuser/MaltaSW/MaltaTbAnalysis/Sr90_tests/FinalPlot_Sr90/"



runProp = [    
    #["W7R12T__IDB070_ITH030_SUB06_PWELL06"    , [759]],
    #["W7R12T__IDB070_ITH030_SUB20_PWELL06"    , [760]],
    #["W7R12T__IDB070_ITH030_SUB12_PWELL06"    , [761]],
    #["W7R12T__IDB070_ITH030_SUB10_PWELL06"    , [762]],
    #["W7R12T__IDB070_ITH030_SUB08_PWELL06"    , [763]],
    #["W7R12T__IDB070_ITH030_SUB16_PWELL06"    , [764]],
    #["W7R12T__IDB070_ITH030_SUB24_PWELL06"    , [765]],
    #["W7R12T__IDB070_ITH030_SUB30_PWELL06"    , [766]],

    #["W11R0T__IDB070_ITH030_SUB06_PWELL06"    , [771]],
    #["W11R0T__IDB070_ITH030_SUB08_PWELL06"    , [772]],
    #["W11R0T__IDB070_ITH030_SUB10_PWELL06"    , [773]],
    #["W11R0T__IDB070_ITH030_SUB12_PWELL06"    , [774]],
    #["W11R0T__IDB070_ITH030_SUB16_PWELL06"    , [775]],
    #["W11R0T__IDB070_ITH030_SUB20_PWELL06"    , [776]],
    #["W11R0T__IDB070_ITH030_SUB24_PWELL06"    , [777]],
    #["W11R0T__IDB070_ITH030_SUB30_PWELL06"    , [778]],

    #["W7R4T__IDB070_ITH030_SUB06_PWELL06"     , [784]],
    #["W7R4T__IDB070_ITH030_SUB08_PWELL06"     , [785]],
    #["W7R4T__IDB070_ITH030_SUB10_PWELL06"     , [786]],
    #["W7R4T__IDB070_ITH030_SUB12_PWELL06"     , [787]],
    #["W7R4T__IDB070_ITH030_SUB16_PWELL06"     , [788]],
    #["W7R4T__IDB070_ITH030_SUB20_PWELL06"     , [789]],
    #["W7R4T__IDB070_ITH030_SUB24_PWELL06"     , [790]],
    #["W7R4T__IDB070_ITH030_SUB30_PWELL06"     , [791]],
    
    #["W9R11T__IDB070_ITH030_SUB06_PWELL06"     , [797]],
    #["W9R11T__IDB070_ITH030_SUB08_PWELL06"     , [798]],
    #["W9R11T__IDB070_ITH030_SUB10_PWELL06"     , [799]],
    #["W9R11T__IDB070_ITH030_SUB11_PWELL06"     , [800]],
    #["W9R11T__IDB070_ITH030_SUB12_PWELL06"     , [801]],
    #["W9R11T__IDB070_ITH030_SUB13_PWELL06"     , [802]],
    #["W9R11T__IDB070_ITH030_SUB14_PWELL06"     , [803]],
    #["W9R11T__IDB070_ITH030_SUB15_PWELL06"     , [804]],
    #["W9R11T__IDB070_ITH030_SUB16_PWELL06"     , [805]],
    #["W9R11T__IDB070_ITH030_SUB15p50_PWELL06"  , [806]],
    #["W9R11T__IDB070_ITH030_SUB15p25_PWELL06"  , [807]],

   # ["W11R11T__IDB127_ITH127_SUB06_PWELL06"     , [809]],
   # ["W11R11T__IDB070_ITH030_SUB06_PWELL06"     , [810]],
   # ["W11R11T__IDB070_ITH030_SUB08_PWELL06"     , [811]],
   # ["W11R11T__IDB070_ITH030_SUB10_PWELL06"     , [812]],
   # ["W11R11T__IDB070_ITH030_SUB11_PWELL06"     , [813]],
   # ["W11R11T__IDB070_ITH030_SUB12_PWELL06"     , [814]],
   # ["W11R11T__IDB070_ITH030_SUB13_PWELL06"     , [815]],
   # ["W11R11T__IDB070_ITH030_SUB14_PWELL06"     , [816]],
   # ["W11R11T__IDB070_ITH030_SUB13p25_PWELL06"     , [820]],
   # ["W11R11T__IDB070_ITH030_SUB13p50_PWELL06"     , [821]],
   # ["W11R11T__IDB070_ITH030_SUB13p75_PWELL06"     , [819]],
  #  ["W11R11T__IDB127_ITH127_SUB06_threshold550_PWELL06"     , [809]],
  #  ["W11R11T__IDB070_ITH030_SUB06_threshold360_PWELL06"     , [810]],
  #  ["W11R11T__IDB0127_ITH0100_SUB06_threshold515_PWELL06"     , [875]],
  #  ["W11R11T__IDB0127_ITH030_SUB06_threshold413_PWELL06"     , [877]],
  #  ["W11R11T__IDB0100_ITH030_SUB06_threshold393_PWELL06"     , [873]],
  #  ["W11R11T__IDB050_ITH010_SUB06_threshold287_PWELL06"     , [878]],
  #  ["W11R11T__IDB050_ITH020_SUB06_threshold310_PWELL06"     , [879]],
  #  ["W11R11T__IDB050_ITH030_SUB06_threshold328_PWELL06"     , [880]],
  
#  ["W7R12T__IDB070_ITH030_SUB06_threshold380_PWELL06"     , [949]],
#  ["W7R12T__IDB070_ITH030_SUB08_threshold380_PWELL06"     , [993]],
#  ["W7R12T__IDB070_ITH030_SUB10_threshold370_PWELL06"     , [950]],
#  ["W7R12T__IDB070_ITH030_SUB15_threshold365_PWELL06"     , [951]],
#  ["W7R12T__IDB070_ITH030_SUB20_threshold363_PWELL06"     , [952]],
#  ["W7R12T__IDB070_ITH030_SUB25_threshold361_PWELL06"     , [953]],
#  ["W7R12T__IDB070_ITH030_SUB30_threshold360_PWELL06"     , [954]],
#  ["W7R12T__IDB070_ITH030_SUB30_threshold380_PWELL06"     , [973]],
#  ["W7R12T__IDB070_ITH030_SUB35_threshold380_PWELL06"     , [974]],
#  ["W7R12T__IDB070_ITH030_SUB40_threshold380_PWELL06"     , [975]],
#  ["W7R12T__IDB070_ITH030_SUB45_threshold380_PWELL06"     , [976]],
#  ["W7R12T__IDB070_ITH030_SUB50_threshold380_PWELL06"     , [977]],

#  ["W7R12T__IDB050_ITH010_SUB06_threshold300_PWELL06"     , [955]],
#   ["W7R12T__IDB050_ITH010_SUB08_threshold300_PWELL06"     , [994]],
#  ["W7R12T__IDB050_ITH010_SUB10_threshold290_PWELL06"     , [956]],
#  ["W7R12T__IDB050_ITH010_SUB15_threshold285_PWELL06"     , [957]],
#  ["W7R12T__IDB050_ITH010_SUB20_threshold282_PWELL06"     , [958]],
#  ["W7R12T__IDB050_ITH010_SUB25_threshold281_PWELL06"     , [959]],
#  ["W7R12T__IDB050_ITH010_SUB30_threshold280_PWELL06"     , [960]],
 # ["W7R12T__IDB050_ITH010_SUB30_threshold300_PWELL06"     , [978]],
 # ["W7R12T__IDB050_ITH010_SUB35_threshold300_PWELL06"     , [979]],
 # ["W7R12T__IDB050_ITH010_SUB40_threshold300_PWELL06"     , [980]],
 # ["W7R12T__IDB050_ITH010_SUB45_threshold300_PWELL06"     , [981]],
 # ["W7R12T__IDB050_ITH010_SUB50_threshold300_PWELL06"     , [982]],

#  ["W7R12T__IDB127_ITH127_SUB06_threshold560_PWELL06"     , [961]],
  #["W7R12T__IDB127_ITH127_SUB08_threshold560_PWELL06"     , [995]],
#  ["W7R12T__IDB127_ITH127_SUB10_threshold550_PWELL06"     , [962]],
#  ["W7R12T__IDB127_ITH127_SUB15_threshold545_PWELL06"     , [963]],
#  ["W7R12T__IDB127_ITH127_SUB20_threshold542_PWELL06"     , [964]],
#  ["W7R12T__IDB127_ITH127_SUB25_threshold541_PWELL06"     , [965]],
#  ["W7R12T__IDB127_ITH127_SUB30_threshold540_PWELL06"     , [966]],
 # ["W7R12T__IDB127_ITH127_SUB30_threshold560_PWELL06"     , [983]],
 # ["W7R12T__IDB127_ITH127_SUB35_threshold560_PWELL06"     , [984]],
 # ["W7R12T__IDB127_ITH127_SUB40_threshold560_PWELL06"     , [985]],
 # ["W7R12T__IDB127_ITH127_SUB45_threshold560_PWELL06"     , [986]],
 # ["W7R12T__IDB127_ITH127_SUB50_threshold560_PWELL06"     , [987]],

  #["W7R12T__IDB127_ITH010_SUB06_threshold460_PWELL06"     , [967]],
  #["W7R12T__IDB127_ITH010_SUB08_threshold460_PWELL06"     , [996]],
#  ["W7R12T__IDB127_ITH010_SUB10_threshold450_PWELL06"     , [968]],
#  ["W7R12T__IDB127_ITH010_SUB15_threshold445_PWELL06"     , [969]],
#  ["W7R12T__IDB127_ITH010_SUB20_threshold442_PWELL06"     , [970]],
#  ["W7R12T__IDB127_ITH010_SUB25_threshold441_PWELL06"     , [971]],
#  ["W7R12T__IDB127_ITH010_SUB30_threshold440_PWELL06"     , [972]],
  #["W7R12T__IDB127_ITH010_SUB30_threshold460_PWELL06"     , [988]],
  #["W7R12T__IDB127_ITH010_SUB35_threshold460_PWELL06"     , [989]],
  #["W7R12T__IDB127_ITH010_SUB40_threshold460_PWELL06"     , [990]],
  #["W7R12T__IDB127_ITH010_SUB45_threshold460_PWELL06"     , [991]],
  #["W7R12T__IDB127_ITH010_SUB50_threshold460_PWELL06"     , [992]],

  #["W7R12T__IDB127_ITH010_SUB06_scintillator_PWELL06"     , [1032]],
  #["W7R12T__IDB127_ITH010_SUB06_NOscintillator_PWELL06"     , [1033]],
#  ["W7R12T__IDB127_ITH010_SUB06_05PlaneWidth_PWELL06"     , [1040]],
#  ["W7R12T__IDB127_ITH010_SUB06_10PlaneWidth_PWELL06"     , [1041]],
#  ["W7R12T__IDB127_ITH010_SUB06_15PlaneWidth_PWELL06"     , [1042]],
#  ["W7R12T__IDB127_ITH010_SUB06_20PlaneWidth_PWELL06"     , [1043]],
#  ["W7R12T__IDB127_ITH010_SUB06_25PlaneWidth_PWELL06"     , [1044]],
#  ["W7R12T__IDB127_ITH010_SUB06_30PlaneWidth_PWELL06"     , [1045]],
#  ["W7R12T__IDB127_ITH010_SUB06_35PlaneWidth_PWELL06"     , [1046]],
#  ["W7R12T__IDB127_ITH010_SUB06_40PlaneWidth_PWELL06"     , [1047]],
 # ["W7R12T__IDB127_ITH010_SUB06_07PlaneWidth_PWELL06"     , [1048]],
#  ["W7R12T__IDB127_ITH010_SUB06_03PlaneWidth_PWELL06"     , [1049]],

  #["W7R12T__IDB127_ITH010_SUB06_noscintillatorPico_PWELL06"     , [1059]],
  #["W7R12T__IDB127_ITH010_SUB06_ScintillatorPico_PWELL06"     , [1060]],
#  ["W7R12T__IDB127_ITH010_SUB06_30PlaneWidth_ScintillatorPico_PWELL06"     , [1060]],
#  ["W7R12T__IDB127_ITH010_SUB06_10PlaneWidth_ScintillatorPico_PWELL06"     , [1060]],
#  ["W7R12T__IDB127_ITH010_SUB06_60PlaneWidth_ScintillatorPico_PWELL06"     , [1060]],


  #["W7R12T__ALL"  , [949,950,951,952,953,954,955,956,957,958,959,960,961,962,963,964,965,966,967,968,969,970,971,972]],
    
  #["W7R12T__ALL"  , [949,993,950,951,952,953,954,973,974,975,976,977,955,994,956,957,958,959,960,978,979,980,981,982,961,995,962,963,964,965,966,983,984,985,986,987,967,996,968,969,970,971,972,988,989,990,991,992]],



#["W7R12T_newsetup_IDB50_ITH10_SUB06_PWELL06"                  ,[1207]],
#["W7R12T_newsetup_IDB50_ITH10_SUB08_PWELL06"                  ,[1209]],
#["W7R12T_newsetup_IDB50_ITH10_SUB10_PWELL06"                  ,[1214]],
#["W7R12T_newsetup_IDB50_ITH10_SUB15_PWELL06"                  ,[1219]],
#["W7R12T_newsetup_IDB50_ITH10_SUB25_PWELL06"                  ,[1221]],
#["W7R12T_newsetup_IDB50_ITH10_SUB35_PWELL06"                  ,[1222]],
#["W7R12T_newsetup_IDB50_ITH10_SUB50_PWELL06"                  ,[1223]],

#["W7R12T_newsetup_IDB50_ITH10_SUB06_PWELL06"                  ,[1224]],
#["W7R12T_newsetup_IDB50_ITH10_SUB06_PWELL06"                  ,[1225]],
#["W7R12T_newsetup_IDB50_ITH10_SUB06_PWELL06"                  ,[1226]],


#["W7R12T_newsetup_IDB127_ITH127_SUB06_PWELL06"                  ,[1230]],
#["W7R12T_newsetup_IDB127_ITH127_SUB08_PWELL06"                  ,[1231]],
#["W7R12T_newsetup_IDB127_ITH127_SUB10_PWELL06"                  ,[1232]],
#["W7R12T_newsetup_IDB127_ITH127_SUB15_PWELL06"                  ,[1233]],
#["W7R12T_newsetup_IDB127_ITH127_SUB25_PWELL06"                  ,[1234]],
#["W7R12T_newsetup_IDB127_ITH127_SUB35_PWELL06"                  ,[1235]],
#["W7R12T_newsetup_IDB127_ITH127_SUB50_PWELL06"                  ,[1236]],

#["W7R12T_newsetup_IDB50_ITH10_SUB06_PWELL06"                    ,[1238]],
#["W7R12T_newsetup_IDB127_ITH127_SUB06_PWELL06"                  ,[1239]],
#["W7R12T_newsetup_IDB127_ITH127_SUB08_PWELL06"                  ,[1240]],
#["W7R12T_newsetup_IDB127_ITH127_SUB10_PWELL06"                  ,[1241]],
#["W7R12T_newsetup_IDB127_ITH127_SUB15_PWELL06"                  ,[1242]],
#["W7R12T_newsetup_IDB127_ITH127_SUB25_PWELL06"                  ,[1243]],
#["W7R12T_newsetup_IDB127_ITH127_SUB35_PWELL06"                  ,[1244]],
#["W7R12T_newsetup_IDB127_ITH127_SUB50_PWELL06"                  ,[1245]],


#["W7R12T_newsetup_IDB50_ITH10_SUB10_PWELL06_IBIAS70"            ,[1246]],
#["W7R12T_newsetup_IDB50_ITH10_SUB10_PWELL06_IBIAS90"            ,[1247]],
#["W7R12T_newsetup_IDB50_ITH10_SUB10_PWELL06_IBIAS100"           ,[1248]],
#["W7R12T_newsetup_IDB50_ITH10_SUB10_PWELL06_IBIAS60"            ,[[1249],

#["W7R12T_newsetup_IDB50_ITH10_SUB10_PWELL06_ICASN13"             ,[1251]],
#["W7R12T_newsetup_IDB50_ITH10_SUB10_PWELL06_ICASN7"              ,[1252]],
#["W7R12T_newsetup_IDB50_ITH10_SUB10_PWELL06_VRESET55"            ,[1253]],
#["W7R12T_newsetup_IDB50_ITH10_SUB10_PWELL06_VRESET60"            ,[1254]],
#["W7R12T_newsetup_IDB50_ITH10_SUB10_PWELL06_VRESETD70"           ,[1255]],


#
#["W7R1T_IDB127_ITH127_SUB06_PWELL06"                  ,[1257]],
#["W7R1T_IDB127_ITH127_SUB10_PWELL06"                  ,[1258]],
#["W7R1T_IDB127_ITH127_SUB15_PWELL06"                  ,[1259]],
#["W7R1T_IDB127_ITH127_SUB20_PWELL06"                  ,[1261]],
#["W7R1T_IDB127_ITH127_SUB25_PWELL06"                  ,[1262]],
#["W7R1T_IDB127_ITH127_SUB30_PWELL06"                  ,[1263]],
#["W7R1T_IDB127_ITH127_SUB35_PWELL06"                  ,[1264]],
#["W7R1T_IDB127_ITH127_SUB40_PWELL06"                  ,[1265]],
#["W7R1T_IDB127_ITH127_SUB45_PWELL06"                  ,[1266]],
#["W7R1T_IDB127_ITH127_SUB50_PWELL06"                  ,[1267]],
#
#["W7R1T_IDB50_ITH127_SUB50_PWELL06"                  ,[1271]],
#
#["W7R1T_IDB100_ITH50_SUB50_PWELL06"                  ,[1273]],
#
#["W7R1T_IDB100_ITH03_SUB50_PWELL06"                  ,[1274]],
#  
##["W7R1T_IDB127_ITH03_SUB50_PWELL06"                  ,[1272]],
#["W7R1T_IDB127_ITH03_SUB50_PWELL06"                  ,[1275]],
#["W7R1T_IDB127_ITH03_SUB40_PWELL06"                  ,[1276]],
#["W7R1T_IDB127_ITH03_SUB30_PWELL06"                  ,[1277]],
#["W7R1T_IDB127_ITH03_SUB20_PWELL06"                  ,[1278]],
#["W7R1T_IDB127_ITH03_SUB10_PWELL06"                  ,[1279]],  
#["W7R1T_IDB127_ITH03_SUB06_PWELL06"                  ,[1280]],
#
#["W7R1T_IDB127_ITH10_SUB50_PWELL06"                  ,[1268]],  
#["W7R1T_IDB127_ITH10_SUB40_PWELL06"                  ,[1270]],  
#["W7R1T_IDB127_ITH10_SUB30_PWELL06"                  ,[1281]],
#["W7R1T_IDB127_ITH10_SUB20_PWELL06"                  ,[1282]],
#["W7R1T_IDB127_ITH10_SUB10_PWELL06"                  ,[1283]],
#["W7R1T_IDB127_ITH10_SUB06_PWELL06"                  ,[1284]],
#


#["W7R0T_IDB127_ITH127_SUB06_PWELL06"                 ,[1300]],
#["W7R0T_IDB127_ITH127_SUB10_PWELL06"                 ,[1299]],
#["W7R0T_IDB127_ITH127_SUB15_PWELL06"                 ,[1298]],
#["W7R0T_IDB127_ITH127_SUB20_PWELL06"                 ,[1297]],
#["W7R0T_IDB127_ITH127_SUB25_PWELL06"                 ,[1296]],
#["W7R0T_IDB127_ITH127_SUB30_PWELL06"                 ,[1295]],
#["W7R0T_IDB127_ITH127_SUB35_PWELL06"                 ,[1294]],
#["W7R0T_IDB127_ITH127_SUB40_PWELL06"                 ,[1292]],
#["W7R0T_IDB127_ITH127_SUB45_PWELL06"                 ,[1293]],
#["W7R0T_IDB127_ITH127_SUB50_PWELL06"                 ,[1290]],
#
#
#["W7R0T_IDB127_ITH050_SUB06_PWELL06"                  ,[1308]],
#["W7R0T_IDB127_ITH050_SUB10_PWELL06"                  ,[1307]],
#["W7R0T_IDB127_ITH050_SUB20_PWELL06"                  ,[1306]],
#["W7R0T_IDB127_ITH050_SUB30_PWELL06"                  ,[1305]],
#["W7R0T_IDB127_ITH050_SUB40_PWELL06"                  ,[1303]],
#["W7R0T_IDB127_ITH050_SUB50_PWELL06"                  ,[1302]],
#
#["W7R0T_IDB127_ITH100_SUB50_PWELL06"                 ,[1309]],
#["W7R0T_IDB100_ITH050_SUB50_PWELL06"                 ,[1311]],
#["W7R0T_IDB127_ITH100_SUB20_PWELL06"                 ,[1305]],
#["W7R0T_IDB127_ITH100_SUB30_PWELL06"                 ,[1303]],
#["W7R0T_IDB127_ITH100_SUB40_PWELL06"                 ,[1302]],
#["W7R0T_IDB127_ITH100_SUB50_PWELL06"                 ,[1301]],
#

#["W7R5T_IDB127_ITH050_SUB06_PWELL06"                  ,[1320]],
#["W7R5T_IDB127_ITH050_SUB10_PWELL06"                  ,[1321]],
#["W7R5T_IDB127_ITH050_SUB20_PWELL06"                  ,[1322]],
#["W7R5T_IDB127_ITH050_SUB30_PWELL06"                  ,[1323]],
#["W7R5T_IDB127_ITH050_SUB40_PWELL06"                  ,[1324]],
#["W7R5T_IDB127_ITH050_SUB50_PWELL06"                  ,[1325]],
#



#["W7R5T_IDB127_ITH050_SUB08_PWELL06"                  ,[1333]],
#["W7R5T_IDB127_ITH050_SUB13_PWELL08"                  ,[1334]],
#["W7R5T_IDB127_ITH050_SUB16_PWELL06"                  ,[1335]],
#["W7R5T_IDB127_ITH050_SUB22_PWELL06"                  ,[1336]],
#["W7R5T_IDB127_ITH050_SUB25_PWELL06"                  ,[1337]],
#["W7R5T_IDB127_ITH050_SUB28_PWELL06"                  ,[1338]],


###########################################################################################################
#    ["W9R5T_IDB127_ITH050_SUB55_PWELL06"                  ,[1339]],
#    ["W9R5T_IDB127_ITH050_SUB50_PWELL06"                  ,[1340]],
#    ["W9R5T_IDB127_ITH050_SUB45_PWELL06"                  ,[1341]],

#    ["W9R5T_IDB127_ITH127_SUB06_PWELL06"                  ,[1349]],
#    ["W9R5T_IDB127_ITH127_SUB10_PWELL08"                  ,[1348]],
#    ["W9R5T_IDB127_ITH127_SUB15_PWELL06"                  ,[1347]],
#    ["W9R5T_IDB127_ITH127_SUB25_PWELL06"                  ,[1346]],
#    ["W9R5T_IDB127_ITH127_SUB35_PWELL06"                  ,[1345]],
#    ["W9R5T_IDB127_ITH127_SUB45_PWELL06"                  ,[1344]],
#    ["W9R5T_IDB127_ITH127_SUB55_PWELL06"                  ,[1343]],

#    ["W9R4T_IDB127_ITH127_SUB45_PWELL06"                  ,[1355]],
#    ["W9R4T_IDB127_ITH127_SUB55_PWELL06"                  ,[1353]],


    ["W9R4T_IDB127_ITH127_SUB06_PWELL06"                  ,[1360]],
    ["W9R4T_IDB127_ITH127_SUB10_PWELL08"                  ,[1361]],
    ["W9R4T_IDB127_ITH127_SUB15_PWELL06"                  ,[1362]],
    ["W9R4T_IDB127_ITH127_SUB25_PWELL06"                  ,[1363]],
    ["W9R4T_IDB127_ITH127_SUB35_PWELL06"                  ,[1364]],
    ["W9R4T_IDB127_ITH127_SUB45_PWELL06"                  ,[1365]],
    ["W9R4T_IDB127_ITH127_SUB55_PWELL06"                  ,[1366]],



    ["W9R4T_IDB127_ITH050_SUB06_PWELL06"                  ,[1369]],
    ["W9R4T_IDB127_ITH050_SUB10_PWELL08"                  ,[1370]],
    ["W9R4T_IDB127_ITH050_SUB15_PWELL06"                  ,[1371]],
    ["W9R4T_IDB127_ITH050_SUB25_PWELL06"                  ,[1372]],
    ["W9R4T_IDB127_ITH050_SUB35_PWELL06"                  ,[1373]],
    ["W9R4T_IDB127_ITH050_SUB45_PWELL06"                  ,[1374]],
    ["W9R4T_IDB127_ITH050_SUB55_PWELL06"                  ,[1375]],

##############################################################################################################


#    ["W11R5T_IDB127_ITH050_SUB06_PWELL06"                  ,[1384]],
#    ["W11R5T_IDB127_ITH050_SUB10_PWELL08"                  ,[1383]],
#    ["W11R5T_IDB127_ITH050_SUB15_PWELL06"                  ,[1382]],
#    ["W11R5T_IDB127_ITH050_SUB25_PWELL06"                  ,[1381]],
#    ["W11R5T_IDB127_ITH050_SUB35_PWELL06"                  ,[1380]],
#    ["W11R5T_IDB127_ITH050_SUB45_PWELL06"                  ,[1379]],
#    ["W11R5T_IDB127_ITH050_SUB55_PWELL06"                  ,[1377]],
#
#
#    ["W11R5T_IDB127_ITH127_SUB06_PWELL06"                  ,[1396]],
#    ["W11R5T_IDB127_ITH127_SUB10_PWELL08"                  ,[1395]],
#    ["W11R5T_IDB127_ITH127_SUB15_PWELL06"                  ,[1394]],
#    ["W11R5T_IDB127_ITH127_SUB25_PWELL06"                  ,[1393]],
#    ["W11R5T_IDB127_ITH127_SUB35_PWELL06"                  ,[1391]],
#    ["W11R5T_IDB127_ITH127_SUB45_PWELL06"                  ,[1390]],
#    ["W11R5T_IDB127_ITH127_SUB55_PWELL06"                  ,[1389]],
#
]


#########################################################################################################################
runList=[]
folderList=glob.glob(baseFolder+"run_00*")
print(folderList)
for r in folderList:
    print (r)
    if "gbl" in r: continue
    run=(r.split("run_00")[1])
    run=int(run.split("_")[0])
    runList.append(run)
runList.sort()

allRuns      =0
uncheckedRuns=0
for r in runList:
    allRuns+=1
    cat=""
    for c in runProp:
        for l in c[1]:
            if l==r:
                cat=c[0]
                break
        if cat!="":
            break
    print (str(r)+"      "+cat     )
    if cat=="": uncheckedRuns+=1
print ("All runs : "+str(allRuns))
print ("Unchecked: "+str(uncheckedRuns) )

print (" ")
##########################################################################################################################
print (" ")

#in the folder 'processed'
for c in runProp:
    waferName=outFolder+"/"+c[0].split("_")[0]
    os.system("mkdir -p "+waferName)
    print (waferName)
    
    baseName = "run__"+c[0]+".root"
    fileName  =waferName+"/run__"+c[0]+".root"
    folderName=fileName.replace(".root","")
    os.system("mkdir -p "+folderName)
    print (folderName)
    hEff =ROOT.TH1D("hEff" ,"Avearge Efficiency ; ; eff [%]",len(c[1]),0,len(c[1]))
    hNoise =ROOT.TH1D("hNoise" ,"Avearge Noise Rate ; ; Noise [Hz]",len(c[1]),0,len(c[1]))
   #make the histogram
    posX =ROOT.TH1D("posX" ,"posX ; ;#DeltaX [#mum]",len(c[1]),0,len(c[1]))
   ##posX.Sumw2()
    posY =ROOT.TH1D("posY" ,"posY ; ;#DeltaY [#mum]",len(c[1]),0,len(c[1]))
   ##posY.Sumw2()

    sigX =ROOT.TH1D("sigX" ,"sigX ; ;#sigmaX [#mum]",len(c[1]),0,len(c[1]))
   ##sigX.Sumw2()
    sigY =ROOT.TH1D("sigY" ,"sigY ; ;#sigmaY [#mum]",len(c[1]),0,len(c[1]))

    line=ROOT.TLine(0,0,len(c[1]),0)
    line.SetLineColor(1)
    line.SetLineWidth(2)
    line.SetLineStyle(2)

    line1=ROOT.TLine(0,36.4,len(c[1]),36.4)
    line1.SetLineColor(ROOT.kGray)
    line1.SetLineWidth(1)
    line1.SetLineStyle(ROOT.kGray)

    line2=ROOT.TLine(0,-36.4,len(c[1]),-36.4)
    line2.SetLineColor(ROOT.kGray)
    line2.SetLineWidth(1)
    line2.SetLineStyle(ROOT.kGray)

    
    command="hadd -k -f "+fileName+" "
    count=0
    runString = ""
    totalNoise = 0.
    totalN = 1e-9
    meanNoise = 0.
    totalNoiseErr = 0.
    NoisePedestal=0.
    for r in c[1]:
        if r in badRuns: continue
        count += 1
        hEff.GetXaxis().SetBinLabel(count,str(r))
        hNoise.GetXaxis().SetBinLabel(count,str(r))
        posY.GetXaxis().SetBinLabel(count,str(r))
        posY.GetXaxis().SetBinLabel(count,str(r))
        sigX.GetXaxis().SetBinLabel(count,str(r))
        sigY.GetXaxis().SetBinLabel(count,str(r))
        fN=baseFolder+("/run_%06i/ana_gbl_%06i.root"%(r,r))
       	fNoise=ROOT.TFile(baseFolder+"/run_%06i/Merged-%06i.root"%(r,r), "READ")
        treeNoise = fNoise.Get("Plane1/Hits")
        treeNoise.Draw("Timing>>h(16,140,300)")
        hNoise=fNoise.Get("h")
        hNoise.Print() 
        NoisePedestal = (hNoise.GetBinContent(3)/hNoise.Integral())/10 #bin 3-4 for pedestal      

        #fN=baseFolder+"/run_00"+str(r)+"/ana00"+str(r)+".root"
        print (fN)
        if not os.path.isfile(fN):
            print ("########### FILE COULD NOT BE FOUND ########")
            continue
        rF=ROOT.TFile(fN)
        resX=rF.Get("res_X")
        resX.Rebin(4)
        fit=ROOT.TF1("fitG","gaus",-3000,3000)
        resX.Fit(fit,"RE0")
        print (fit.GetParameter(1))
        posY.SetBinContent(count,fit.GetParameter(1))
        posX.SetBinError(count,fit.GetParError(1))
        sigX.SetBinContent(count,fit.GetParameter(2))
        sigX.SetBinError(count,fit.GetParError(2))
        print ("Residuals: ",count,": ",fit.GetParameter(2) )
        ##fit
        resY=rF.Get("res_Y")
        resY.Rebin(4)
        fit2=ROOT.TF1("fitG2","gaus",-3000,3000)
        resY.Fit(fit2,"RE0")
        print (fit2.GetParameter(1))
        posY.SetBinContent(count,fit2.GetParameter(1))
        posY.SetBinError(count,fit2.GetParError(1))
        sigY.SetBinContent(count,fit2.GetParameter(2))
        sigY.SetBinError(count,fit2.GetParError(2))
    
        MAll=rF.Get("MAll")
        MPass=rF.Get("MPass")
        num=MPass.Integral() 
        den=MAll.Integral()
        print ("################## num and den: ##################:")
        print (num,den)
        if den==0 : continue
        eff=num/den *100
        err=math.sqrt(((eff*(1-eff/100.))/100.)/num) 
        print ("Run with EFF: "+str(eff))
        hEff.SetBinContent(count,eff)
        hEff.SetBinError(count,err)

        fram_E = rF.Get("fram_E")
        numTracks = rF.Get("numTracks")
        nTrack = numTracks.GetEntries()
        canFramE = ROOT.TCanvas("CFramE","FramE",1000,600)
        fram_E.Draw();
        canFramE.Print(fileName.replace(".root","")+"/framE_"+str(r)+".pdf")
        ##canFramE.Print(fileName.replace(".root","")+"/framE_"+str(r)+".C")

        canResXY = ROOT.TCanvas("cResXY","ResXY",800,600)
        resX.GetXaxis().SetRangeUser(-5000,5000)
        resX.SetMaximum(resX.GetMaximum()*1.2)
        resX.Draw("")
        resY.SetMarkerColor(2)
        resY.SetLineColor(2)
        resY.Draw("same")
        fit.SetLineColor(1)
        fit.Draw("same")
        fit2.Draw("same")
        bS = ROOT.TPaveText(50,3000, 100, 2000)
        bS.AddText("ResX: %f " %(fit.GetParameter(2)))
        bS.Draw()
        bS2 = ROOT.TPaveText(50,2000, 100, 1000)
        bS2.AddText("ResY: %f" %(fit2.GetParameter(2)))
        bS2.Draw()
        canResXY.Update()
        canResXY.Print(fileName.replace(".root","")+"/resXY_hist_"+str(r)+".pdf")
        ##canResXY.Print(fileName.replace(".root","")+"/resXY_hist_"+str(r)+".C")
        
        rF.Close()
        '''
        noiseF = ROOT.TFile("/home/sbmuser/MaltaSW/MaltaTbAnalysis/DESY_oct2019/run_00%i/run_00%i_1.root.root" %(r, r)) #get raw data for plane 1, the DUT
        timing = noiseF.Get("Timing")
        peakPos = timing.GetBinCenter(timing.GetMaximumBin())
        if peakPos < 100:
          print "Run: ",r, 
          print "Timing peak below 100ns, refine noise rate calculation!" 
          print "(But is hardcoded to 200 at the moment, check the run!)"
        peakPos = 200;
        noiseRateHz = timing.Integral(0,int(peakPos-50.0)) 
        nNoise = timing.Integral(0,int(peakPos-50.0)) 
        noiseRateHzErr = np.sqrt(noiseRateHz)
        noiseRateHz /= (peakPos-50.0) #noise per ns
        noiseRateHz /= (peakPos -50.0)#error on noise
        noiseRateHz /= nTrack #noise per ns per event
        noiseRateHz *= 1E9;  #noise per s per event  

        noiseRateHzErr /= (peakPos-50.0) #noise per ns
        noiseRateHzErr /= (peakPos -50.0)#error on noise
        noiseRateHzErr /= nTrack #noise per ns per event
        noiseRateHzErr *= 1E9;  #noise per s per event    
        
        hNoise.SetBinContent(count, noiseRateHz)
        hNoise.SetBinError(count, noiseRateHzErr)

        totalNoise += noiseRateHz * nNoise
        totalN += nNoise
        totalNoiseErr += noiseRateHzErr * noiseRateHzErr * nNoise * nNoise 
        noiseF.Close() 
        '''
        theRun=""
        theRun=baseFolder+"run_%06i/ana_gbl_%06i.root"%(r,r)
        #theRun=baseFolder+"run_00"+str(r)+"/ana00"+str(r)+".root "
        command+=theRun

        runString += str(r)+","
    #sys.exit()
    #write a text file with the 
    meanNoise = totalNoise/totalN
    stdNoise = np.sqrt(totalNoiseErr)/totalN
    noiseOut =open("%s/run__%s/Noise.txt" %(waferName,c[0]),"w+")
    noiseOut.write("Noise Rate (Hz), Error (Hz)\n")
    noiseOut.write("%f,%f" %(meanNoise, stdNoise))
    noiseOut.close()
    noisePedestal =open("%s/run__%s/NoisePedestal.txt" %(waferName,c[0]),"w+")
    noisePedestal.write("%f %f" %(NoisePedestal*1000, 0))
    noisePedestal.close()
    print ("#########################################################################################################")
    print ("#########################################################################################################")
    print ("#########################################################################################################")
    print ("#########################################################################################################")
    print ("#########################################################################################################") 
    print (command)
    os.system(command)
    badSt=len(badRuns)

    print ("#########################################################################################################")
    print ("#########################################################################################################")
    print ("#########################################################################################################")
    print ("#########################################################################################################")
    print ("#########################################################################################################")
   # command='root -l -b -q FancyPlot_MALTA.C\(\\"'+fileName.replace(".root","")+'\\",\\"'+baseName.replace(".root","").replace("run__","")+'\\"\)'
    command='root -l -b -q FancyPlot_MALTA.C\(\\"'+fileName.replace(".root","")+'\\",\\"'+baseName.replace(".root","").replace("run__","")+'\\"\)'
    print (command)
    print (fileName)
    print (baseName)
    os.system(command)


    print ("#########################################################################################################")
    print ("#########################################################################################################")
    print ("#########################################################################################################")
    print ("#########################################################################################################")
    print ("#########################################################################################################")
    #command2='root -l -b -q -x /home/sbmuser/MaltaSW/MaltaTbAnalysis/share/makeOverviewPlots.C\(\\"'+baseFolder+'\\",\{'+runString[:-1]+'\}\)'
    command2='root -l -b -q -x makeOverviewPlots.C\(\\"'+baseFolder+'\\",\{'+runString[:-1]+'\}\)'
    print (command2)
    ############os.system(command2)
       
    print ("#########################################################################################################")
    print ("#########################################################################################################")
    print ("#########################################################################################################")
    print ("#########################################################################################################")
    print ("#########################################################################################################")
    print ("#########################################################################################################")
 
    can=ROOT.TCanvas("CRes","Residuals_Average",1500,600)
    ROOT.gStyle.SetOptStat(0)
    posX.SetMaximum( 1500)
    posX.SetMinimum(-1500)
    posX.SetMarkerSize(0.8)
    posX.SetMarkerStyle(20)
    posX.SetMarkerColor(2)
    posX.SetLineWidth(2)
    posX.SetLineColor(2)
    posY.SetMarkerSize(0.8)
    posY.SetMarkerStyle(20)
    posY.SetMarkerColor(4)
    posY.SetLineColor(4)
    posY.SetLineWidth(2)
    posX.Draw("E")
    line.Draw("SAMEL")
    line1.Draw("SAMEL")
    line2.Draw("SAMEL")
    posX.Draw("SAMEE")
    posY.Draw("SAMEE")
    posX.GetXaxis().SetLabelSize(0.07)
    can.Print(fileName.replace(".root","")+"/Residuals_Av.pdf")

###########################################################################################################
###########################################################################################################

    can3=ROOT.TCanvas("CRes2","Residuals_RMS",1500,600)    
    sigX.SetMaximum(3000)
    sigX.SetMinimum(1000)
    sigX.SetMarkerSize(0.8)
    sigX.SetMarkerStyle(20)
    sigX.SetMarkerColor(2)
    sigX.SetLineColor(2)
    sigX.SetLineWidth(3)
    sigY.SetMarkerSize(0.8)
    sigY.SetMarkerStyle(20)
    sigY.SetMarkerColor(4)
    sigY.SetLineColor(4)
    sigY.SetLineWidth(2)
    sigX.Draw("E")
    sigY.Draw("SAMEE")
    line1.Draw("SAMEL")
    sigX.GetXaxis().SetLabelSize(0.07)
    can3.Print(fileName.replace(".root","")+"/Residuals_RMS.pdf")
    ##can3.Print(fileName.replace(".root","")+"/Residuals_RMS.C")
    
    canE=ROOT.TCanvas("CEff","Efficiency",1500,600)
    hEff.SetMaximum(100)
    hEff.SetMinimum( 20) #20
    if "W4R12"  in fileName: hEff.SetMinimum( 90)
    if "W7R12"  in fileName: hEff.SetMinimum( 90)
    if "W7R4"   in fileName: hEff.SetMinimum( 90)
    if "W9R11"  in fileName: hEff.SetMinimum( 90)
    if "W11R11" in fileName: hEff.SetMinimum( 90)
    if "R8" in fileName:
        hEff.SetMaximum(30)
        hEff.SetMinimum(0)
        
    hEff.SetMarkerSize(0.8)
    hEff.SetMarkerStyle(20)
    hEff.SetMarkerColor(2)
    hEff.SetLineColor(2)
    hEff.Draw("E")
    l1 = ROOT.TLine(0,95,len(c[1]) , 95)
    l1.SetLineStyle(8)
    l1.Draw("same")
    hEff.GetXaxis().SetLabelSize(0.07)
    canE.Update()
    canE.Print(fileName.replace(".root","")+"/Stability_Eff.pdf")
    canE.Print(fileName.replace(".root","")+"/Stability_Eff.root")
    command1="mv "+fileName+" "+fileName.replace(".root","")+"/"
    ###print command1
    os.system(command1)

    #CHECK!!!!!! next 3 lines
    #command2="mv /home/sbmuser/MaltaSW/MaltaTbAnalysis/FinalPlot_DESY_Oct19/overViewPlots.root "+fileName.replace(".root","")+"/"
    #print (command2)
    #os.system(command2)

print (" ")


sys.exit()

