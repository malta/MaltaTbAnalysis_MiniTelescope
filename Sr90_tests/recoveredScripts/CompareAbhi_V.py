#!/usr/bin/env python
import sys
import glob
import ROOT
from array import array
import math
import argparse
import os

#ROOT.gROOT.LoadMacro("~/AtlasStyleBase/AtlasStyle.C")
#ROOT.gROOT.LoadMacro("~/AtlasStyleBase/AtlasUtils.C")
#ROOT.SetAtlasStyle()

ROOT.gStyle.SetOptStat(0)
#ROOT.gStyle.SetPalette(1)
ROOT.gROOT.SetBatch(1)
#ROOT.gStyle.SetEndErrorSize(10)
#ROOT.gStyle.SetTitleOffset(1.25,"y")        

parser=argparse.ArgumentParser()
parser.add_argument("-c","--chip",nargs="+",type=str,default="W7R12")
parser.add_argument("-s2","--s2only",action='store_true')
parser.add_argument("-s3","--s3only",action='store_true')
args=parser.parse_args()

os.system("mkdir -p SummaryPlots/")

plots=[
    #["W7R12Cz",[ROOT.kBlack ] ],
    #["W7R12T" ,[ROOT.kGray+1] ],
    #["W7R4Epi" ,[2]],
    ["W11R5T" ,[ROOT.kOrange+1]], ##ROOT.kAzure+10]],
    #["W11R0T"  ,[6]],
    #["W11R11Cz",[3,8,ROOT.kSpring+9,ROOT.kSpring-1,ROOT.kSpring+4]],
    #["W9R11Cz", [4]],
    #["W9R11T" , [ROOT.kAzure+10]],
    #["W11R11T" , [ROOT.kGreen+2]]
]



base=ROOT.TH1D("vale",";Substrate Voltage [V];Efficiency [%]",10,5,31)
base.SetMaximum(100)
base.SetMinimum( 90)##was 92

can=ROOT.TCanvas("cEffVsSub","EffVsSub",800,600)
can.SetGridy(1)
base.Draw("AXIS")
graphs=[]
for w in plots:
    print (" ")
    print (w[0])

    path =""
    #files=glob.glob(w[0]+"/*PWELL06*/Eff.txt")
    if "T" in w[0]:
       path="FinalPlot_Sr90/"
    else :
       path="/home/sbmuser/MaltaSW/MaltaTbAnalysis/FinalPlot_DESY_Oct19/"

    files=glob.glob(path+w[0]+"/*70_ITH030*PWELL06*/Eff.txt")
    files.sort()
    configs=[]
    for f in files:
        conf="IDB"+f.split("__IDB")[1].split("_SUB")[0]
        configs.append(conf)
    configs=list(set(configs))
    configs.sort()
    print (configs)
    countC=-1
    for c in configs:
        countC+=1
        gS2=ROOT.TGraphErrors()
        gS3=ROOT.TGraphErrors()
        graphs.append(gS2)
        graphs.append(gS3)
        gS2.SetLineWidth(2)
        gS3.SetLineWidth(2)
        gS2.SetLineColor(w[1][countC])
        gS3.SetLineColor(w[1][countC])
        gS3.SetLineStyle(2)
        gS2.SetMarkerColor(w[1][countC])
        gS3.SetMarkerColor(w[1][countC])
        gS2.SetMarkerSize(1.2)
        gS3.SetMarkerSize(1.2)
        gS2.SetMarkerStyle(20)
        gS3.SetMarkerStyle(21)
        countF=-1
        for f in files:
            if c not in f: continue
            print (f)
            countF+=1
            sub=0;
            if "p2" in f or "p5" in f or "p7" in f:
                tmp=(f.split("SUB")[1].split("_PW")[0])
                tmp=tmp.replace("p",".")
                sub=float(tmp)+0.02*countC
            else:
                sub=float(f.split("SUB")[1].split("_PW")[0])+0.02*countC
            fr=open(f,"r")
            left =fr.readline().split(" ")
            right=fr.readline().split(" ")
            effL=left[1]
            effR=right[1]
            errL=left[3].split("\n")[0]
            errR=right[3].split("\n")[0]
            #print ("SUB= "+str(sub)+" --> EFF_L: "+str(effL)+" +/- "+str(errL))
            gS2.SetPoint(countF,sub,float(effL))
            gS2.SetPointError(countF,0.01,float(errL))
            gS3.SetPoint(countF,sub,float(effR))
            gS3.SetPointError(countF,0.01,float(errR))
            fr.close()
        gS2.Draw("PL")
        gS3.Draw("PL")

can.Update()
can.Print("SummaryPlots/Summary.pdf")








sys.exit()
###################################################################################################################################################################


files = []
for r, d, f in os.walk("/home/sbmuser/MaltaSW/MaltaTbAnalysis/FinalPlot_DESY_Oct19/"):
    for fil in f:
        if "Eff.txt" in fil:# and "overView" not in file:#and "SUB16" not in file:
            files.append(os.path.join(r, fil))
files = sorted(files)

gS2={}
gS3={}
counter=0
#color=2
colors=[#["W4R0",2,"30 #mum EPI + n-gap 1E15 n_{eq}/cm^{2}"]
        ["W4R1",2,"30 #mum EPI + n-gap 1E15 n_{eq}/cm^{2}"],
        ["W4R4",3,"30 #mum EPI + n-gap 2E15 n_{eq}/cm^{2}"],
        ["W7R1Cz",4,"Cz Std 1E15 n_{eq}/cm^{2}"],
        ["W9R0Cz",1,"Cz n-gap 1E15 n_{eq}/cm^{2}"],
        ["W9R1Cz",6,"Cz n-gap 1E15 n_{eq}/cm^{2}"],
        ["W9R4Cz",7,"Cz n-gap 2E15 n_{eq}/cm^{2}"],
        ["W11R0Cz",8,"Cz xdpw 1E15 n_{eq}/cm^{2}"],
        ["W11R11Cz",9,"Cz xdpw  unirradiated"],
        ["W7R4",11,"30 #mum EPI Std unirradiated"],
]
configs={#"W4R0":[""],
         "W4R1":["IDB127_ITHR127"],
         "W4R4":["IDB127_ITHR3"],#"IDB127_ITHR127"],
         "W7R1Cz":["IDB127_ITHR127"],
         "W9R0Cz":["IDB127_ITHR127"],
         "W9R1Cz":["IDB127_ITH127"],#,"IDB100_ITH30"],
         "W9R4Cz":["IDB127_ITHR127"],#"IDB80_ITHR30"],
         "W11R0Cz":["IDB127_ITHR127"],#,"IDB100_ITHR30"],
         "W11R11Cz":["IDB127_ITH100"],#"IDB70_ITH30","IDB100_ITH30","IDB127_ITH30"],
         "W11R5":["IDB127_ITHR127"],
}
name=""

#for chip in args.chip:
for plot in xrange(len(plots)):
    cEffvsSub=ROOT.TCanvas("cEffVsSub","EffVsSub",600,600)
    ROOT.gPad.SetTicks()
    mg=ROOT.TMultiGraph()

    if len(plots[plot])==2:
        lg=ROOT.TLegend(0.446488-0.147993,0.191638,0.765886-0.147993,0.310105)
        lg2=ROOT.TLegend(0.436455-0.147993,0.191638,0.536789-0.147993,0.310105)
        tt=ROOT.TLatex(18.645,19.9535,"Sector 2")
        tt2=ROOT.TLatex(15.9,19.9535,"Sector 3")
    else:
        lg=ROOT.TLegend(0.446488,0.191638,0.765886,0.512195)
        lg2=ROOT.TLegend(0.436455,0.191638,0.536789,0.512195)
        tt=ROOT.TLatex(28.9,47,"Sector 2")
        tt2=ROOT.TLatex(26.2,47,"Sector 3")

    tt.SetTextAngle(90)
    tt.SetTextFont(42)
    tt.SetTextSize(0.04)
    tt2.SetTextAngle(90)
    tt2.SetTextFont(42)
    tt2.SetTextSize(0.04)

    print (len(plots[plot]))
    lg.SetTextFont(42)
    lg.SetTextSize(0.038)
    lg.SetFillColor(0)
    lg.SetLineColor(0)
    lg.SetFillStyle(0)
    lg.SetBorderSize(0)
    lg2.SetTextFont(42)
    lg2.SetTextSize(0.038)
    lg2.SetFillColor(0)
    lg2.SetLineColor(0)
    lg2.SetFillStyle(0)
    lg2.SetBorderSize(0)
    for chip in plots[plot]:
        gS2[chip]=ROOT.TGraph()
        gS3[chip]=ROOT.TGraph()
        s=0
        while s < 65:
            if "_" in chip:chipN=chip.replace("_","")
            else:chipN=chip
            for file in files:
                okS2=True
                okS3=True
                if chip not in file:continue
                skip=True
                #if "W4R0_" and "SUB%i" in file: skip=False
                if "SUB%i_"%s not in file and skip==True:continue
                if "W11R0Cz" in chip and s>53:continue
                if "W11R11Cz" in chip and s>12 or "PWELL2" in file:continue
                #if "W4R4" in chip:
                #    if "ITHR3_" not in file: continue
                if chipN in configs:
                    if configs[chipN] != "" and chip!="W7R4":
                        idb=file.split("__")[2].split("_")[0]
                        ithr=file.split("__")[2].split("_")[1]
                        idb_ithr=idb+"_"+ithr
                        if idb_ithr not in configs[chipN]:continue
                        pass
                    pass
                fr=open(file,"r")
                effL=fr.readline().split(" ")[1]
                effR=fr.readline().split(" ")[1]
                baseName=fr.readline().strip()
                if "nan" in effL:okS2=False
                if "nan" in effR:okS3=False
                effL=float("%.4f"%float(effL))
                effR=float("%.4f"%float(effR))
                sub=float("%.3f"%float(baseName.split("SUB")[1].split("_")[0]))
                if "W7R1Cz" in chip and s==50 and effL<96.55:continue
                if "W7R1Cz" in chip and s==50 and effR<96.55:continue
                if okS2==True and effL >1:
                    gS2[chip].SetPoint(gS2[chip].GetN(),sub,effL)
                    pass
                if okS3==True and effR >1 and args.s2only==False:
                    gS3[chip].SetPoint(gS3[chip].GetN(),sub,effR)
                    pass
                #print effL,effR,baseName
                fr.close()
                pass
            s+=1
            for color in colors:
                if chipN in color[0]:
                    gS2[chip].SetLineColor(color[1])
                    gS2[chip].SetMarkerStyle(21)
                    gS2[chip].SetMarkerColor(color[1])
                    gS2[chip].Draw("APL")
                    gS2[chip].GetYaxis().SetRangeUser(0,100)

                    gS3[chip].SetLineColor(color[1])
                    gS3[chip].SetMarkerStyle(22)
                    gS3[chip].SetMarkerColor(color[1])
                    gS3[chip].Draw("APL")
                    gS3[chip].GetYaxis().SetRangeUser(0,100)
                    pass
                pass
            counter+=1
            pass

        for color in colors:
            if args.s2only and chipN in color:
                lg.AddEntry(gS2[chip],"%s"%color[2],"p")
                mg.Add(gS2[chip])
                pass
            elif args.s3only and chipN in color:
                lg.AddEntry(gS3[chip],"%s"%color[2],"p")
                mg.Add(gS3[chip])
                pass
            elif not args.s2only and not args.s3only and chipN in color:
                lg.AddEntry(gS2[chip],"%s"%color[2],"p")
                lg2.AddEntry(gS3[chip],"","p")
                mg.Add(gS2[chip])
                mg.Add(gS3[chip])
                pass
            pass
        name+=chipN+"_"
        pass
        
    mg.SetTitle(";Substrate Voltage [V];Efficiency [%]")
    mg.Draw("APL")
    lg.Draw()
    lg2.Draw()
    if not args.s2only and not args.s3only:
        tt.Draw()
        tt2.Draw()
        pass
    mg.GetYaxis().SetRangeUser(0,100)
    cEffvsSub.Modified()
    cEffvsSub.Update()
    if args.s2only:name=name[:-1]+"_s2"
    if args.s3only:name=name[:-1]+"_s3"
    cEffvsSub.SaveAs("../SummaryPlots/%s.pdf"%name)
    wm=ROOT.TLatex(9.51306,4.39024,"TJ MALTA INTERNAL")
    wm.SetTextColorAlpha(17,0.476)
    wm.SetTextAngle(45.8384)
    wm.SetTextSize(0.097561)
    wm.Draw()
    cEffvsSub.Update()
    cEffvsSub.SaveAs("../SummaryPlots/%s_Watermarked.png"%name)

    raw_input("")
    pass
