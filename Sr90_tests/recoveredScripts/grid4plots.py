import os
import argparse
import re
import numpy as np
import time
from ROOT import TFile, TH1D, TGraphErrors, TCanvas, gROOT, gPad, TF1, TGaxis, Double

#parser = argparse.ArgumentParser()
#parser.add_argument("-f","--folder"     , type=str           , default='W7R12_testanalysis', help="specify folder e.g. './ThScansMalta/W7R12_testanalysis/blahblah'")
#parser.add_argument("-x","--xvariable"     , type=str           , default='IDB', help="specify variable")
#parser.add_argument("-y","--yvariable"     , type=str           , default='ITHR', help="specify variable")
#parser.add_argument("-no","--novariable"     , type=str           , default='IRESET', help="specify variable")
#args = parser.parse_args()

gROOT.SetBatch(True)
aa="0"
colors = { 10: 1, 30: 2, 50: 4, 70: 3, 90: 6, 100:3, 127:6  }
def takeSecond(elem):
    return elem[1]

def makeTGraph(list_scan, x, y, z, v_z, x_title, y_title):
  g=TGraphErrors()
  list_selected=[]
  for i,k in list_scan.items(): list_selected.append(k)
  list_selected = list(filter(lambda x: x[z] == v_z, list_selected))

  if x ==0 :list_selected.sort()
  else : list_selected.sort(key=takeSecond)
  print (list_selected)

  point=0
  for i in list_selected:
   print(i,  i[x], i[y])
   g.SetPoint(point, i[x], i[y])
   point+=1
  g.SetLineWidth(2)
  print(v_z)
  print(colors[v_z])
  g.SetMarkerStyle(8)
  g.SetLineColor(colors[v_z])
  g.SetMarkerColor(colors[v_z])
  g.GetXaxis().SetTitle(x_title)
  g.GetYaxis().SetTitle(y_title)
  return g 

def scaleTG(g, scale):
   
  for i in range(0,g.GetN()):
    x=Double()
    y=Double()
    g.GetPoint(i,x,y)
    g.SetPoint(i,x,y/scale)

  return g


list_scan = { 
	884: [50, 10],
	885: [50, 30],
	886: [50, 50],
	887: [50, 100],
	888: [50, 127],
	889: [70, 10],
	890: [70, 30],
	891: [70, 50],
	892: [70, 100],
	893: [70, 127],
	894: [90, 10],
	895: [90, 30],
	896: [90, 50],
	897: [90, 100],
	898: [90, 127],
	899: [100, 10],
	900: [100, 30],
	901: [100, 50],
	902: [100, 100],
	903: [100, 127],
	904: [127, 10],
	905: [127, 30],
	906: [127, 50],
	907: [127, 100],
	908: [127, 127],
	#909: [6, 10],
	#910: [5, 10],
	#911: [4, 10],
	#912: [3, 10],
	#913: [2, 10],
	#914: [1, 10],
 }


print(list_scan.items())
print()
#for y in np.unique(np_y):
gcl = []

gcl.append( TGraphErrors())
gcl.append( TGraphErrors())
gcl.append( TGraphErrors())
gcl.append( TGraphErrors())
gcl.append( TGraphErrors())



for f,v in list_scan.items():
  print f
  print v


  #### get cluster size
  file_=TFile("run_000"+str(f)+"/ana_gbl_000"+str(f)+".root", "read")
  clsize = file_.Get("ClSize_match")
  list_scan[f].append(clsize.GetMean())


  ####  get efficiency
  MAll=file_.Get("MAll")
  MPass=file_.Get("MPass")

  num=MPass.Integral()
  den=MAll.Integral()
  if den==0 : eff =0
  else: eff=num/den *100
  #      err=math.sqrt(((eff*(1-eff/100.))/100.)/num)
  #      print "Run with EFF: "+str(eff)
  #      hEff.SetBinContent(count,eff)
  #      hEff.SetBinError(count,err)
  list_scan[f].append(eff)



  #### get noise from pedestal

  fNoise=TFile("run_000"+str(f)+"/Merged-000"+str(f)+".root", "READ")
  treeNoise = fNoise.Get("Plane1/Hits")
  treeNoise.Draw("Timing>>h(16,140,300)")
  hNoise=fNoise.Get("h")
  #NoisePedestal = 1000*(hNoise.GetBinContent(3)/hNoise.Integral())/10 #bin 3 for pedestal   1 bin = 10ns   
  NoisePedestal = 1000*(hNoise.GetBinContent(3)/treeNoise.GetEntries())/10 #bin 3 for pedestal   1 bin = 10ns   

  list_scan[f].append(NoisePedestal)


  #### get max in-time eff

  intime = file_.Get("time_P")
  v_intime=[]
  for i in range(1,152):
    v_intime.append( intime.Integral(i,i+8))
  if den ==0 : intimeeff =0
  else :intimeeff = max(v_intime)/ den
  list_scan[f].append(intimeeff)

  #### get relative max in-time eff

  if num ==0 : intimeeff_rel =0
  else :intimeeff_rel = max(v_intime)/ num
  list_scan[f].append(intimeeff_rel)
  
  file_.Close()
  fNoise.Close()
  del hNoise
  del clsize

print(list_scan)


g_cl_IDB_ITHR10  =   makeTGraph(list_scan,  0, 2, 1 ,  10, "IDB", "cl size")
g_cl_IDB_ITHR30  =   makeTGraph(list_scan,  0, 2, 1 ,  30, "IDB", "cl size")
g_cl_IDB_ITHR50  =   makeTGraph(list_scan,  0, 2, 1 ,  50, "IDB", "cl size")
g_cl_IDB_ITHR100 =   makeTGraph(list_scan,  0, 2, 1 , 100, "IDB", "cl size")
g_cl_IDB_ITHR127 =   makeTGraph(list_scan,  0, 2, 1 , 127, "IDB", "cl size")


g_eff_IDB_ITHR10  =   makeTGraph(list_scan,  0, 3, 1 ,  10, "IDB", "eff %")
g_eff_IDB_ITHR30  =   makeTGraph(list_scan,  0, 3, 1 ,  30, "IDB", "eff %")
g_eff_IDB_ITHR50  =   makeTGraph(list_scan,  0, 3, 1 ,  50, "IDB", "eff %")
g_eff_IDB_ITHR100 =   makeTGraph(list_scan,  0, 3, 1 , 100, "IDB", "eff %")
g_eff_IDB_ITHR127 =   makeTGraph(list_scan,  0, 3, 1 , 127, "IDB", "eff %")

g_noise_IDB_ITHR10  =   makeTGraph(list_scan,  0, 4, 1 ,  10, "IDB", "Noise MHz")
g_noise_IDB_ITHR30  =   makeTGraph(list_scan,  0, 4, 1 ,  30, "IDB", "Noise MHz")
g_noise_IDB_ITHR50  =   makeTGraph(list_scan,  0, 4, 1 ,  50, "IDB", "Noise MHz")
g_noise_IDB_ITHR100 =   makeTGraph(list_scan,  0, 4, 1 , 100, "IDB", "Noise MHz")
g_noise_IDB_ITHR127 =   makeTGraph(list_scan,  0, 4, 1 , 127, "IDB", "Noise MHz")


g_intime_IDB_ITHR10  =   makeTGraph(list_scan,  0, 5, 1 ,  10, "IDB", "in-time eff %")
g_intime_IDB_ITHR30  =   makeTGraph(list_scan,  0, 5, 1 ,  30, "IDB", "in-time eff %")
g_intime_IDB_ITHR50  =   makeTGraph(list_scan,  0, 5, 1 ,  50, "IDB", "in-time eff %")
g_intime_IDB_ITHR100 =   makeTGraph(list_scan,  0, 5, 1 , 100, "IDB", "in-time eff %")
g_intime_IDB_ITHR127 =   makeTGraph(list_scan,  0, 5, 1 , 127, "IDB", "in-time eff %")

g_intime_rel_IDB_ITHR10  =   makeTGraph(list_scan,  0, 6, 1 ,  10, "IDB", "in-time rel eff %")
g_intime_rel_IDB_ITHR30  =   makeTGraph(list_scan,  0, 6, 1 ,  30, "IDB", "in-time rel eff %")
g_intime_rel_IDB_ITHR50  =   makeTGraph(list_scan,  0, 6, 1 ,  50, "IDB", "in-time rel eff %")
g_intime_rel_IDB_ITHR100 =   makeTGraph(list_scan,  0, 6, 1 , 100, "IDB", "in-time rel eff %")
g_intime_rel_IDB_ITHR127 =   makeTGraph(list_scan,  0, 6, 1 , 127, "IDB", "in-time rel eff %")




c = TCanvas("c","c",800, 800)

g_cl_IDB_ITHR10.Draw("ALP")
g_cl_IDB_ITHR30.Draw("LP,same")
g_cl_IDB_ITHR50.Draw("LP,same")
g_cl_IDB_ITHR100.Draw("LP,same")
g_cl_IDB_ITHR127.Draw("LP,same")

c.SaveAs("SummaryPlots/andrea/cl.root")


g_eff_IDB_ITHR10.Draw("apl")
g_eff_IDB_ITHR30.Draw("pl,same")
g_eff_IDB_ITHR50.Draw("pl,same")
g_eff_IDB_ITHR100.Draw("pl,same")
g_eff_IDB_ITHR127.Draw("pl,same")

c.SaveAs("SummaryPlots/andrea/eff.root")
#g_eff_IDB_ITHR10.SaveAs("SummaryPlots/andrea/eff"+aa+".root")

y_min=0.01
y_max=10
x_min=45
x_max=130


g_noise_IDB_ITHR10.GetXaxis().SetLimits(x_min,x_max)
g_noise_IDB_ITHR10.GetYaxis().SetRangeUser(y_min,y_max)
g_noise_IDB_ITHR10.Draw("apl")
g_noise_IDB_ITHR30.Draw("pl,same")
g_noise_IDB_ITHR50.Draw("pl,same")
g_noise_IDB_ITHR100.Draw("pl,same")
g_noise_IDB_ITHR127.Draw("pl,same")

g_noise_IDB_ITHR10.Print("v")
c.SaveAs("SummaryPlots/andrea/noise.root")

g_noise_IDB_ITHR10.SaveAs("SummaryPlots/andrea/noise"+aa+".root")
##################3 noise normalized 


c.SetLogy(1)

y_min=0.05
y_max=5
x_min=45
x_max=130

npix=512*512

scale=1.863*1.863  #chip dimension
y_min_p=scale*(25./1000.) *y_min/npix
y_max_p=scale*(25./1000.) *y_max/npix

g_noise_IDB_ITHR10=scaleTG(g_noise_IDB_ITHR10,scale)
g_noise_IDB_ITHR30=scaleTG(g_noise_IDB_ITHR30,scale)
g_noise_IDB_ITHR50=scaleTG(g_noise_IDB_ITHR50,scale)
g_noise_IDB_ITHR100=scaleTG(g_noise_IDB_ITHR100,scale)
g_noise_IDB_ITHR127=scaleTG(g_noise_IDB_ITHR127,scale)


g_noise_IDB_ITHR10.GetXaxis().SetLimits(x_min,x_max)
g_noise_IDB_ITHR10.GetYaxis().SetRangeUser(y_min,y_max)
g_noise_IDB_ITHR10.GetYaxis().SetTitle("Noise/ 1 cm^{2} [MHz/cm^{2}] " )
g_noise_IDB_ITHR10.Draw("apl")
g_noise_IDB_ITHR30.Draw("pl,same")
g_noise_IDB_ITHR50.Draw("pl,same")
g_noise_IDB_ITHR100.Draw("pl,same")
g_noise_IDB_ITHR127.Draw("pl,same")



#//draw an axis on the right side
#axis = TGaxis(gPad.GetUxmax(),gPad.GetUymin(),gPad.GetUxmax(), gPad.GetUymax(),0,1000000,510,"+L")




axis = TGaxis(x_max,y_min,x_max, y_max, y_min_p  ,y_max_p,510,"+L,G")
axis.SetTitle("hits/(pixel * 25ns)")
axis.SetLineColor(1)
axis.SetLabelColor(1)
axis.SetLabelFont(42)
axis.SetTextFont(42)
axis.Draw("same")

c.SaveAs("SummaryPlots/andrea/noiseNormalized.root")



c.SetLogy(0)


g_intime_IDB_ITHR10.Draw("apl")
g_intime_IDB_ITHR30.Draw("pl,same")
g_intime_IDB_ITHR50.Draw("pl,same")
g_intime_IDB_ITHR100.Draw("pl,same")
g_intime_IDB_ITHR127.Draw("pl,same")
c.SaveAs("SummaryPlots/andrea/intime.root")

g_intime_rel_IDB_ITHR10.Draw("apl")
g_intime_rel_IDB_ITHR30.Draw("pl,same")
g_intime_rel_IDB_ITHR50.Draw("pl,same")
g_intime_rel_IDB_ITHR100.Draw("pl,same")
g_intime_rel_IDB_ITHR127.Draw("pl,same")


g_intime_rel_IDB_ITHR10.SaveAs("SummaryPlots/andrea/intime_rel"+aa+".root")
g_intime_IDB_ITHR10.SaveAs("SummaryPlots/andrea/intime_"+aa+".root")



c.SaveAs("SummaryPlots/andrea/intime_rel.root")





