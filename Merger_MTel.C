#include "TROOT.h"
#include "TFile.h"
#include "TDirectory.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH2D.h"

#include <iomanip>
#include <cmath>
#include <string>
#include <vector>
//#include <array>
#include <iostream>
#include <fstream>

using namespace std;

int Merger_MTel(string Final="FULL_000080.root",
		string P0="",
		string P1="",
		string P2="",
		string P3="",
		string P4="",
		string P5="",
		string P6="",
		string P7="") {

  std::cout << " OUTputFile: " <<  Final << std::endl;
  bool has2=false;
  bool has3=false;
  bool has4=false;
  bool has5=false;
  bool has6=false;
  bool has7=false;
  if ( P2!="" ) has2=true;
  if ( P3!="" ) has3=true;
  if ( P4!="" ) has4=true;
  if ( P5!="" ) has5=true;
  if ( P6!="" ) has6=true;
  if ( P7!="" ) has7=true;

  std::cout << std::endl << "Input files: "  << std::endl;
  std::cout << " P0: " << P0 << std::endl;
  std::cout << " P1: " << P1 << std::endl;
  if (has2) std::cout << " P2: " << P2 << std::endl;
  if (has3) std::cout << " P3: " << P3 << std::endl;
  if (has4) std::cout << " P4: " << P4 << std::endl;
  if (has5) std::cout << " P5: " << P5 << std::endl;
  if (has6) std::cout << " P6: " << P6 << std::endl;
  if (has7) std::cout << " P7: " << P7 << std::endl;
  std::cout << std::endl;
  
  //allocate TFile
  TFile* out=new TFile(Final.c_str(),"RECREATE");
  
  TFile* tel0  =new TFile(P0.c_str(),"READ");
  TFile* tel1  =new TFile(P1.c_str(),"READ");
  TFile* tel2,*tel3,*tel4,*tel5,*tel6,*tel7;
  if (has2) tel2=new TFile(P2.c_str(),"READ");
  if (has3) tel3=new TFile(P3.c_str(),"READ");
  if (has4) tel4=new TFile(P4.c_str(),"READ");
  if (has5) tel5=new TFile(P5.c_str(),"READ");
  if (has6) tel6=new TFile(P6.c_str(),"READ");
  if (has7) tel7=new TFile(P7.c_str(),"READ");

  //get all the tress
  TTree* mEvent=(TTree*)tel0->Get("Event");
  TTree* mTree0=(TTree*)tel0->Get("Plane0/Hits");
  TTree* mTree1=(TTree*)tel1->Get("Plane0/Hits");
  TTree* mTree2,*mTree3,*mTree4,*mTree5,*mTree6,*mTree7;
  if (has2) mTree2=(TTree*)tel2->Get("Plane0/Hits");
  if (has3) mTree3=(TTree*)tel3->Get("Plane0/Hits");
  if (has4) mTree4=(TTree*)tel4->Get("Plane0/Hits");
  if (has5) mTree5=(TTree*)tel5->Get("Plane0/Hits");
  if (has6) mTree6=(TTree*)tel6->Get("Plane0/Hits");
  if (has7) mTree7=(TTree*)tel7->Get("Plane0/Hits");
  
  out->cd();
  TDirectory *out0= out->mkdir("Plane0/");
  TDirectory *out1= out->mkdir("Plane1/");
  TDirectory *out2,*out3,*out4,*out5,*out6,*out7;
  if (has2) out2=out->mkdir("Plane2/");
  if (has3) out3=out->mkdir("Plane3/");
  if (has4) out4=out->mkdir("Plane4/");
  if (has5) out5=out->mkdir("Plane5/");
  if (has6) out6=out->mkdir("Plane6/");
  if (has7) out7=out->mkdir("Plane7/");

  ULong64_t allTel   =mEvent->GetEntries();
  ULong64_t allMALTA0=mTree0->GetEntries();
  ULong64_t allMALTA1=mTree1->GetEntries();
  ULong64_t allMALTA2=0,allMALTA3=0,allMALTA4=0,allMALTA5=0,allMALTA6=0,allMALTA7=0;
  if (has2) allMALTA2=mTree2->GetEntries();
  if (has3) allMALTA3=mTree3->GetEntries();
  if (has4) allMALTA4=mTree4->GetEntries();
  if (has5) allMALTA5=mTree5->GetEntries();
  if (has6) allMALTA6=mTree6->GetEntries();
  if (has7) allMALTA7=mTree7->GetEntries();

  ULong64_t allMin  = allTel;
  if (allMALTA0<=allMin) allMin = allMALTA0;
  if (allMALTA1<=allMin) allMin = allMALTA1;
  if (has2 and allMALTA2<=allMin) allMin = allMALTA2;
  if (has3 and allMALTA3<=allMin) allMin = allMALTA3;
  if (has4 and allMALTA4<=allMin) allMin = allMALTA4;
  if (has5 and allMALTA5<=allMin) allMin = allMALTA5;
  if (has6 and allMALTA6<=allMin) allMin = allMALTA6;
  if (has7 and allMALTA7<=allMin) allMin = allMALTA7;

  std::cout << "All: " << allMin
	    << " , P0: " << allMALTA0
	    << " , P1: " << allMALTA1
	    << " , P2: " << allMALTA2
	    << " , P3: " << allMALTA3
	    << " , P4: " << allMALTA4
	    << " , P5: " << allMALTA5 
	    << " , P6: " << allMALTA6 
	    << " , P7: " << allMALTA7 << std::endl;

 
  TTree* NEvent=mEvent->CloneTree(allMin);
  out0->cd();
  TTree* N0=mTree0->CloneTree(allMin);
  out1->cd();
  TTree* N1=mTree1->CloneTree(allMin);
  if (has2) {
    out2->cd();
    TTree* N2=mTree2->CloneTree(allMin);
  }
  if (has3) {
    out3->cd();
    TTree* N3=mTree3->CloneTree(allMin);
  }
  if (has4) {
    out4->cd();
    TTree* N4=mTree4->CloneTree(allMin);
  }
  if (has5) {
    out5->cd();
    TTree* N5=mTree5->CloneTree(allMin);
  }
  if (has6) {
    out6->cd();
    TTree* N6=mTree6->CloneTree(allMin);
  }
  if (has7) {
    out7->cd();
    TTree* N7=mTree7->CloneTree(allMin);
  }

  out->Write();
  out->Close();


  tel0->Close();
  tel1->Close();
  if (has2) tel2->Close();
  if (has3) tel3->Close();
  if (has4) tel4->Close();
  if (has5) tel5->Close();
  if (has6) tel6->Close();
  if (has7) tel7->Close();

 
  return 0;
}
