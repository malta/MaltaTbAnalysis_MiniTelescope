#include <vector> 
#include <stdio.h>


void fixPicoSkippedEvents(string picoFile);
void mergePico(const char * /*, const char */);
std :: pair <int, int>   find_best_match(std::vector<vector<double>> distance_PM);
std::vector<std :: vector <double>> doMerging(std::vector<double> malta, std::vector<double> pico,std::vector<double> pico_corrected, std::vector<double> tot);
double picoCorrection( double pico, int pixY);
double maltaCorrection( double malta, int pixY);
