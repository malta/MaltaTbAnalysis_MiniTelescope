#ifndef TBEvent_h
#define TBEvent_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class TBEvent {
public :
   TTree          *fEvent;     //!pointer to the analyzed TTree or TChain
   TTree          *fTrack;     //!pointer to the analyzed TTree or TChain
   TTree          *fIntercept; //!pointer to the analyzed TTree or TChain
   TTree          *fCluster;   //!pointer to the analyzed TTree or TChain
   TTree          *fHit;       //!pointer to the analyzed TTree or TChain

   TTree          *fIntercept_1; //!pointer to the analyzed TTree or TChain
   TTree          *fCluster_1;   //!pointer to the analyzed TTree or TChain
   TTree          *fHit_1;       //!pointer to the analyzed TTree or TChain
   TTree          *fIntercept_2; //!pointer to the analyzed TTree or TChain
   TTree          *fCluster_2;   //!pointer to the analyzed TTree or TChain
   TTree          *fHit_2;       //!pointer to the analyzed TTree or TChain

   Int_t           fCurrent; //!current Tree number in a TChain

   // Event leaves
   ULong64_t       FrameNumber;
   ULong64_t       TimeStamp;
   ULong64_t       TriggerTime;
   Int_t           TriggerInfo;
   Int_t           TriggerOffset;
   Int_t           TriggerPhase;
   Bool_t          Invalid;

   // Track leaves
   Int_t           maxTracks=10;
   Int_t           NTracks;
   Double_t        Chi2[10];      //[NTracks]
   Int_t           Dof[10];       //[NTracks]
   Double_t        X[10];         //[NTracks]
   Double_t        Y[10];         //[NTracks]
   Double_t        SlopeX[10];    //[NTracks]
   Double_t        SlopeY[10];    //[NTracks]
   Double_t        TCov[10][10];  //[NTracks]

   // Intercept leaves
   Int_t           NIntercepts;
   Double_t        U[10];        //[NIntercepts]
   Double_t        V[10];        //[NIntercepts]
   Double_t        SlopeU[10];   //[NIntercepts]
   Double_t        SlopeV[10];   //[NIntercepts]
   Double_t        ICov[10][10]; //[NIntercepts]
   Int_t           ITrack[10];   //[NIntercepts]

   // Clusters leaves
   Int_t           NClusters;
   Double_t        Col[100];       //[NClusters]
   Double_t        Row[100];       //[NClusters]
   Double_t        VarCol[100];    //[NClusters]
   Double_t        VarRow[100];    //[NClusters]
   Double_t        CovColRow[100]; //[NClusters]
   Double_t        CTiming[100];   //[NClusters]
   Double_t        CTiming_pico[100];   //[NClusters]
   Double_t        CTiming_corrected[100];   //[NClusters]
   Double_t        CTiming_tot[100];   //[NClusters]
   Double_t        CValue[100];    //[NClusters]
   Int_t           CTrack[100];    //[NClusters]

   // Hits leaves
   Int_t           NHits;
   Int_t           PixX[100];         //[NHits]
   Int_t           PixY[100];         //[NHits]
   Double_t        HTiming[100];      //[NHits]
   Double_t        HTiming_pico[100];      //[NHits]
   Double_t        HTiming_tot[100];      //[NHits]
   Int_t           HTimeFast[100];      //[NHits]
   Int_t           HBCIDshort[100];       //[NHits]
   Int_t           HBCIDlong[100];       //[NHits]
   Double_t        HValue[100];       //[NHits]
   Int_t           HWord[100];       //[NHits]
   Int_t           HitInCluster[100]; //[NHits]

   
   // This is for Plane1
   Int_t           NIntercepts_1;
   Double_t        U_1[10];        //[NIntercepts]
   Double_t        V_1[10];        //[NIntercepts]
   Double_t        SlopeU_1[10];   //[NIntercepts]
   Double_t        SlopeV_1[10];   //[NIntercepts]
   Double_t        ICov_1[10][10]; //[NIntercepts]
   Int_t           ITrack_1[10];   //[NIntercepts]
   Int_t           NClusters_1;
   Double_t        Col_1[100];       //[NClusters]
   Double_t        Row_1[100];       //[NClusters]
   Double_t        VarCol_1[100];    //[NClusters]
   Double_t        VarRow_1[100];    //[NClusters]
   Double_t        CovColRow_1[100]; //[NClusters]
   Double_t        CTiming_1[100];   //[NClusters]
   Double_t        CTiming_pico_1[100];   //[NClusters]
   Double_t        CTiming_tot_1[100];   //[NClusters]
   Double_t        CValue_1[100];    //[NClusters]
   Int_t           CTrack_1[100];    //[NClusters]
   Int_t           NHits_1;
   Int_t           PixX_1[100];         //[NHits]
   Int_t           PixY_1[100];         //[NHits]
   Double_t        HTiming_1[100];      //[NHits]
   Double_t        HTiming_pico_1[100]; //[NHits]
   Double_t        HTiming_tot_1[100]; //[NHits]
   Int_t           HTimeFast_1[100];    //[NHits]
   Double_t        HValue_1[100];       //[NHits]
   Int_t           HWord_1[100];        //[NHits]
   Int_t           HitInCluster_1[100]; //[NHits]

   // This is for Plane2
   Int_t           NIntercepts_2;
   Double_t        U_2[10];        //[NIntercepts]
   Double_t        V_2[10];        //[NIntercepts]
   Double_t        SlopeU_2[10];   //[NIntercepts]
   Double_t        SlopeV_2[10];   //[NIntercepts]
   Double_t        ICov_2[10][10]; //[NIntercepts]
   Int_t           ITrack_2[10];   //[NIntercepts]
   Int_t           NClusters_2;
   Double_t        Col_2[100];       //[NClusters]
   Double_t        Row_2[100];       //[NClusters]
   Double_t        VarCol_2[100];    //[NClusters]
   Double_t        VarRow_2[100];    //[NClusters]
   Double_t        CovColRow_2[100]; //[NClusters]
   Double_t        CTiming_2[100];   //[NClusters]
   Double_t        CTiming_pico_2[100];   //[NClusters]
   Double_t        CTiming_tot_2[100];   //[NClusters]
   Double_t        CValue_2[100];    //[NClusters]
   Int_t           CTrack_2[100];    //[NClusters]
   Int_t           NHits_2;
   Int_t           PixX_2[100];         //[NHits]
   Int_t           PixY_2[100];         //[NHits]
   Double_t        HTiming_2[100];      //[NHits]
   Double_t        HTiming_pico_2[100]; //[NHits]
   Double_t        HTiming_tot_2[100];  //[NHits]
   Int_t           HTimeFast_2[100];    //[NHits]
   Double_t        HValue_2[100];       //[NHits]
   Int_t           HWord_2[100];        //[NHits]
   Int_t           HitInCluster_2[100]; //[NHits]


   ////////////////////////////////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////////////////////////////////
   // List of branches
   TBranch        *b_FrameNumber;   //!
   TBranch        *b_TimeStamp;   //!
   TBranch        *b_TriggerTime;   //!
   TBranch        *b_TriggerInfo;   //!
   TBranch        *b_TriggerOffset;   //!
   TBranch        *b_TriggerPhase;   //!
   TBranch        *b_Invalid;   //!

   TBranch        *b_NTracks;   //!
   TBranch        *b_Chi2;   //!
   TBranch        *b_Dof;   //!
   TBranch        *b_X;   //!
   TBranch        *b_Y;   //!
   TBranch        *b_SlopeX;   //!
   TBranch        *b_SlopeY;   //!
   TBranch        *b_TCov;   //!

   TBranch        *b_NIntercepts;   //!
   TBranch        *b_U;   //!
   TBranch        *b_V;   //!
   TBranch        *b_SlopeU;   //!
   TBranch        *b_SlopeV;   //!
   TBranch        *b_ICov;   //!
   TBranch        *b_ITrack;   //!

   TBranch        *b_NClusters;   //!
   TBranch        *b_Col;   //!
   TBranch        *b_Row;   //!
   TBranch        *b_VarCol;   //!
   TBranch        *b_VarRow;   //!
   TBranch        *b_CovColRow;   //!
   TBranch        *b_CTiming;   //!
   TBranch        *b_CTiming_pico;   //!
   TBranch        *b_CTiming_tot;   //!
   TBranch        *b_CTiming_corrected;   //!
   TBranch        *b_CValue;   //!
   TBranch        *b_CTrack;   //!

   TBranch        *b_NHits;   //!
   TBranch        *b_PixX;   //!
   TBranch        *b_PixY;   //!
   TBranch        *b_HTiming;   //!
   TBranch        *b_HTiming_pico;   //!
   TBranch        *b_HTiming_tot;   //!
   TBranch        *b_HTimeFast;   //!
   TBranch        *b_HBCIDshort;   //!
   TBranch        *b_HBCIDlong;   //!
   TBranch        *b_HWord;   //! 
   TBranch        *b_HValue;   //!
   TBranch        *b_HitInCluster;   //!


   TBranch        *b_NIntercepts_1;   //!
   TBranch        *b_U_1;   //!
   TBranch        *b_V_1;   //!
   TBranch        *b_SlopeU_1;   //!
   TBranch        *b_SlopeV_1;   //!
   TBranch        *b_ICov_1;   //!
   TBranch        *b_ITrack_1;   //!
   TBranch        *b_NClusters_1;   //!
   TBranch        *b_Col_1;   //!
   TBranch        *b_Row_1;   //!
   TBranch        *b_VarCol_1;   //!
   TBranch        *b_VarRow_1;   //!
   TBranch        *b_CovColRow_1;   //!
   TBranch        *b_CTiming_1;   //!
   TBranch        *b_CTiming_pico_1;   //!
   TBranch        *b_CTiming_tot_1;   //!
   TBranch        *b_CValue_1;   //!
   TBranch        *b_CTrack_1;   //!
   TBranch        *b_NHits_1;   //!
   TBranch        *b_PixX_1;   //!
   TBranch        *b_PixY_1;   //!
   TBranch        *b_HTiming_1;   //!
   TBranch        *b_HTiming_pico_1;   //!
   TBranch        *b_HTiming_tot_1;   //!
   TBranch        *b_HTimeFast_1;   //!
   TBranch        *b_HWord_1;   //! 
   TBranch        *b_HValue_1;   //!
   TBranch        *b_HitInCluster_1;   //!
   
   TBranch        *b_NIntercepts_2;   //!
   TBranch        *b_U_2;   //!
   TBranch        *b_V_2;   //!
   TBranch        *b_SlopeU_2;   //!
   TBranch        *b_SlopeV_2;   //!
   TBranch        *b_ICov_2;   //!
   TBranch        *b_ITrack_2;   //!
   TBranch        *b_NClusters_2;   //!
   TBranch        *b_Col_2;   //!
   TBranch        *b_Row_2;   //!
   TBranch        *b_VarCol_2;   //!
   TBranch        *b_VarRow_2;   //!
   TBranch        *b_CovColRow_2;   //!
   TBranch        *b_CTiming_2;   //!
   TBranch        *b_CTiming_pico_2;   //!
   TBranch        *b_CTiming_tot_2;   //!
   TBranch        *b_CValue_2;   //!
   TBranch        *b_CTrack_2;   //!
   TBranch        *b_NHits_2;   //!
   TBranch        *b_PixX_2;   //!
   TBranch        *b_PixY_2;   //!
   TBranch        *b_HTiming_2;   //!
   TBranch        *b_HTiming_pico_2;   //!
   TBranch        *b_HTiming_tot_2;   //!
   TBranch        *b_HTimeFast_2;   //!
   TBranch        *b_HWord_2;   //! 
   TBranch        *b_HValue_2;   //!
   TBranch        *b_HitInCluster_2;   //!

   TBEvent();
   virtual ~TBEvent();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry, int advance=0);
   virtual Long64_t LoadTree(Long64_t entry, int advance=0);
   virtual void     Init(TTree *tE,TTree *tT,TTree *tI,TTree *tC,TTree *tH,TTree *tI1,TTree *tC1,TTree *tH1,TTree *tI2,TTree *tC2,TTree *tH2);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef TBEvent_cxx
TBEvent::TBEvent() : fEvent(0),fTrack(0),fIntercept(0),fCluster(0),fHit(0),fIntercept_1(0),fCluster_1(0),fHit_1(0),fIntercept_2(0),fCluster_2(0),fHit_2(0) 
{

}

TBEvent::~TBEvent() {
   if (!fEvent) return;
   delete fEvent    ->GetCurrentFile();
   delete fTrack    ->GetCurrentFile();
   delete fIntercept->GetCurrentFile();
   delete fCluster  ->GetCurrentFile();
   delete fHit      ->GetCurrentFile();
   delete fIntercept_1->GetCurrentFile();
   delete fCluster_1  ->GetCurrentFile();
   delete fHit_1      ->GetCurrentFile();
   delete fIntercept_2->GetCurrentFile();
   delete fCluster_2  ->GetCurrentFile();
   delete fHit_2      ->GetCurrentFile();
}

Int_t TBEvent::GetEntry(Long64_t entry, int advance)
{
// Read contents of entry.
   if (!fEvent) return 0;
   int vE=fEvent    ->GetEntry(entry);
   //int vT=
   fTrack    ->GetEntry(entry);
   //int vI=
   fIntercept->GetEntry(entry);
   //int vC=
   fCluster  ->GetEntry(entry+advance);
   //int vH=
   fHit      ->GetEntry(entry+advance);
   //int vI1=
   fIntercept_1->GetEntry(entry);
   //int vC1=
   fCluster_1  ->GetEntry(entry);
   //int vH1=
   fHit_1      ->GetEntry(entry);
   //int vI2=
   fIntercept_2->GetEntry(entry);
   //int vC2=
   fCluster_2  ->GetEntry(entry);
   //int vH2=
   fHit_2      ->GetEntry(entry);
   //if (vT!=vE) std::cout << "ERROR in GetEntry()::: vE= " << vE << " != vT= " << vT << std::endl;
   //if (vI!=vE) std::cout << "ERROR in GetEntry()::: vE= " << vE << " != vI= " << vI << std::endl;
   //if (vC!=vE) std::cout << "ERROR in GetEntry()::: vE= " << vE << " != vC= " << vC << std::endl;
   //if (vH!=vE) std::cout << "ERROR in GetEntry()::: vE= " << vE << " != vH= " << vH << std::endl;
   return vE;
}

Long64_t TBEvent::LoadTree(Long64_t entry, int advance)
{
  // Set the environment to read one entry
  if (!fEvent) return -5;
  
  Long64_t centryE = fEvent    ->LoadTree(entry);
  //Long64_t centryT = 
  fTrack    ->LoadTree(entry);
  //Long64_t centryI = 
  fIntercept->LoadTree(entry);
  //Long64_t centryC = 
  fCluster  ->LoadTree(entry+advance);
  //Long64_t centryH = 
  fHit      ->LoadTree(entry+advance);
  //Long64_t centryI1 = 
  fIntercept_1->LoadTree(entry);
  //Long64_t centryC1 = 
  fCluster_1  ->LoadTree(entry);
  //Long64_t centryH1 = 
  fHit_1      ->LoadTree(entry);
  //Long64_t centryI2 = 
  fIntercept_2->LoadTree(entry);
  //Long64_t centryC2 = 
  fCluster_2  ->LoadTree(entry);
  //Long64_t centryH2 = 
  fHit_2      ->LoadTree(entry);
  /*
  if (centryT!=centryE) std::cout << "ERROR in LoadEntry()::: centryE= " << centryE << " != centryT= " << centryT << std::endl;
  if (centryI!=centryE) std::cout << "ERROR in LoadEntry()::: centryE= " << centryE << " != centryI= " << centryI << std::endl;
  if (centryC!=centryE) std::cout << "ERROR in LoadEntry()::: centryE= " << centryE << " != centryC= " << centryC << std::endl;
  if (centryH!=centryE) std::cout << "ERROR in LoadEntry()::: centryE= " << centryE << " != centryH= " << centryH << std::endl;
  */
  if (centryE < 0) return centryE;
  if (fEvent->GetTreeNumber() != fCurrent) {
    fCurrent = fEvent->GetTreeNumber();
    //Notify();
  }
  return centryE;
}

void TBEvent::Init(TTree *tE,TTree *tT,TTree *tI,TTree *tC,TTree *tH,TTree *tI1,TTree *tC1,TTree *tH1,TTree *tI2,TTree *tC2,TTree *tH2)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.

   // Set branch addresses and branch pointers
   if (!tE) return;
   fEvent = tE;
   fTrack = tT;
   fIntercept = tI;
   fCluster   = tC;
   fHit       = tH;
   fIntercept_1 = tI1;
   fCluster_1   = tC1;
   fHit_1       = tH1;
   fIntercept_2 = tI2;
   fCluster_2   = tC2;
   fHit_2       = tH2;
   fCurrent = -1;
   fEvent->SetMakeClass(1);
   fTrack->SetMakeClass(1);
   fIntercept->SetMakeClass(1);
   fCluster  ->SetMakeClass(1);
   fHit      ->SetMakeClass(1);
   fIntercept_1->SetMakeClass(1);
   fCluster_1  ->SetMakeClass(1);
   fHit_1      ->SetMakeClass(1);
   fIntercept_2->SetMakeClass(1);
   fCluster_2  ->SetMakeClass(1);
   fHit_2      ->SetMakeClass(1);

   TBranch *br_exist;

   fEvent->SetBranchAddress("FrameNumber"  , &FrameNumber  , &b_FrameNumber  );
   fEvent->SetBranchAddress("TimeStamp"    , &TimeStamp    , &b_TimeStamp    );
   fEvent->SetBranchAddress("TriggerTime"  , &TriggerTime  , &b_TriggerTime  );
   //fEvent->SetBranchAddress("TriggerInfo"  , &TriggerInfo  , &b_TriggerInfo  );
   //fEvent->SetBranchAddress("TriggerOffset", &TriggerOffset, &b_TriggerOffset);
   //fEvent->SetBranchAddress("TriggerPhase" , &TriggerPhase , &b_TriggerPhase );
   fEvent->SetBranchAddress("Invalid"      , &Invalid      , &b_Invalid      );

   fTrack->SetBranchAddress("NTracks", &NTracks, &b_NTracks);
   fTrack->SetBranchAddress("Chi2"   , Chi2    , &b_Chi2   );
   fTrack->SetBranchAddress("Dof"    , Dof     , &b_Dof    );
   fTrack->SetBranchAddress("X"      , X       , &b_X      );
   fTrack->SetBranchAddress("Y"      , Y       , &b_Y      );
   fTrack->SetBranchAddress("SlopeX" , SlopeX  , &b_SlopeX );
   fTrack->SetBranchAddress("SlopeY" , SlopeY  , &b_SlopeY );
   fTrack->SetBranchAddress("Cov"    , TCov     , &b_TCov   );

   fIntercept->SetBranchAddress("NIntercepts", &NIntercepts, &b_NIntercepts);
   fIntercept->SetBranchAddress("U"          , U           , &b_U          );
   fIntercept->SetBranchAddress("V"          , V           , &b_V          );
   fIntercept->SetBranchAddress("SlopeU"     , SlopeU      , &b_SlopeU     );
   fIntercept->SetBranchAddress("SlopeV"     , SlopeV      , &b_SlopeV     );
   fIntercept->SetBranchAddress("Cov"        , ICov        , &b_ICov       );
   fIntercept->SetBranchAddress("Track"      , ITrack      , &b_ITrack     );

   fCluster->SetBranchAddress("NClusters", &NClusters, &b_NClusters);
   fCluster->SetBranchAddress("Col"      , Col       , &b_Col      );
   fCluster->SetBranchAddress("Row"      , Row       , &b_Row      );
   fCluster->SetBranchAddress("VarCol"   , VarCol    , &b_VarCol   );
   fCluster->SetBranchAddress("VarRow"   , VarRow    , &b_VarRow   );
   fCluster->SetBranchAddress("CovColRow", CovColRow , &b_CovColRow);

   br_exist = (TBranch *) fCluster->GetListOfBranches()->FindObject("Timing_malta");
   if (br_exist) fCluster->SetBranchAddress("Timing_malta"      , CTiming     , &b_CTiming     );
   else fCluster->SetBranchAddress("Timing"      , CTiming     , &b_CTiming     );
   
   if (br_exist) fCluster->SetBranchAddress("Timing_corrected", CTiming_corrected , &b_CTiming_corrected );
   fCluster->SetBranchAddress("Timing_pico_corrected"   , CTiming_pico   , &b_CTiming_pico  );
   fCluster->SetBranchAddress("Timing_tot"   , CTiming_tot   , &b_CTiming_tot  );
   fCluster->SetBranchAddress("Value"    , CValue    , &b_CValue   );
   fCluster->SetBranchAddress("Track"    , CTrack    , &b_CTrack   );

   fHit->SetBranchAddress("NHits"       , &NHits      , &b_NHits       );
   fHit->SetBranchAddress("PixX"        , PixX        , &b_PixX        );
   fHit->SetBranchAddress("PixY"        , PixY        , &b_PixY        );

   br_exist = (TBranch *) fHit->GetListOfBranches()->FindObject("Timing_malta");
   if (br_exist) fHit->SetBranchAddress("Timing_malta"      , HTiming     , &b_HTiming     );
   else fHit->SetBranchAddress("Timing"      , HTiming     , &b_HTiming     );

   fHit->SetBranchAddress("Timing_pico_corrected" , HTiming_pico, &b_HTiming_pico);
   fHit->SetBranchAddress("Timing_tot" , HTiming_tot, &b_HTiming_tot);
   fHit->SetBranchAddress("time_fast"    , HTimeFast   , &b_HTimeFast   );
   fHit->SetBranchAddress("bcid_short"   , HBCIDshort  , &b_HBCIDshort  );
   fHit->SetBranchAddress("bcid_long"    , HBCIDlong   , &b_HBCIDlong   );
   fHit->SetBranchAddress("word"        , HWord       , &b_HWord       );
   fHit->SetBranchAddress("Value"       , HValue      , &b_HValue      );
   fHit->SetBranchAddress("HitInCluster", HitInCluster, &b_HitInCluster);


   fIntercept_1->SetBranchAddress("NIntercepts", &NIntercepts_1, &b_NIntercepts_1);
   fIntercept_1->SetBranchAddress("U"          , U_1           , &b_U_1          );
   fIntercept_1->SetBranchAddress("V"          , V_1           , &b_V_1          );
   fIntercept_1->SetBranchAddress("SlopeU"     , SlopeU_1      , &b_SlopeU_1     );
   fIntercept_1->SetBranchAddress("SlopeV"     , SlopeV_1      , &b_SlopeV_1     );
   fIntercept_1->SetBranchAddress("Cov"        , ICov_1        , &b_ICov_1       );
   fIntercept_1->SetBranchAddress("Track"      , ITrack_1      , &b_ITrack_1     );
   fCluster_1->SetBranchAddress("NClusters", &NClusters_1, &b_NClusters_1);
   fCluster_1->SetBranchAddress("Col"      , Col_1       , &b_Col_1      );
   fCluster_1->SetBranchAddress("Row"      , Row_1       , &b_Row_1      );
   fCluster_1->SetBranchAddress("VarCol"   , VarCol_1    , &b_VarCol_1   );
   fCluster_1->SetBranchAddress("VarRow"   , VarRow_1    , &b_VarRow_1   );
   fCluster_1->SetBranchAddress("CovColRow", CovColRow_1 , &b_CovColRow_1);

   br_exist = (TBranch *) fCluster_1->GetListOfBranches()->FindObject("Timing_malta");
   if (br_exist) fCluster_1->SetBranchAddress("Timing_malta"      , CTiming_1     , &b_CTiming_1     );
   else fCluster_1->SetBranchAddress("Timing"      , CTiming_1     , &b_CTiming_1     );


   fCluster_1->SetBranchAddress("Timing_pico_corrected"  , CTiming_pico_1   , &b_CTiming_pico_1  );
   fCluster_1->SetBranchAddress("Timing_tot"   , CTiming_tot_1    , &b_CTiming_tot_1   );
   fCluster_1->SetBranchAddress("Value"    , CValue_1    , &b_CValue_1   );
   fCluster_1->SetBranchAddress("Track"    , CTrack_1    , &b_CTrack_1   );
   fHit_1->SetBranchAddress("NHits"       , &NHits_1      , &b_NHits_1       );
   fHit_1->SetBranchAddress("PixX"        , PixX_1        , &b_PixX_1        );
   fHit_1->SetBranchAddress("PixY"        , PixY_1        , &b_PixY_1        );

   br_exist = (TBranch *) fHit_1->GetListOfBranches()->FindObject("Timing_malta");
   if (br_exist) fHit_1->SetBranchAddress("Timing_malta"      , HTiming_1     , &b_HTiming_1     );
   else fHit_1->SetBranchAddress("Timing"      , HTiming_1     , &b_HTiming_1     );

   fHit_1->SetBranchAddress("Timing_pico_corrected" , HTiming_pico_1, &b_HTiming_pico_1);
   fHit_1->SetBranchAddress("Timing_tot"  , HTiming_tot_1 , &b_HTiming_tot_1);
   fHit_1->SetBranchAddress("time_fast"   , HTimeFast_1   , &b_HTimeFast_1   );
   fHit_1->SetBranchAddress("word"        , HWord_1       , &b_HWord_1       );
   fHit_1->SetBranchAddress("Value"       , HValue_1      , &b_HValue_1      );
   fHit_1->SetBranchAddress("HitInCluster", HitInCluster_1, &b_HitInCluster_1);
   
   fIntercept_2->SetBranchAddress("NIntercepts", &NIntercepts_2, &b_NIntercepts_2);
   fIntercept_2->SetBranchAddress("U"          , U_2           , &b_U_2          );
   fIntercept_2->SetBranchAddress("V"          , V_2           , &b_V_2          );
   fIntercept_2->SetBranchAddress("SlopeU"     , SlopeU_2      , &b_SlopeU_2     );
   fIntercept_2->SetBranchAddress("SlopeV"     , SlopeV_2      , &b_SlopeV_2     );
   fIntercept_2->SetBranchAddress("Cov"        , ICov_2        , &b_ICov_2       );
   fIntercept_2->SetBranchAddress("Track"      , ITrack_2      , &b_ITrack_2     );
   fCluster_2->SetBranchAddress("NClusters", &NClusters_2, &b_NClusters_2);
   fCluster_2->SetBranchAddress("Col"      , Col_2       , &b_Col_2      );
   fCluster_2->SetBranchAddress("Row"      , Row_2       , &b_Row_2      );
   fCluster_2->SetBranchAddress("VarCol"   , VarCol_2    , &b_VarCol_2   );
   fCluster_2->SetBranchAddress("VarRow"   , VarRow_2    , &b_VarRow_2   );
   fCluster_2->SetBranchAddress("CovColRow", CovColRow_2 , &b_CovColRow_2);

   br_exist = (TBranch *) fCluster_2->GetListOfBranches()->FindObject("Timing_malta");
   if (br_exist) fCluster_2->SetBranchAddress("Timing_malta"      , CTiming_2     , &b_CTiming_2     );
   else fCluster_2->SetBranchAddress("Timing"      , CTiming_2     , &b_CTiming_2     );

   fCluster_2->SetBranchAddress("Timing_pico_corrected"  , CTiming_pico_2  , &b_CTiming_pico_2  );
   fCluster_2->SetBranchAddress("Timing_tot"   , CTiming_tot_2   , &b_CTiming_tot_2  );
   fCluster_2->SetBranchAddress("Value"    , CValue_2    , &b_CValue_2   );
   fCluster_2->SetBranchAddress("Track"    , CTrack_2    , &b_CTrack_2   );
   fHit_2->SetBranchAddress("NHits"       , &NHits_2      , &b_NHits_2       );
   fHit_2->SetBranchAddress("PixX"        , PixX_2        , &b_PixX_2        );
   fHit_2->SetBranchAddress("PixY"        , PixY_2        , &b_PixY_2        );

   br_exist = (TBranch *) fHit_2->GetListOfBranches()->FindObject("Timing_malta");
   if (br_exist) fHit_2->SetBranchAddress("Timing_malta"      , HTiming_2     , &b_HTiming_2     );
   else fHit_2->SetBranchAddress("Timing"      , HTiming_2     , &b_HTiming_2     );

   fHit_2->SetBranchAddress("Timing_pico_corrected" , HTiming_pico_2, &b_HTiming_pico_2);
   fHit_2->SetBranchAddress("Timing_tot"  , HTiming_tot_2 , &b_HTiming_tot_2 );


   fHit_2->SetBranchAddress("time_fast"   , HTimeFast_2   , &b_HTimeFast_2   );
   fHit_2->SetBranchAddress("word"        , HWord_2       , &b_HWord_2       );
   fHit_2->SetBranchAddress("Value"       , HValue_2      , &b_HValue_2      );
   fHit_2->SetBranchAddress("HitInCluster", HitInCluster_2, &b_HitInCluster_2);
   //Notify();
}

Bool_t TBEvent::Notify() {
   return kTRUE;
}

void TBEvent::Show(Long64_t entry) { }

Int_t TBEvent::Cut(Long64_t entry) {
   return 1;
}
#endif // #ifdef TBEvent_cxx
