#!/usr/bin/env python
import sys
import glob
import ROOT
from array import array
import math
import argparse
import os
import numpy as np
import urllib2

def getThreshold(name, idb, ithr, sub):
  # name - name of the chip, example W4R12Epi
  # idb - IDB of the config
  # ithr - ITHR of the config
  # sub - substrate voltage of the config
  # returns 2-element array of threhold from sectors 2 and 3 as available from the google sheet
  # if nothing found, the default return values are 0
  url="https://docs.google.com/spreadsheets/d/e/2PACX-1vTktc83sznl4EOBH31nS2Ykyk-04GDB52vVV-bjViCPhohAGIxMApxa27ZZxt37jYuoHM-TuvEdaskT/pub?gid=317684079&single=true&output=csv"
  response = urllib2.urlopen(url)
  lines = response.read().split("\n") # "\r\n" if needed
  separator = 'comma'
  count = -1
  ths =[0.,0.]
  for line in lines:
        count+=1
	if separator=="comma":
          cols = line.split(",")
        elif separator=="tab":
          cols = line.split("\t")
        pass
        for col in cols:
    	  cell=col.replace("\"","")
          cell=cell.replace("\\","")
        pass
     	if count > 7:
	  NAME = cols[4]
          IDB = cols[5]
          ITHR = cols[6]
	  SUB = cols[7]
	  thS2 = cols[9]
	  thS3 = cols[13]
          if NAME =='' or SUB == '': continue
	  if int(IDB) == int(idb) and int(ITHR) == int(ithr) and int(SUB) == int(sub) and NAME == name: 
 	    ths[0] = float(thS2)
            ths[1] = float(thS3)
	    break
  print "The threshold is:",ths
  return ths

#ROOT.gROOT.LoadMacro("~/AtlasStyleBase/AtlasStyle.C")
#ROOT.gROOT.LoadMacro("~/AtlasStyleBase/AtlasUtils.C")
#ROOT.SetAtlasStyle()

ROOT.gStyle.SetOptStat(0)
#ROOT.gStyle.SetPalette(1)
ROOT.gROOT.SetBatch(1)
#ROOT.gStyle.SetEndErrorSize(10)
#ROOT.gStyle.SetTitleOffset(1.25,"y")        

parser=argparse.ArgumentParser()
parser.add_argument("-c","--chip",nargs="+",type=str,default="W7R12")
parser.add_argument("-s2","--s2only",action='store_true')
parser.add_argument("-s3","--s3only",action='store_true')
args=parser.parse_args()

os.system("mkdir -p SummaryPlots/")
base=ROOT.TH1D("vale",";Substrate Voltage [V];Efficiency [%]",10,5,60)#31)
baseC=ROOT.TH1D("vale",";Substrate Voltage [V];Cluster Size",10,5,65)#31)

plots=[
    # ["W4R12Epi",[ROOT.kBlue, ROOT.kBlue+2,ROOT.kBlue+3, ROOT.kBlue-3, ROOT.kBlue+4, ROOT.kBlue - 6],"n- gap" ],
#    ["W4R1Epi" ,[7,4,ROOT.kAzure+10] ],[ROOT.kOrange, ROOT.kOrange+2, ROOT.kOrange+3, ROOT.kOrange-3, ROOT.kOrange+6, ROOT.kOrange - 6], "n- gap"],
    ["W4R4Epi" ,[ROOT.kBlue, ROOT.kBlue+3, ROOT.kBlue-3, ROOT.kBlue+4, ROOT.kBlue-6, ROOT.kBlue-10, ROOT.kViolet, ROOT.kViolet+3, ROOT.kViolet-3, ROOT.kViolet+6, ROOT.kViolet-6],"n- gap" ],
    ["W9R4Cz" , [ROOT.kCyan, ROOT.kCyan+2, ROOT.kCyan+3, ROOT.kCyan-3, ROOT.kBlack, ROOT.kCyan - 6,  ROOT.kCyan+2, ROOT.kCyan+3, ROOT.kCyan-3, ROOT.kCyan+4, ROOT.kCyan - 6], "n- gap"],
#base.SetMaximum(100)
#base.SetMinimum( 50)
    
    # ["W11R11Cz" ,[ROOT.kMagenta, ROOT.kMagenta+3, ROOT.kMagenta-3, ROOT.kMagenta+4, ROOT.kBlack], "extra p-well" ],

    #["W4R0Epi",[ROOT.kBlue, ROOT.kBlue+3,ROOT.kBlue-3, ROOT.kBlue-6, ROOT.kBlue+4, ROOT.kBlue - 10],"n- gap" ],
    #["W7R1Cz",[ROOT.kOrange, ROOT.kOrange+3, ROOT.kOrange-3, ROOT.kOrange-6, ROOT.kOrange+6, ROOT.kOrange - 9] ,"n- cont."],
    #["W9R0Cz",[ROOT.kCyan, ROOT.kCyan+3, ROOT.kCyan-3, ROOT.kCyan-6, ROOT.kCyan+4, ROOT.kCyan - 10], "n- gap"],
    #["W11R0Cz",[ROOT.kMagenta, ROOT.kMagenta+3, ROOT.kMagenta-3, ROOT.kMagenta-6, ROOT.kMagenta+4, ROOT.kMagenta - 10], "extra p-well" ],
     #["W7R4Epi",[ROOT.kGreen+3,ROOT.kSpring+4,ROOT.kGreen-7,3,ROOT.kSpring+10,ROOT.kGreen-1,ROOT.kTeal+1,ROOT.kTeal+4,ROOT.kTeal+5, ROOT.kTeal+6,ROOT.kTeal+7,ROOT.kTeal+8], "n- cont." ],
    # ["W7R12Cz" ,[ROOT.kOrange, ROOT.kOrange+2, ROOT.kOrange+3, ROOT.kOrange-3, ROOT.kOrange+6, ROOT.kOrange - 6] ,"n- cont." ],
    # ["W9R11Cz" , [ROOT.kCyan, ROOT.kCyan+2, ROOT.kCyan+3, ROOT.kCyan-3, ROOT.kCyan+4, ROOT.kCyan - 6], "n- gap"],
]

base.SetMaximum( 100)
base.SetMinimum(  50)

baseC.SetMaximum( 1.3)
baseC.SetMinimum(  1)

can=ROOT.TCanvas("cEffVsSub","EffVsSub",800,600)
can.SetGridy(1)
base.Draw("AXIS")

canC=ROOT.TCanvas("cClVsSub","ClVsSub",800,600)
canC.SetGridy(1)
baseC.Draw("AXIS")

graphs=[]
graphsC=[]
leg = ROOT.TLegend(0.4,0.1,0.9,0.45)
leg.SetTextSize(0.029)
#legC = ROOT.TLegend(0.13,0.4,0.63,0.85)
legC = ROOT.TLegend(0.15,0.55,0.65,0.85)
legC.SetTextSize(0.029)
for w in plots:
    print (" ")
    countF = -1
    print (w[0])
    files=glob.glob("/home/sbmuser/MaltaSW/MaltaTbAnalysis/FinalPlot_DESY_Oct19/"+w[0]+"/*PWELL06*/Eff.txt")
    print files
    files.sort()
    configs=[]
    for f in files:    
        countF+=1
        if (w[0] == "W11R11Cz" and countF ==3)or (w[0] == "W9R4Cz" and countF ==4) or (w[0] == "W4R4Epi" and countF ==18): 
          conf="IDB"+f.split("__IDB")[1].split("_SUB")[0]
          configs.append(conf)
        else: continue
        #if "IDB127_ITH127" not in f and "W4R0" in f: continue
        conf="IDB"+f.split("__IDB")[1].split("_SUB")[0]
        configs.append(conf)
    configs=list(set(configs))
    configs.sort()
    tmpC=[]
    for i in range(0,len(configs)):
        #print (i)
        tmpC.append(configs[len(configs)-1-i])
    configs=tmpC
    print (configs)
    countC=-1
    for c in configs:
        print (c)
        countC+=1

        gS2=ROOT.TGraphErrors()
        gS3=ROOT.TGraphErrors()
        graphs.append(gS2)
        graphs.append(gS3)
        gS2.SetLineWidth(2)
        gS3.SetLineWidth(2)
        gS2.SetLineColor(w[1][countC])
        gS3.SetLineColor(w[1][countC])
        gS3.SetLineStyle(2)
        gS2.SetMarkerColor(w[1][countC])
        gS3.SetMarkerColor(w[1][countC])
        gS2.SetMarkerSize(1.4)
        gS3.SetMarkerSize(1.4)
        gS2.SetMarkerStyle(24)
        gS3.SetMarkerStyle(21)

        gC2=ROOT.TGraphErrors()
        gC3=ROOT.TGraphErrors()
        graphs.append(gC2)
        graphs.append(gC3)
        gC2.SetLineWidth(2)
        gC3.SetLineWidth(2)
        gC2.SetLineColor(w[1][countC])
        gC3.SetLineColor(w[1][countC])
        gC3.SetLineStyle(2)
        gC2.SetMarkerColor(w[1][countC])
        gC3.SetMarkerColor(w[1][countC])
        gC2.SetMarkerSize(1.4)
        gC3.SetMarkerSize(1.4)
        gC2.SetMarkerStyle(24)
        if w[2] =="extra p-well": 
          gC2.SetMarkerStyle(26)
          gS2.SetMarkerStyle(26)
        if w[2] =="n- gap": 
          gC2.SetMarkerStyle(25)
          gS2.SetMarkerStyle(25)
        gC3.SetMarkerStyle(21)
        countF=-1
        thresh = [0,0]
        for f in files:
            if c not in f: continue
            print (f)
            countF+=1
            idb =int( f.split("__IDB")[1].split("_ITH")[0])
            ith = int( f.split("_ITH")[1].split("_SUB")[0])
            sub=f.split("SUB")[1].split("_PW")[0]
            if sub == "13p5": sub = 13.5
            sub = float(sub)
            oldThresh = thresh
            thresh = getThreshold(w[0], idb, ith, sub)
            if thresh[0] == 0: thresh=oldThresh
            threshMean = np.mean(thresh)
            fr=open(f,"r")
            left =fr.readline().split(" ")
            right=fr.readline().split(" ")
            clLeft= fr.readline().split(" ")
            clRight= fr.readline().split(" ")
            effL=left[1]
            effR=right[1]
            errL=left[3].split("\n")[0]
            errR=right[3].split("\n")[0]
            ClL=clLeft[1].split("\n")[0]
            ClR=clRight[1].split("\n")[0]
            #print ("SUB= "+str(sub)+" --> EFF_L: "+str(effL)+" +/- "+str(errL))
            if "_S3" not in f:
                gS2.SetPoint(gS2.GetN(),sub,float(effL))
                gS2.SetPointError(gS2.GetN()-1,0.01,float(errL))
                gC2.SetPoint(gC2.GetN(),sub,float(ClL))
                gC2.SetPointError(gC2.GetN()-1,0.01,float(0))

            if "_S2" not in f:
                gS3.SetPoint(gS3.GetN(),sub,float(effR))
                gS3.SetPointError(gS3.GetN()-1,0.01,float(errR))
                gC3.SetPoint(gC3.GetN(),sub,float(ClR))
                gC3.SetPointError(gC3.GetN()-1,0.01,float(0))
            fr.close()
        leg.AddEntry(gS2,"%s %s, Threshold = %d e"%(w[0], w[2],thresh[0] ),"lp")
        legC.AddEntry(gS2,"%s %s, Threshold = %d e"%(w[0], w[2],thresh[0] ),"lp")
        can.cd()
        gS2.Draw("PL")
     #   gS3.Draw("PL")
        canC.cd()
        gC2.Draw("PL")
        #gC3.Draw("PL")
        



can.cd()
leg.Draw()
can.Update()
can.Print("SummaryPlots/Summary_Irrad.pdf")
can.SaveAs("SummaryPlots/Summary_Irrad.C")

canC.cd()
legC.Draw()
canC.Update()
canC.Print("SummaryPlots/Summary_Cluster.pdf")
#canC.SaveAs("SummaryPlots/Summary_Cluster.C")
base.Delete()
baseC.Delete()
raw_input("press any key to exit")

sys.exit()

