import sys
import glob
import ROOT
from array import array
import math
import argparse
import os

#gROOT.LoadMacro("~/AtlasStyleBase/AtlasStyle.C")
#gROOT.LoadMacro("~/AtlasStyleBase/AtlasUtils.C")
#SetAtlasStyle()

#ROOT.gStyle.SetOptStat(0)
#ROOT.gStyle.SetPalette(1)
#ROOT.gROOT.SetBatch(1)
#ROOT.gStyle.SetEndErrorSize(10)


W4R0_IDB127_ITHR127={-999:"W4R0_IDB127_ITHR127",
                     20:78.9613,
                     18:80.5511,
                     16:81.6874,
                     12:83.6627,
                     9:82.8,
                     8:82.7948,
                     7:71.3762,
                     6:52.6847,}
W4R0_IDB127_ITHR80={-999:"W4R0_IDB127_ITHR80",
                    12:91.4565,}
W4R0_IDB127_ITHR30={-999:"W4R0_IDB127_ITHR30",
                    6:66.4241,
                    12:87.7859,}

W4R4_IDB127_ITHR127={-999:"W4R4_IDB127_ITHR127",
                     6:50.3483,
                     7:67.191,
                     8:77.0301,
                     9:78.182,
                     10:77.8748,
                     11:76.8838,
                     12:76.9279,
                     13:76.1019,
                     15:56.4613,
                     18:52.847,
                     20:50.5531,}

W7R0Cz_IDB127_ITHR127={-999:"W7R0Cz-9C_IDB127_ITHR127",
                      6:35.2851,
                      9:47.01,
                      12:59.3071,
                      16:74.3046,
                      20:78.8108,
                      25:81.6511,
                      30:83.233,}

W7R0Cz_IDB127_ITHR30={-999:"W7R0Cz_IDB127_ITHR30",
                      #30:64.3643,
                      40:93.7123,
                      #45:85.2582,
                      50:95.5377,
                      53:95.6186,
                      54:92.581,
                      55:88.56,}

W7R0Cz_IDB127_ITHR127_NEW={-999:"W7R0Cz-14C_IDB127_ITHR127_NEW",
                      6:37.2093,
                      12:70.2973,
                      15:76.2298,
                      20:81.0971,
                      25:83.8552,
                      30:86.6918,
                      #35:86.0537,
                      40:90.8437,
                      45:91.7177,
                      50:91.9635,}

W7R1Cz_IDB127_ITHR127={-999:"W7R1Cz_IDB127_ITHR127",
                       6:29.6856,
                       12:59.6532,
                       15:65.795,
                       20:69.9982,
                       25:72.3793,
                       30:75.2613,
                       35:78.0881,
                       40:80.2879,
                       45:82.6174,
                       50:84.3457,}

W7R4Cz_IDB70_ITHR127={-999:"W7R4Cz_IDB70_ITHR127",
                       6:97.9032,
                       12:96.5277,
                       20:94.5945,
                       30:94.0362,}

W9R1Cz_IDB127_ITHR30={-999:"W9R1Cz_IDB127_ITHR30",
                       50:96.7417,}

W11R0Cz_IDB127_ITHR127={-999:"W11R0Cz_IDB127_ITHR127",
                        6:47.3275,
                        12:61.2029,
                        15:67.7541,
                        20:76.7609,
                        25:83.2486,
                        30:87.2544,}

W9R4Cz_IDB127_ITHR127={-999:"W9R4Cz_IDB127_ITHR127",
                       6:34.4118,
                       12:50.0138,
                       15:55.3606,
                       20:65.0467,
                       25:73.2713,
                       30:79.6817,
                       35:84.0601,
                       40:86.999,
                       45:89.1165,
                       50:91.0499,
                       55:92.3328,}

'''W9R0Cz_IDB127_ITHR127={-999:"W9R0Cz_IDB127_ITHR127",
                       6:,
                       12:,
                       15:,
                       20:,
                       25:,
                       30:,
                       35:,
                       40:,
                       45:,
                       50:,
                       55:,}'''

W9R0Cz_IDB127_ITHR127={-999:"W9R0Cz_IDB127_ITHR127",
                       6:50.7277,
                       12:71.9629,
                       15:77.8605,
                       20:85.0792,
                       25:89.3916,
                       30:91.1968,
                       35:91.6174,
                       40:92.527,
                       45:93.0524,
                       50:92.6122,}
W9R0Cz_IDB127_ITHR127
W9R0Cz_IDB127_ITHR30={-999:"W9R0Cz_IDB127_ITHR30",
                       50:96.2171,}


W11R0Cz_IDB127_ITHR127={-999:"W11R0Cz_IDB127_ITHR127",
                        6:47.3275,
                        9:54.2144,
                        12:61.2029,
                        15:67.7541,
                        20:76.7609,
                        25:83.2486,
                        30:87.2544,
                        35:89.0153,
                        40:89.8849,
                        45:91.3195,
                        50:92.2874,
                        52:92.072,
                        54:85.3839,}

W11R0Cz_IDB127_ITHR30={-999:"W11R0Cz_IDB127_ITHR30",
                       50:94.8158,}


chips = [W4R0_IDB127_ITHR127,W4R0_IDB127_ITHR80,W4R0_IDB127_ITHR30,W7R0Cz_IDB127_ITHR127,W7R0Cz_IDB127_ITHR127_NEW,W7R0Cz_IDB127_ITHR30,W7R1Cz_IDB127_ITHR127,W9R1Cz_IDB127_ITHR30,W11R0Cz_IDB127_ITHR127,W9R4Cz_IDB127_ITHR127]

chips = [W7R0Cz_IDB127_ITHR127,
         W7R0Cz_IDB127_ITHR30,
         W9R0Cz_IDB127_ITHR127,
         W9R0Cz_IDB127_ITHR30,
         W11R0Cz_IDB127_ITHR127,
         W11R0Cz_IDB127_ITHR30,]

chips = [W4R0_IDB127_ITHR127,
         W4R0_IDB127_ITHR80,
         W4R0_IDB127_ITHR30,]

chips = [W4R4_IDB127_ITHR127,W9R4Cz_IDB127_ITHR127]
#chips = [W7R0Cz_IDB127_ITHR127,W7R0Cz_IDB127_ITHR127_NEW,W7R1Cz_IDB127_ITHR127]

cEffvsSub=ROOT.TCanvas("cEffVsSub","EffVsSub",600,600)
mgEffvsSub=ROOT.TMultiGraph()
gEffvsSub={}

legend=ROOT.TLegend(0.282609,0.134146,0.884615,0.357143)
legend.SetTextFont(42)
legend.SetTextSize(0.038)
legend.SetFillColor(0)
legend.SetLineColor(0)
legend.SetFillStyle(0)
legend.SetBorderSize(0)

samples=[]
color=1
marker=20
for chip in xrange(len(chips)):
    gEffvsSub[chip]=ROOT.TGraph()
    sublg=0
    for sub in sorted(chips[chip]):
        if isinstance(chips[chip][sub],str)==True:
            sample=chips[chip][sub].split("_")[0]
            ithr=chips[chip][sub].split("ITHR")[1].split("_")[0]
            idb=chips[chip][sub].split("IDB")[1].split("_")[0]
            continue
        gEffvsSub[chip].SetTitle(";SUB [V]; Efficiency %")
        gEffvsSub[chip].SetMarkerSize(1.2)
        gEffvsSub[chip].SetLineWidth(2)
        gEffvsSub[chip].SetPoint(gEffvsSub[chip].GetN(),sub,chips[chip][sub])
        gEffvsSub[chip].Draw("APL")
        sample = chips[chip][-999].split("_")[0]
        print sample
        if sublg == 0:
            gEffvsSub[chip].SetMarkerStyle(marker)
            legend.AddEntry(gEffvsSub[chip], "%s, IDB %s, ITHR %s"%(sample,idb,ithr), "P")
            marker+=1
            if sample not in samples:
                samples.append(sample)
                gEffvsSub[chip].SetLineColor(color)
                gEffvsSub[chip].SetMarkerColor(color)
                color+=1
                pass
            pass
        sublg+=1
        pass
    mgEffvsSub.Add(gEffvsSub[chip])
    pass

mgEffvsSub.SetTitle(";SUB [V]; Efficiency %")
mgEffvsSub.Draw("APL")
legend.Draw()
cEffvsSub.Update()
cEffvsSub.SaveAs("cEffvsSub.pdf")
mgEffvsSub.SaveAs("mgEffvsSub.root")

raw_input("")
sys.exit()


























legend.SetTextSize(0.038)
legend.SetFillColor(0)
legend.SetLineColor(0)
legend.SetFillStyle(0)
legend.SetBorderSize(0)

for file in xrange(len(sorted(files))):
    if args.input in files[file] and "overView" not in files[file]:
        print file
        print files[file]
        #sys.exit()
        print "color%s"%color
        f[file]=ROOT.TFile(files[file])
        print f[file]
        posX[file]=f[file].Get("time_P")
        posY[file]=f[file].Get("ClSize_match")
        posX[file].Scale(1./posX[file].Integral())
        posY[file].Scale(1./posY[file].Integral())

        if posX[file].GetMaximum()>max:
            maxX=posX[file].GetMaximum()
            posX[file].SetMaximum(maxX*1.2)
            pass
        
        if posY[file].GetMaximum()>maxY:
            maxY=posY[file].GetMaximum()
            posY[file].SetMaximum(maxY*1.2)
            pass
        
        posX[file].SetLineColor(color)
        posY[file].SetLineColor(color)
        posX[file].SetMarkerColor(color)
        posY[file].SetMarkerColor(color)

        fit[file]=ROOT.TF1("fitG%s"%file,"gaus",-10000,10000)
        #posX.Fit(fit,"RE0")
        #SX=fit.GetParameter(2)
        #posX2.Fit(fit,"RE0")
        #SX2=fit.GetParameter(2)
        #posY.Fit(fit2,"RE0")
        #SY=fit2.GetParameter(2)
        #posY2.Fit(fit2,"RE0")
        #SY2=fit2.GetParameter(2)

        can.cd(1)
        posX[file].GetYaxis().SetTitle("a.u.")
        posX[file].GetXaxis().SetRangeUser(170,240)
        if color==1:posX[file].Draw("E")
        #fit.Draw("SAME")
        posX[file].Draw("SAMEE")

        sub=int(files[file].split("SUB")[1].split("/")[0])

        legend.AddEntry(posX[file] , "SUB  -%iV"%sub, "LPE")

        legend.Draw()

        can.cd(2)
        posY[file].GetYaxis().SetTitle("a.u.")
        #posY2.GetXaxis().SetRangeUser0,10000)
        if color==1:posY[file].Draw("E")
        posY[file].Draw("SAMEE")
        legend.Draw()

        color+=1
        pass
    pass

legend.Draw()
can.Print("Comparison_%s.pdf"%args.input)
os.system("evince Comparison_%s.pdf &"%args.input)

raw_input("All done")

