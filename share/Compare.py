#!/usr/bin/env python
import sys
import os
import os.path
import glob
import ROOT
from array import *
import math

#gROOT.LoadMacro("~/AtlasStyleBase/AtlasStyle.C")
#gROOT.LoadMacro("~/AtlasStyleBase/AtlasUtils.C")
#SetAtlasStyle()
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPalette(1)
ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetEndErrorSize(10)

###############################################################################

#f1=ROOT.TFile("../FinalPlot_DESY_Oct19/run__W7R4__IDB70_SUB06/run__W7R4__IDB70_SUB06.root")
#f2=ROOT.TFile("../FinalPlot_DESY_Oct19/run__W7R4__IDB70_SUB30/run__W7R4__IDB70_SUB30.root")

#f1=ROOT.TFile("../FinalPlot_DESY_Oct19/run__W7R12Cz__IDB70_SUB06/run__W7R12Cz__IDB70_SUB06.root")
#f2=ROOT.TFile("../FinalPlot_DESY_Oct19/run__W7R12Cz__IDB70_SUB30/run__W7R12Cz__IDB70_SUB30.root")
#f1.ls()

f1=ROOT.TFile("../FinalPlot_DESY_Oct19/run__W9R11Cz__IDB70_SUB06/run__W9R11Cz__IDB70_SUB06.root")
f2=ROOT.TFile("../FinalPlot_DESY_Oct19/run__W9R11Cz__IDB70_SUB12/run__W9R11Cz__IDB70_SUB12.root")

posX =f1.Get("time_P")
posX2=f2.Get("time_P")
posY =f1.Get("ClSize_match")
posY2=f2.Get("ClSize_match")

posX.Scale(1./posX.Integral())
posX2.Scale(1./posX2.Integral())
posY.Scale(1./posY.Integral())
posY2.Scale(1./posY2.Integral())

if posY.GetMaximum()>posY2.GetMaximum(): posY2.SetMaximum(posY.GetMaximum()*1.2)
if posX.GetMaximum()>posX2.GetMaximum(): posX2.SetMaximum(posX.GetMaximum()*1.2)

posX.SetLineColor(4)
posY.SetLineColor(4)
posX2.SetLineColor(2)
posY2.SetLineColor(2)

posX.SetMarkerColor(4)
posY.SetMarkerColor(4)
posX2.SetMarkerColor(2)
posY2.SetMarkerColor(2)

fit=ROOT.TF1("fitG","gaus",-10000,10000)
fit2=ROOT.TF1("fitG","gaus",-10000,10000)
#posX.Fit(fit,"RE0")
#SX=fit.GetParameter(2)
#posX2.Fit(fit,"RE0")
#SX2=fit.GetParameter(2)
#posY.Fit(fit2,"RE0")
#SY=fit2.GetParameter(2)
#posY2.Fit(fit2,"RE0")
#SY2=fit2.GetParameter(2)


can=ROOT.TCanvas("CRes","Residuals_Average",1200,600)
can.Divide(2,1)
can.cd(1)
posX2.GetYaxis().SetTitle("a.u.")
posX2.GetXaxis().SetRangeUser(170,300)
posX2.Draw("E")
#fit.Draw("SAME")
posX.Draw("SAMEE")

legend4=ROOT.TLegend(0.65,0.60,0.90,0.80)
legend4.SetTextFont(42)
legend4.SetTextSize(0.038)
legend4.SetFillColor(0)
legend4.SetLineColor(0)
legend4.SetFillStyle(0)
legend4.SetBorderSize(0)
legend4.AddEntry(posX , "SUB  -6V", "LPE")
legend4.AddEntry(posX2, "SUB -30V", "LPE")
legend4.Draw()


can.cd(2)
posY2.GetYaxis().SetTitle("a.u.")
#posY2.GetXaxis().SetRangeUser0,10000)
posY2.Draw("E")
posY.Draw("SAMEE")
legend4.Draw()


can.Print("Comparison.pdf")
os.system("evince Comparison.pdf &")

sys.exit()

