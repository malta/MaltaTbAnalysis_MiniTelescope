import sys
import glob
import ROOT
from array import array
import math
import argparse
import os

#gROOT.LoadMacro("~/AtlasStyleBase/AtlasStyle.C")
#gROOT.LoadMacro("~/AtlasStyleBase/AtlasUtils.C")
#SetAtlasStyle()

#ROOT.gStyle.SetOptStat(0)
#ROOT.gStyle.SetPalette(1)
#ROOT.gROOT.SetBatch(1)
#ROOT.gStyle.SetEndErrorSize(10)

parser=argparse.ArgumentParser()
parser.add_argument("-o","--outdir",type=str,default="/home/sbmuser/MaltaSW/MaltaTbAnalysis/FinalPlot_DESY_Oct19/output/")
parser.add_argument("-i","--input",type=str,default=["W7R12"])
args=parser.parse_args()

files = []
for r, d, f in os.walk("/home/sbmuser/MaltaSW/MaltaTbAnalysis/FinalPlot_DESY_Oct19/"):
    for file in f:
        #if args.input not in file: continue
        if "Eff.txt" in file and args.input in r:
            files.append(os.path.join(r, file))
files = sorted(files)

for f in files:
    print f

cEffvsSub=ROOT.TCanvas("cEffVsSub","EffVsSub",600,600)
gEffvsSub=ROOT.TGraph()

legend=ROOT.TLegend(0.282609,0.134146,0.884615,0.357143)
legend.SetTextFont(42)
legend.SetTextSize(0.038)
legend.SetFillColor(0)
legend.SetLineColor(0)
legend.SetFillStyle(0)
legend.SetBorderSize(0)

color=1
marker=20
sublg=0
for chip in sorted(files):

    if "SUB" in chip: sub=chip.split("SUB")[1]
    if "/" in sub: sub=sub.split("/")[0]
    if "PWELL" in sub: sub=int(sub.split("_")[0])
    sub = float(sub)
    
    if "IDB" in chip: idb=chip.split("IDB")[-1]
    idb = idb.split("_")[0]
    
    if "ITH" in chip: ithr=chip.split("ITHR")[-1]
    ithr = ithr.split("_")[0]

    if "W" in chip: sample=chip.split("run__")[1]
    sample = sample.split("__")[0]

    fr = open(chip,"r")
    eff = float(fr.readline().split(" ")[0])

    print ithr,idb,sub,sample,eff

    gEffvsSub.SetPoint(gEffvsSub.GetN(),sub,eff)

    #color+=1
    pass

legend.AddEntry(gEffvsSub, "%s, IDB %s, ITHR %s"%(sample,idb,ithr), "P")
gEffvsSub.SetMarkerStyle(marker)
gEffvsSub.SetLineColor(color)
gEffvsSub.SetMarkerColor(color)
gEffvsSub.SetTitle(";SUB [V]; Efficiency %")
gEffvsSub.SetMarkerSize(1.2)
gEffvsSub.SetLineWidth(2)
gEffvsSub.Draw("AP")

legend.Draw()
cEffvsSub.Update()
cEffvsSub.SaveAs("cEffvsSub.pdf")
fw = ROOT.TFile("%sgEffvsSub_%s.root"%(args.outdir,args.input),"RECREATE")
gEffvsSub.Write("%s_IDB%s_ITHR%s"%(sample,idb,ithr))
fw.Close()
raw_input("")
