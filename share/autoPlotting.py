#!/usr/bin/env python
####################################
# Daq script for MALTA read-out
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# December 2017
####################################

import argparse
import array
import sys
import ROOT

ROOT.gStyle.SetPalette(1)


class Tuple(dict):
    def __init__(self):
        self.treenumber=-1
    def LoadBranches(self,t):
        if self.treenumber == t.GetTreeNumber(): return
        self.treenumber = t.GetTreeNumber()
        for i in range(t.GetListOfLeaves().GetEntries()):
            #print t.GetListOfLeaves().At(i).GetName()
            self.__setattr__(t.GetListOfLeaves().At(i).GetName(),
                             t.GetListOfLeaves().At(i))
            pass
        pass

ROOT.gStyle.SetPadLeftMargin(0.134)
ROOT.gStyle.SetPadBottomMargin(0.146)
ROOT.gStyle.SetPadRightMargin(0.035)
ROOT.gStyle.SetPadTopMargin(0.078)
ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)
ROOT.gStyle.SetLabelSize(0.05,"x")
ROOT.gStyle.SetLabelSize(0.05,"y")
ROOT.gStyle.SetTitleSize(0.05,"t")
ROOT.gStyle.SetTitleSize(0.05,"x")
ROOT.gStyle.SetTitleSize(0.05,"y")
ROOT.gStyle.SetTitleOffset(1.40,"y")
#ROOT.gStyle.SetOptTitle(0);
ROOT.gStyle.SetOptStat(0);

def autoPlotting(inputFile,outputFile,applyTimingCut=None,
                 batch=True,sort=True,fromPython=False):

    if batch: ROOT.gROOT.SetBatch(1)
    chain = ROOT.TChain("MALTA")
    chain.AddFile(inputFile)
    chain.SetBranchStatus("ithres"   ,0)
    chain.SetBranchStatus("runNumber",0)
    chain.SetBranchStatus("delay"    ,0)
    chain.SetBranchStatus("chipbcid" ,0)
    tree = Tuple()

    xmin= -0.5
    xmax=511.5
    ymin= -0.5
    ymax=511.5

    #cPixelHitMap = ROOT.TCanvas("cPixelHitMap","PixelHitMap",600,600)
    #cPixelHitMap.SetRightMargin(0.11)
    hPixelHitMap = ROOT.TH2D("hPixelHitMap","PixelHitMap",512,xmin,xmax,512,ymin,ymax)
    hTimer = ROOT.TH1D("hTimer","Timer",2000,0,1000)
    hTimer.SetCanExtend(ROOT.TH1.kXaxis)
    hJumpDetect = ROOT.TH2D("hL1idVsL1idC",";Extended L1ID;L1ID",1000,0,10000,4096,0,4096)
    hJumpDetect.SetCanExtend(ROOT.TH1.kXaxis)
    hTimeSinceL1A = ROOT.TH1D("hTimeSinceL1A","TimeSinceL1A",160,0,500)
    
    allEvents=0
    uniqueEvents=0
    a = 0
    eventsPassed=0
    maxTimer=-999
    
    entry = 0
    while True:
        chain.GetEntry(entry)
        if chain.LoadTree(entry)<0:
            break
        entry+=1
        if entry%500000==0: print entry
        tree.LoadBranches(chain)
        
        if tree.isDuplicate.GetValue() == 1: continue #add args
        if tree.phase.GetValue()==7        : continue ##VD: noise protection
        ###if phase[event]==6: continue ##VD: noise protection

        if not fromPython:
            hTimer.Fill(tree.timer.GetValue())
            if tree.timer.GetValue() > maxTimer: maxTimer=tree.timer.GetValue()

            hJumpDetect.Fill(tree.l1idC.GetValue(),tree.l1id.GetValue())
            hTimeSinceL1A.Fill(tree.bcid.GetValue()*25. + tree.winid.GetValue()*3.125)
        
            fullTime=tree.bcid.GetValue()*25+tree.winid.GetValue()*3.125+tree.phase.GetValue()*0.39
            
        ###print fullTime
        if applyTimingCut:
            if fullTime>300: continue
            if fullTime< 30: continue
        #######################if fullTime>200: continue
        ##if fullTime<250: continue

        #md = MaltaData.MaltaData()
        #md.setWord1(int(word1[event]))
        #md.setWord2(int(word2[event]))
        #md.getInfo()

        ##if bcid[event]!=0: continue ##VALERIO!!!!!!!!!!!!!!11 AAAAAAARRRRRHHHHHHH
        FUCKpix = int(tree.pixel.GetValue())
        eventsPassed+=1
        #pixelStr = format(int("%.f"%(pixel[event])),'016b')
        for pix in xrange(16):
            if ((FUCKpix>>pix)&1)==0: continue

            xpos = tree.dcolumn.GetValue()*2
            if pix>7: xpos+=1

            ypos = tree.group.GetValue()*16
            if tree.parity.GetValue()==1: ypos += 8

            if (pix == 0) or (pix == 8):  ypos+=0
            if (pix == 1) or (pix == 9):  ypos+=1
            if (pix == 2) or (pix == 10): ypos+=2
            if (pix == 3) or (pix == 11): ypos+=3
            if (pix == 4) or (pix == 12): ypos+=4
            if (pix == 5) or (pix == 13): ypos+=5
            if (pix == 6) or (pix == 14): ypos+=6
            if (pix == 7) or (pix == 15): ypos+=7
            #print " XPOS: "+str(xpos)+" -- YPOS: "+str(ypos)

            #some on-the-fly pixel removal
            if applyTimingCut:
                if (xpos==502 and ypos==470): continue
                if (xpos==398 and ypos==189): continue
                if (xpos==226 and ypos==173): continue
                if (xpos==438 and ypos== 32): continue
                if (xpos== 51 and ypos==240): continue
                if (xpos== 51 and ypos==328): continue
                if (xpos== 80 and ypos==271): continue
                if (xpos==415 and ypos== 80): continue
                if (xpos==449 and ypos==133): continue
                if (xpos==  8 and ypos==450): continue
                if (xpos== 66 and ypos==104): continue
                if (xpos== 73 and ypos==195): continue
                if (xpos== 82 and ypos==392): continue
                if (xpos== 92 and ypos==282): continue
                if (xpos==125 and ypos==241): continue
                if (xpos==128 and ypos==141): continue
                if (xpos==147 and ypos==426): continue
                if (xpos==177 and ypos==161): continue
                if (xpos==482 and ypos== 93): continue
                if (xpos==504 and ypos==385): continue
                pass
            
            hPixelHitMap.Fill(xpos,ypos)

            pass
        pass
    
    oFile=ROOT.TFile(outputFile,"RECREATE")
    print "Total event number: %s"%entry
    print "Number of events after duplication: %s"%(eventsPassed)
    print "Number of pixel hits after duplication: %s"%(hPixelHitMap.Integral())
    print " " 
    sumV=0
    seen=0
    AllHit=float(hPixelHitMap.Integral())+0.001
    hotPixels=[]
    for x in xrange(0,516):
        for y in xrange(0,516):
            cont=hPixelHitMap.GetBinContent(x,y)
            sumV+=cont
            if cont!=0: seen+=1
            if cont!=0 and float(cont)/AllHit>0.001:
                pix={"x":int(hPixelHitMap.GetXaxis().GetBinCenter(x)),
                     "y":int(hPixelHitMap.GetYaxis().GetBinCenter(y)),
                     "v":int(cont)}
                hotPixels.append(pix)
                pass
            pass
        pass
    if sort: hotPixels=sorted(hotPixels,key=lambda tup: tup["v"],reverse=True)
    for pix in hotPixels:
        print "  x=%3i, y=%3i count=%6i" % (pix["x"],pix["y"],pix["v"])
        pass

    print " "
    print " Represented fraction is: "+str( float(sumV)/(AllHit+0.0001)*100 )+" %"
    print " number of pixels with hits: "+str(seen)
    print " "

    c1=ROOT.TCanvas("c1","c1",1000,800)
    c1.Divide(2,2)

    c1.cd(1)
    hJumpDetect.Draw("COLZ")
    c1.Update()

    tpad=c1.cd(2)
    tpad.SetLogz()
    tpad.SetRightMargin(0.11)
    hPixelHitMap.SetTitle("Frequency;Pixel PosX;Pixel PosY")
    hPixelHitMap.SetMinimum(0.)
    hPixelHitMap.SetMaximum(hPixelHitMap.GetMaximum()*0.03)
    hPixelHitMap.RebinX(2)
    hPixelHitMap.RebinY(2)
    hPixelHitMap.Draw("COLZ")
    c1.Update()

    if not fromPython:
        c1.cd(3)
        hTimer.SetBins(int(maxTimer),0,maxTimer)
        hTimer.Draw()
        c1.Update()
        
        c1.cd(4)
        hTimeSinceL1A.SetMinimum(0)
        hTimeSinceL1A.Draw()
        c1.Update()

    oFile.WriteObject(hPixelHitMap,"Map2D")
    oFile.WriteObject(hTimer,"Timer")
    oFile.WriteObject(hJumpDetect,"JumpDetect")
    oFile.WriteObject(hTimeSinceL1A,"TimeSinceL1A")
    c1.Write()
    oFile.Close()

    #oName="Plots/PixelHitMap_%s.pdf"%inputFile[:-5]
    #cPixelHitMap.SaveAs(oName)
    ####cPixelHitMap.SaveAs("Plots/PixelHitMap_%s.root"%args.file[:-5])
    #cPixelHitMap.SaveAs("Plots/PixelHitMap_%s.c"%args.file[:-5])

    if batch: ROOT.gROOT.SetBatch(0)
    else: raw_input("press any key to continue")
    pass

if __name__=='__main__':
    parser=argparse.ArgumentParser()
    parser.add_argument('-f','--file',help='input file',default="input.root")
    parser.add_argument('-o','--output',help='output file',default="output.root")
    parser.add_argument('-t','--applyTimingCut',help='apply timing cut [30,300]',action='store_true')
    parser.add_argument('-s','--sort',help='sort by noise',action='store_true')
    parser.add_argument('-b','--batch',help='sort by noise',action='store_true')
    args=parser.parse_args()
    autoPlotting(args.file,args.output,args.applyTimingCut,args.batch,args.sort)
    pass
