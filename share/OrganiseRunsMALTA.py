import os
import sys
import glob
import ROOT
import math

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(1)
ROOT.gStyle.SetOptFit(1)

badRuns=[]#[6787, 6690,6771,  6767,6772,  6774,6784,6907, 6970, 7140, 7110, 6830]   
#all removed runs: 6690 6703 6723 6756 6757 6758 6771 6772, 6773, 6774, 6775 6767 6780 6784 6787,6839,6840, 6870, 6900, 6907, 6970, 6972, 6973,7052,7053,7054,7055, 7056, 7126, 7128, 7139, 7140, 7189, 7204, 7110]
#missing runs: 6787, 6690, 6771, 6772, 6773, 6774, 6767, 6780, 6784, 6870,7907,7140 
#fixedRuns = 6703, 6723,6775?, 6773?,6774?,6756, 6757 6758 6839, 6840 6900? 6970?, 6972? 6973?,7052?, 7053?,7054?,7055?, 7056?, 7126, 7128, 7139 7204  
#alignment issues:6699 6716,6717,6719, 6722, 6723,6724, 6734, 6754, 6799,6830,6831, 6948, 7056, 7150, 7151, 7163, 7164, 7165, 7166, 7154, 7155,  6823,
baseFolder="/home/sbmuser/MaltaSW/MaltaTbAnalysis/DESY_oct2019/"
outFolder="/home/sbmuser/MaltaSW/MaltaTbAnalysis/FinalPlot_DESY_Oct19/"

runProp = [
   ["W9R4Cz__IDB100_ITHR80_SUB55_PWELL6_SEC3",[7044,7045,7046,7047,7050]],
    ["W4R0Epi__IDB127_ITH127_SUB06_PWELL06" , [6785,6786,6797]], 
    ["W4R0Epi__IDB127_ITH127_SUB07_PWELL06" , [6796]],
    ["W4R0Epi__IDB127_ITH127_SUB08_PWELL06" , [6795]],
    ["W4R0Epi__IDB127_ITH127_SUB09_PWELL06" , [6794]],
    ["W4R0Epi__IDB127_ITH127_SUB12_PWELL06" , [6788,6789]],
    ["W4R0Epi__IDB127_ITH127_SUB16_PWELL06" , [6792,6793]],
    ["W4R0Epi__IDB127_ITH127_SUB18_PWELL06" , [6798]],
    ["W4R0Epi__IDB127_ITH127_SUB20_PWELL06" , [6790,6791]],
    ["W4R0Epi__IDB127_ITH080_SUB12_PWELL06" , [6799,6800,6801,6802]],
    ["W4R0Epi__IDB127_ITH030_SUB12_PWELL06" , [6803,6804,6805,6806,6808,6809,6810,6811,6818,6819]], 
    ["W4R0Epi__IDB100_ITH030_SUB12_PWELL06" , [6813,6814]],
    ["W4R0Epi__IDB127_ITH030_SUB06_PWELL06" , [6817]],
    ["W4R0Epi__IDB127_ITH030_SUB06_PWELL02" , [6815]],
    ["W4R0Epi__IDB127_ITH030_SUB06_PWELL04" , [6816]],
]
'''
   ["W11R8Cz__IDB127_ITHR127_SUB6_PWELL6",[6974,6975, 6976]],
   ["W11R8Cz__IDB127_ITHR127_SUB9_PWELL6",[6977,6978]],
   ["W11R8Cz__IDB127_ITHR127_SUB12_PWELL6",[6979,6980]],
   ["W11R8Cz__IDB127_ITHR127_SUB15_PWELL6",[6981,6982]],
   ["W11R8Cz__IDB127_ITHR127_SUB25_PWELL6",[6984]],
   ["W11R8Cz__IDB127_ITHR30_SUB30_PWELL6",[6985]],
   ["W11R8Cz__IDB127_ITHR100_SUB30_PWELL6",[6986]],
   ["W11R8Cz__IDB100_ITHR100_SUB30_PWELL6",[6987]],
   ["W11R8Cz__IDB127_ITHR100_SUB40_PWELL6",[6988]],
   ["W11R8Cz__IDB127_ITHR127_SUB60_PWELL6",[6990,6991]],
   ["W11R8Cz__IDB127_ITHR110_SUB60_PWELL6",[6992]],
   ["W11R8Cz__IDB127_ITHR100_SUB60_PWELL6",[6993]],
   ["W11R8Cz__IDB127_ITHR90_SUB60_PWELL6",[6994]],
   ["W11R8Cz__IDB127_ITHR80_SUB60_PWELL6",[6995]],
   ["W11R8Cz__IDB127_ITHR70_SUB60_PWELL6",[6996]],
   ["W11R8Cz__IDB127_ITHR30_SUB60_PWELL6",[6997, 6998]],
   ["W11R8Cz__IDB127_ITHR30_SUB55_PWELL6",[6999]],
   ["W11R8Cz__IDB127_ITHR30_SUB64_PWELL6",[7000]],
   ["W11R8Cz__IDB110_ITHR30_SUB64_PWELL6",[7001]],

   ["W7R8Cz__IDB127_ITHR127_SUB6_PWELL6",[7002]],
   ["W7R8Cz__IDB127_ITHR127_SUB30_PWELL6",[7003]],
   ["W7R8Cz__IDB127_ITHR127_SUB50_PWELL6",[7005]],
   ["W7R8Cz__IDB127_ITHR127_SUB60_PWELL6",[7006]],
   ["W7R8Cz__IDB100_ITHR30_SUB60_PWELL6",[7007, 7008]],
   ["W7R8Cz__IDB80_ITHR30_SUB6_PWELL6",[7009, 7010]],
   ["W7R8Cz__IDB60_ITHR30_SUB6_PWELL6",[7011, 7012]],
   ["W7R8Cz__IDB40_ITHR30_SUB6_PWELL6",[701, 7014, 7015, 7016]],



   ["W7R4__IDB127"         , [6691,6692,6693,6694,6695,6696,6697,6698]], #6694 says "TESTING DO NOT USE", has slightly better efficiency #6690 removed 
   ["W7R4__IDB70_SUB06"    , [6699,6700,6701,6702]], #6699 slight alignment problem
   ["W7R4__IDB70_SUB20"    , [6703, 6704,6705]], #removed 6703 for low efficiency 
   ["W7R4__IDB70_SUB12"    , [6706,6707,6708,6709]],
   ["W7R4__IDB70_SUB30"    , [6710,6711,6712]],


   ["W4R12__IDB70_SUB06"   , [6713,6714,6716,6717,6719, 6722, 6723]], #worse residuals for 3 GeV runs, eff the same tho
   ["W4R12__IDB70_SUB09"   , [6720,6721]],

   ["W7R12Cz__IDB70_SUB06" , [6722, 6723]], #6723 removed
   ["W7R12Cz__IDB70_SUB12" , [6724,6725,6726]], #6724 slight misalignment, better residuals and efficiency 
   ["W7R12Cz__IDB70_SUB20" , [6729,6730,6734]], #6734 has better residusals due to 4 GeV e-
   ["W7R12Cz__IDB70_SUB30" , [6732,6733]],
   ["W7R12Cz__IDB70_SUB16" , [6735]],
   ["W7R12Cz__IDB70_SUB25" , [6736]],

   ["W9R11Cz__IDB70_SUB06"   , [6739,6740,6741,6742,6743,6744,6745]],
   ["W9R11Cz__IDB70_SUB09"   , [6746, 6747, 6748, 6750, 6751]],
   ["W9R11Cz__IDB70_SUB12"   , [6752, 6753, 6754]], # worse residuals with 5 GeV?!

   ["W11R11Cz__IDB70_ITH30_SUB6"   ,[6756]], #poor efficiency (fixed), residuals
   ["W11R11Cz__IDB100_ITH30_SUB6"   , [6757]], #poor efficiency
   ["W11R11Cz__IDB127_ITH30_SUB6"   , [6758]], #poor efficiency
   ["W11R11Cz__IDB127_ITH100_SUB6"   , [6759,6761,6762,6768,6779]], #removed runs 6756 and 6757
   ["W11R11Cz__IDB127_ITH100_SUB8"   , [6769]],
   ["W11R11Cz__IDB127_ITH100_SUB9"   , [6763,6764]],
   ["W11R11Cz__IDB127_ITH100_SUB12"   , [6765,6766,6770]], # removed 6771
   ["W11R11Cz__IDB127_ITH100_SUB13"   , [6771, 6775]], #added this, but should not have runs are bad BUT NOW FIXED
   ["W11R11Cz__IDB127_ITH100_SUB16"   , [6767]], # also bad
   ["W11R11Cz__IDB127_ITH100_SUB13p5"   , [6772, 6773, 6774]], # bad runs... but why...
   ["W11R11Cz__IDB127_ITH127_SUB6"   , [ 6781]],#problem with 6780
   ["W11R11Cz__IDB127_ITH100_SUB6_PWELL2"   , [6782]],

   ["W4R0__IDB127_ITH127_SUB06"  , [ 6785, 6786,6797]], #6797 missing?
   ["W4R0__IDB127_ITH127_SUB07"  , [6796]],
   ["W4R0__IDB127_ITH127_SUB08"  , [6795]],
   ["W4R0__IDB127_ITH127_SUB09"  , [6794]],
   ["W4R0__IDB127_ITH127_SUB12"  , [6788, 6789]],
   ["W4R0__IDB127_ITH127_SUB16"  , [6792, 6793]],
   ["W4R0__IDB127_ITH127_SUB20"  , [6790, 6791]],
   ["W4R0__IDB127_ITH127_SUB18"  , [6798]],
   ["W4R0__IDB127_ITH80_SUB12"   , [6799, 6800, 6801, 6802]],#changed from ITH30 #6799 has worse residuals
   ["W4R0__IDB127_ITH30_SUB12"   , [6803, 6804, 6805, 6806, 6811, 6818, 6819]], #changed from ITH80 #efficncies ok but notes suggest minor differences betweeen runs
   ["W4R0__IDB70_ITH30_SUB12"   , [ 6808, 6809]], #changed from ITH80 #efficncies ok but notes suggest minor
   ["W4R0__IDB127_ITH30_SUB06_PWELL02"  , [6815]],
   ["W4R0__IDB127_ITH30_SUB06_PWELL04"  , [6816]],
   ["W4R0__IDB127_ITH30_SUB06"  , [6817]],

   ["W7R0Cz__IDB127_ITH127_SUB06_PWELL6", [6820]],
   ["W7R0Cz__IDB127_ITH127_SUB12_PWELL6", [6821, 6822, 6823]], #6823 slightly better residual
   ["W7R0Cz__IDB127_ITH127_SUB30_PWELL6", [6824, 6825]],
   ["W7R0Cz__IDB127_ITH127_SUB25_PWELL6", [6826, 6827]],
   ["W7R0Cz__IDB127_ITH127_SUB20_PWELL6", [6828, 6829]],
   ["W7R0Cz__IDB127_ITH127_SUB16_PWELL6", [ 6831]], #inconsistent residuals
   ["W7R0Cz__IDB127_ITH127_SUB09_PWELL6", [6832]], #6833]
   ["W7R0Cz__IDB127_ITH030_SUB30_PWELL6", [6834,6835]],
   ["W7R0Cz__IDB127_ITH080_SUB30_PWELL6", [6837, 6836,6837, 6839, 6840]], #removed other runs b/c different masking configs according to spreadsheet#[6836,6837,6839,6840]],
   ["W7R0Cz__IDB127_ITH080_SUB30_PWELL6_S2", [6841]],
   ["W7R0Cz__IDB127_ITH080_SUB30_PWELL6_S3", [6842]],
   ["W7R0Cz__IDB127_ITH030_SUB30_PWELL6_S3", [6843,6844,6848,6849,6850,6851,6852,6854,6855]], #slight differences due to temperature and masking
   ["W7R0Cz__IDB100_ITH030_SUB30_PWELL6_S3", [6845,6846]], #changed from 6843 and 6844
   ["W7R0Cz__IDB127_ITH030_SUB40_PWELL6_S3", [6853, 6856]],
   ["W7R0Cz__IDB127_ITH030_SUB45_PWELL6_S3", [6857, 6858, 6859]],
   ["W7R0Cz__IDB127_ITH030_SUB53_PWELL6_S3", [6860, 6861]],
   ["W7R0Cz__IDB127_ITH030_SUB54p7_PWELL6_S3", [6862, 6863]],
   ["W7R0Cz__IDB127_ITH030_SUB55p2_PWELL6_S3", [6864, 6865]],
   ["W7R0Cz__IDB127_ITH030_SUB50_PWELL6_S3", [6866,6867]],
   ["W7R0Cz__IDB100_ITH030_SUB50_PWELL6_S3", [6868,6869]],
  #   ["W7R0Cz__IDB127_ITH030_SUB16_PWELL6_S3", [6870]],#marked as red


   ["W9R1Cz__IDB127_ITH127_SUB06_PWELL6", [6871, 6872, 6873]],
   ["W9R1Cz__IDB127_ITH127_SUB09_PWELL6", [6874, 6875]],
   ["W9R1Cz__IDB127_ITH127_SUB12_PWELL6", [6876, 6877]],
   ["W9R1Cz__IDB127_ITH127_SUB15_PWELL6", [6878, 6879]],
   ["W9R1Cz__IDB127_ITH127_SUB20_PWELL6", [6880, 6881]],
   ["W9R1Cz__IDB127_ITH127_SUB25_PWELL6", [6882, 6883]],
   ["W9R1Cz__IDB127_ITH127_SUB30_PWELL6", [6884, 6885]],
   ["W9R1Cz__IDB127_ITH127_SUB35_PWELL6", [6886, 6887]],
   ["W9R1Cz__IDB127_ITH127_SUB40_PWELL6", [6888, 6889]],
   ["W9R1Cz__IDB127_ITH127_SUB45_PWELL6", [6890, 6891]],
   ["W9R1Cz__IDB127_ITH127_SUB50_PWELL6", [6892, 6893, 6894, 6895, 6896, 6897]],
   ["W9R1Cz__IDB127_ITH30_SUB50_PWELL6_UPDATE",  [6898, 6899]], # updating noise map for extra pixels #removed 6900
   ["W9R1Cz__IDB127_ITH30_SUB50_PWELL6_BEST",  [6900]],
   ["W9R1Cz__IDB127_ITH30_SUB50_PWELL6",  [6901, 6905, 6906, 6909, 6910,6911]],
   ["W9R1Cz__IDB100_ITH30_SUB50_PWELL6",  [6902]],
   ["W9R1Cz__IDB100_ITH30_SUB50_PWELL6_S3",  [6903]],
   ["W9R1Cz__IDB127_ITH30_SUB50_PWELL6_LARGE_FIDUCIAL",  [ 6913, 6914, 6915, 6916]],
   ["W9R1Cz__IDB127_ITH30_SUB50_PWELL6_LARGE_FIDUCIAL_3GeV",  [  6917, 6918, 6919, 6920, 6921, 6922, 6923, 6924]],
   ["W9R1Cz__IDB127_ITH127_SUB6_PWELL6_LARGE_FIDUCIAL",  [6925, 6926, 6928, 6930, 6931, 6932, 6933, 6934, 6935]],

#sub 6 data missing? no it is before the full mask



   ["W11R0Cz__IDB127_ITHR127_SUB6_PWELL6",[6936,6937]],
   ["W11R0Cz__IDB127_ITHR127_SUB9_PWELL6",[6948,6949]], # alignment problem for 6948
   ["W11R0Cz__IDB127_ITHR127_SUB12_PWELL6",[6938, 6939]],
   ["W11R0Cz__IDB127_ITHR127_SUB15_PWELL6",[6940, 6941]],
   ["W11R0Cz__IDB127_ITHR127_SUB20_PWELL6",[6942, 6943]],
   ["W11R0Cz__IDB127_ITHR127_SUB25_PWELL6",[6944, 6945]], 
   ["W11R0Cz__IDB127_ITHR127_SUB30_PWELL6",[6946, 6947]],    
   ["W11R0Cz__IDB127_ITHR127_SUB35_PWELL6",[6950, 6951]], #efficiency map looks like an inverted beam spot => rate becomes to high? actually true for most of these runs...
   ["W11R0Cz__IDB127_ITHR127_SUB40_PWELL6",[6952, 6953]],
   ["W11R0Cz__IDB127_ITHR127_SUB45_PWELL6",[6954, 6955]],
   ["W11R0Cz__IDB127_ITHR127_SUB50_PWELL6",[6956, 6957]],
   ["W11R0Cz__IDB127_ITHR127_SUB54_PWELL6",[6958]],
   ["W11R0Cz__IDB127_ITHR127_SUB52_PWELL6",[6959]],
   ["W11R0Cz__IDB127_ITHR30_SUB50_PWELL6",[ 6963, 6964, 6965, 6966, 6967, 6968, 6969]], # do we want pre-masking run, no you dont
   ["W11R0Cz__IDB100_ITHR30_SUB50_PWELL6",[ 6971, 6970, 6972, 6973]], #  6972, 6973 low efficiency


   ["W9R4Cz__IDB127_ITHR127_SUB6_PWELL6",[7017,7018,7019]],
   ["W9R4Cz__IDB127_ITHR127_SUB12_PWELL6",[7020,7021]],
   ["W9R4Cz__IDB127_ITHR127_SUB15_PWELL6",[7022,7023]],
   ["W9R4Cz__IDB127_ITHR127_SUB20_PWELL6",[7024,7025]],
   ["W9R4Cz__IDB127_ITHR127_SUB25_PWELL6",[7026,7027]],
   ["W9R4Cz__IDB127_ITHR127_SUB30_PWELL6",[7028,7029]],
   ["W9R4Cz__IDB127_ITHR127_SUB35_PWELL6",[7030,7031]],
   ["W9R4Cz__IDB127_ITHR127_SUB40_PWELL6",[7032,7033]],
   ["W9R4Cz__IDB127_ITHR127_SUB45_PWELL6",[7034,7035]],
   ["W9R4Cz__IDB127_ITHR127_SUB50_PWELL6",[7036,7037]],
   ["W9R4Cz__IDB127_ITHR127_SUB55_PWELL6",[7039]],
   ["W9R4Cz__IDB127_ITHR127_SUB55_PWELL6_S2",[7040,7041,7042,7043]],
   ["W9R4Cz__IDB100_ITHR80_SUB55_PWELL6_SEC3",[7044,7045,7046,7047,7050]],
   ["W9R4Cz__IDB80_ITHR30_SUB55_PWELL6_ROI",[7051]], # 7056 removed for alignment , 7051 only one with good efficiency


   ["W4R4__IDB127_ITHR127_SUB6_PWELL6",[7058, 7059, 7060]],
   ["W4R4__IDB127_ITHR127_SUB9_PWELL6",[7061, 7062]],
   ["W4R4__IDB127_ITHR127_SUB12_PWELL6",[7063, 7064]],
   ["W4R4__IDB127_ITHR127_SUB15_PWELL6",[7065, 7066]],
   ["W4R4__IDB127_ITHR127_SUB18_PWELL6",[7067, 7068]],
   ["W4R4__IDB127_ITHR127_SUB20_PWELL6",[7069, 7070]],
   ["W4R4__IDB100_ITHR3_SUB9_PWELL6",[7086, 7087]],  
   ["W4R4__IDB100_ITHR3_SUB9_PWELL6",[7073, 7074]],  
   ["W4R4__IDB127_ITHR3_SUB6_PWELL6",[7088]],  # Y residuals
   ["W4R4__IDB127_ITHR3_SUB8_PWELL6",[7089]],
   ["W4R4__IDB127_ITHR30_SUB9_PWELL6",[7080, 7081]],
   ["W4R4__IDB127_ITHR10_SUB9_PWELL6",[7082, 7083 ]],
   ["W4R4__IDB127_ITHR3_SUB9_PWELL6",[7084, 7085 ]],
   ["W4R4__IDB127_ITHR3_SUB10_PWELL6",[7090]],
   ["W4R4__IDB127_ITHR3_SUB12_PWELL6",[7091]],
   ["W4R4__IDB127_ITHR3_SUB14_PWELL6",[7092]],
   ["W4R4__IDB127_ITHR3_SUB16_PWELL6",[7093]],
   ["W4R4__IDB127_ITHR3_SUB10_PWELL6_S2",[7094]],
   ["W4R4__IDB127_ITHR30_SUB10_PWELL6_S2",[7095, 7096 ]],

   ["W7R1Cz__IDB127_ITHR127_SUB6_PWELL6",[7097, 7098]],  
   ["W7R1Cz__IDB127_ITHR127_SUB12_PWELL6",[7099, 7100]],
   ["W7R1Cz__IDB127_ITHR127_SUB15_PWELL6",[7101, 7102]],
   ["W7R1Cz__IDB127_ITHR127_SUB20_PWELL6",[7103, 7104]],
   ["W7R1Cz__IDB127_ITHR127_SUB25_PWELL6",[7105, 7106]],
   ["W7R1Cz__IDB127_ITHR127_SUB30_PWELL6",[7107, 7108]],
   ["W7R1Cz__IDB127_ITHR127_SUB35_PWELL6",[7109 ]], #7110 residuals
   ["W7R1Cz__IDB127_ITHR127_SUB40_PWELL6",[7111, 7112]],
   ["W7R1Cz__IDB127_ITHR127_SUB45_PWELL6",[7113, 7114]],
   ["W7R1Cz__IDB127_ITHR127_SUB50_PWELL6",[7115, 7116]],
   ["W7R1Cz__IDB127_ITHR30_SUB50_PWELL6",[7117, 7118]],
   ["W7R1Cz__IDB127_ITHR10_SUB50_PWELL6",[7119, 7120]],
   ["W7R1Cz__IDB100_ITHR10_SUB50_PWELL6",[7121]],
   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6",[7122,7126, 7128,7127]], #removed 7126, 7128
   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6_MOREMASK",[7129,7130,7131,7132,7133]],
   ["W7R1Cz__IDB80_ITHR3_SUB50_PWELL6",[7123,7125]],
   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6_S5_S6",[7134]],
   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6_VRESET_30",[7135]],
   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6_VRESET_20",[7136]],
   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6_VRESET_55",[7137]],
   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6_90MASK",[7138, 7139 ]], # removed 7139, 7140

#in general W4R4 runs have problem with y-residuals
# also low stats... in short, this data looks like shit
   ["W4R1__IDB127_ITHR127_SUB6_PWELL6" , [7148,7149]], # Y residuals
   ["W4R1__IDB127_ITHR127_SUB7_PWELL6" , [7150,7151]], #residuals 
   ["W4R1__IDB127_ITHR127_SUB8_PWELL6" , [7152,7153]], # Y residuals
   ["W4R1__IDB127_ITHR127_SUB9_PWELL6" , [7154,7155]], #residuals 
   ["W4R1__IDB127_ITHR127_SUB10_PWELL6" , [7156,7157]],  # Y residuals
   ["W4R1__IDB127_ITHR127_SUB11_PWELL6" , [7158,7162]],  # Y residuals
   ["W4R1__IDB127_ITHR127_SUB12_PWELL6" , [7160,7161]],  # Y residuals
   ["W4R1__IDB127_ITHR127_SUB13_PWELL6" , [7163,7164]], #Y residuals fucked
   ["W4R1__IDB127_ITHR30_SUB10_PWELL6" , [7166]], #7166 different mask & efficiency, residuals bad
   ["W4R1__IDB127_ITHR10_SUB10_PWELL6" , [7167]],   # Y residuals

   ["W9R0Cz__IDB127_ITHR127_SUB6_PWELL6" , [7169,7170]],
   ["W9R0Cz__IDB127_ITHR127_SUB12_PWELL6" , [7171,7172]],
   ["W9R0Cz__IDB127_ITHR127_SUB15_PWELL6" , [7173,7174]],
   ["W9R0Cz__IDB127_ITHR127_SUB20_PWELL6" , [7175,7176]],
   ["W9R0Cz__IDB127_ITHR127_SUB25_PWELL6" , [7177,7178]],
   ["W9R0Cz__IDB127_ITHR127_SUB30_PWELL6" , [7179,7180]],
   ["W9R0Cz__IDB127_ITHR127_SUB35_PWELL6" , [7181,7182]],
   ["W9R0Cz__IDB127_ITHR127_SUB40_PWELL6" , [7183,7184]],
   ["W9R0Cz__IDB127_ITHR127_SUB45_PWELL6" , [7185,7186]],
   ["W9R0Cz__IDB127_ITHR127_SUB50_PWELL6" , [7187,7188]],
   ["W9R0Cz__IDB127_ITHR30_SUB50_PWELL6" , [7195,7196,7199]],
   ["W9R0Cz__IDB127_ITHR30_SUB50_PWELL6_MOREMASK" , [7200,7201,7202,7203, 7204]],
   ["W9R0Cz__IDB127_ITHR3_SUB50_PWELL6" , [7197,7198]],
  
   ["W7R0Cz__IDB127_ITHR127_SUB6_PWELL6_NEW" , [7206]],
   ["W7R0Cz__IDB127_ITHR127_SUB12_PWELL6_NEW" , [7207]],
   ["W7R0Cz__IDB127_ITHR127_SUB15_PWELL6_NEW" , [7208]],
   ["W7R0Cz__IDB127_ITHR127_SUB20_PWELL6_NEW" , [7209]],
   ["W7R0Cz__IDB127_ITHR127_SUB25_PWELL6_NEW" , [7210]],
   ["W7R0Cz__IDB127_ITHR127_SUB30_PWELL6_NEW" , [7211]],
   ["W7R0Cz__IDB127_ITHR127_SUB35_PWELL6_NEW" , [7212]],
   ["W7R0Cz__IDB127_ITHR127_SUB40_PWELL6_NEW" , [7213]],
   ["W7R0Cz__IDB127_ITHR127_SUB45_PWELL6_NEW" , [7214]],
   ["W7R0Cz__IDB127_ITHR127_SUB50_PWELL6_NEW" , [7215,7216]],
   ["W7R0Cz__IDB127_ITHR127_SUB50_PWELL6_NEW_MOREMASK" , [7217,7218]], # and 7219 (different masks)
 
   ["W7R12Cz__IDB50_ITHR20_SUB12_PWELL6" , [7223]],
   ["W7R12Cz__IDB50_ITHR20_SUB20_PWELL6" , [7224]],
   ["W7R12Cz__IDB50_ITHR20_SUB30_PWELL6" , [7225]],
   ["W7R12Cz__IDB50_ITHR20_SUB40_PWELL6" , [7226]],
   ["W7R12Cz__IDB50_ITHR20_SUB6_PWELL6" , [7227]],
   ["W7R12Cz__IDB30_ITH3_SUB40_PWELL6" , [7228]],
   ["W7R12Cz__IDB50_ITHR20_SUB9_PWELL6" , [7229]],
   ["W7R12Cz__IDB50_ITH80_SUB40_PWELL6" , [7230]],
   ["W7R12Cz__IDB50_ITH127_SUB40_PWELL6" , [7231]],
   ["W7R12Cz__IDB127_ITH127_SUB40_PWELL6" , [7232]],

   ["W4R11Epi__IDB127_ITHR127_SUB6_PWELL6" , [7234]],
   ["W4R11Epi__IDB50_ITHR20_SUB6_PWELL6" , [7235]],
   ["W4R11Epi__IDB100_ITHR30_SUB6_PWELL6" , [7236]],
   #runs 7238 - 7243 missing?
]

'''
#########################################################################################################################
runList=[]
folderList=glob.glob(baseFolder+"run_00*")
print(folderList)
for r in folderList:
    print r
    if "gbl" in r: continue
    run=(r.split("run_00")[1])
    run=int(run.split("_")[0])
    runList.append(run)
runList.sort()

allRuns      =0
uncheckedRuns=0
for r in runList:
    allRuns+=1
    cat=""
    for c in runProp:
        for l in c[1]:
            if l==r:
                cat=c[0]
                break
        if cat!="":
            break
    print str(r)+"      "+cat     
    if cat=="": uncheckedRuns+=1
print "All runs : "+str(allRuns)
print "Unchecked: "+str(uncheckedRuns) 

print " "
##########################################################################################################################
print " "

#in the folder 'processed'
for c in runProp:
    baseName = "run__"+c[0]+".root"
    fileName=outFolder+"run__"+c[0]+".root"
    folderName=fileName.replace(".root","")
    os.system("mkdir -p "+folderName)
    hEff =ROOT.TH1D("hEff" ,"Avearge Efficiency ; ; eff [%]",len(c[1]),0,len(c[1]))
   #make the histogram
    posX =ROOT.TH1D("posX" ,"posX ; ;#DeltaX [#mum]",len(c[1]),0,len(c[1]))
   ##posX.Sumw2()
    posY =ROOT.TH1D("posY" ,"posY ; ;#DeltaY [#mum]",len(c[1]),0,len(c[1]))
   ##posY.Sumw2()

    sigX =ROOT.TH1D("sigX" ,"sigX ; ;#sigmaX [#mum]",len(c[1]),0,len(c[1]))
   ##sigX.Sumw2()
    sigY =ROOT.TH1D("sigY" ,"sigY ; ;#sigmaY [#mum]",len(c[1]),0,len(c[1]))

    line=ROOT.TLine(0,0,len(c[1]),0)
    line.SetLineColor(1)
    line.SetLineWidth(2)
    line.SetLineStyle(2)

    line1=ROOT.TLine(0,36.4,len(c[1]),36.4)
    line1.SetLineColor(2)
    line1.SetLineWidth(1)
    line1.SetLineStyle(3)

    line2=ROOT.TLine(0,-36.4,len(c[1]),-36.4)
    line2.SetLineColor(3)
    line2.SetLineWidth(1)
    line2.SetLineStyle(3)

    
    command="hadd -k -f "+fileName+" "
    count=0
    runString = ""
    for r in c[1]:
        if r in badRuns: continue
        count += 1
        hEff.GetXaxis().SetBinLabel(count,str(r))
        posX.GetXaxis().SetBinLabel(count,str(r))
        posY.GetXaxis().SetBinLabel(count,str(r))
        sigX.GetXaxis().SetBinLabel(count,str(r))
        sigY.GetXaxis().SetBinLabel(count,str(r))
        fN=baseFolder+"/run_00"+str(r)+"/ana_gbl_00"+str(r)+".root"
        #fN=baseFolder+"/run_00"+str(r)+"/ana00"+str(r)+".root"
        print fN
        if not os.path.isfile(fN):
            print "########### FILE COULD NOT BE FOUND ########"
            continue
        rF=ROOT.TFile(fN)
        resX=rF.Get("res_X")
        fit=ROOT.TF1("fitG","gaus",-200,200)
        resX.Fit(fit,"RE0")
        print fit.GetParameter(1)
        posX.SetBinContent(count,fit.GetParameter(1))
        posX.SetBinError(count,fit.GetParError(1))
        sigX.SetBinContent(count,fit.GetParameter(2))
        sigX.SetBinError(count,fit.GetParError(2))
        print "Residuals: ",count,": ",fit.GetParameter(2) 
        ##fit
        resY=rF.Get("res_Y")
        fit2=ROOT.TF1("fitG2","gaus",-200,200)
        resY.Fit(fit2,"RE0")
        print fit2.GetParameter(1)
        posY.SetBinContent(count,fit2.GetParameter(1))
        posY.SetBinError(count,fit2.GetParError(1))
        sigY.SetBinContent(count,fit2.GetParameter(2))
        sigY.SetBinError(count,fit2.GetParError(2))
    
        MAll=rF.Get("MAll")
        MPass=rF.Get("MPass")
        num=MPass.Integral() 
        den=MAll.Integral()
        print "################## num and den: ##################:"
        print num,den
        if den==0 : continue
        eff=num/den *100
        err=math.sqrt(((eff*(1-eff/100.))/100.)/num) 
        print "Run with EFF: "+str(eff)
        hEff.SetBinContent(count,eff)
        hEff.SetBinError(count,err)

        fram_E = rF.Get("fram_E")
        canFramE = ROOT.TCanvas("CFramE","FramE",800,600)
        fram_E.Draw();
        canFramE.Print(fileName.replace(".root","")+"/framE_"+str(r)+".pdf")
        canFramE.Print(fileName.replace(".root","")+"/framE_"+str(r)+".C")

        canResXY = ROOT.TCanvas("cResXY","ResXY",800,600)
        resX.Draw("")
        resY.SetMarkerColor(2)
        resY.SetLineColor(2)
        resY.Draw("same")
        fit.SetLineColor(1)
        fit.Draw("same")
        fit2.Draw("same")
        bS = ROOT.TPaveText(50,3000, 200, 2000)
        bS.AddText("ResX: %f   ResY: %f  " %(fit.GetParameter(2), fit2.GetParameter(2)))
        bS.Draw()
        canResXY.Update()
        canResXY.Print(fileName.replace(".root","")+"/resXY_hist_"+str(r)+".pdf")
        canResXY.Print(fileName.replace(".root","")+"/resXY_hist_"+str(r)+".C")
        
        rF.Close()
        

        theRun=""
        theRun=baseFolder+"run_00"+str(r)+"/ana_gbl_00"+str(r)+".root "
        #theRun=baseFolder+"run_00"+str(r)+"/ana00"+str(r)+".root "
        command+=theRun

        runString += str(r)+","

    print command
    os.system(command)
    badSt=len(badRuns)
    command='root -l -b -q FancyPlot_MALTA.C\(\\"'+baseName.replace(".root","")+'\\"\)'
    print command

    command2='root -l -b -q -x /home/sbmuser/MaltaSW/MaltaTbAnalysis/share/makeOverviewPlots.C\(\\"'+baseFolder+'\\",\{'+runString[:-1]+'\}\)'
    print command2
    os.system(command2)
    
    
    os.system(command)

    can=ROOT.TCanvas("CRes","Residuals_Average",1500,600)
    posX.SetMaximum( 150)
    posX.SetMinimum(-150)
    posX.SetMarkerSize(0.8)
    posX.SetMarkerStyle(20)
    posX.SetMarkerColor(2)
    posX.SetLineColor(2)
    posY.SetMarkerSize(0.8)
    posY.SetMarkerStyle(20)
    posY.SetMarkerColor(4)
    posY.SetLineColor(4)
    posX.Draw("E")
    line.Draw("SAMEL")
    line1.Draw("SAMEL")
    line2.Draw("SAMEL")
    posX.Draw("SAMEE")
    posY.Draw("SAMEE")
    can.Print(fileName.replace(".root","")+"/Residuals_Av.pdf")
    can.Print(fileName.replace(".root","")+"/Residuals_Av.C")

###########################################################################################################
###########################################################################################################

    can3=ROOT.TCanvas("CRes2","Residuals_RMS",1500,600)    
    sigX.SetMaximum(50)
    sigX.SetMinimum(  0)
    sigX.SetMarkerSize(0.8)
    sigX.SetMarkerStyle(20)
    sigX.SetMarkerColor(2)
    sigX.SetLineColor(2)
    sigY.SetMarkerSize(0.8)
    sigY.SetMarkerStyle(20)
    sigY.SetMarkerColor(4)
    sigY.SetLineColor(4)
    sigX.Draw("E")
    sigY.Draw("SAMEE")
    line1.Draw("SAMEL")
    can3.Print(fileName.replace(".root","")+"/Residuals_RMS.pdf")
    can3.Print(fileName.replace(".root","")+"/Residuals_RMS.C")
    
    canE=ROOT.TCanvas("CEff","Efficiency",1500,600)
    hEff.SetMaximum(100)
    hEff.SetMinimum(0)
    hEff.SetMarkerSize(0.8)
    hEff.SetMarkerStyle(20)
    hEff.SetMarkerColor(2)
    hEff.SetLineColor(2)
    hEff.Draw("E")
    l1 = ROOT.TLine(0,95,len(c[1]) , 95)
    l1.SetLineStyle(8)
    l1.Draw("same")
    canE.Update()
    canE.Print(fileName.replace(".root","")+"/Stability_Eff.pdf")
    canE.Print(fileName.replace(".root","")+"/Stability_Eff.C")
    command1="mv "+fileName+" "+fileName.replace(".root","")+"/"
    ###print command1
    os.system(command1)

 

    command2="mv /home/sbmuser/MaltaSW/MaltaTbAnalysis/FinalPlot_DESY_Oct19/overViewPlots.root "+fileName.replace(".root","")+"/"
    print command2
    os.system(command2)

print " "


sys.exit()
