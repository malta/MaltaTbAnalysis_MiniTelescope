#!/usr/bin/env python
import os
import os.path
import sys
import pexpect
import getpass
import argparse
import glob
import base64
import ROOT
import autoDQ


path     ="/home/sbmuser/MaltaSW/MaltaTbAnalysis/"
webfolder="/home/sbmuser/DESY_oct2019/web/"
os.system("mkdir -p "+webfolder)
adecmospassword = base64.b64decode("S2lnZUJlbGUxOQ==")#getpass.getpass("Enter adecmos password:")

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--ini"     , help="Initial Run number" , type=int           , required=True)
parser.add_argument("-f", "--fin"     , help="Final Run number"   , type=int           , required=True)

args=parser.parse_args()


if args.ini>args.fin:
    print " PLEASE GIVE ME initial<final " 
    print " "
    sys.exit()

for r in range(args.ini,args.fin+1):
    print r
    pathF="/home/sbmuser/MaltaSW/MaltaTbAnalysis/DESY_oct2019/run_%06i/"%(r)
    if os.path.exists(pathF):
        command="python MaltaDQ_DESY_oct_2019.py -a  -r "+str(r)
        #command="python clearRun.py -r "+str(r)
        #print command
        os.system(command)

sys.exit()
