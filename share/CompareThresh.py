#!/usr/bin/env python
import sys
import glob
import ROOT
from array import array
import math
import argparse
import os

import urllib2

def getThreshold(name, idb, ithr, sub):
  url="https://docs.google.com/spreadsheets/d/e/2PACX-1vTktc83sznl4EOBH31nS2Ykyk-04GDB52vVV-bjViCPhohAGIxMApxa27ZZxt37jYuoHM-TuvEdaskT/pub?gid=317684079&single=true&output=csv"
  response = urllib2.urlopen(url)
  lines = response.read().split("\n") # "\r\n" if needed
  separator = 'comma'
  count = -1
  ths =[0.,0.]
  for line in lines:
        count+=1
	if separator=="comma":
          cols = line.split(",")
        elif separator=="tab":
          cols = line.split("\t")
        pass
        for col in cols:
    	  cell=col.replace("\"","")
          cell=cell.replace("\\","")
        pass
     	if count > 7:
	  NAME = cols[4]
          IDB = cols[5]
          ITHR = cols[6]
	  SUB = cols[7]
	  thS2 = cols[9]
	  thS3 = cols[13]
          if NAME =='' or SUB == '': continue
	  if int(IDB) == int(idb) and int(ITHR) == int(ithr) and int(SUB) == int(sub) and NAME == name: 
 	    ths[0] = float(thS2)
            ths[1] = float(thS3)
	    break
 # print "The threshold is:",ths
  return ths


#ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(0)
#ROOT.gStyle.SetOptFit(1)


parser=argparse.ArgumentParser()
parser.add_argument("-c","--chip",nargs="+",type=str,default="W7R12")
parser.add_argument("-s2","--s2only",action='store_true')
parser.add_argument("-s3","--s3only",action='store_true')
args=parser.parse_args()

os.system("mkdir -p SummaryPlots/")
base=ROOT.TH1D("vale",";Threshold [e];Efficiency [%]",10,75,625)#31)
baseN=ROOT.TH1D("val",";Threshold [e];Noise [kHz]",10,75,625)#31)

plots=[
#  #  ["W4R1Epi" ,[7,4,ROOT.kAzure+10] ],[ROOT.kOrange, ROOT.kOrange+2, ROOT.kOrange+3, ROOT.kOrange-3, ROOT.kOrange+6, ROOT.kOrange - 6], "n- gap", ],
    ["W4R4Epi" ,[ROOT.kBlue, ROOT.kBlue+2, ROOT.kBlue-2, ROOT.kBlue+3, ROOT.kBlue-3, ROOT.kBlue+6, ROOT.kBlue - 6, ROOT.kViolet, ROOT.kViolet+2, ROOT.kViolet-2, ROOT.kViolet+3, ROOT.kViolet-3, ROOT.kViolet+6, ROOT.kViolet - 6], "n- gap", 9 ],
    ["W9R4Cz" , [ROOT.kCyan, ROOT.kCyan+2, ROOT.kCyan+3, ROOT.kCyan-3, ROOT.kCyan+6, ROOT.kCyan - 6,  ROOT.kCyan+2, ROOT.kCyan+3, ROOT.kCyan-3, ROOT.kCyan+6, ROOT.kCyan - 6], "n- gap", 55],


   # ["W4R0Epi",[ROOT.kBlue, ROOT.kBlue+2,ROOT.kBlue+3, ROOT.kBlue-3, ROOT.kBlue+6, ROOT.kBlue - 6],"n- gap", 12 ],
   # ["W7R1Cz",[ROOT.kOrange, ROOT.kOrange+2, ROOT.kOrange+3, ROOT.kOrange-3, ROOT.kOrange+6, ROOT.kOrange - 6] ,"n- cont.", 50],
   # ["W9R0Cz",[ROOT.kCyan, ROOT.kCyan+2, ROOT.kCyan+3, ROOT.kCyan-3, ROOT.kCyan+6, ROOT.kCyan - 6], "n- gap", 50],
    #["W11R0Cz",[ROOT.kMagenta, ROOT.kMagenta+2, ROOT.kMagenta+3, ROOT.kMagenta-3, ROOT.kMagenta+6, ROOT.kMagenta - 6], "extra p-well", 50 ],
   # ["W11R11Cz" ,[ROOT.kMagenta, ROOT.kMagenta+2, ROOT.kMagenta+3, ROOT.kMagenta-3, ROOT.kMagenta+6, ROOT.kMagenta - 6], "extra p-well", 50 ],
]
#base.SetMaximum(100)
#base.SetMinimum( 50)


  

base.SetMaximum( 100)
base.SetMinimum(  50)

baseN.SetMaximum( 1e4)
baseN.SetMinimum(  10)



can=ROOT.TCanvas("cEffVsSub","EffVsSub",800,600)
can.SetGridy(1)
can.DrawFrame(0,0,600,100);
base.Draw("AXIS")

canN=ROOT.TCanvas("cNoiseVsSub","NoiseVsSub",800,600)
canN.SetGridy(1)
canN.DrawFrame(0,0,600,100);
baseN.Draw("AXIS")


graphs=[]
count=-1
leg = ROOT.TLegend(0.45,0.15,0.85,0.4)
leg.SetTextSize(0.04)
for w in plots:
    count+=1
    print (" ")
    print (w[0])
    filesN=glob.glob("/home/sbmuser/MaltaSW/MaltaTbAnalysis/FinalPlot_DESY_Oct19/"+w[0]+"/*PWELL06*/*Noise.txt")
    files=glob.glob("/home/sbmuser/MaltaSW/MaltaTbAnalysis/FinalPlot_DESY_Oct19/"+w[0]+"/*PWELL06*/Eff.txt")
    files.sort()
    filesN.sort()
    print files
    print filesN
    configs=[]
    for f in files:
	#print f
        ##if "IDB127_ITH127" not in f and "W4R0" in f: continue
        conf="IDB"+f.split("__IDB")[1].split("_SUB")[0]
        sub=int(f.split("SUB")[1].split("_PWEL")[0])
        if sub != w[3]:
	  continue
        configs.append(conf)
        ths = getThreshold(w[0],f.split("__IDB")[1].split("_ITH")[0],f.split("_ITH")[1].split("_SUB")[0] ,  w[3])
    configs=list(set(configs))
    configs.sort()
    tmpC=[]
    for i in range(0,len(configs)):
        #print (i)
        tmpC.append(configs[len(configs)-1-i])
    configs=tmpC
    print (configs)
    countC=-1
    gS2=ROOT.TGraphErrors()
    gS3=ROOT.TGraphErrors()
    gN2 = ROOT.TGraphErrors()
    gN3 = ROOT.TGraphErrors()
    graphs.append(gS2)
    graphs.append(gS3)
    graphs.append(gN2)
    graphs.append(gN3)
    gS2.SetLineWidth(2)
    gN2.SetLineWidth(2)
    gN3.SetLineWidth(2)
    gS3.SetLineWidth(2)
    gS2.SetLineColor(w[1][0])
    gS3.SetLineColor(w[1][0])
    gN2.SetLineColor(w[1][0])
    gN3.SetLineColor(w[1][0])
    gS3.SetLineStyle(2)
    gN2.SetLineStyle(1)
    gN3.SetLineStyle(1)
    gS2.SetMarkerColor(w[1][0])
    gS3.SetMarkerColor(w[1][0])
    gN2.SetMarkerColor(w[1][0])
    gN3.SetMarkerColor(w[1][0])
    gS2.SetMarkerSize(1.4)
    gS3.SetMarkerSize(1.4)
    gN2.SetMarkerSize(1.4)
    gN3.SetMarkerSize(1.4)
    gS2.SetMarkerStyle(24)
    gS3.SetMarkerStyle(21)
    gN2.SetMarkerStyle(24)
    gN3.SetMarkerStyle(21)
    if w[2] =="extra p-well": 
          gN2.SetMarkerStyle(26)
          gS2.SetMarkerStyle(26)
    if w[2] =="n- gap": 
          gN2.SetMarkerStyle(25)
          gS2.SetMarkerStyle(25)
    for c in configs:
        print (c)
        countC+=1
   

        for f in files:
            if c not in f: continue
            print (f)
            sub=float(f.split("SUB")[1].split("_PW")[0])
            if sub != w[3]:
	      continue

   	    ths = getThreshold(w[0],f.split("__IDB")[1].split("_ITH")[0],f.split("_ITH")[1].split("_SUB")[0] ,  w[3])
            fN = open(filesN[countC])
            fN.readline()
            noiseLine = fN.readline().split(",")
            noise = float(noiseLine[0])/1e3 # noise in MHz
            print "The file is ",filesN[countC]
            print "The number is: ",countC
            print "#######################################"
            print "The noise rate is ",noise
            print "#######################################"
            noiseErr = float( noiseLine[1])/1e3             
            #print "THRESHOLD = ",ths
            fr=open(f,"r")
            left =fr.readline().split(" ")
            right=fr.readline().split(" ")
            effL=left[1]
            effR=right[1]
            errL=left[3].split("\n")[0]
            errR=right[3].split("\n")[0]
            #print ("SUB= "+str(sub)+" --> EFF_L: "+str(effL)+" +/- "+str(errL))

            if "_S3" not in f:
                print "THRESHOLD = ",ths
                print "Eff = ",effL
                if ths[0] == 0: continue
                gS2.SetPoint(gS2.GetN(),float(ths[0]),float(effL))
                gS2.SetPointError(gS2.GetN()-1,0.01,float(errL))
                if noise == 0: continue
                gN2.SetPoint(gN2.GetN(), ths[0], float(noise))
               # print "inf for the tgraph: n",gS2.GetN(),", x: ",gS2.GetPointX(gS2.GetN()),", y: ",gS2.GetPointY(gS2.GetN())
            if "_S2" not in f:
                if ths[1] == 0: continue
                print "THRESHOLD = ",ths
                print "Eff = ",effR
                gS3.SetPoint(gS3.GetN(),float(ths[1]),float(effR))
                gS3.SetPointError(gS3.GetN()-1,0.01,float(errR))
                if noise == 0: continue
                gN3.SetPoint(gN3.GetN(), ths[1], float(noise))
            fr.close()
    leg.AddEntry(gS3,"%s, SUB = %d V"%(w[0], w[3] ),"lp")
    can.cd()
    #gS3.Draw("PL")
    gS2.Draw("PL")
    canN.cd()
    gN2.Draw("PL")
    #gN3.Draw("PL")

can.cd()
leg.Draw()
can.Update()
can.Print("SummaryPlots/EffVThresh.pdf")
can.SaveAs("SummaryPlots/EffVThresh.C")

canN.cd()
leg.Draw()
canN.SetLogy(1)
canN.Update()
canN.Print("SummaryPlots/NoiseVThresh.pdf")





###################################################################################################################################################################

