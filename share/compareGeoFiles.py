#!/usr/bin/env python

import ROOT
import argparse
import os.path
import os
import time

parser = argparse.ArgumentParser()
parser.add_argument('-d','--directory',help='directory of runs',default="/eos/user/a/adecmos/TB_SPS_April18/processed/")
parser.add_argument('-r','--run',nargs='+',help='run numbers: [start] [end]',type=int,required=True)
parser.add_argument('-c','--coarse',help='TelCoarse-geo File, else: TelFine-Geo',action='store_true')
parser.add_argument('-p','--planes',help='Number of planes',type=int,default=7)
args = parser.parse_args()

offset={}
unit_u={}
unit_v={}
runlist=[]
for run in xrange(args.run[0],args.run[1]+1):
    offset[run]={}
    unit_u[run]={}
    unit_v[run]={}
    for id in xrange(args.planes+1):
        if not os.path.isdir(args.directory+"run_%06d"%run): continue
        if args.coarse and os.path.isfile(args.directory+"run_%06d/TelCoarse-%06d-geo.toml"%(run,run)):
            fr = open(args.directory+"run_%06d/TelCoarse-%06d-geo.toml"%(run,run))
            print "run_%06d/TelCoarse-%06d-geo.toml"%(run,run)
        elif os.path.isfile(args.directory+"run_%06d/TelFine-geo.toml"): 
            fr = open(args.directory+"run_%06d/TelFine-geo.toml")
        else:continue
        runlist.append(run)
        offset[run][id]=[]
        unit_u[run][id]=[]
        unit_v[run][id]=[]
        lines=[]
        stopOffset=stopUnit_u=stopUnit_v=False
        for line in fr.readlines():
            line = line.strip()
            if len(line) == 0: continue
            if ("offset" in line) and (stopOffset == True):
                for coord in xrange(3):
                    offset[run][id].append(float(line.split("[")[1].split("]")[0].split(",")[coord].strip(" ")))
                stopOffset=False
                stopUnit_u=True
            if ("unit_u" in line) and (stopUnit_u == True):
                for coord in xrange(3):
                    unit_u[run][id].append(float(line.split("[")[1].split("]")[0].split(",")[coord].strip(" ")))
                stopUnit_u=False
                stopUnit_v=True
            if ("unit_v" in line) and (stopUnit_v == True):
                for coord in xrange(3):
                    unit_v[run][id].append(float(line.split("[")[1].split("]")[0].split(",")[coord].strip(" ")))
                    stopUnit_v=False
            if "id = "+str(id) in line:
                stopOffset=True
            pass
        fr.close()

if 0==0:
    types=["offset","unit_u","unit_v"]
    c1={}
    mg = {}
    lg = {}
    gx = {}
    gy = {}
    gz = {}
    shifts = []
    for type in types:
        c1[type] = ROOT.TCanvas("c%s"%type,"%s"%type,800,600)
        c1[type].DivideSquare(args.planes)
        mg[type] = {}
        lg[type] = {}
        gx[type] = {}
        gy[type] = {}
        gz[type] = {}
        for id in xrange(args.planes+1):
            mg[type][id]=ROOT.TMultiGraph()
            mg[type][id].SetTitle("Plane %s;run;#Delta in x,y,z"%id)
            lg[type][id] = ROOT.TLegend(0.743961,0.629529,0.991921,0.883152)
            lg[type][id].SetLineWidth(0)
            lg[type][id].SetFillStyle(0)
            c1[type].cd(id+1)
            gx[type][id] = ROOT.TGraph()
            gy[type][id] = ROOT.TGraph()
            gz[type][id] = ROOT.TGraph()
            gx[type][id].SetLineColor(1)
            gy[type][id].SetLineColor(2)
            gz[type][id].SetLineColor(3)
            lg[type][id].AddEntry(gx[type][id],"x","L")
            lg[type][id].AddEntry(gy[type][id],"y","L")
            lg[type][id].AddEntry(gz[type][id],"z","L")
            for run in sorted(offset):
                if run not in runlist:continue
                if len(offset[run][id])==0:
                    continue
                if type == "offset":
                    gx[type][id].SetPoint(gx[type][id].GetN(),run,(offset[run][id][0])-(offset[runlist[0]][id][0]))
                    gy[type][id].SetPoint(gy[type][id].GetN(),run,(offset[run][id][1])-(offset[runlist[0]][id][1]))
                    gz[type][id].SetPoint(gz[type][id].GetN(),run,(offset[run][id][2])-(offset[runlist[0]][id][2]))
                    if (((offset[run][id][0])-(offset[runlist[0]][id][0]) !=0) or ((offset[run][id][1])-(offset[runlist[0]][id][1]) !=0) or ((offset[run][id][2])-(offset[runlist[0]][id][2]) !=0)) and (run not in shifts):shifts.append(run)
                if type == "unit_u":
                    gx[type][id].SetPoint(gx[type][id].GetN(),run,(unit_u[run][id][0])-(unit_u[runlist[0]][id][0]))
                    gy[type][id].SetPoint(gy[type][id].GetN(),run,(unit_u[run][id][1])-(unit_u[runlist[0]][id][1]))
                    gz[type][id].SetPoint(gz[type][id].GetN(),run,(unit_u[run][id][2])-(unit_u[runlist[0]][id][2]))
                    if (((unit_u[run][id][0])-(unit_u[runlist[0]][id][0]) !=0) or ((unit_u[run][id][1])-(unit_u[runlist[0]][id][1]) !=0) or ((unit_u[run][id][2])-(unit_u[runlist[0]][id][2]) !=0)) and (run not in shifts):shifts.append(run)
                if type == "unit_v":
                    gx[type][id].SetPoint(gx[type][id].GetN(),run,(unit_v[run][id][0])-(unit_v[runlist[0]][id][0]))
                    gy[type][id].SetPoint(gy[type][id].GetN(),run,(unit_v[run][id][1])-(unit_v[runlist[0]][id][1]))
                    gz[type][id].SetPoint(gz[type][id].GetN(),run,(unit_v[run][id][2])-(unit_v[runlist[0]][id][2]))
                    if (((unit_v[run][id][0])-(unit_v[runlist[0]][id][0]) !=0) or ((unit_v[run][id][1])-(unit_v[runlist[0]][id][1]) !=0) or ((unit_v[run][id][2])-(unit_v[runlist[0]][id][2]) !=0)) and (run not in shifts):shifts.append(run)
            mg[type][id].Add(gx[type][id])
            mg[type][id].Add(gy[type][id])
            mg[type][id].Add(gz[type][id])
            mg[type][id].Draw("APL")
            lg[type][id].Draw("L")
            #mg[type][id].GetXaxis().LabelsOption("v")
        c1[type].Update()

    print "\nThe following runs show a shift in position: \n",shifts
    raw_input("")
