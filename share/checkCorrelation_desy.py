#!/usr/bin/env python
########################################################
# Make two correlation plots
# check if there is desync without assumption of alignment
# Based on the code by Valerio.Dao@cern.ch
# Carlos.Solans@cern.ch
# May 2018
#######################################################

import os
import sys
import ROOT

#ROOT.TStyle().SetBatch(1)

def getSize(plane):
    #if plane==7: return [ -0.5,   15.5,-0.5, 63.5 ]
    #if plane==0: return [149.5,  240.5,149.5,300.5 ] #123.5,  255.5,99.5, 300.5] 
    return [0.5, 511.5,0.5, 511.5]

def checkCorrelation(args):

    ROOT.gStyle.SetOptStat(0)
    #SetAtlasStyle();
    ROOT.gStyle.SetPalette(1)
    ROOT.gStyle.SetPadRightMargin(0.15)
    
    fr=ROOT.TFile("../cosmics_tests/run_%06i/Merged-%06i.root"%(args.run,args.run))
  
    myT0 = fr.Get("Plane"+str(args.first)+"/Hits")
    myT1 = fr.Get("Plane"+str(args.second)+"/Hits")
    myT  = fr.Get("Event")
    print myT0
    print myT1
    print myT
    myT.AddFriend( myT0, "P0" )
    myT.AddFriend( myT1, "P1" )
    
    val1=getSize(args.first)
    val2=getSize(args.second)

    print "Do plot for X"
    #hX=ROOT.TH2D("All","All",40, 179.5, 220.5, 16, -0.5, 15.5)
    #hX=ROOT.TH2D("All","All",1200, -0.5, 1199.5,1200, -0.5, 1199.5)
    #hX=ROOT.TH2D("All","All",1200, -0.5, 1199.5,300, -0.5, 299.5)
    hX=ROOT.TH2D("All","All",int(val1[1]-val1[0]),val1[0],val1[1], int(val2[1]-val2[0]),val2[0],val2[1] )
    hX.SetTitle(";Plane "+str(args.first)+" X [int]; Plane "+str(args.second)+" X [int];")
    myT.Draw("P1.PixX:P0.PixX>>All" ,
             "P1.NHits!=0 && P0.NHits!=0 ", ##&& P1.PixX!=192 && P0.PixY>100 && P0.PixX>600 && (P0.PixY>200 || P1.PixY<150)",
             "goff",
             args.nevents,args.initial) #50000);
    
    print "Do plot for Y"
    #hY=ROOT.TH2D("All2","All2",100, 149.5, 250.5,64,-0.5,63.5)
    #hY=ROOT.TH2D("All2","All2",1200, -0.5, 1199.5,64,-0.5,63.5)
    hY=ROOT.TH2D("All2","All2",int(val1[3]-val1[2]),val1[2],val1[3], int(val2[3]-val2[2]),val2[2],val2[3] )
    hY.SetTitle(";Plane "+str(args.first)+" Y [int]; Plane "+str(args.second)+" Y [int];")
    myT.Draw("P1.PixY:P0.PixY>>All2",
             "P1.NHits!=0 && P0.NHits!=0 ", ##&& P1.PixX!=192 && P0.PixY>100 && P0.PixX>600 && (P0.PixY>200 || P1.PixY<150)",
             "goff",
             args.nevents,args.initial) #50000);

    c1=ROOT.TCanvas("X","X",1600,600);
    c1.Divide(2,1)
    c1.cd(1)
    #c1.cd(1).SetLogz()
    fa1 = ROOT.TF1("fa1","(x-187.5)", 179.5, 220.5);
    hX.Rebin2D(2,2)
    hX.Draw("COLZ")
    hX.SetMaximum(1)
    fa1.SetLineColor(1)
    fa1.SetLineWidth(2)
    fa1.SetLineStyle(2)
    #fa1.Draw("LSAME");
    c1.cd(2)
    #c1.cd(2).SetLogz()
    fa2 = ROOT.TF1("fa1","(x-175.5)",149.5, 250.5);
    hY.Rebin2D(2,2)
    hY.SetMaximum(1)
    hY.Draw("COLZ")
    fa2.SetLineColor(1)
    fa2.SetLineWidth(2)
    fa2.SetLineStyle(2)
    #fa2.Draw("LSAME");
    folderName="Corr_"+str(args.run)
    os.system("mkdir -p "+folderName)
    c1.Print(folderName+"/run_"+str(args.run)+"_"+str(args.first)+"_"+str(args.second)+".pdf")
    
    print ("CorrX: "+str( hX.GetCorrelationFactor()*100 )+"   ,  CorrY: "+str( hY.GetCorrelationFactor()*100 ) )

    raw_input("press any key to continue...")

    pass

###########################################################################################################
if __name__=="__main__":
    import argparse

    parser=argparse.ArgumentParser()
    ##parser.add_argument('-f' ,'--file',help="Merged file")
    parser.add_argument('-r' ,'--run'     ,help="Merged file"   , type=int, default=666)
    parser.add_argument('-n' ,'--nevents' ,help="First n events", type=int, default=100000)
    parser.add_argument('-i' ,'--initial' ,help="starting point", type=int, default=0)
    parser.add_argument('-pA','--first'   ,help="first plane"   , type=int, default=0)
    parser.add_argument('-pB','--second'  ,help="second plane"  , type=int, default=0)
    args=parser.parse_args()

    checkCorrelation(args)
    print "Have a stupid day"
    
