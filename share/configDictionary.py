#dictionary of runs to plot y extents for
configD = []

configD.append({'chip':"W4R4" ,'sub':9, 'idb':127,'ithr':30,'ths2':204, 'ths3':213})
configD.append({'chip':"W4R4" ,'sub':9, 'idb':100,'ithr':3,'ths2':166, 'ths3':176})
configD.append({'chip':"W4R4" ,'sub':9, 'idb':127,'ithr':10,'ths2':188, 'ths3':197})
configD.append({'chip':"W4R4" ,'sub':9, 'idb':127,'ithr':3,'ths2':182, 'ths3':191})


#configD.append({'chip':"W9R4Cz" ,'sub':55, 'idb':127,'ithr':127,'ths2':100, 'ths3':100})
#configD.append({'chip':"W9R4Cz" ,'sub':55, 'idb':100,'ithr':80,'ths2':100, 'ths3':100})
#configD.append({'chip':"W9R4Cz" ,'sub':55, 'idb':80,'ithr':80,'ths2':100, 'ths3':100})
#configD.append({'chip':"W9R4Cz" ,'sub':55, 'idb':100,'ithr':60,'ths2':100, 'ths3':100})
#configD.append({'chip':"W9R4Cz" ,'sub':55, 'idb':127,'ithr':30,'ths2':100, 'ths3':100})
#configD.append({'chip':"W9R4Cz" ,'sub':55, 'idb':100,'ithr':30,'ths2':100, 'ths3':100})

#configD.append({'chip':"W7R1Cz" ,'sub':50, 'idb':127,'ithr':127,'ths2':388, 'ths3':385})
configD.append({'chip':"W7R1Cz" ,'sub':50, 'idb':127,'ithr':30,'ths2':296, 'ths3':295})
#configD.append({'chip':"W7R1Cz" ,'sub':50, 'idb':100,'ithr':30,'ths2':279, 'ths3':276})
configD.append({'chip':"W7R1Cz" ,'sub':50, 'idb':100,'ithr':10,'ths2':253, 'ths3':252})
configD.append({'chip':"W7R1Cz" ,'sub':50, 'idb':100,'ithr':3,'ths2':243, 'ths3':243})
configD.append({'chip':"W7R1Cz" ,'sub':50, 'idb':80,'ithr':3,'ths2':226, 'ths3':224})
configD.append({'chip':"W7R1Cz" ,'sub':50, 'idb':127,'ithr':10,'ths2':100, 'ths3':100})


configD.append({'chip':"W4R0" ,'sub':0, 'idb':127,'ithr':127,'ths2':368, 'ths3':365})
configD.append({'chip':"W4R0" ,'sub':0, 'idb':127,'ithr':80,'ths2':335, 'ths3':332})
configD.append({'chip':"W4R0" ,'sub':0, 'idb':127,'ithr':30,'ths2':288, 'ths3':283})
configD.append({'chip':"W4R0" ,'sub':0, 'idb':100,'ithr':30,'ths2':275, 'ths3':268})
configD.append({'chip':"W4R0" ,'sub':0, 'idb':70,'ithr':30,'ths2':242, 'ths3':238})


configD.append({'chip':"W9R4Cz" ,'sub':55, 'idb':127,'ithr':127,'ths2':271, 'ths3':234})
#configD.append({'chip':"W9R4Cz" ,'sub':55, 'idb':80,'ithr':30,'ths2':226, 'ths3':190}) # missing?
configD.append({'chip':"W9R4Cz" ,'sub':55, 'idb':100,'ithr':80,'ths2':226, 'ths3':190}) # missing?


configD.append({'chip':"W11R0Cz" ,'sub':50, 'idb':127,'ithr':127,'ths2':382, 'ths3':361})
configD.append({'chip':"W11R0Cz" ,'sub':50, 'idb':127,'ithr':30,'ths2':294, 'ths3':277})
configD.append({'chip':"W11R0Cz" ,'sub':50, 'idb':100,'ithr':30,'ths2':258, 'ths3':257})

configD.append({'chip':"W7R4" ,'sub':12, 'idb':70,'ithr':30,'ths2':258, 'ths3':257})


configD.append({'chip':"W11R11Cz" ,'sub':12, 'idb':127,'ithr':100,'ths2':499, 'ths3':506})





  



'''



   ["W11R0Cz__IDB127_ITHR127_SUB50_PWELL6",[6956, 6957]],
   ["W11R0Cz__IDB127_ITHR127_SUB54_PWELL6",[6958]],
   ["W11R0Cz__IDB127_ITHR127_SUB52_PWELL6",[6959]],
   ["W11R0Cz__IDB127_ITHR30_SUB50_PWELL6",[ 6963, 6964, 6965, 6966, 6967, 6968, 6969]], # do we want pre-masking run, no you dont
   ["W11R0Cz__IDB100_ITHR30_SUB50_PWELL6",[ 

"W9R4Cz__IDB127__ITHR127_SUB50_PWELL6",[7036,7037]],
   ["W9R4Cz__IDB127__ITHR127_SUB55_PWELL6",[7039]],
   ["W9R4Cz__IDB127__ITHR127_SUB55_PWELL6_S2",[7040,7041,7042,7043]],
   ["W9R4Cz__IDB100__ITHR80_SUB55_PWELL6_SEC3",[7044,7045,7046,7047,7050]],
   ["W9R4Cz__IDB80__ITHR30_SUB55_PWELL6_ROI",[7051]], # 7056 removed for alignment , 7051 only one with good efficiency


   ["W9R0Cz__IDB127_ITHR127_SUB50_PWELL6" , [7187,7188]],
   ["W9R0Cz__IDB127_ITHR30_SUB50_PWELL6" , [7195,7196,7199]],
   ["W9R0Cz__IDB127_ITHR30_SUB50_PWELL6_MOREMASK" , [7200,7201,7202,7203, 7204]],
   ["W9R0Cz__IDB127_ITHR3_SUB50_PWELL6" , [7197,7198]],


NEED the threshold scans for w9r0cz



"W7R1Cz__IDB127_ITHR127_SUB50_PWELL6",[7115, 7116]],
   ["W7R1Cz__IDB127_ITHR30_SUB50_PWELL6",[7117, 7118]],
   ["W7R1Cz__IDB127_ITHR10_SUB50_PWELL6",[7119, 7120]], #missing threshold scan?
   ["W7R1Cz__IDB100_ITHR10_SUB50_PWELL6",[7121]],
   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6",[7122,7126, 7128,7127]], #removed 7126, 7128
   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6_MOREMASK",[7129,7130,7131,7132,7133]],
   ["W7R1Cz__IDB80_ITHR3_SUB50_PWELL6",[7123,7125]],
   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6_S5_S6",[7134]], #maybe should be excluded?
#   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6_VRESET_30",[7135]],
#   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6_VRESET_20",[7136]],
#   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6_VRESET_55",[7137]],
#   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6_90MASK",[7138, 7139 ]], # removed 7139, 7140

'''


