#!/usr/bin/env python
import os
import os.path
import sys
import pexpect
import getpass
import argparse
import glob
import base64
import ROOT
import autoDQ


path     ="/home/sbmuser/MaltaSW/MaltaTbAnalysis/DESY_oct2019/"

parser = argparse.ArgumentParser()
parser.add_argument("-r", "--run"     , help="Run number"              , type=int           , required=True)
args=parser.parse_args()
run=args.run

folder=path+("/run_%06i/"%run)
print folder

os.system("rm "+folder+"*.toml")
os.system("rm "+folder+"Track*")
os.system("rm "+folder+"*hists*")
os.system("rm "+folder+"proc*.root")
os.system("rm "+folder+"ana*.root")
os.system("rm "+folder+"DQ*")
