#!/usr/bin/env python

import os
import argparse
import ROOT

def autoDQ(input, output, thres=0.3):

    fr=ROOT.TFile(input)
    fram_E = fr.Get("fram_E")
    fram_A = fr.Get("fram_A")
    avg=ROOT.TH1F("avg","avg",110,0,1.1)
    
    goodFraction=0
    enoughAboveThreshold=0
    good=True
    prev=True
    shape=[[0,True],]
    
    for b in range(fram_E.GetNbinsX()):
        if prev==True and fram_E.GetBinContent(b+1)<thres:
            prev=False
            shape.append([fram_E.GetBinCenter(b+1),False])
            pass
        elif prev==False and fram_E.GetBinContent(b+1)>=thres:
            prev=True
            shape.append([fram_E.GetBinCenter(b+1),True ])
            pass
        if fram_E.GetBinContent(b+1)<thres: good=False
        else:
            goodFraction+=1
            if prev==True: enoughAboveThreshold+=1
            if fram_E.GetBinError(b+1)<0.3:
                avg.Fill(fram_E.GetBinContent(b+1))
            pass
        pass
    shape.append([fram_E.GetBinCenter(fram_E.GetNbinsX()),False])
    
    ###print "Number of hits above threshold: "+str( enoughAboveThreshold )
    if enoughAboveThreshold<20: 
        ##nasty hack 
        shape[0][1]=False

    fw=open(output,"w")
    fw.write("Status: %r\n"%good)
    fw.write("AvgEff: %4.2f%%\n"%(avg.GetMean()*100))
    fw.write("Usable fraction: %4.0f%%\n"%( float(goodFraction)/float(fram_E.GetNbinsX())*100 ) )
    fw.write("Number of tracks in fiducial: "+str(fram_A.Integral())+"\n" )
    print ("Average Efficiency: %4.2f%%\n"%( avg.GetMean()*100 ))
    print ("Usable fraction   : %4.0f%%\n"%( float(goodFraction)/float(fram_E.GetNbinsX())*100 ))
    for step in shape:
        fw.write("Shape: %i : %r\n"%(step[0],step[1]))
        pass
    fw.close()
    fr.Close()

    return good

if __name__=='__main__':
    parser=argparse.ArgumentParser()
    parser.add_argument("-i","--input",help="Input file", required=True)
    parser.add_argument("-o","--output",help="Output file", required=True)
    args=parser.parse_args()
    autoDQ(args.input,args.output)
    pass
