#!/usr/bin/env python
import sys
import glob
import ROOT
from array import array
import math
import argparse
import os

import urllib2

def getThreshold(name, idb, ithr, sub):
  url="https://docs.google.com/spreadsheets/d/e/2PACX-1vTktc83sznl4EOBH31nS2Ykyk-04GDB52vVV-bjViCPhohAGIxMApxa27ZZxt37jYuoHM-TuvEdaskT/pub?gid=317684079&single=true&output=csv"
  response = urllib2.urlopen(url)
  lines = response.read().split("\n") # "\r\n" if needed
  separator = 'comma'
  count = -1
  ths =[0.,0.]
  for line in lines:
        count+=1
	if separator=="comma":
          cols = line.split(",")
        elif separator=="tab":
          cols = line.split("\t")
        pass
        for col in cols:
    	  cell=col.replace("\"","")
          cell=cell.replace("\\","")
        pass
     	if count > 7:
	  NAME = cols[4]
          IDB = cols[5]
          ITHR = cols[6]
	  SUB = cols[7]
	  thS2 = cols[9]
	  thS3 = cols[13]
          if NAME =='' or SUB == '': continue
	  if int(IDB) == int(idb) and int(ITHR) == int(ithr) and int(SUB) == int(sub) and NAME == name: 
 	    ths[0] = float(thS2)
            ths[1] = float(thS3)
	    break
 # print "The threshold is:",ths
  return ths


#ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(0)
#ROOT.gStyle.SetOptFit(1)


parser=argparse.ArgumentParser()
parser.add_argument("-c","--chip",nargs="+",type=str,default="W7R12")
parser.add_argument("-s2","--s2only",action='store_true')
parser.add_argument("-s3","--s3only",action='store_true')
args=parser.parse_args()

os.system("mkdir -p SummaryPlots/")
base=ROOT.TH1D("vale",";Threshold [e];Efficiency [%]",10,150,450)#31)

#plots=[
#    ["W4R12Epi",[1] ],
#    ["W4R0Epi" ,[ROOT.kGreen+3,ROOT.kSpring+4,ROOT.kGreen-7,3] ],
#    ["W4R1Epi" ,[7,4,ROOT.kAzure+10] ],
#    ["W4R4Epi" ,[2,ROOT.kRed-7,ROOT.kPink-9,ROOT.kOrange+5,ROOT.kOrange-3,ROOT.kOrange,ROOT.kMagenta,ROOT.kMagenta+8] ],
#]
#base.SetMaximum(100)
#base.SetMinimum( 50)


plots=[
   # ["W7R8Cz" ,[2,ROOT.kRed-7,ROOT.kPink-9,ROOT.kOrange+5,ROOT.kOrange-3,ROOT.kOrange,ROOT.kMagenta,ROOT.kMagenta+8] ],
    #["W11R8Cz",[2,50,ROOT.kGreen+3,ROOT.kSpring+4,ROOT.kGreen-7,3,ROOT.kSpring+10,ROOT.kGreen-1,ROOT.kTeal+1,ROOT.kTeal+4,ROOT.kTeal+5,ROOT.kTeal+6,ROOT.kTeal+7,ROOT.kTeal+8] ],
   # ["W4R0Epi" ,[2,12, ROOT.kRed] ],
    ["W4R4Epi" ,[2,9, ROOT.kBlue+2] ],
   # ["W4R1Epi" ,[2,10, ROOT.kBlack] ],
    #["W7R8Epi" ,[2,60, ROOT.kOrange] ],
    #["W11R8Epi" ,[2,60, ROOT.kGreen+2] ],
   # ["W9R1Cz" ,[2,50, ROOT.kOrange] ],
   # ["W7R0Cz" ,[2,50, ROOT.kOrange-7] ],
   # ["W7R1Cz" ,[2,50, ROOT.kMagenta+1] ],
    ["W9R4Cz" ,[2,55, ROOT.kGreen+1] ],
   # ["W9R0Cz" ,[2,50, ROOT.kCyan+3] ],
   # ["W11R0Cz" ,[2,50, ROOT.kViolet+3] ],


    #["W11R11Cz" ,[2,50,ROOT.kRed-7,ROOT.kPink-9,ROOT.kOrange+5,ROOT.kOrange-3,ROOT.kOrange,ROOT.kMagenta,ROOT.kMagenta+8] ],
    #["W9R0Cz" ,[2,55,ROOT.kRed-7,ROOT.kPink-9,ROOT.kOrange+5,ROOT.kOrange-3,ROOT.kOrange,ROOT.kMagenta,ROOT.kMagenta+8] ],
    #["W4R0Cz" ,[2,12,ROOT.kRed-7,ROOT.kPink-9,ROOT.kOrange+5,ROOT.kOrange-3,ROOT.kOrange,ROOT.kMagenta,ROOT.kMagenta+8] ],  
#  ["W9R1Cz" ,[2,ROOT.kRed-7,ROOT.kPink-9,ROOT.kOrange+5,ROOT.kOrange-3,ROOT.kOrange,ROOT.kMagenta,ROOT.kMagenta+8] ],
   # ["W9R4Cz" ,[2,ROOT.kRed-7,ROOT.kPink-9,ROOT.kOrange+5,ROOT.kOrange-3,ROOT.kOrange,ROOT.kMagenta,ROOT.kMagenta+8] ],
]
  

base.SetMaximum( 100)
base.SetMinimum(  50)


can=ROOT.TCanvas("cEffVsSub","EffVsSub",800,600)
can.SetGridy(1)
can.DrawFrame(0,0,600,100);
base.Draw("AXIS")
graphs=[]
count=-1
leg = ROOT.TLegend(0.5,0.2,0.85,0.4)
leg.SetTextSize(0.04)
for w in plots:
    count+=1
    print (" ")
    print (w[0])

    files=glob.glob("/home/sbmuser/MaltaSW/MaltaTbAnalysis/FinalPlot_DESY_Oct19/"+w[0]+"/*PWELL06*/Eff.txt")
    files.sort()
    configs=[]
    for f in files:
	#print f
        ##if "IDB127_ITH127" not in f and "W4R0" in f: continue
        conf="IDB"+f.split("__IDB")[1].split("_SUB")[0]
        sub=int(f.split("SUB")[1].split("_PWEL")[0])
        if sub != w[1][1]:
	  continue
        configs.append(conf)
        ths = getThreshold(w[0],f.split("__IDB")[1].split("_ITH")[0],f.split("_ITH")[1].split("_SUB")[0] ,  w[1][1])
    configs=list(set(configs))
    configs.sort()
    tmpC=[]
    for i in range(0,len(configs)):
        #print (i)
        tmpC.append(configs[len(configs)-1-i])
    configs=tmpC
    print (configs)
    countC=-1
    gS2=ROOT.TGraphErrors()
    gS3=ROOT.TGraphErrors()
    graphs.append(gS2)
    graphs.append(gS3)
    gS2.SetLineWidth(2)
    gS3.SetLineWidth(2)
    gS2.SetLineColor(w[1][2])
    gS3.SetLineColor(w[1][2])
    gS3.SetLineStyle(2)
    gS2.SetMarkerColor(w[1][2])
    gS3.SetMarkerColor(w[1][2])
    gS2.SetMarkerSize(1.4)
    gS3.SetMarkerSize(1.4)
    gS2.SetMarkerStyle(20)
    gS3.SetMarkerStyle(21)
    for c in configs:
        print (c)
        countC+=1
   
        countF=-1
        for f in files:
            if c not in f: continue
            print (f)
            sub=float(f.split("SUB")[1].split("_PW")[0])
            if sub != w[1][1]:
	      continue
            countF+=1
   	    ths = getThreshold(w[0],f.split("__IDB")[1].split("_ITH")[0],f.split("_ITH")[1].split("_SUB")[0] ,  w[1][1])
            #print "THRESHOLD = ",ths
            fr=open(f,"r")
            left =fr.readline().split(" ")
            right=fr.readline().split(" ")
            effL=left[1]
            effR=right[1]
            errL=left[3].split("\n")[0]
            errR=right[3].split("\n")[0]
            #print ("SUB= "+str(sub)+" --> EFF_L: "+str(effL)+" +/- "+str(errL))
            if "_S3" not in f:
                print "THRESHOLD = ",ths
                print "Eff = ",effL
                if ths[0] == 0: 
                  print "OH MY SWEET LORD WE ARE MISSING A THRESHOLD SCAN!?"
                  print "The configuration is ",f
                  continue
                  
                gS2.SetPoint(gS2.GetN(),float(ths[0]),float(effL))
                gS2.SetPointError(gS2.GetN()-1,0.01,float(errL))
                gS2.Draw("PL")
               # print "inf for the tgraph: n",gS2.GetN(),", x: ",gS2.GetPointX(gS2.GetN()),", y: ",gS2.GetPointY(gS2.GetN())
            if "_S2" not in f:
                if ths[1] == 0: 
                  print "OH MY SWEET LORD WE ARE MISSING A THRESHOLD SCAN!?"
                  print "The configuration is ",f
                  continue
                print "THRESHOLD = ",ths
                print "Eff = ",effR
                gS3.SetPoint(gS3.GetN(),float(ths[1]),float(effR))
                gS3.SetPointError(gS3.GetN()-1,0.01,float(errR))
            fr.close()
    leg.AddEntry(gS3,"%s, SUB = %d V"%(w[0], w[1][1] ),"lp")
    gS3.Draw("PL")
    gS2.Draw("PL")
        #if count == 0:
        #  gS3.Draw("PL")
        #  gS2.Draw("PL")
	#else :
	#  gS3.Draw("PL&same")
        #  gS2.Draw("PL&same")

leg.Draw()
can.Update()
can.Print("SummaryPlots/EffVThresh2e15.pdf")








###################################################################################################################################################################

