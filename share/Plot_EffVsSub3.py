import sys
import glob
import ROOT
from array import array
import math
import argparse
import os

#gROOT.LoadMacro("~/AtlasStyleBase/AtlasStyle.C")
#gROOT.LoadMacro("~/AtlasStyleBase/AtlasUtils.C")
#SetAtlasStyle()

#ROOT.gStyle.SetOptStat(0)
#ROOT.gStyle.SetPalette(1)
#ROOT.gROOT.SetBatch(1)
#ROOT.gStyle.SetEndErrorSize(10)

parser=argparse.ArgumentParser()
parser.add_argument("-o","--outdir",type=str,default="/home/sbmuser/MaltaSW/MaltaTbAnalysis/FinalPlot_DESY_Oct19/output/")
parser.add_argument("-i","--input",nargs='+',type=str,default=["W7R12"])
args=parser.parse_args()

files = []
for r, d, f in os.walk("/home/sbmuser/MaltaSW/MaltaTbAnalysis/FinalPlot_DESY_Oct19/output/"):
    for file in f:
        #if args.input not in file: continue
        files.append(os.path.join(r, file))
files = sorted(files)


for f in args.input:
    print f

cEffvsSub=ROOT.TCanvas("cEffVsSub","EffVsSub",600,600)
mgEffvsSub=ROOT.TMultiGraph()

legend=ROOT.TLegend(0.282609,0.134146,0.884615,0.357143)
legend.SetTextFont(42)
legend.SetTextSize(0.038)
legend.SetFillColor(0)
legend.SetLineColor(0)
legend.SetFillStyle(0)
legend.SetBorderSize(0)

color=0
for f in files:
    f2 = f.split(".root")[0].split("__")[1]
    print "file: ",f,f2
    if f2 in args.input:
        print "opening: ",f
        fr = ROOT.TFile.Open(f)
        g=fr.Get("%s"%f2)
        g.SetLineColor(color)
        g.Draw("APL")
        mgEffvsSub.Add(g)
        color+=1
        pass
    pass

mgEffvsSub.Draw("APL")
cEffvsSub.Update()
raw_input("")
sys.exit()

samples=[]
color=1
marker=20
sublg=0
for plot in args.input:

    
    samples.append(sample)
    #color+=1
    pass

legend.AddEntry(gEffvsSub, "%s, IDB %s, ITHR %s"%(sample,idb,ithr), "P")
gEffvsSub.SetMarkerStyle(marker)
gEffvsSub.SetLineColor(color)
gEffvsSub.SetMarkerColor(color)
gEffvsSub.SetTitle("blaba;SUB [V]; Efficiency %")
gEffvsSub.SetMarkerSize(1.2)
gEffvsSub.SetLineWidth(2)
gEffvsSub.Draw("APL")

legend.Draw()
cEffvsSub.Update()
cEffvsSub.SaveAs("cEffvsSub.pdf")
gEffvsSub.SaveAs("%sgEffvsSub_%s.root"%(args.outdir,args.input))

raw_input("")
