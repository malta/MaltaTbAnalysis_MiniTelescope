#!/usr/bin/env python

import os
import sys
import ROOT
import argparse
import numpy as np

import urllib2

def getThreshold(name, idb, ithr, sub):
  url="https://docs.google.com/spreadsheets/d/e/2PACX-1vTktc83sznl4EOBH31nS2Ykyk-04GDB52vVV-bjViCPhohAGIxMApxa27ZZxt37jYuoHM-TuvEdaskT/pub?gid=317684079&single=true&output=csv"
  response = urllib2.urlopen(url)
  lines = response.read().split("\n") # "\r\n" if needed
  separator = 'comma'
  count = -1
  ths =[0.,0.]
  for line in lines:
        count+=1
	if separator=="comma":
          cols = line.split(",")
        elif separator=="tab":
          cols = line.split("\t")
        pass
        for col in cols:
    	  cell=col.replace("\"","")
          cell=cell.replace("\\","")
        pass
     	if count > 7:
	  NAME = cols[4]
          IDB = cols[5]
          ITHR = cols[6]
	  SUB = cols[7]
	  thS2 = cols[9]
	  thS3 = cols[13]
          if NAME =='' or SUB == '': continue
	  if int(IDB) == int(idb) and int(ITHR) == int(ithr) and int(SUB) == int(sub) and NAME == name: 
 	    ths[0] = float(thS2)
            ths[1] = float(thS3)
	    break
 # print "The threshold is:",ths
  return ths


parser=argparse.ArgumentParser()
parser.add_argument("-r","--rotation",help="rotate by 0,90,180,270 degrees",type=int,default=0)
parser.add_argument("-f","--files",help="list of files with histograms",nargs="*")
parser.add_argument("-o","--output",help="base name of outputs",type=str,default="test")
parser.add_argument("-p","--position",help="list of positions",nargs="*",type=int)
parser.add_argument("-n","--name",help="name of primitive in root file",default="hEff")
args=parser.parse_args()

ROOT.gStyle.SetPalette(ROOT.kDarkBodyRadiator)
ROOT.gStyle.SetOptStat(0)
ROOT.TColor.InvertPalette();

nChipsX=8
nChipsY=7
sWaferX=710
sWaferY=857

cWafer=ROOT.TCanvas("Wafer","Wafer",int(sWaferY*1.2),sWaferY)
cWafer.SetLeftMargin(0.12)
cWafer.SetRightMargin(0.18)


hWafer=ROOT.TH2F("hWafer","Wafer %s  Efficiency; Pix X; Pix Y; Efficiency [%%]" % args.output,nChipsX*sWaferX/5,0,nChipsX*sWaferX,nChipsY*sWaferY/5,0,nChipsY*sWaferY)
hBase=ROOT.TH2F("hBase","Wafer %s  Efficiency; Pix X; Pix Y; Efficiency [%%]" % args.output,40, -300, -100, 43, 100, 430 )
hWafer.GetZaxis().SetTitleOffset(1.5)
hWafer.GetZaxis().SetLabelOffset(.01)
xMin = 500
xMax = 3000
yMin = 2500
yMax = 5200
hWafer.GetYaxis().SetRangeUser(yMin, yMax)
hWafer.GetXaxis().SetRangeUser(xMin, xMax)




hWafer.SetStats(0)
hWafer.SetMaximum(100)
hWafer.SetMinimum(80)
if args.output=="W4": 
  hWafer.SetMinimum(50)
if args.name=="MMatch_clSize_pyx":
  hWafer.SetMaximum(1.7)
  hWafer.SetMinimum(1)
  if args.output=="W7": 
    hWafer.SetMaximum(2)
  if args.output=="W4": 
    hWafer.SetMaximum(1.3)
  hWafer.SetTitle("Wafer %s Cluster Size;Pix X; Pix Y;Cluster Size" % args.output)
hWafer.Draw()

xCenter = nChipsX/2.0*sWaferX
yCenter = nChipsY/2.*sWaferY
radius = nChipsX/2.*sWaferX
phi  =  np.arccos((yMax -yCenter)/(radius*0.99))* 180./np.pi
theta = np.arccos((xCenter - xMin)/(radius*0.99))* 180./np.pi
phi95  =  np.arccos((yMax -yCenter)/(radius*0.95))* 180./np.pi
theta95 = np.arccos((xCenter - xMin)/(radius*0.95))* 180./np.pi
print "phi = ",phi
print "theta = ",theta
eOut = ROOT.TArc(xCenter , yCenter,radius *0.99, 90+phi, 180 -theta  )
eOut.SetLineColor(ROOT.kBlack)
eOut.Draw("only")
eOut2 = ROOT.TArc(xCenter, yCenter, radius*0.95, 90 + phi95, 180-theta95)
eOut2.SetLineColor(ROOT.kBlack)
eOut2.Draw("only")

#draw lines
print   radius *(1. + np.sin(theta * np.pi / 180.  ))
tline=ROOT.TLine()
for xi in xrange(nChipsX+1): 
  theta95 = np.arccos((xCenter - xi * sWaferX)/(radius*0.95))* 180./np.pi
  if xi * sWaferX < xMin or xi *sWaferX >xMax: continue 
  yLim = radius *(1. + np.sin(theta95 * np.pi / 180.  )) 
  tline.DrawLine(max(sWaferX*xi,xMin), max(0,yMin), min(sWaferX*xi, xMax),  min( yLim , min(sWaferY*nChipsY, yMax))  )
for yi in xrange(nChipsY+1): 
  phi95  =  np.arccos((yi*sWaferY -yCenter)/(radius*0.95))* 180./np.pi
  if yi * sWaferY < yMin or yi *sWaferY >yMax: continue 
  xLim = xCenter - radius * np.sin(phi95 * np.pi/180.)
  print "xLimit: ",xLim,yLim
  tline.DrawLine(max(xLim,  max(0, xMin)), max(sWaferY*yi,yMin), min(sWaferX*nChipsX, xMax), min(sWaferY*yi, yMax))

#write names
names=[ ["","  ","  ","  ","  ","  ","  ",""],
        ["","  ","22","23","24","25","  ",""],
        ["","16","17","18","19","20","21",""],
        ["","10","11","12","","","",""],
        [""," 4"," 5"," 6"," "," "," ",""],
        ["","  "," 0"," 1"," "," ","  ",""],
        ["","  ","  ","  ","  ","  ","  ",""], 
      ]
      
tlatex=ROOT.TLatex()
for xi in xrange(nChipsX): 
    for yi in xrange(nChipsY): 
        tlatex.DrawLatex(sWaferX*xi,sWaferY*yi,names[yi][xi])
        pass
    pass

fluence = ROOT.TLatex()
fluence.SetTextSize(0.02)
fluence.DrawLatex(sWaferX*2.01,sWaferY*5.85, "\phi = 1e15")
fluence.DrawLatex(sWaferX*3.01,sWaferY*5.85, "\phi = 1e15")
fluence.DrawLatex(sWaferX*1.01,sWaferY*4.85, "\phi = 2e15")
#fluence.DrawLatex(sWaferX*5.01,sWaferY*4.85, "\phi = 5e15")


cWafer.Update()

#place histograms
shift = [
[ sWaferX*3/5 , sWaferY*5/5  ], #reticle 0
[ sWaferX*4/5 , sWaferY*5/5  ], #reticle 1
[ sWaferX*5/5 , sWaferY*5/5  ], #,reticle 2
[ sWaferX*6/5 , sWaferY*5/5  ], #reticle 3
[ sWaferX*2/5 , sWaferY*4/5  ], #reticle 4
[ sWaferX*3/5 , sWaferY*4/5  ], #reticle 5 
[ sWaferX*4/5 , sWaferY*4/5  ], #reticle 6 
[ sWaferX*5/5 , sWaferY*4/5  ], #reticle 7 
[ sWaferX*6/5 , sWaferY*4/5  ], #reticle 8 
[ sWaferX*7/5 , sWaferY*4/5  ], #reticle 9 
[ sWaferX*2/5 , sWaferY*3/5  ], #reticle 10 
[ sWaferX*3/5 , sWaferY*3/5  ], #reticle 11 
[ sWaferX*4/5 , sWaferY*3/5  ], #reticle 12 
]


mem=[]
thText = ROOT.TLatex()
thText.SetTextSize(0.02)
subText = ROOT.TLatex()
subText.SetTextSize(0.02)

cChip=ROOT.TCanvas("Chip","Chip",int(sWaferY*1.2),sWaferY)
cChip.SetLeftMargin(0.12)
cChip.SetRightMargin(0.18)
for i in range(len(args.files)):
    cWafer.cd()
    print("Open file: %s" % args.files[i])
    config =args.files[i].split("/")[-2]
    idb =  config.split("IDB")[1].split("_")[0]
    ith =  config.split("ITH")[1].split("_")[0]
    sub =  config.split("SUB")[1].split("_")[0]
    wName = args.files[i].split("/")[-3]
    if config == "run__W7R12Cz__IDB70_ITH030_SUB25_PWELL06": 
      thresh = getThreshold(wName, idb, ith, 30)
      print "using the sub from the scan with bias = 30 V"
    elif config == "run__W4R1Epi__IDB127_ITH127_SUB13_PWELL06": 
      thresh = getThreshold(wName, idb, ith, 10)
      print "using the sub from the scan with bias = 10 V"
    else:       thresh = getThreshold(wName, idb, ith, sub)
    print config 
  #  ith = config.split("ITH",1).split("_",0)
  #  sub = config.split("SUB",1).split("_",0)thresh = getThreshold(wName, idb, ith, sub)
    th = np.mean(thresh)
    if th == 0: th = "N/A"
    else: th = "%.0f e" %th 
    fr=ROOT.TFile(args.files[i])
    print("Get object: %s" % args.name)
    h2=fr.Get(args.name)
    #place histogram
    print("Place in reticle: %i" % args.position[i])
    pos=args.position[i]
    xLim = h2.GetNbinsX()
    yLim = h2.GetNbinsY()
    xShift = shift [int(args.position[i])][ 0]
    yShift = shift [int(args.position[i])][ 1]
    print "Drawing the SUB and THRES text??"
    subText.DrawLatex(xShift*5 - sWaferX *0.97, yShift*5 + sWaferY*0.7, "SUB = %s V" % sub)
    thText.DrawLatex( xShift*5 - sWaferX * 0.97, yShift*5 + sWaferY*0.55, "Th = %s" % th)
    meanEff = 0 
    count = 0
    #get threshold from google sheet
    #print threshold next to reticle labels
    for xi in range(xLim):
      for yi in range(yLim):
        Eff = h2.GetBinContent(xi, yi)
        if Eff>0 :
          meanEff+= Eff
          count += 1.
    pass
    meanEff/=count
    hChip = ROOT.TH2F("Chip","Chip %s Efficiency; Pix X; Pix Y; Efficiency [%%]" % wName,sWaferX/(5),-sWaferX, 0, sWaferY/(5),0,sWaferY)
    hChip.SetMaximum(100)
    hChip.SetMinimum(80)
    if args.output=="W4": hChip.SetMinimum(50) 
    if args.name=="MMatch_clSize_pyx":
      hChip.SetTitle("Chip %s Cluster Size; Pix X; Pix Y; Cluster Size" % args.output)
      hChip.SetMaximum(1.7)
      hChip.SetMinimum(1)
      if args.output=="W7": hChip.SetMaximum(1.7) 
      if args.output=="W4": 
        hChip.SetMaximum(1.3)
    
   
    for xi in range(xLim):
      for yi in range(yLim):
        Eff = h2.GetBinContent(xi, yi)
        #Eff= float(Eff/meanEff * 100.0)
        if Eff>0 :
          hWafer.SetBinContent(-xi+ xShift, yi +yShift, Eff ) 
          hChip.SetBinContent(sWaferX/(5)-xi, yi, Eff ) 

    cChip.cd()
  #  hBase.Draw("A")
    hChip.GetXaxis().SetRangeUser(-270,-130)
    hChip.GetYaxis().SetRangeUser(130,420)
    hChip.GetZaxis().SetTitleOffset(1.5)
    hChip.GetZaxis().SetLabelOffset(.01)
    hChip.Draw("colz")
    cChip.Print("SummaryPlots/FancyPlots/"+wName+"_"+args.name+"_map.pdf")
    cChip.SaveAs("SummaryPlots/FancyPlots/"+wName+"_"+args.name+"_map.C")
    pass
   
cWafer.cd()
hWafer.Draw("colz && same")
cWafer.Update()

cWafer.Print("SummaryPlots/"+args.output+"_"+args.name+"_wafer_map.pdf")
cWafer.Print("SummaryPlots/"+args.output+"_"+args.name+"_wafer_map.png")
cWafer.SaveAs("SummaryPlots/"+args.output+"_"+args.name+"_wafer_map.C")

#raw_input("press any key to continue...")

print("Have a nice day")

