import os
import sys
import glob
import ROOT
import math
import numpy as np

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(1)
ROOT.gStyle.SetOptFit(1)

badRuns=[]
baseFolder="/home/sbmuser/MaltaSW/MaltaTbAnalysis/DESY_oct2019/"
outFolder ="/home/sbmuser/MaltaSW/MaltaTbAnalysis/FinalPlot_DESY_Oct19/"


'''
runProp = [  
    #["W7R8Cz__IDB100_ITH030_SUB60_PWELL06",[7007,7008]],
   #["W11R0Cz__IDB127_ITH127_SUB50_PWELL06",[6956, 6957]], 
   # ["W7R12Cz__IDB70_ITH030_SUB25_PWELL06"   , [6736]],   
   # ["W4R12Epi__IDB70_ITH030_SUB06_PWELL06"   , [6713,6714,6715,6716,6717,6719]], 
   # ["W9R11Cz__IDB70_ITH030_SUB12_PWELL06"   , [6752, 6753, 6754]], # worse residuals with 5 GeV?!
    ["W11R11Cz__IDB127_ITH100_SUB12_PWELL06"  , [6765,6766,6770]],

            #wafer plot
            #["W4R0Epi__IDB127_ITH127_SUB12_PWELL06" , [6788,6789]],
            #["W4R4Epi__IDB127_ITH127_SUB12_PWELL06",[7063,7064]],
            
            #["W7R0Cz__IDB127_ITH030_SUB50_PWELL06_S3", [6866,6867]],
            #["W7R1Cz__IDB127_ITH030_SUB50_PWELL06",[7117, 7118]],
            #["W7R8Cz__IDB060_ITH030_SUB60_PWELL06",[7011,7012]],
            
            #["W9R0Cz__IDB127_ITH127_SUB50_PWELL06" , [7187,7188]],   
            #["W9R1Cz__IDB127_ITH127_SUB50_PWELL06", [6892, 6893, 6894, 6895, 6896, 6897]],
            #["W9R4Cz__IDB127_ITH127_SUB55_PWELL06",[7038,7039,7040,7041,7042,7043]],

            #["W11R0Cz__IDB127_ITH127_SUB54_PWELL06",[6958]],
            #["W11R8Cz__IDB127_ITH030_SUB60_PWELL06",[6997,6998]],
            #  ["W9R1Cz__IDB127_ITH030_SUB50_PWELL06_LARGEFIDUCIAL", [6911,6912,6913,6914,6915,6916,6917,6918,6919,6920,6921,6922,6923,6924]],
            #  ["W7R1Cz__IDB100_ITH003_SUB50_PWELL06_MOREMASK",[7129,7130,7131,7132,7133]],
            #  ["W7R4Epi__IDB070_ITH030_SUB06_PWELL06"    , [6699,6700,6701,6702]],
             # ["W4R4Epi__IDB127_ITH003_SUB10_PWELL06_S2",[7094]],
]


runProp = [    ["W7R4Epi__ALL", [6689,6690,6691,6692,6693,6694,6695,6696,6697,6698,6699,6700,6701,6702,6703,6704,6705,6706,6707,6708,6709,6710,6711,6712]],
    ["W7R4Epi__IDB127_ITH100_SUB06_PWELL06"    , [6689,6690          ]],
    ["W7R4Epi__IDB127_ITH030_SUB06_PWELL06"    , [6691,6692,6693,6694,6695,6696,6697,6698]],
    ["W7R4Epi__IDB070_ITH030_SUB06_PWELL06"    , [6699,6700,6701,6702]],
    ["W7R4Epi__IDB070_ITH030_SUB20_PWELL06"    , [6703,6704,6705     ]], 
    ["W7R4Epi__IDB070_ITH030_SUB12_PWELL06"    , [6706,6707,6708,6709]],
    ["W7R4Epi__IDB070_ITH030_SUB30_PWELL06"    , [6710,6711,6712     ]],


    ["W4R12Epi__ALL", [6713,6714,6715,6716,6717,6719,6720,6721]],
    ["W4R12Epi__IDB70_ITH030_SUB06_PWELL06"   , [6713,6714,6715,6716,6717,6719]],
    ["W4R12Epi__IDB70_ITH030_SUB09_PWELL06"   , [6720,6721]],


    ["W7R12Cz__ALL" , [6722,6723,6724,6725,6726,6729,6730,6732,6733,6734,6735,6736,6737]],
    ["W7R12Cz__IDB70_ITH030_SUB06_PWELL06"   , [6722,6723]],
    ["W7R12Cz__IDB70_ITH030_SUB12_PWELL06"   , [6724,6725,6726]],
    ["W7R12Cz__IDB70_ITH030_SUB20_PWELL06"   , [6729,6730,6734]],
    ["W7R12Cz__IDB70_ITH030_SUB30_PWELL06"   , [6732,6733]],
    ["W7R12Cz__IDB70_ITH030_SUB16_PWELL06"   , [6735]],
    ["W7R12Cz__IDB70_ITH030_SUB25_PWELL06"   , [6736]],
    ["W7R12Cz__IDB70_ITH030_SUB09_PWELL06"   , [6737]],
    

    ["W9R11Cz__ALL" , [6738,6739,6740,6741,6742,6743,6744,6745,6746,6747,6748,6749,6750,6751,6752,6753,6754]],
    ["W9R11Cz__IDB70_ITH030_SUB06_PWELL06"   , [6738,6739,6740,6741,6742,6743,6744,6745]],
    ["W9R11Cz__IDB70_ITH030_SUB09_PWELL06"   , [6746, 6747, 6748, 6749 ,6750, 6751]],
    ["W9R11Cz__IDB70_ITH030_SUB12_PWELL06"   , [6752, 6753, 6754]], # worse residuals with 5 GeV?!
    

    ["W11R11Cz__ALL", [6756,6757,6758,6759,6761,6762,6763,6764,6765,6766,6768,6769,6770,6771,6775,6779,6780,6781,6782]], 
    ["W11R11Cz__IDB070_ITH030_SUB06_PWELL06"  , [6756]]     ,
    ["W11R11Cz__IDB100_ITH030_SUB06_PWELL06"  , [6757]]     ,
    ["W11R11Cz__IDB127_ITH127_SUB06_PWELL06"  , [6780,6781]],
    ["W11R11Cz__IDB127_ITH030_SUB06_PWELL06"  , [6758]], #poor efficiency
    ["W11R11Cz__IDB127_ITH100_SUB06_PWELL06"  , [6759,6761,6762,6768,6779]], 
    ["W11R11Cz__IDB127_ITH100_SUB08_PWELL06"  , [6769]], 
    ["W11R11Cz__IDB127_ITH100_SUB09_PWELL06"  , [6763,6764]],     
    ["W11R11Cz__IDB127_ITH100_SUB12_PWELL06"  , [6765,6766,6770]],
    ["W11R11Cz__IDB127_ITH100_SUB13_PWELL06"  , [6771, 6775]],
    ["W11R11Cz__IDB127_ITH100_SUB13p5_PWELL06", [6773,6774]], 
    ["W11R11Cz__IDB127_ITH100_SUB06_PWELL02"  , [6782]],


    ["W4R0Epi__ALL" , [6785,6786,6787,6788,6789,6790,6791,6792,6793,6794,6795,6796,6797,6798,6799,6800,6801,6802,6803,6804,6805,6806,6807,6808,6809,6810,6811,6812,6813,6814,6815,6816,6817,6818,6819]], 
    ["W4R0Epi__IDB127_ITH127_SUB06_PWELL06" , [6785,6786,6797]], 
    ["W4R0Epi__IDB127_ITH127_SUB07_PWELL06" , [6796]],
    ["W4R0Epi__IDB127_ITH127_SUB08_PWELL06" , [6795]],
    ["W4R0Epi__IDB127_ITH127_SUB09_PWELL06" , [6794]],
    ["W4R0Epi__IDB127_ITH127_SUB12_PWELL06" , [6788,6789]],
    ["W4R0Epi__IDB127_ITH127_SUB16_PWELL06" , [6792,6793]],
    ["W4R0Epi__IDB127_ITH127_SUB18_PWELL06" , [6798]],
    ["W4R0Epi__IDB127_ITH127_SUB20_PWELL06" , [6790,6791]],
    ["W4R0Epi__IDB127_ITH080_SUB12_PWELL06" , [6799,6800,6801,6802]],
    ["W4R0Epi__IDB127_ITH030_SUB12_PWELL06" , [6803,6804,6805,6806,6808,6809,6810,6811,6818,6819]], 
    ["W4R0Epi__IDB100_ITH030_SUB12_PWELL06" , [6813,6814]],
    ["W4R0Epi__IDB127_ITH030_SUB06_PWELL06" , [6817]],
    ["W4R0Epi__IDB127_ITH030_SUB06_PWELL02" , [6815]],
    ["W4R0Epi__IDB127_ITH030_SUB06_PWELL04" , [6816]],


    ["W4R1Epi__ALL", [7148,7149,7150,7151,7152,7153,7154,7155,7156,7157,7158,7160,7162,7163,7164,7165,7166,7167,7168]],
    ["W4R1Epi__IDB127_ITH127_SUB06_PWELL06" , [7148,7149]], 
    ["W4R1Epi__IDB127_ITH127_SUB07_PWELL06" , [7150,7151]],  
    ["W4R1Epi__IDB127_ITH127_SUB08_PWELL06" , [7152,7153]], 
    ["W4R1Epi__IDB127_ITH127_SUB09_PWELL06" , [7154,7155]],  
    ["W4R1Epi__IDB127_ITH127_SUB10_PWELL06" , [7156,7157]], 
    ["W4R1Epi__IDB127_ITH127_SUB11_PWELL06" , [7158,7162]],  
    ["W4R1Epi__IDB127_ITH127_SUB12_PWELL06" , [7160,7161]], 
    ["W4R1Epi__IDB127_ITH127_SUB13_PWELL06" , [7163,7164,7165]], 
    ["W4R1Epi__IDB127_ITH030_SUB10_PWELL06" , [7166]], 
    ["W4R1Epi__IDB127_ITH010_SUB10_PWELL06" , [7167]],
    ["W4R1Epi__IDB127_ITH010_SUB10_PWELL02" , [7168]],

    ["W4R4Epi__ALL",[7057,7058,7059,7060,7061,70627063,7064,7065,7066,7067,7068,7069,7070,7071,7072,7073,7074,7075,7076,7077,7078,7079,7080,7081,7082,7083,7084,7085,7086,7087,7088,7089,7090,7091,7092,7093,7094,7095, 7096 ]],
    ["W4R4Epi__IDB127_ITH127_SUB06_PWELL06",[7057,7058, 7059, 7060]],
    ["W4R4Epi__IDB127_ITH127_SUB09_PWELL06",[7061,7062]],
    ["W4R4Epi__IDB127_ITH127_SUB12_PWELL06",[7063,7064]],
    ["W4R4Epi__IDB127_ITH127_SUB15_PWELL06",[7065,7066]],
    ["W4R4Epi__IDB127_ITH127_SUB18_PWELL06",[7067,7068]],
    ["W4R4Epi__IDB127_ITH127_SUB20_PWELL06",[7069,7070]],
    ["W4R4Epi__IDB127_ITH080_SUB09_PWELL06",[7071,7072]],
    ["W4R4Epi__IDB080_ITH080_SUB09_PWELL06",[7073,7074]],
    ["W4R4Epi__IDB127_ITH050_SUB09_PWELL06",[7075,7076]],
    ["W4R4Epi__IDB127_ITH050_SUB09_PWELL06_S3",[7077,7078,7079]],
    ["W4R4Epi__IDB127_ITH030_SUB09_PWELL06_S3",[7080,7081]],
    ["W4R4Epi__IDB127_ITH010_SUB09_PWELL06_S3",[7082,7083]],
    ["W4R4Epi__IDB127_ITH003_SUB09_PWELL06_S3",[7084,7085]],
    ["W4R4Epi__IDB100_ITH003_SUB09_PWELL06_S3",[7086,7087]],
    ["W4R4Epi__IDB127_ITH003_SUB06_PWELL06_S3",[7088]],
    ["W4R4Epi__IDB127_ITH003_SUB08_PWELL06_S3",[7089]],
    ["W4R4Epi__IDB127_ITH003_SUB10_PWELL06_S3",[7090]],
    ["W4R4Epi__IDB127_ITH003_SUB12_PWELL06_S3",[7091]],
    ["W4R4Epi__IDB127_ITH003_SUB14_PWELL06_S3",[7092]],
    ["W4R4Epi__IDB127_ITH003_SUB16_PWELL06_S3",[7093]],
    ["W4R4Epi__IDB127_ITH003_SUB10_PWELL06_S2",[7094]],
    ["W4R4Epi__IDB127_ITH030_SUB10_PWELL06_S2",[7095,7096]],

    
    ["W7R8Cz__ALL",[7002,7003,7004,7005,7006,7007,7008,7009,7010,7011,7012,7013,7014,7015,7016]],
    ["W7R8Cz__IDB127_ITH127_SUB06_PWELL06",[7002]],
    ["W7R8Cz__IDB127_ITH127_SUB30_PWELL06",[7003]],
    ["W7R8Cz__IDB127_ITH127_SUB50_PWELL06",[7004,7005]],
    ["W7R8Cz__IDB127_ITH127_SUB60_PWELL06",[7006]],
    ["W7R8Cz__IDB100_ITH030_SUB60_PWELL06",[7007,7008]],
    ["W7R8Cz__IDB080_ITH030_SUB60_PWELL06",[7009,7010]],
    ["W7R8Cz__IDB060_ITH030_SUB60_PWELL06",[7011,7012]],
    ["W7R8Cz__IDB040_ITH030_SUB60_PWELL06",[7013,7014,7015,7016]],


    ["W11R8Cz__ALL",[6974,6975,6976,6977,6978,6979,6980,6981,6982,6984,6984,6985,6986,6987,6988,6989,6990,6991,6992,6993,6994,6995,6996,6997,6998,6999,7000,7001]],
    ["W11R8Cz__IDB127_ITH127_SUB06_PWELL06",[6974,6975]],
    ["W11R8Cz__IDB127_ITH127_SUB09_PWELL06",[6976,6977,6978]],
    ["W11R8Cz__IDB127_ITH127_SUB12_PWELL06",[6979,6980]],
    ["W11R8Cz__IDB127_ITH127_SUB15_PWELL06",[6981,6982]],
    ["W11R8Cz__IDB127_ITH127_SUB25_PWELL06",[6984]],
    ["W11R8Cz__IDB127_ITH030_SUB30_PWELL06",[6985]],
    ["W11R8Cz__IDB127_ITH100_SUB30_PWELL06",[6986]],
    ["W11R8Cz__IDB100_ITH100_SUB30_PWELL06",[6987]],
    ["W11R8Cz__IDB127_ITH100_SUB40_PWELL06",[6988]],
    ["W11R8Cz__IDB127_ITH127_SUB60_PWELL06",[6990,6991]],
    ["W11R8Cz__IDB127_ITH110_SUB60_PWELL06",[6992]],
    ["W11R8Cz__IDB127_ITH100_SUB60_PWELL06",[6993]],
    ["W11R8Cz__IDB127_ITH090_SUB60_PWELL06",[6994]],
    ["W11R8Cz__IDB127_ITH080_SUB60_PWELL06",[6995]],
    ["W11R8Cz__IDB127_ITH070_SUB60_PWELL06",[6996]],
    ["W11R8Cz__IDB127_ITH030_SUB60_PWELL06",[6997,6998]],
    ["W11R8Cz__IDB127_ITH030_SUB55_PWELL06",[6999]],
    ["W11R8Cz__IDB127_ITH030_SUB64_PWELL06",[7000]],
    ["W11R8Cz__IDB110_ITH030_SUB64_PWELL06",[7001]],

 #  W7R0 missing runs: 6830, 6833 [kept out], 6838 (cannot align [still]), 6847, 6870
   ["W7R0Cz__ALL",
   [6820, 6821, 6822, 6823, 6824, 6825, 6826, 6827, 6828,  6829, 6830, 6831, 6832, 6834, 6835, 6836, 6837, 6839, 6840, 6841, 6842, 6843, 6844, 6845, 6846,6847, 6848, 6849, 6850, 6851, 6852,6853, 6854, 6855, 6856, 6857, 6858, 6859, 6860, 6861, 6862, 6863, 6864, 6865, 6866, 6867, 6868, 6869, 6870, 7206, 7207, 7209, 7210, 7211, 7212, 7213, 7214, 7215, 7216, 7217, 7218  ] ],
   ["W7R0Cz__IDB127_ITH127_SUB06_PWELL06", [6820]],
   ["W7R0Cz__IDB127_ITH127_SUB12_PWELL06", [6821, 6822, 6823]], #6823 slightly better residual
   ["W7R0Cz__IDB127_ITH127_SUB30_PWELL06", [6824, 6825]],
   ["W7R0Cz__IDB127_ITH127_SUB25_PWELL06", [6826, 6827]],
   ["W7R0Cz__IDB127_ITH127_SUB20_PWELL06", [6828, 6829]],
   ["W7R0Cz__IDB127_ITH127_SUB16_PWELL06", [  6831]], #inconsistent residuals, 6830 tossed for stats, low eff
   ["W7R0Cz__IDB127_ITH127_SUB09_PWELL06", [6832]], #6833]
   ##########################################################################["W7R0Cz__IDB127_ITH030_SUB30_PWELL06", [6834,6835]], drop IT!!!!!!
   ["W7R0Cz__IDB127_ITH080_SUB30_PWELL06___ALL", [  6836,6837,6839,6840]],
   ["W7R0Cz__IDB127_ITH080_SUB30_PWELL06", [ 6837,6839, 6840]], #removed other runs b/c different masking configs according to spreadsheet#[6836,6837,6839,6840]],
   ["W7R0Cz__IDB127_ITH080_SUB30_PWELL06_S2", [6841]],
   ["W7R0Cz__IDB127_ITH080_SUB30_PWELL06_S3", [6842]],
   ["W7R0Cz__IDB127_ITH030_SUB30_PWELL06_S3", [6844,6848,6849,6850,6851,6852]], #slight differences due to temperature and masking (removed 6843, 6854,6855 for masking and temp changes)
   ["W7R0Cz__IDB100_ITH030_SUB30_PWELL06_S3", [6846]], #changed from 6843 and 6844, 6845 excluded due to masking 
   ["W7R0Cz__IDB127_ITH030_SUB40_PWELL06_S3", [6853, 6856]],
   ["W7R0Cz__IDB127_ITH030_SUB45_PWELL06_S3", [6857, 6858, 6859]],
   ["W7R0Cz__IDB127_ITH030_SUB53_PWELL06_S3", [6860, 6861]],
   ["W7R0Cz__IDB127_ITH030_SUB54_PWELL06_S3", [6862, 6863]],
   ["W7R0Cz__IDB127_ITH030_SUB55_PWELL06_S3", [6864, 6865]],
   ["W7R0Cz__IDB127_ITH030_SUB50_PWELL06_S3", [6866,6867]],
   ["W7R0Cz__IDB100_ITH030_SUB50_PWELL06_S3", [6868,6869]],
   ["W7R0Cz__IDB127_ITH030_SUB16_PWELL06_S3", [6870]],#marked as red


  # [6904, 6907, 6908, 6912, 6927, 6929 (bad alignment)] 
   ["W9R1Cz__ALL",
   [6871,6872,6873,6874,6875,6876,6877,6878,6879,6880,6881,6882,6883, 6884,6885,6886,6887,6888,6889,6890,6891,6892,6893,6894,6895,6896,6897, 6898,6899,6900,6901,6902,6903,6904,6905,6906,6907,6908,6909,6910,6911, 6912,6913,6914,6915,6916,6917,6918,6919,6920,6921,6922,6923,6924,6925, 6926,6927,6928,6929,6930,6931,6932,6933,6934,6935 ] ],
   ["W9R1Cz__IDB127_ITH127_SUB06_PWELL06", [6871, 6872, 6873]],
   ["W9R1Cz__IDB127_ITH127_SUB09_PWELL06", [6874, 6875]],
   ["W9R1Cz__IDB127_ITH127_SUB12_PWELL06", [6876, 6877]],
   ["W9R1Cz__IDB127_ITH127_SUB15_PWELL06", [6878, 6879]],
   ["W9R1Cz__IDB127_ITH127_SUB20_PWELL06", [6880, 6881]],
   ["W9R1Cz__IDB127_ITH127_SUB25_PWELL06", [6882, 6883]],
   ["W9R1Cz__IDB127_ITH127_SUB30_PWELL06", [6884, 6885]],
   ["W9R1Cz__IDB127_ITH127_SUB35_PWELL06", [6886, 6887]],
   ["W9R1Cz__IDB127_ITH127_SUB40_PWELL06", [6888, 6889]],
   ["W9R1Cz__IDB127_ITH127_SUB45_PWELL06", [6890, 6891]],
   ["W9R1Cz__IDB127_ITH127_SUB50_PWELL06", [6892, 6893, 6894, 6895, 6896, 6897]],
   ["W9R1Cz__IDB127_ITH030_SUB50_PWELL06_PREV", [6898,6899]],
   ["W9R1Cz__IDB127_ITH030_SUB50_PWELL06"     , [6900,6901,6905,6906,6907,6908,6909,6910]],
   ["W9R1Cz__IDB100_ITH030_SUB50_PWELL06",  [6902]],
   ["W9R1Cz__IDB100_ITH030_SUB50_PWELL06_S3",  [6903]],
   ["W9R1Cz__IDB127_ITH030_SUB50_PWELL06_LARGEFIDUCIAL", [6911,6912,6913,6914,6915,6916,6917,6918,6919,6920,6921,6922,6923,6924]],
   ["W9R1Cz__IDB127_ITH127_SUB06_PWELL06_LARGEFIDUCIAL", [6925,6926,6927,6928,6929,6930,6931,6932,6933,6934,6935]],

    
   #7124,
  #  7134-40 seem to have some problem with the analysis  
   ["W7R1Cz__ALL", [7097,7098,7099,7100,7101,7102,7103,7104,7105, 7106,7107,7108,7109,7110,7111,7112,7113,7114,7115, 7116,7117,7118,7119,7120,7121,7122,7123,7124,7125, 7126,7127,7128,7129,7130,7131,7132,7133,7134,7135,7136,7137,7138,7139 ] ],
 #  ["W7R1Cz__IDB127_ITH127_SUB06_PWELL06",[7097, 7098]],  


   ["W7R1Cz__IDB127_ITH127_SUB12_PWELL06",[7099, 7100]],
   ["W7R1Cz__IDB127_ITH127_SUB15_PWELL06",[7101, 7102]],
   ["W7R1Cz__IDB127_ITH127_SUB20_PWELL06",[7103, 7104]],
   ["W7R1Cz__IDB127_ITH127_SUB25_PWELL06",[7105, 7106]],
   ["W7R1Cz__IDB127_ITH127_SUB30_PWELL06",[7107, 7108]],
   ["W7R1Cz__IDB127_ITH127_SUB35_PWELL06",[7109, 7110]],
   ["W7R1Cz__IDB127_ITH127_SUB40_PWELL06",[7111, 7112]],
   ["W7R1Cz__IDB127_ITH127_SUB45_PWELL06",[7113, 7114]],
   ["W7R1Cz__IDB127_ITH127_SUB50_PWELL06",[7115, 7116]],
   ["W7R1Cz__IDB127_ITH030_SUB50_PWELL06",[7117, 7118]],
   ["W7R1Cz__IDB127_ITH010_SUB50_PWELL06",[7119, 7120]],
   ["W7R1Cz__IDB100_ITH010_SUB50_PWELL06",[7121]],
   ["W7R1Cz__IDB100_ITH003_SUB50_PWELL06",[7122,7126,7127,7128]],
   ["W7R1Cz__IDB100_ITH003_SUB50_PWELL06_MOREMASK",[7129,7130,7131,7132,7133]],
   ["W7R1Cz__IDB080_ITH003_SUB50_PWELL06",[7123,7125]],
   ["W7R1Cz__IDB100_ITH003_SUB50_PWELL06_S5_S6",[7134]],
   ["W7R1Cz__IDB100_ITH003_SUB50_PWELL06_VRESET_30",[7135]],
'''
runProp = [
 # ["W7R1Cz__IDB100_ITH003_SUB50_PWELL06_VRESET_20",[7136]],
  # ["W7R1Cz__IDB100_ITH003_SUB50_PWELL06_VRESET_55",[7137]],
  #["W7R1Cz__IDB100_ITH003_SUB50_PWELL06_90MASK",[7138, 7139, 1740]],


   #7038,7048, 7049
   # problems  with 7051, 7052, 7053, 7054, 7055, 7056], fiducial region?
 #  ["W9R4Cz__ALL",[7017,7018,7019,7020,7021,7022,7023,7024,7025,7026, 7027,7028,7029,7030,7031,7032,7033,7034,7035,7036,7037,7038,7039, 7040,7041,7042,7043,7044,7045,7046,7047,7048,7049,7050,7051,7052, 7053, 7054, 7055, 7056] ],
   ["W9R4Cz__IDB127_ITH127_SUB06_PWELL06",[7017,7018,7019]],
   ["W9R4Cz__IDB127_ITH127_SUB12_PWELL06",[7020,7021]],
   ["W9R4Cz__IDB127_ITH127_SUB15_PWELL06",[7022,7023]],
   ["W9R4Cz__IDB127_ITH127_SUB20_PWELL06",[7024,7025]],
   ["W9R4Cz__IDB127_ITH127_SUB25_PWELL06",[7026,7027]],
   ["W9R4Cz__IDB127_ITH127_SUB30_PWELL06",[7028,7029]],
   ["W9R4Cz__IDB127_ITH127_SUB35_PWELL06",[7030,7031]],
   ["W9R4Cz__IDB127_ITH127_SUB40_PWELL06",[7032,7033]],
   ["W9R4Cz__IDB127_ITH127_SUB45_PWELL06",[7034,7035]],
   ["W9R4Cz__IDB127_ITH127_SUB50_PWELL06",[7036,7037]],
   ["W9R4Cz__IDB127_ITH127_SUB55_PWELL06",[7038,7039,7040,7041,7042,7043]],
   ["W9R4Cz__IDB127_ITH127_SUB55_PWELL06_S2",[7040,7041,7042,7043]],
   ["W9R4Cz__IDB100_ITH080_SUB55_PWELL06_S2",[7044,7045,7046,7047]],
   ["W9R4Cz__IDB080_ITH080_SUB55_PWELL06_S2",[7049]], # 7048 seems to create some problem...
   ["W9R4Cz__IDB100_ITH060_SUB55_PWELL06_S2",[7050]],
   ["W9R4Cz__IDB080_ITH030_SUB55_PWELL06_ROI",[7051,7052,7053,7054,7055]], # 7056 removed for alignment , 7051 only one with good efficiency



   #W9R0Cz 
   #7189-94 missing
   ["W9R0Cz__ALL",
   [7169,7170,7171,7172,7173,7174,7175,7176,7177,7178,7179, 7180,7181,7182,7183,7184,7185,7186,7187,7188,7189,7190,7191, 7192,7193,7194,7195,7196,7197,7198,7199,7200,7201,7202,7203,7204,7205]],
   ["W9R0Cz__IDB127_ITH127_SUB06_PWELL06" , [7169,7170]],
   ["W9R0Cz__IDB127_ITH127_SUB12_PWELL06" , [7171,7172]],
   ["W9R0Cz__IDB127_ITH127_SUB15_PWELL06" , [7173,7174]],
   ["W9R0Cz__IDB127_ITH127_SUB20_PWELL06" , [7175,7176]],
   ["W9R0Cz__IDB127_ITH127_SUB25_PWELL06" , [7177,7178]],
   ["W9R0Cz__IDB127_ITH127_SUB30_PWELL06" , [7179,7180]],
   ["W9R0Cz__IDB127_ITH127_SUB35_PWELL06" , [7181,7182]],
   ["W9R0Cz__IDB127_ITH127_SUB40_PWELL06" , [7183,7184]],
   ["W9R0Cz__IDB127_ITH127_SUB45_PWELL06" , [7185,7186]],
   ["W9R0Cz__IDB127_ITH127_SUB50_PWELL06" , [7187,7188]],
   ["W9R0Cz__IDB127_ITH030_SUB50_PWELL06"          , [7195,7196,7199,7204]],
   ["W9R0Cz__IDB127_ITH030_SUB50_PWELL06_MOREMASK" , [7200,7201,7202,7203,7205]],
   ["W9R0Cz__IDB127_ITH003_SUB50_PWELL06" , [7197,7198]],

  #removed: 6960, 6961, 6970 
   ["W11R0Cz_ALL",
   [6936,6937,6938,6939,6940,6941,6942,6943,6944,6945, 6946,6947,6948,6949,6950,6951,6952,6953,6954,6955,6956, 6957,6958,6959,6960,6961,6962,6963,6964,6965,6966,6967,6968,6969,6970,6971,6972,6973] ],
   ["W11R0Cz__IDB127_ITH127_SUB06_PWELL06",[6936,6937]],
   ["W11R0Cz__IDB127_ITH127_SUB09_PWELL06",[6948,6949]], # alignment problem for 6948
   ["W11R0Cz__IDB127_ITH127_SUB12_PWELL06",[6938, 6939]],
   ["W11R0Cz__IDB127_ITH127_SUB15_PWELL06",[6940, 6941]],
   ["W11R0Cz__IDB127_ITH127_SUB20_PWELL06",[6942, 6943]],
   ["W11R0Cz__IDB127_ITH127_SUB25_PWELL06",[6944, 6945]], 
   ["W11R0Cz__IDB127_ITH127_SUB30_PWELL06",[6946, 6947]],    
   ["W11R0Cz__IDB127_ITH127_SUB35_PWELL06",[6950, 6951]], #efficiency map looks like an inverted beam spot => rate becomes to high? actually true for most of these runs...
   ["W11R0Cz__IDB127_ITH127_SUB40_PWELL06",[6952, 6953]],
   ["W11R0Cz__IDB127_ITH127_SUB45_PWELL06",[6954, 6955]],
   ["W11R0Cz__IDB127_ITH127_SUB50_PWELL06",[6956, 6957]],
   ["W11R0Cz__IDB127_ITH127_SUB54_PWELL06",[6958]],
   ["W11R0Cz__IDB127_ITH127_SUB52_PWELL06",[6959]],
   ["W11R0Cz__IDB127_ITH030_SUB50_PWELL06",[ 6960,  6962, 6963, 6964, 6965, 6966, 6967, 6968, 6969]], # 6960 and 6961 habe low efficiency... possibly due to masking? do we want pre-masking run, no you dont
   ["W11R0Cz__IDB100_ITH030_SUB50_PWELL06",[ 6971, 6970, 6972, 6973]], #  6970,low efficiency


   ["W7R0Cz__IDB127_ITH127_SUB6_PWELL06_NEW" , [7206]],
   ["W7R0Cz__IDB127_ITH127_SUB12_PWELL06_NEW" , [7207]],
   ["W7R0Cz__IDB127_ITH127_SUB15_PWELL06_NEW" , [7208]],
   ["W7R0Cz__IDB127_ITH127_SUB20_PWELL06_NEW" , [7209]],
   ["W7R0Cz__IDB127_ITH127_SUB25_PWELL06_NEW" , [7210]],
   ["W7R0Cz__IDB127_ITH127_SUB30_PWELL06_NEW" , [7211]],
   ["W7R0Cz__IDB127_ITH127_SUB35_PWELL06_NEW" , [7212]],
   ["W7R0Cz__IDB127_ITH127_SUB40_PWELL06_NEW" , [7213]],
   ["W7R0Cz__IDB127_ITH127_SUB45_PWELL06_NEW" , [7214]],
   ["W7R0Cz__IDB127_ITH127_SUB50_PWELL06_NEW" , [7215,7216]],
   ["W7R0Cz__IDB127_ITH127_SUB50_PWELL06_NEW_MOREMASK" , [7217,7218]], # and 7219 (different masks)

]

#########################################################################################################################
runList=[]
folderList=glob.glob(baseFolder+"run_00*")
print(folderList)
for r in folderList:
    print r
    if "gbl" in r: continue
    run=(r.split("run_00")[1])
    run=int(run.split("_")[0])
    runList.append(run)
runList.sort()

allRuns      =0
uncheckedRuns=0
for r in runList:
    allRuns+=1
    cat=""
    for c in runProp:
        for l in c[1]:
            if l==r:
                cat=c[0]
                break
        if cat!="":
            break
    print str(r)+"      "+cat     
    if cat=="": uncheckedRuns+=1
print "All runs : "+str(allRuns)
print "Unchecked: "+str(uncheckedRuns) 

print " "
##########################################################################################################################
print " "

#in the folder 'processed'
for c in runProp:
    waferName=outFolder+"/"+c[0].split("_")[0]
    os.system("mkdir -p "+waferName)
    print waferName
    
    baseName = "run__"+c[0]+".root"
    fileName  =waferName+"/run__"+c[0]+".root"
    folderName=fileName.replace(".root","")
    os.system("mkdir -p "+folderName)
    print (folderName)
    hEff =ROOT.TH1D("hEff" ,"Avearge Efficiency ; ; eff [%]",len(c[1]),0,len(c[1]))
    hNoise =ROOT.TH1D("hNoise" ,"Avearge Noise Rate ; ; Noise [Hz]",len(c[1]),0,len(c[1]))
   #make the histogram
    posX =ROOT.TH1D("posX" ,"posX ; ;#DeltaX [#mum]",len(c[1]),0,len(c[1]))
   ##posX.Sumw2()
    posY =ROOT.TH1D("posY" ,"posY ; ;#DeltaY [#mum]",len(c[1]),0,len(c[1]))
   ##posY.Sumw2()

    sigX =ROOT.TH1D("sigX" ,"sigX ; ;#sigmaX [#mum]",len(c[1]),0,len(c[1]))
   ##sigX.Sumw2()
    sigY =ROOT.TH1D("sigY" ,"sigY ; ;#sigmaY [#mum]",len(c[1]),0,len(c[1]))

    line=ROOT.TLine(0,0,len(c[1]),0)
    line.SetLineColor(1)
    line.SetLineWidth(2)
    line.SetLineStyle(2)

    line1=ROOT.TLine(0,36.4,len(c[1]),36.4)
    line1.SetLineColor(ROOT.kGray)
    line1.SetLineWidth(1)
    line1.SetLineStyle(ROOT.kGray)

    line2=ROOT.TLine(0,-36.4,len(c[1]),-36.4)
    line2.SetLineColor(ROOT.kGray)
    line2.SetLineWidth(1)
    line2.SetLineStyle(ROOT.kGray)

    
    command="hadd -k -f "+fileName+" "
    count=0
    runString = ""
    totalNoise = 0.
    totalN = 1e-9
    meanNoise = 0.
    totalNoiseErr = 0.
    for r in c[1]:
        if r in badRuns: continue
        count += 1
        hEff.GetXaxis().SetBinLabel(count,str(r))
        hNoise.GetXaxis().SetBinLabel(count,str(r))
        posY.GetXaxis().SetBinLabel(count,str(r))
        posY.GetXaxis().SetBinLabel(count,str(r))
        sigX.GetXaxis().SetBinLabel(count,str(r))
        sigY.GetXaxis().SetBinLabel(count,str(r))
        fN=baseFolder+"/run_00"+str(r)+"/ana_gbl_00"+str(r)+".root"
        #fN=baseFolder+"/run_00"+str(r)+"/ana00"+str(r)+".root"
        print fN
        if not os.path.isfile(fN):
            print "########### FILE COULD NOT BE FOUND ########"
            continue
        rF=ROOT.TFile(fN)
        resX=rF.Get("res_X")
        fit=ROOT.TF1("fitG","gaus",-80,80)
        resX.Fit(fit,"RE0")
        print fit.GetParameter(1)
        posY.SetBinContent(count,fit.GetParameter(1))
        posX.SetBinError(count,fit.GetParError(1))
        sigX.SetBinContent(count,fit.GetParameter(2))
        sigX.SetBinError(count,fit.GetParError(2))
        print "Residuals: ",count,": ",fit.GetParameter(2) 
        ##fit
        resY=rF.Get("res_Y")
        fit2=ROOT.TF1("fitG2","gaus",-80,80)
        resY.Fit(fit2,"RE0")
        print fit2.GetParameter(1)
        posY.SetBinContent(count,fit2.GetParameter(1))
        posY.SetBinError(count,fit2.GetParError(1))
        sigY.SetBinContent(count,fit2.GetParameter(2))
        sigY.SetBinError(count,fit2.GetParError(2))
    
        MAll=rF.Get("MAll")
        MPass=rF.Get("MPass")
        num=MPass.Integral() 
        den=MAll.Integral()
        print "################## num and den: ##################:"
        print num,den
        if den==0 : continue
        eff=num/den *100
        err=math.sqrt(((eff*(1-eff/100.))/100.)/num) 
        print "Run with EFF: "+str(eff)
        hEff.SetBinContent(count,eff)
        hEff.SetBinError(count,err)

        fram_E = rF.Get("fram_E")
        numTracks = rF.Get("numTracks")
        nTrack = numTracks.GetEntries()
        canFramE = ROOT.TCanvas("CFramE","FramE",1000,600)
        fram_E.Draw();
        canFramE.Print(fileName.replace(".root","")+"/framE_"+str(r)+".pdf")
        ##canFramE.Print(fileName.replace(".root","")+"/framE_"+str(r)+".C")

        canResXY = ROOT.TCanvas("cResXY","ResXY",800,600)
        resX.GetXaxis().SetRangeUser(-100,100)
        resX.Draw("")
        resY.SetMarkerColor(2)
        resY.SetLineColor(2)
        resY.Draw("same")
        fit.SetLineColor(1)
        fit.Draw("same")
        fit2.Draw("same")
        bS = ROOT.TPaveText(50,3000, 100, 2000)
        bS.AddText("ResX: %f " %(fit.GetParameter(2)))
        bS.Draw()
        bS2 = ROOT.TPaveText(50,2000, 100, 1000)
        bS2.AddText("ResY: %f" %(fit2.GetParameter(2)))
        bS2.Draw()
        canResXY.Update()
        canResXY.Print(fileName.replace(".root","")+"/resXY_hist_"+str(r)+".pdf")
        ##canResXY.Print(fileName.replace(".root","")+"/resXY_hist_"+str(r)+".C")
        
        rF.Close()
        noiseF = ROOT.TFile("/home/sbmuser/MaltaSW/MaltaTbAnalysis/DESY_oct2019/run_00%i/run_00%i_1.root.root" %(r, r)) #get raw data for plane 1, the DUT
        timing = noiseF.Get("Timing")
        peakPos = timing.GetBinCenter(timing.GetMaximumBin())
        if peakPos < 100:
          print "Run: ",r, 
          print "Timing peak below 100ns, refine noise rate calculation!" 
          print "(But is hardcoded to 200 at the moment, check the run!)"
        peakPos = 200;
        noiseRateHz = timing.Integral(0,int(peakPos-50.0)) 
        nNoise = timing.Integral(0,int(peakPos-50.0)) 
        noiseRateHzErr = np.sqrt(noiseRateHz)
        noiseRateHz /= (peakPos-50.0) #noise per ns
        noiseRateHz /= (peakPos -50.0)#error on noise
        noiseRateHz /= nTrack #noise per ns per event
        noiseRateHz *= 1E9;  #noise per s per event  

        noiseRateHzErr /= (peakPos-50.0) #noise per ns
        noiseRateHzErr /= (peakPos -50.0)#error on noise
        noiseRateHzErr /= nTrack #noise per ns per event
        noiseRateHzErr *= 1E9;  #noise per s per event    
        
        hNoise.SetBinContent(count, noiseRateHz)
        hNoise.SetBinError(count, noiseRateHzErr)

        totalNoise += noiseRateHz * nNoise
        totalN += nNoise
        totalNoiseErr += noiseRateHzErr * noiseRateHzErr * nNoise * nNoise 
        noiseF.Close() 
    
        theRun=""
        theRun=baseFolder+"run_00"+str(r)+"/ana_gbl_00"+str(r)+".root "
        #theRun=baseFolder+"run_00"+str(r)+"/ana00"+str(r)+".root "
        command+=theRun

        runString += str(r)+","
    
    #write a text file with the 
    meanNoise = totalNoise/totalN
    stdNoise = np.sqrt(totalNoiseErr)/totalN
    noiseOut =open("%s/run__%s/Noise.txt" %(waferName,c[0]),"w+")
    noiseOut.write("Noise Rate (Hz), Error (Hz)\n")
    noiseOut.write("%f,%f" %(meanNoise, stdNoise))
    noiseOut.close()
    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"   
    print command
    os.system(command)
    badSt=len(badRuns)

    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
   # command='root -l -b -q FancyPlot_MALTA.C\(\\"'+fileName.replace(".root","")+'\\",\\"'+baseName.replace(".root","").replace("run__","")+'\\"\)'
    command='root -l -b -q FancyPlot_MALTA.C\(\\"'+fileName.replace(".root","")+'\\",\\"'+baseName.replace(".root","").replace("run__","")+'\\"\)'
    print command
    print fileName
    print baseName
    os.system(command)


    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
    #command2='root -l -b -q -x /home/sbmuser/MaltaSW/MaltaTbAnalysis/share/makeOverviewPlots.C\(\\"'+baseFolder+'\\",\{'+runString[:-1]+'\}\)'
    command2='root -l -b -q -x makeOverviewPlots.C\(\\"'+baseFolder+'\\",\{'+runString[:-1]+'\}\)'
    print command2
    os.system(command2)
       
    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
 
    can=ROOT.TCanvas("CRes","Residuals_Average",1500,600)
    ROOT.gStyle.SetOptStat(0)
    posX.SetMaximum( 50)
    posX.SetMinimum(-50)
    posX.SetMarkerSize(0.8)
    posX.SetMarkerStyle(20)
    posX.SetMarkerColor(2)
    posX.SetLineWidth(2)
    posX.SetLineColor(2)
    posY.SetMarkerSize(0.8)
    posY.SetMarkerStyle(20)
    posY.SetMarkerColor(4)
    posY.SetLineColor(4)
    posY.SetLineWidth(2)
    posX.Draw("E")
    line.Draw("SAMEL")
    line1.Draw("SAMEL")
    line2.Draw("SAMEL")
    posX.Draw("SAMEE")
    posY.Draw("SAMEE")
    posX.GetXaxis().SetLabelSize(0.07)
    can.Print(fileName.replace(".root","")+"/Residuals_Av.pdf")
    #can.Print(fileName.replace(".root","")+"/Residuals_Av.C")

###########################################################################################################
###########################################################################################################

    can3=ROOT.TCanvas("CRes2","Residuals_RMS",1500,600)    
    sigX.SetMaximum(40)
    sigX.SetMinimum(  0)
    sigX.SetMarkerSize(0.8)
    sigX.SetMarkerStyle(20)
    sigX.SetMarkerColor(2)
    sigX.SetLineColor(2)
    sigX.SetLineWidth(3)
    sigY.SetMarkerSize(0.8)
    sigY.SetMarkerStyle(20)
    sigY.SetMarkerColor(4)
    sigY.SetLineColor(4)
    sigY.SetLineWidth(2)
    sigX.Draw("E")
    sigY.Draw("SAMEE")
    line1.Draw("SAMEL")
    sigX.GetXaxis().SetLabelSize(0.07)
    can3.Print(fileName.replace(".root","")+"/Residuals_RMS.pdf")
    ##can3.Print(fileName.replace(".root","")+"/Residuals_RMS.C")
    
    canE=ROOT.TCanvas("CEff","Efficiency",1500,600)
    hEff.SetMaximum(100)
    hEff.SetMinimum( 20) #20
    if "W4R12"  in fileName: hEff.SetMinimum( 90)
    if "W7R12"  in fileName: hEff.SetMinimum( 90)
    if "W7R4"   in fileName: hEff.SetMinimum( 90)
    if "W9R11"  in fileName: hEff.SetMinimum( 90)
    if "W11R11" in fileName: hEff.SetMinimum( 90)
    if "R8" in fileName:
        hEff.SetMaximum(30)
        hEff.SetMinimum(0)
        
    hEff.SetMarkerSize(0.8)
    hEff.SetMarkerStyle(20)
    hEff.SetMarkerColor(2)
    hEff.SetLineColor(2)
    hEff.Draw("E")
    l1 = ROOT.TLine(0,95,len(c[1]) , 95)
    l1.SetLineStyle(8)
    l1.Draw("same")
    hEff.GetXaxis().SetLabelSize(0.07)
    canE.Update()
    canE.Print(fileName.replace(".root","")+"/Stability_Eff.pdf")
    #canE.Print(fileName.replace(".root","")+"/Stability_Eff.C")
    command1="mv "+fileName+" "+fileName.replace(".root","")+"/"
    ###print command1
    os.system(command1)

    command2="mv /home/sbmuser/MaltaSW/MaltaTbAnalysis/FinalPlot_DESY_Oct19/overViewPlots.root "+fileName.replace(".root","")+"/"
    print command2
    os.system(command2)

print " "


sys.exit()
