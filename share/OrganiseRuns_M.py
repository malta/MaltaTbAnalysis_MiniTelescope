import os
import sys
import glob
import ROOT
import math
import numpy as np

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(1)
ROOT.gStyle.SetOptFit(1)

badRuns=[]
baseFolder="/home/sbmuser/MaltaSW/MaltaTbAnalysis/DESY_oct2019/"
outFolder ="/home/sbmuser/MaltaSW/MaltaTbAnalysis/FinalPlot_DESY_Oct19_M/"

runProp = [
 
    ["W7R12Cz__IDB70_ITH030_SUB06_PWELL06"   , [6723]],
    ["W7R12Cz__IDB70_ITH030_SUB20_PWELL06"   , [6730]],
    ["W7R12Cz__IDB70_ITH030_SUB30_PWELL06"   , [6733]],

]

#########################################################################################################################
runList=[]
folderList=glob.glob(baseFolder+"run_00*")
print(folderList)
for r in folderList:
    print r
    if "gbl" in r: continue
    run=(r.split("run_00")[1])
    run=int(run.split("_")[0])
    runList.append(run)
runList.sort()

allRuns      =0
uncheckedRuns=0
for r in runList:
    allRuns+=1
    cat=""
    for c in runProp:
        for l in c[1]:
            if l==r:
                cat=c[0]
                break
        if cat!="":
            break
    print str(r)+"      "+cat     
    if cat=="": uncheckedRuns+=1
print "All runs : "+str(allRuns)
print "Unchecked: "+str(uncheckedRuns) 

print " "
##########################################################################################################################
print " "

#in the folder 'processed'
for c in runProp:
    waferName=outFolder+"/"+c[0].split("_")[0]
    os.system("mkdir -p "+waferName)
    print waferName
    
    baseName = "run__"+c[0]+".root"
    fileName  =waferName+"/run__"+c[0]+".root"
    folderName=fileName.replace(".root","")
    os.system("mkdir -p "+folderName)
    print (folderName)
    hEff =ROOT.TH1D("hEff" ,"Avearge Efficiency ; ; eff [%]",len(c[1]),0,len(c[1]))
    hNoise =ROOT.TH1D("hNoise" ,"Avearge Noise Rate ; ; Noise [Hz]",len(c[1]),0,len(c[1]))
   #make the histogram
    posX =ROOT.TH1D("posX" ,"posX ; ;#DeltaX [#mum]",len(c[1]),0,len(c[1]))
   ##posX.Sumw2()
    posY =ROOT.TH1D("posY" ,"posY ; ;#DeltaY [#mum]",len(c[1]),0,len(c[1]))
   ##posY.Sumw2()

    sigX =ROOT.TH1D("sigX" ,"sigX ; ;#sigmaX [#mum]",len(c[1]),0,len(c[1]))
   ##sigX.Sumw2()
    sigY =ROOT.TH1D("sigY" ,"sigY ; ;#sigmaY [#mum]",len(c[1]),0,len(c[1]))

    line=ROOT.TLine(0,0,len(c[1]),0)
    line.SetLineColor(1)
    line.SetLineWidth(2)
    line.SetLineStyle(2)

    line1=ROOT.TLine(0,36.4,len(c[1]),36.4)
    line1.SetLineColor(ROOT.kGray)
    line1.SetLineWidth(1)
    line1.SetLineStyle(ROOT.kGray)

    line2=ROOT.TLine(0,-36.4,len(c[1]),-36.4)
    line2.SetLineColor(ROOT.kGray)
    line2.SetLineWidth(1)
    line2.SetLineStyle(ROOT.kGray)

    
    command="hadd -k -f "+fileName+" "
    count=0
    runString = ""
    totalNoise = 0.
    totalN = 1e-9
    meanNoise = 0.
    totalNoiseErr = 0.
    for r in c[1]:
        if r in badRuns: continue
        count += 1
        hEff.GetXaxis().SetBinLabel(count,str(r))
        hNoise.GetXaxis().SetBinLabel(count,str(r))
        posY.GetXaxis().SetBinLabel(count,str(r))
        posY.GetXaxis().SetBinLabel(count,str(r))
        sigX.GetXaxis().SetBinLabel(count,str(r))
        sigY.GetXaxis().SetBinLabel(count,str(r))
        fN=baseFolder+"/run_00"+str(r)+"/ana_gbl_00"+str(r)+".root"
        #fN=baseFolder+"/run_00"+str(r)+"/ana00"+str(r)+".root"
        print fN
        if not os.path.isfile(fN):
            print "########### FILE COULD NOT BE FOUND ########"
            continue
        rF=ROOT.TFile(fN)
        resX=rF.Get("res_X")
        fit=ROOT.TF1("fitG","gaus",-80,80)
        resX.Fit(fit,"RE0")
        print fit.GetParameter(1)
        posY.SetBinContent(count,fit.GetParameter(1))
        posX.SetBinError(count,fit.GetParError(1))
        sigX.SetBinContent(count,fit.GetParameter(2))
        sigX.SetBinError(count,fit.GetParError(2))
        print "Residuals: ",count,": ",fit.GetParameter(2) 
        ##fit
        resY=rF.Get("res_Y")
        fit2=ROOT.TF1("fitG2","gaus",-80,80)
        resY.Fit(fit2,"RE0")
        print fit2.GetParameter(1)
        posY.SetBinContent(count,fit2.GetParameter(1))
        posY.SetBinError(count,fit2.GetParError(1))
        sigY.SetBinContent(count,fit2.GetParameter(2))
        sigY.SetBinError(count,fit2.GetParError(2))
    
        MAll=rF.Get("MAll")
        MPass=rF.Get("MPass")
        num=MPass.Integral() 
        den=MAll.Integral()
        print "################## num and den: ##################:"
        print num,den
        if den==0 : continue
        eff=num/den *100
        err=math.sqrt(((eff*(1-eff/100.))/100.)/num) 
        print "Run with EFF: "+str(eff)
        hEff.SetBinContent(count,eff)
        hEff.SetBinError(count,err)

        fram_E = rF.Get("fram_E")
        numTracks = rF.Get("numTracks")
        nTrack = numTracks.GetEntries()
        canFramE = ROOT.TCanvas("CFramE","FramE",1000,600)
        fram_E.Draw();
        canFramE.Print(fileName.replace(".root","")+"/framE_"+str(r)+".pdf")
        ##canFramE.Print(fileName.replace(".root","")+"/framE_"+str(r)+".C")

        canResXY = ROOT.TCanvas("cResXY","ResXY",800,600)
        resX.GetXaxis().SetRangeUser(-100,100)
        resX.Draw("")
        resY.SetMarkerColor(2)
        resY.SetLineColor(2)
        resY.Draw("same")
        fit.SetLineColor(1)
        fit.Draw("same")
        fit2.Draw("same")
        bS = ROOT.TPaveText(50,3000, 100, 2000)
        bS.AddText("ResX: %f " %(fit.GetParameter(2)))
        bS.Draw()
        bS2 = ROOT.TPaveText(50,2000, 100, 1000)
        bS2.AddText("ResY: %f" %(fit2.GetParameter(2)))
        bS2.Draw()
        canResXY.Update()
        canResXY.Print(fileName.replace(".root","")+"/resXY_hist_"+str(r)+".pdf")
        ##canResXY.Print(fileName.replace(".root","")+"/resXY_hist_"+str(r)+".C")
        
        rF.Close()
        noiseF = ROOT.TFile("/home/sbmuser/MaltaSW/MaltaTbAnalysis/DESY_oct2019/run_00%i/run_00%i_1.root.root" %(r, r)) #get raw data for plane 1, the DUT
        timing = noiseF.Get("Timing")
        peakPos = timing.GetBinCenter(timing.GetMaximumBin())
        if peakPos < 100:
          print "Run: ",r, 
          print "Timing peak below 100ns, refine noise rate calculation!" 
          print "(But is hardcoded to 200 at the moment, check the run!)"
        peakPos = 200;
        noiseRateHz = timing.Integral(0,int(peakPos-50.0)) 
        nNoise = timing.Integral(0,int(peakPos-50.0)) 
        noiseRateHzErr = np.sqrt(noiseRateHz)
        noiseRateHz /= (peakPos-50.0) #noise per ns
        noiseRateHz /= (peakPos -50.0)#error on noise
        noiseRateHz /= nTrack #noise per ns per event
        noiseRateHz *= 1E9;  #noise per s per event  

        noiseRateHzErr /= (peakPos-50.0) #noise per ns
        noiseRateHzErr /= (peakPos -50.0)#error on noise
        noiseRateHzErr /= nTrack #noise per ns per event
        noiseRateHzErr *= 1E9;  #noise per s per event    
        
        hNoise.SetBinContent(count, noiseRateHz)
        hNoise.SetBinError(count, noiseRateHzErr)

        totalNoise += noiseRateHz * nNoise
        totalN += nNoise
        totalNoiseErr += noiseRateHzErr * noiseRateHzErr * nNoise * nNoise 
        noiseF.Close() 
    
        theRun=""
        theRun=baseFolder+"run_00"+str(r)+"/ana_gbl_00"+str(r)+".root "
        #theRun=baseFolder+"run_00"+str(r)+"/ana00"+str(r)+".root "
        command+=theRun

        runString += str(r)+","
    
    #write a text file with the 
    meanNoise = totalNoise/totalN
    stdNoise = np.sqrt(totalNoiseErr)/totalN
    noiseOut =open("%s/run__%s/Noise.txt" %(waferName,c[0]),"w+")
    noiseOut.write("Noise Rate (Hz), Error (Hz)\n")
    noiseOut.write("%f,%f" %(meanNoise, stdNoise))
    noiseOut.close()
    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"   
    print command
    os.system(command)
    badSt=len(badRuns)

    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
   # command='root -l -b -q FancyPlot_MALTA.C\(\\"'+fileName.replace(".root","")+'\\",\\"'+baseName.replace(".root","").replace("run__","")+'\\"\)'
    command='root -l -b -q FancyPlot_MALTA.C\(\\"'+fileName.replace(".root","")+'\\",\\"'+baseName.replace(".root","").replace("run__","")+'\\"\)'
    print command
    print fileName
    print baseName
    os.system(command)


    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
    #command2='root -l -b -q -x /home/sbmuser/MaltaSW/MaltaTbAnalysis/share/makeOverviewPlots.C\(\\"'+baseFolder+'\\",\{'+runString[:-1]+'\}\)'
    command2='root -l -b -q -x makeOverviewPlots.C\(\\"'+baseFolder+'\\",\{'+runString[:-1]+'\}\)'
    print command2
    os.system(command2)
       
    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
    print "#########################################################################################################"
 
    can=ROOT.TCanvas("CRes","Residuals_Average",1500,600)
    ROOT.gStyle.SetOptStat(0)
    posX.SetMaximum( 50)
    posX.SetMinimum(-50)
    posX.SetMarkerSize(0.8)
    posX.SetMarkerStyle(20)
    posX.SetMarkerColor(2)
    posX.SetLineWidth(2)
    posX.SetLineColor(2)
    posY.SetMarkerSize(0.8)
    posY.SetMarkerStyle(20)
    posY.SetMarkerColor(4)
    posY.SetLineColor(4)
    posY.SetLineWidth(2)
    posX.Draw("E")
    line.Draw("SAMEL")
    line1.Draw("SAMEL")
    line2.Draw("SAMEL")
    posX.Draw("SAMEE")
    posY.Draw("SAMEE")
    posX.GetXaxis().SetLabelSize(0.07)
    can.Print(fileName.replace(".root","")+"/Residuals_Av.pdf")
    #can.Print(fileName.replace(".root","")+"/Residuals_Av.C")

###########################################################################################################
###########################################################################################################

    can3=ROOT.TCanvas("CRes2","Residuals_RMS",1500,600)    
    sigX.SetMaximum(40)
    sigX.SetMinimum(  0)
    sigX.SetMarkerSize(0.8)
    sigX.SetMarkerStyle(20)
    sigX.SetMarkerColor(2)
    sigX.SetLineColor(2)
    sigX.SetLineWidth(3)
    sigY.SetMarkerSize(0.8)
    sigY.SetMarkerStyle(20)
    sigY.SetMarkerColor(4)
    sigY.SetLineColor(4)
    sigY.SetLineWidth(2)
    sigX.Draw("E")
    sigY.Draw("SAMEE")
    line1.Draw("SAMEL")
    sigX.GetXaxis().SetLabelSize(0.07)
    can3.Print(fileName.replace(".root","")+"/Residuals_RMS.pdf")
    ##can3.Print(fileName.replace(".root","")+"/Residuals_RMS.C")
    
    canE=ROOT.TCanvas("CEff","Efficiency",1500,600)
    hEff.SetMaximum(100)
    hEff.SetMinimum( 20) #20
    if "W4R12"  in fileName: hEff.SetMinimum( 90)
    if "W7R12"  in fileName: hEff.SetMinimum( 90)
    if "W7R4"   in fileName: hEff.SetMinimum( 90)
    if "W9R11"  in fileName: hEff.SetMinimum( 90)
    if "W11R11" in fileName: hEff.SetMinimum( 90)
    if "R8" in fileName:
        hEff.SetMaximum(30)
        hEff.SetMinimum(0)
        
    hEff.SetMarkerSize(0.8)
    hEff.SetMarkerStyle(20)
    hEff.SetMarkerColor(2)
    hEff.SetLineColor(2)
    hEff.Draw("E")
    l1 = ROOT.TLine(0,95,len(c[1]) , 95)
    l1.SetLineStyle(8)
    l1.Draw("same")
    hEff.GetXaxis().SetLabelSize(0.07)
    canE.Update()
    canE.Print(fileName.replace(".root","")+"/Stability_Eff.pdf")
    #canE.Print(fileName.replace(".root","")+"/Stability_Eff.C")
    command1="mv "+fileName+" "+fileName.replace(".root","")+"/"
    ###print command1
    os.system(command1)

    command2="mv /home/sbmuser/MaltaSW/MaltaTbAnalysis/FinalPlot_DESY_Oct19/overViewPlots.root "+fileName.replace(".root","")+"/"
    print command2
    os.system(command2)

print " "


sys.exit()
