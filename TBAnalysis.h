#ifndef TBANALYSIS_h
#define TBANALYSIS_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TH1D.h"
#include "TH2D.h"

#include <vector>
#include <map>
#include <iostream>
#include "TBEvent.C"

using namespace std;
class TBAnalysis;


//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

class TBAnalysis: public TBEvent {
 public :

  TBAnalysis();
  ~TBAnalysis();

  void Dump(int clIndex, vector<float> &timeH, int entry);
  void Difference(float &BCIDlongDiff, float &BCIDshortDiff, float &TimeFastDiff);
  bool HorizontalCluster(int cIndex, int entry, float &HorCluBCIDlongDiff, float &HorCluBCIDshortDiff, float &HorCluTimeFastDiff);
  bool HorizontalClusterDC(int cIndex, int entry, float &HorCluBCIDlongDiffDC, float &HorCluBCIDshortDiffDC, float &HorCluTimeFastDiffDC);
  bool VerticalClusterDG(int cIndex, int entry, float &VerCluBCIDlongDiffDG, float &VerCluBCIDshortDiffDG, float &VerCluTimeFastDiffDG);
  bool VerticalCluster(int cIndex, int entry, float &VerCluBCIDlongDiff, float &VerCluBCIDshortDiff, float &VerCluTimeFastDiff);
  void Loop();
  //int GetClusterSize(int clIndex, int nHits, vector<float> &timeH, bool print=false);
  int GetClusterSize(int cIndex, int nHits, int hitincluster[], double htiming[], int pixX[], int pixY[], double ctiming[], double row[], double col[], int ncluster, vector<float> &timeH, bool print);
  float ClusterTiming(int cIndex, vector<float> &clustertime, vector<float> &CTime);
  float ClusterTiming2(int cIndex, vector<float> &clustertime, vector<float> &CTime); 
  void picoAnalysis(string outPutName);
  vector<double> EvalPicoSigma(double sigma01, double sigma02, double sigma12);
  double EvalFWHM(TH1D *h);
  double findIntercept(double x0, double x1, double y0, double y1, double val);
  //bool MatchTrackInsidePixel(float interceptX, float interceptY, int pixX, int pixY, float d);
  bool MatchTrackInsidePixel(float interceptX, float interceptY, int pixX[], int pixY[], int nhits, int hits[], int clID, float d);
  int TrackID(int ntrk, int itrk[], int duttrkid);

  std::map<string,double>  map_analysis;
 private :
  int maxumtracksToPrint = 300;
  int numtracksToPrint = 0;
};
#endif

#ifdef TBAnalysis_cxx
TBAnalysis::TBAnalysis()  { }
TBAnalysis::~TBAnalysis() { }

#endif // #ifdef TBAnalysis_cxx
