#ifndef TBHits_h
#define TBHits_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class TBHits {
public :
   TTree          *fEvent;     //!pointer to the analyzed TTree or TChain
   TTree          *fFEI4;      //!pointer to the analyzed TTree or TChain
   TTree          *fMIMOSA1;   //!pointer to the analyzed TTree or TChain
   TTree          *fMIMOSA2;   //!pointer to the analyzed TTree or TChain
   TTree          *fMalta;     //!pointer to the analyzed TTree or TChain

   Int_t           fCurrent; //!current Tree number in a TChain

   // Event leaves
   ULong64_t       FrameNumber;
   ULong64_t       TimeStamp;
   ULong64_t       TriggerTime;
   Int_t           TriggerInfo;
   Int_t           TriggerOffset;
   Int_t           TriggerPhase;
   Bool_t          Invalid;

   // Hits leaves
   Int_t           NHits_FE;
   Int_t           PixX_FE[1000];         //[NHits]
   Int_t           PixY_FE[1000];         //[NHits]
   Double_t        HTiming_FE[1000];      //[NHits]

   // Hits leaves
   Int_t           NHits_M1;
   Int_t           PixX_M1[1000];         //[NHits]
   Int_t           PixY_M1[1000];         //[NHits]
   Double_t        HTiming_M1[1000];      //[NHits]

   // Hits leaves
   Int_t           NHits_M2;
   Int_t           PixX_M2[1000];         //[NHits]
   Int_t           PixY_M2[1000];         //[NHits]
   Double_t        HTiming_M2[1000];      //[NHits]

   // Hits leaves
   Int_t           NHits_MA;
   Int_t           PixX_MA[1000];         //[NHits]
   Int_t           PixY_MA[1000];         //[NHits]
   Double_t        HTiming_MA[1000];      //[NHits]

   ////////////////////////////////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////////////////////////////////
   // List of branches
   TBranch        *b_FrameNumber;   //!
   TBranch        *b_TimeStamp;     //!
   TBranch        *b_TriggerTime;   //!
   TBranch        *b_TriggerInfo;   //!
   TBranch        *b_TriggerOffset; //!
   TBranch        *b_TriggerPhase;  //!
   TBranch        *b_Invalid;       //!

   TBranch        *b_NHits_FE;     //!
   TBranch        *b_PixX_FE;      //!
   TBranch        *b_PixY_FE;      //!
   TBranch        *b_HTiming_FE;   //!

   TBranch        *b_NHits_M1;     //!
   TBranch        *b_PixX_M1;      //!
   TBranch        *b_PixY_M1;      //!
   TBranch        *b_HTiming_M1;   //!

   TBranch        *b_NHits_M2;     //!
   TBranch        *b_PixX_M2;      //!
   TBranch        *b_PixY_M2;      //!
   TBranch        *b_HTiming_M2;   //!

   TBranch        *b_NHits_MA;     //!
   TBranch        *b_PixX_MA;      //!
   TBranch        *b_PixY_MA;      //!
   TBranch        *b_HTiming_MA;   //!

   TBHits();
   virtual ~TBHits();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry,Long64_t advance=0);
   virtual Long64_t LoadTree(Long64_t entry,Long64_t advance=0);
   virtual void     Init(TTree *tE,TTree *tF,TTree *tM1,TTree *tM2,TTree *tMA);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef TBHits_cxx
TBHits::TBHits() : fEvent(0),fFEI4(0),fMIMOSA1(0),fMIMOSA2(0),fMalta(0) 
{

}

TBHits::~TBHits() {
   if (!fEvent) return;
   delete fEvent  ->GetCurrentFile();
   delete fFEI4   ->GetCurrentFile();
   delete fMIMOSA1->GetCurrentFile();
   delete fMIMOSA2->GetCurrentFile();
   delete fMalta  ->GetCurrentFile();
}

Int_t TBHits::GetEntry(Long64_t entry, Long64_t advance)
{
// Read contents of entry.
   if (!fEvent) return 0;
   int vE=fEvent  ->GetEntry(entry);
   int vT=fFEI4   ->GetEntry(entry);
   int vI=fMIMOSA1->GetEntry(entry);
   int vC=fMIMOSA2->GetEntry(entry);
   int vH=fMalta  ->GetEntry(entry+advance);
   return vE;
}

Long64_t TBHits::LoadTree(Long64_t entry, Long64_t advance)
{
  // Set the environment to read one entry
  if (!fEvent) return -5;
  
  Long64_t centryE = fEvent  ->LoadTree(entry);
  Long64_t centryT = fFEI4   ->LoadTree(entry);
  Long64_t centryI = fMIMOSA1->LoadTree(entry);
  Long64_t centryC = fMIMOSA1->LoadTree(entry);
  Long64_t centryH = fMalta  ->LoadTree(entry+advance);
 
  if (centryE < 0) return centryE;
  if (fEvent->GetTreeNumber() != fCurrent) {
    fCurrent = fEvent->GetTreeNumber();
    //Notify();
  }
  return centryE;
}

void TBHits::Init(TTree *tE,TTree *tF,TTree *tM1,TTree *tM2,TTree *tMA)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.

   // Set branch addresses and branch pointers
   if (!tE) return;
   fEvent = tE;
   fFEI4  = tF;
   fMIMOSA1= tM1;
   fMIMOSA2= tM2;
   fMalta  = tMA;

   fCurrent = -1;
   fEvent  ->SetMakeClass(1);
   fFEI4   ->SetMakeClass(1);
   fMIMOSA1->SetMakeClass(1);
   fMIMOSA1->SetMakeClass(1);
   fMalta  ->SetMakeClass(1);

   fEvent->SetBranchAddress("FrameNumber"  , &FrameNumber  , &b_FrameNumber  );
   fEvent->SetBranchAddress("TimeStamp"    , &TimeStamp    , &b_TimeStamp    );
   //fEvent->SetBranchAddress("TriggerTime"  , &TriggerTime  , &b_TriggerTime  );
   fEvent->SetBranchAddress("TriggerInfo"  , &TriggerInfo  , &b_TriggerInfo  );
   fEvent->SetBranchAddress("TriggerOffset", &TriggerOffset, &b_TriggerOffset);
   //fEvent->SetBranchAddress("TriggerPhase" , &TriggerPhase , &b_TriggerPhase );
   fEvent->SetBranchAddress("Invalid"      , &Invalid      , &b_Invalid      );

   fFEI4->SetBranchAddress("NHits"       , &NHits_FE      , &b_NHits_FE       );
   fFEI4->SetBranchAddress("PixX"        , PixX_FE        , &b_PixX_FE        );
   fFEI4->SetBranchAddress("PixY"        , PixY_FE        , &b_PixY_FE        );
   fFEI4->SetBranchAddress("Timing"      , HTiming_FE     , &b_HTiming_FE     );

   fMIMOSA1->SetBranchAddress("NHits"       , &NHits_M1      , &b_NHits_M1       );
   fMIMOSA1->SetBranchAddress("PixX"        , PixX_M1        , &b_PixX_M1        );
   fMIMOSA1->SetBranchAddress("PixY"        , PixY_M1        , &b_PixY_M1        );
   fMIMOSA1->SetBranchAddress("Timing"      , HTiming_M1     , &b_HTiming_M1     );

   fMIMOSA2->SetBranchAddress("NHits"       , &NHits_M2      , &b_NHits_M2       );
   fMIMOSA2->SetBranchAddress("PixX"        , PixX_M2        , &b_PixX_M2        );
   fMIMOSA2->SetBranchAddress("PixY"        , PixY_M2        , &b_PixY_M2        );
   fMIMOSA2->SetBranchAddress("Timing"      , HTiming_M2     , &b_HTiming_M2     );

   fMalta->SetBranchAddress("NHits"       , &NHits_MA      , &b_NHits_MA       );
   fMalta->SetBranchAddress("PixX"        , PixX_MA        , &b_PixX_MA        );
   fMalta->SetBranchAddress("PixY"        , PixY_MA        , &b_PixY_MA        );
   fMalta->SetBranchAddress("Timing"      , HTiming_MA     , &b_HTiming_MA     );

   //Notify();
}

Bool_t TBHits::Notify() {
   return kTRUE;
}

void TBHits::Show(Long64_t entry) { }

Int_t TBHits::Cut(Long64_t entry) {
   return 1;
}
#endif // #ifdef TBHits_cxx
