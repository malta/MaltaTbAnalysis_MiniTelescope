import os
import sys
import glob
import ROOT
import math

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(0000)
badRuns=[]
baseFolder="/home/sbmuser/MaltaSW/MaltaTbAnalysis/DESY_oct2019/"

runProp=[
#   ["W7R4__IDB127"         , [6691,6692,6693,6694,6695,6696,6697,6698]],
#   ["W7R4__IDB70_SUB06"    , [6699,6700,6701,6702]],
#   ["W7R4__IDB70_SUB20"    , [6703,6704,6705]],
#   ["W7R4__IDB70_SUB12"    , [6706,6707,6708,6709]],
#   ["W7R4__IDB70_SUB30"    , [6710,6711,6712]],

#   ["W11R11Cz__IDB127_ITH100_SUB6_PWELL2"   , [6782]],
#   ["W11R11Cz__IDB127_ITH100_SUB6"   , [6756, 6757 , 6758 ,6759,6761,6762,6768,6779]],
#   ["W11R11Cz__IDB127_ITH100_SUB8"   , [6769]],
#   ["W11R11Cz__IDB127_ITH100_SUB9"   , [6763,6764]],
#   ["W11R11Cz__IDB127_ITH100_SUB12"   , [6765,6766,6770,6771]],
#   ["W11R11Cz__IDB127_ITH100_SUB16"   , [6767]],
#   ["W11R11Cz__IDB127_ITH100_SUB14"   , [6772, 6773, 6774]],
#   ["W11R11Cz__IDB127_ITH127_SUB6"   , [6780, 6781]],


#   ["W4R0__IDB127_ITH30_SUB06_PWELL02"  , [6815]],
#   ["W4R0__IDB127_ITH30_SUB06_PWELL04"  , [6816]],
#   ["W4R0__IDB127_ITH30_SUB06"  , [6817]],
#   ["W4R0__IDB127_ITH127_SUB06"  , [6784, 6785, 6786]],
#   ["W4R0__IDB127_ITH127_SUB07"  , [6796]],
#   ["W4R0__IDB127_ITH127_SUB08"  , [6795]],
#   ["W4R0__IDB127_ITH127_SUB09"  , [6794]],
#   ["W4R0__IDB127_ITH127_SUB12"  , [6788, 6789]],
#   ["W4R0__IDB127_ITH127_SUB16"  , [6792, 6793]],
#   ["W4R0__IDB127_ITH127_SUB20"  , [6790, 6791]],
#   ["W4R0__IDB127_ITH127_SUB18"  , [6798]],
#   ["W4R0__IDB127_ITH30_SUB12"   , [6799, 6800, 6801, 6802]],
#   ["W4R0__IDB127_ITH80_SUB12"   , [6803, 6804, 6805, 6806, 6808, 6809, 6811, 6818, 6819]],

#   ["W7R0Cz__IDB127_ITH030_SUB16_PWELL6_S3", [6870]],
#   ["W7R0Cz__IDB100_ITH030_SUB50_PWELL6_S3", [6868,6869]],
#   ["W7R0Cz__IDB127_ITH030_SUB50_PWELL6_S3", [6866,6867]],
#   ["W7R0Cz__IDB127_ITH030_SUB55p2_PWELL6_S3", [6864, 6865]],
#   ["W7R0Cz__IDB127_ITH030_SUB54p7_PWELL6_S3", [6862, 6863]],
#   ["W7R0Cz__IDB127_ITH030_SUB53_PWELL6_S3", [6860, 6861]],
#   ["W7R0Cz__IDB127_ITH030_SUB45_PWELL6_S3", [6857, 6858, 6859]],
#   ["W7R0Cz__IDB127_ITH030_SUB40_PWELL6_S3", [6853, 6856]],
#   ["W7R0Cz__IDB100_ITH030_SUB30_PWELL6_S3", [6843,6844]],
#   ["W7R0Cz__IDB127_ITH030_SUB30_PWELL6_S3", [6843,6844,6848,6849,6850,6851,6852,6854,6855]],
#   ["W7R0Cz__IDB127_ITH080_SUB30_PWELL6_S3", [6842]],
#   ["W7R0Cz__IDB127_ITH080_SUB30_PWELL6_S2", [6841]],
#   ["W7R0Cz__IDB127_ITH080_SUB30_PWELL6", [6836,6837,6839,6840]],
#   ["W7R0Cz__IDB127_ITH030_SUB30_PWELL6", [6834,6835]],
#   ["W7R0Cz__IDB127_ITH127_SUB06_PWELL6", [6820]],
#   ["W7R0Cz__IDB127_ITH127_SUB09_PWELL6", [6832]], #6833]
#   ["W7R0Cz__IDB127_ITH127_SUB12_PWELL6", [6821, 6822, 6823]],
#   ["W7R0Cz__IDB127_ITH127_SUB16_PWELL6", [6830, 6831]],
#   ["W7R0Cz__IDB127_ITH127_SUB20_PWELL6", [6828, 6829]],
#   ["W7R0Cz__IDB127_ITH127_SUB25_PWELL6", [6826, 6827]],
#   ["W7R0Cz__IDB127_ITH127_SUB30_PWELL6", [6824, 6825]],


#   ["W9R1Cz__IDB127_ITH127_SUB06_PWELL6", [6871, 6872, 6873]],
#   ["W9R1Cz__IDB127_ITH127_SUB09_PWELL6", [6874, 6875]],
#   ["W9R1Cz__IDB127_ITH127_SUB12_PWELL6", [6876, 6877]],
#   ["W9R1Cz__IDB127_ITH127_SUB15_PWELL6", [6878, 6879]],
#   ["W9R1Cz__IDB127_ITH127_SUB20_PWELL6", [6880, 6881]],
#   ["W9R1Cz__IDB127_ITH127_SUB25_PWELL6", [6882, 6883]],
#   ["W9R1Cz__IDB127_ITH127_SUB30_PWELL6", [6884, 6885]],
#   ["W9R1Cz__IDB127_ITH127_SUB35_PWELL6", [6886, 6887]],
#   ["W9R1Cz__IDB127_ITH127_SUB40_PWELL6", [6888, 6889]],
#   ["W9R1Cz__IDB127_ITH127_SUB45_PWELL6", [6890, 6891]],
   ["W9R1Cz__IDB127_ITH127_SUB50_PWELL6", [6892, 6893, 6894, 6895, 6896, 6897]],
   ["W9R1Cz__IDB127_ITH30_SUB50_PWELL6_UPDATE",  [6898, 6899, 6900]], # updating noise map for extra pixels
   ["W9R1Cz__IDB127_ITH30_SUB50_PWELL6",  [6900, 6901, 6905, 6906, 6907, 6909, 6910]],
#   ["W9R1Cz__IDB127_ITH30_SUB50_PWELL6_LARGE_FIDUCIAL",  [6911, 6913, 6914, 6915, 6916, 6917, 6918, 6919, 6920, 6921, 6922, 6923, 6924]],
#   ["W9R1Cz__IDB127_ITH127_SUB6_PWELL6_LARGE_FIDUCIAL",  [6925, 6926, 6928, 6930, 6931, 6932, 6933, 6934, 6935]],


#   ["W11R0Cz__IDB127_ITHR127_SUB9_PWELL6",[6948,6949]],
#   ["W11R0Cz__IDB127_ITHR127_SUB12_PWELL6",[6938, 6939]],
#   ["W11R0Cz__IDB127_ITHR127_SUB15_PWELL6",[6940, 6941]],
#   ["W11R0Cz__IDB127_ITHR127_SUB20_PWELL6",[6942, 6943]],
#   ["W11R0Cz__IDB127_ITHR127_SUB25_PWELL6",[6944, 6945]], 
#   ["W11R0Cz__IDB127_ITHR127_SUB30_PWELL6",[6946, 6947]],    
#   ["W11R0Cz__IDB127_ITHR127_SUB35_PWELL6",[6950, 6951]],
#   ["W11R0Cz__IDB127_ITHR127_SUB40_PWELL6",[6952, 6953]],
#   ["W11R0Cz__IDB127_ITHR127_SUB45_PWELL6",[6954, 6955]],
#   ["W11R0Cz__IDB127_ITHR127_SUB50_PWELL6",[6956, 6957]],
#   ["W11R0Cz__IDB127_ITHR127_SUB54_PWELL6",[6958]],
#   ["W11R0Cz__IDB127_ITHR127_SUB52_PWELL6",[6959]],
#   ["W11R0Cz__IDB127_ITHR30_SUB50_PWELL6",[6960, 6963, 6964, 6965, 6966, 6967, 6968, 6969]],
#   ["W11R0Cz__IDB127_ITHR127_SUB40_PWELL6",[6950, 6951]],  


#   ["W9R4Cz_IDB127_ITHR127_SUB6_PWELL6",[7017,7018,7019]],
#   ["W9R4Cz_IDB127_ITHR127_SUB12_PWELL6",[7020,7021]],
#   ["W9R4Cz_IDB127_ITHR127_SUB15_PWELL6",[7022,7023]],
#   ["W9R4Cz_IDB127_ITHR127_SUB20_PWELL6",[7024,7025]],
#   ["W9R4Cz_IDB127_ITHR127_SUB25_PWELL6",[7026,7027]],
#   ["W9R4Cz_IDB127_ITHR127_SUB30_PWELL6",[7028,7029]],
#   ["W9R4Cz_IDB127_ITHR127_SUB35_PWELL6",[7030,7031]],
#   ["W9R4Cz_IDB127_ITHR127_SUB40_PWELL6",[7032,7033]],
#   ["W9R4Cz_IDB127_ITHR127_SUB45_PWELL6",[7034,7035]],
#   ["W9R4Cz_IDB127_ITHR127_SUB50_PWELL6",[7036,7037]],
#   ["W9R4Cz_IDB127_ITHR127_SUB55_PWELL6",[7038,7039,7040,7041,7042,7043]],
#   ["W9R4Cz_IDB100_ITHR80_SUB50_PWELL6_SEC3",[7044,7045,7046,7047,7050]],
#   ["W9R4Cz_IDB80_ITHR30_SUB50_PWELL6_ROI",[7051,7052,7053,7054,7055,7056]],


#   ["W4R4__IDB127_ITHR127_SUB6_PWELL6",[7058, 7059, 7060]],
#   ["W4R4__IDB127_ITHR127_SUB9_PWELL6",[7061, 7062]],
#   ["W4R4__IDB127_ITHR127_SUB12_PWELL6",[7063, 7064]],
#   ["W4R4__IDB127_ITHR127_SUB15_PWELL6",[7065, 7066]],
#   ["W4R4__IDB127_ITHR127_SUB18_PWELL6",[7067, 7068]],
#   ["W4R4__IDB127_ITHR127_SUB20_PWELL6",[7069, 7070]],

#   ["W7R1Cz__IDB127_ITHR127_SUB6_PWELL6",[7097, 7098]],  
#   ["W7R1Cz__IDB127_ITHR127_SUB12_PWELL6",[7099, 7100]],
#   ["W7R1Cz__IDB127_ITHR127_SUB15_PWELL6",[7101, 7102]],
#   ["W7R1Cz__IDB127_ITHR127_SUB20_PWELL6",[7103, 7104]],
#   ["W7R1Cz__IDB127_ITHR127_SUB25_PWELL6",[7105, 7106]],
#   ["W7R1Cz__IDB127_ITHR127_SUB30_PWELL6",[7107, 7108]],
#   ["W7R1Cz__IDB127_ITHR127_SUB35_PWELL6",[7109, 7110]],
#   ["W7R1Cz__IDB127_ITHR127_SUB40_PWELL6",[7111, 7112]],
#   ["W7R1Cz__IDB127_ITHR127_SUB45_PWELL6",[7113, 7114]],
#   ["W7R1Cz__IDB127_ITHR127_SUB50_PWELL6",[7115, 7116]],
#   ["W7R1Cz__IDB127_ITHR30_SUB50_PWELL6",[7117, 7118]],
#   ["W7R1Cz__IDB127_ITHR10_SUB50_PWELL6",[7119, 7120]],
#   ["W7R1Cz__IDB100_ITHR10_SUB50_PWELL6",[7121]],
#   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6",[7122,7126,7127,7128]],
#   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6_MOREMASK",[7129,7130,7131,7132,7133]],
#   ["W7R1Cz__IDB80_ITHR3_SUB50_PWELL6",[7123,7125]],
#   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6_S5_S6",[7134]],
#   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6_VRESET_30",[7135]],
#   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6_VRESET_20",[7136]],
#   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6_VRESET_55",[7137]],
#   ["W7R1Cz__IDB100_ITHR3_SUB50_PWELL6_90MASK",[7138, 7139, 1740]],z
#   ["W9R11Cz__IDB70_SUB06"   , [6739,6740,6741,6742,6743,6744,6745]],
#   ["W9R11Cz__IDB70_SUB09"   , [6746, 6747, 6748, 6750, 6751]],
#   ["W9R11Cz__IDB70_SUB12"   , [6752, 6753, 6754]],

#   ["W4R12__IDB70_SUB06"   , [6713,6714,6716,6717,6719]],
#   ["W4R12__IDB70_SUB09"   , [6720,6721]],
#   ["W7R12Cz__IDB70_SUB06" , [6722,6723]],
#   ["W7R12Cz__IDB70_SUB12" , [6724,6725,6726]],
#   ["W7R12Cz__IDB70_SUB20" , [6729,6730,6734]],
#   ["W7R12Cz__IDB70_SUB30" , [6732,6733]],
#   ["W7R12Cz__IDB70_SUB16" , [6735]],
#   ["W7R12Cz__IDB70_SUB25" , [6736]],

#   ["W4R4__IDB127_ITHR127_SUB6_PWELL6" , [7148,7149]],
#   ["W4R4__IDB127_ITHR127_SUB7_PWELL6" , [7150,7151]],
#   ["W4R4__IDB127_ITHR127_SUB8_PWELL6" , [7152,7153]],
#   ["W4R4__IDB127_ITHR127_SUB9_PWELL6" , [7154,7155]],
#   ["W4R4__IDB127_ITHR127_SUB10_PWELL6" , [7156,7157]],
#   ["W4R4__IDB127_ITHR127_SUB11_PWELL6" , [7158,7162]],
#   ["W4R4__IDB127_ITHR127_SUB12_PWELL6" , [7160,7161]],
#   ["W4R4__IDB127_ITHR127_SUB13_PWELL6" , [7163,7164]],
#   ["W4R4__IDB127_ITHR30_SUB10_PWELL6" , [7165,7166]],
#   ["W4R4__IDB127_ITHR10_SUB10_PWELL6" , [7167]],

#    ["W9R0Cz__IDB127_ITHR127_SUB6_PWELL6" , [7169,7170]],
#    ["W9R0Cz__IDB127_ITHR127_SUB12_PWELL6" , [7171,7172]],
#    ["W9R0Cz__IDB127_ITHR127_SUB15_PWELL6" , [7173,7174]],
#    ["W9R0Cz__IDB127_ITHR127_SUB20_PWELL6" , [7175,7176]],
#    ["W9R0Cz__IDB127_ITHR127_SUB25_PWELL6" , [7177,7178]],
#    ["W9R0Cz__IDB127_ITHR127_SUB30_PWELL6" , [7179,7180]],
#    ["W9R0Cz__IDB127_ITHR127_SUB35_PWELL6" , [7181,7182]],
#    ["W9R0Cz__IDB127_ITHR127_SUB40_PWELL6" , [7183,7184]],
#    ["W9R0Cz__IDB127_ITHR127_SUB45_PWELL6" , [7185,7186]],
#    ["W9R0Cz__IDB127_ITHR127_SUB50_PWELL6" , [7187,7188,7189]],
#    ["W9R0Cz__IDB127_ITHR30_SUB50_PWELL6" , [7195,7196,7199]],
#    ["W9R0Cz__IDB127_ITHR30_SUB50_PWELL6_MOREMASK" , [7200,7201,7202,7203,7204]],
#    ["W9R0Cz__IDB127_ITHR3_SUB50_PWELL6" , [7197,7198]],
#    ["W7R0Cz__IDB127_ITHR127_SUB6_PWELL6_NEW" , [7206]],
#    ["W7R0Cz__IDB127_ITHR127_SUB12_PWELL6_NEW" , [7207]],
#    ["W7R0Cz__IDB127_ITHR127_SUB15_PWELL6_NEW" , [7208]],
#    ["W7R0Cz__IDB127_ITHR127_SUB20_PWELL6_NEW" , [7209]],
#    ["W7R0Cz__IDB127_ITHR127_SUB25_PWELL6_NEW" , [7210]],
#    ["W7R0Cz__IDB127_ITHR127_SUB30_PWELL6_NEW" , [7211]],
#    ["W7R0Cz__IDB127_ITHR127_SUB35_PWELL6_NEW" , [7212]],
#    ["W7R0Cz__IDB127_ITHR127_SUB40_PWELL6_NEW" , [7213]],
#    ["W7R0Cz__IDB127_ITHR127_SUB45_PWELL6_NEW" , [7214]],
#    ["W7R0Cz__IDB127_ITHR127_SUB50_PWELL6_NEW" , [7215,7216]],
#    ["W7R0Cz__IDB127_ITHR127_SUB50_PWELL6_NEW_MOREMASK" , [7217,7218,7219]]
 ]
    

#########################################################################################################################
runList=[]
folderList=glob.glob(baseFolder+"run_00*")
print(folderList)
for r in folderList:
    print r
    if "gbl" in r: continue
    run=int(r.split("run_00")[1])
    runList.append(run)
runList.sort()

allRuns      =0
uncheckedRuns=0
for r in runList:
    allRuns+=1
    cat=""
    for c in runProp:
        for l in c[1]:
            if l==r:
                cat=c[0]
                break
        if cat!="":
            break
    print str(r)+"      "+cat     
    if cat=="": uncheckedRuns+=1
print "All runs : "+str(allRuns)
print "Unchecked: "+str(uncheckedRuns) 

print " "
##########################################################################################################################
print " "

#in the folder 'processed'
for c in runProp:

    fileName="run__"+c[0]+".root"
    folderName=fileName.replace(".root","")
    os.system("mkdir -p "+folderName)
    posX =ROOT.TH1D("posX" ,"Avearge Efficiency ; ; eff [%]",len(c[1]),0,len(c[1]))
    
    command="hadd -k -f "+fileName+" "
    count=0
    runString = ""
    for r in c[1]:
        if r in badRuns: continue
        count += 1
        posX.GetXaxis().SetBinLabel(count,str(r))
        fN=baseFolder+"/run_00"+str(r)+"/ana00"+str(r)+".root"
        print fN
        if not os.path.isfile(fN):
            print "########### FILE COULD NOT BE FOUND ########"
            continue
        rF=ROOT.TFile(fN)
        MAll=rF.Get("MAll")
        MPass=rF.Get("MPass")
        num=MPass.Integral() 
        den=MAll.Integral()
        print "################## num and den: ##################:"
        print num,den
        if den==0 : continue
        eff=num/den *100
        err=math.sqrt(((eff*(1-eff/100.))/100.)/num) 
        print "Run with EFF: "+str(eff)
        posX.SetBinContent(count,eff)
        posX.SetBinError(count,err)
        rF.Close()
        
        theRun=""
        ##theRun=baseFolder+"run_00"+str(r)+"/ana_gbl00"+str(r)+".root "
        theRun=baseFolder+"run_00"+str(r)+"/ana00"+str(r)+".root "
        command+=theRun

        runString += str(r)+","

    print command
    os.system(command)
    badSt=len(badRuns)
    command='root -l -b -q FancyPlot_MALTA.C\(\\"'+fileName.replace(".root","")+'\\"\)'
    print command

    command2='root -l -b -q -x makeOverviewPlots.C\(\\"'+baseFolder+'\\",\{'+runString[:-1]+'\}\)'
    print command2
    os.system(command2)
    
    
    os.system(command)

    can=ROOT.TCanvas("CRes","Residuals_Average",1500,600)
    posX.SetMaximum(100)
    posX.SetMinimum(0)
    posX.SetMarkerSize(0.8)
    posX.SetMarkerStyle(20)
    posX.SetMarkerColor(2)
    posX.SetLineColor(2)
    posX.Draw("E")
    can.Update()
    can.Print(fileName.replace(".root","")+"/Stability_Eff.pdf")

    command1="mv "+fileName+" "+fileName.replace(".root","")+"/"
    ###print command1
    os.system(command1)

    command2="mv overViewPlots.root "+fileName.replace(".root","")+"/"
    os.system(command2)

print " "


sys.exit()
