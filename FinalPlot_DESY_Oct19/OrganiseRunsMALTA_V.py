import os
import sys
import glob
import ROOT
import math

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(0000)
badRuns=[]
baseFolder="/home/sbmuser/MaltaSW/MaltaTbAnalysis/DESY_oct2019/"

runProp=[



 ]
    

#########################################################################################################################
runList=[]
folderList=glob.glob(baseFolder+"run_00*")
print(folderList)
for r in folderList:
    print r
    if "gbl" in r: continue
    run=int(r.split("run_00")[1])
    runList.append(run)
runList.sort()

allRuns      =0
uncheckedRuns=0
for r in runList:
    allRuns+=1
    cat=""
    for c in runProp:
        for l in c[1]:
            if l==r:
                cat=c[0]
                break
        if cat!="":
            break
    print str(r)+"      "+cat     
    if cat=="": uncheckedRuns+=1
print "All runs : "+str(allRuns)
print "Unchecked: "+str(uncheckedRuns) 

print " "
##########################################################################################################################
print " "

#in the folder 'processed'
for c in runProp:

    fileName="run__"+c[0]+".root"
    folderName=fileName.replace(".root","")
    os.system("mkdir -p "+folderName)
    posX =ROOT.TH1D("posX" ,"Avearge Efficiency ; ; eff [%]",len(c[1]),0,len(c[1]))
    
    command="hadd -k -f "+fileName+" "
    count=0
    runString = ""
    for r in c[1]:
        if r in badRuns: continue
        count += 1
        posX.GetXaxis().SetBinLabel(count,str(r))
        fN=baseFolder+"/run_00"+str(r)+"/ana00"+str(r)+".root"
        print fN
        if not os.path.isfile(fN):
            print "########### FILE COULD NOT BE FOUND ########"
            continue
        rF=ROOT.TFile(fN)
        MAll=rF.Get("MAll")
        MPass=rF.Get("MPass")
        num=MPass.Integral() 
        den=MAll.Integral()
        print "################## num and den: ##################:"
        print num,den
        if den==0 : continue
        eff=num/den *100
        err=math.sqrt(((eff*(1-eff/100.))/100.)/num) 
        print "Run with EFF: "+str(eff)
        posX.SetBinContent(count,eff)
        posX.SetBinError(count,err)
        rF.Close()
        
        theRun=""
        ##theRun=baseFolder+"run_00"+str(r)+"/ana_gbl00"+str(r)+".root "
        theRun=baseFolder+"run_00"+str(r)+"/ana00"+str(r)+".root "
        command+=theRun

        runString += str(r)+","

    print command
    os.system(command)
    badSt=len(badRuns)
    command='root -l -b -q FancyPlot_MALTA.C\(\\"'+fileName.replace(".root","")+'\\"\)'
    print command

    command2='root -l -b -q -x makeOverviewPlots.C\(\\"'+baseFolder+'\\",\{'+runString[:-1]+'\}\)'
    print command2
    os.system(command2)
    
    
    os.system(command)

    can=ROOT.TCanvas("CRes","Residuals_Average",1500,600)
    posX.SetMaximum(100)
    posX.SetMinimum(0)
    posX.SetMarkerSize(0.8)
    posX.SetMarkerStyle(20)
    posX.SetMarkerColor(2)
    posX.SetLineColor(2)
    posX.Draw("E")
    can.Update()
    can.Print(fileName.replace(".root","")+"/Stability_Eff.pdf")

    command1="mv "+fileName+" "+fileName.replace(".root","")+"/"
    ###print command1
    os.system(command1)

    command2="mv overViewPlots.root "+fileName.replace(".root","")+"/"
    os.system(command2)

print " "


sys.exit()
