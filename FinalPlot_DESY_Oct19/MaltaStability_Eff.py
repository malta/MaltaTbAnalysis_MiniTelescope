import sys,os
import os.path
import glob
from ROOT import *
from array import *
import math

#gROOT.LoadMacro("~/AtlasStyleBase/AtlasStyle.C")
#gROOT.LoadMacro("~/AtlasStyleBase/AtlasUtils.C")
#SetAtlasStyle()
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.SetBatch(1)
gStyle.SetEndErrorSize(10)

###############################################################################

baseFolder ="/home/sbmuser/MiniMaltaResults_ELSANew/web/"

print baseFolder
listFolders=glob.glob(baseFolder+"*")
runList=[]
for f in listFolders:
    if "run_" not in f: continue
    run=int(f.split("run_gbl_00")[1]) 
    runList.append( f.split("run_gbl_00")[1] )
runList.sort()
print runList


#make the histogram
posX =TH1D("posX" ,"Avearge Efficiency ; ; eff [%]",len(runList),0,len(runList))
posX2=TH1D("posX2","posX2; ;#DeltaX [#mum]",len(runList),0,len(runList))
##posX.Sumw2()
posY =TH1D("posY" ,"posY ; ;#DeltaY [#mum]",len(runList),0,len(runList))
posY2=TH1D("posY2","posY2; ;#DeltaY [#mum]",len(runList),0,len(runList))
##posY.Sumw2()

sigX =TH1D("sigX" ,"sigX ; ;#sigmaX [#mum]",len(runList),0,len(runList))
sigX2=TH1D("sigX2","sigX2; ;#sigmaX [#mum]",len(runList),0,len(runList))
##sigX.Sumw2()
sigY =TH1D("sigY" ,"sigY ; ;#sigmaY [#mum]",len(runList),0,len(runList))
sigY2=TH1D("sigY2","sigY2; ;#sigmaY [#mum]",len(runList),0,len(runList))

line=TLine(0,0,len(runList),0)
line.SetLineColor(kGray)
line.SetLineWidth(2)
line.SetLineStyle(2)

line1=TLine(0,36.4,len(runList),36.4)
line1.SetLineColor(kBlack)
line1.SetLineWidth(1)
line1.SetLineStyle(3)

line2=TLine(0,-36.4,len(runList),-36.4)
line2.SetLineColor(kBlack)
line2.SetLineWidth(1)
line2.SetLineStyle(3)



count=0
for r in runList:
    count+=1
    posX.GetXaxis().SetBinLabel(count,r)
    posY.GetXaxis().SetBinLabel(count,r)
    sigX.GetXaxis().SetBinLabel(count,r)
    sigY.GetXaxis().SetBinLabel(count,r)
    fileName=baseFolder+"/run_gbl_00"+r+"/ana_gbl00"+r+".root"
    if not os.path.isfile(fileName): continue
    rF=TFile(fileName)


    MAll=rF.Get("MAll")
    MPass=rF.Get("MPass")
    num=MPass.Integral() 
    den=MAll.Integral() 
    if den==0 : continue
    eff=num/den *100
    err=math.sqrt(((eff*(1-eff/100.))/100.)/num)
    
    posX.SetBinContent(count,eff)
    posX.SetBinError(count,err)
    rF.Close()


of=TFile("outStatility.root","RECREATE")

can=TCanvas("CRes","Residuals_Average",1500,600)
posX.SetMaximum(100)
posX.SetMinimum(50)
posX.SetMarkerSize(0.8)
posX.SetMarkerStyle(20)
posX.SetMarkerColor(2)
posX.SetLineColor(2)
posY.SetMarkerSize(0.8)
posY.SetMarkerStyle(20)
posY.SetMarkerColor(4)
posY.SetLineColor(4)
posX.Draw("E")

can.Print("Efficiency.pdf")
os.system("evince Efficiency.pdf &")


###########################################################################################################
###########################################################################################################
###########################################################################################################
###########################################################################################################
###########################################################################################################
###########################################################################################################

of.Close()
sys.exit()

