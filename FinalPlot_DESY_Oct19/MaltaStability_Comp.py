import sys,os
import os.path
import glob
from ROOT import *
from array import *
import math

#gROOT.LoadMacro("~/AtlasStyleBase/AtlasStyle.C")
#gROOT.LoadMacro("~/AtlasStyleBase/AtlasUtils.C")
#SetAtlasStyle()
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.SetBatch(1)
gStyle.SetEndErrorSize(10)

###############################################################################

baseFolder ="/home/sbmuser/MiniMaltaResults_ELSANew/web/"
#####baseFolder ="/home/sbmuser/MiniMaltaResults_DESY/web/"
baseFolder2="/home/sbmuser/MiniMaltaResults_ELSA/web_old/"

print baseFolder
listFolders=glob.glob(baseFolder+"*")
runList=[]
for f in listFolders:
    if "run_" not in f: continue
    print f
    run=int(f.split("run_gbl_00")[1]) #>3131: continue
    #if run<5026: continue
    #if run<5118: continue
    #if run<5200: continue
    #if run>5190: continue
    #if run<5173: continue
    runList.append( f.split("run_gbl_00")[1] )
runList.sort()
print runList
#for r in runList:
#    print r

#make the histogram
posX =TH1D("posX" ,"posX ; ;#DeltaX [#mum]",len(runList),0,len(runList))
posX2=TH1D("posX2","posX2; ;#DeltaX [#mum]",len(runList),0,len(runList))
##posX.Sumw2()
posY =TH1D("posY" ,"posY ; ;#DeltaY [#mum]",len(runList),0,len(runList))
posY2=TH1D("posY2","posY2; ;#DeltaY [#mum]",len(runList),0,len(runList))
##posY.Sumw2()

sigX =TH1D("sigX" ,"sigX ; ;#sigmaX [#mum]",len(runList),0,len(runList))
sigX2=TH1D("sigX2","sigX2; ;#sigmaX [#mum]",len(runList),0,len(runList))
##sigX.Sumw2()
sigY =TH1D("sigY" ,"sigY ; ;#sigmaY [#mum]",len(runList),0,len(runList))
sigY2=TH1D("sigY2","sigY2; ;#sigmaY [#mum]",len(runList),0,len(runList))

line=TLine(0,0,len(runList),0)
line.SetLineColor(kGray)
line.SetLineWidth(2)
line.SetLineStyle(2)

line1=TLine(0,36.4,len(runList),36.4)
line1.SetLineColor(kBlack)
line1.SetLineWidth(1)
line1.SetLineStyle(3)

line2=TLine(0,-36.4,len(runList),-36.4)
line2.SetLineColor(kBlack)
line2.SetLineWidth(1)
line2.SetLineStyle(3)


count=0
for r in runList:
    count+=1
    posX.GetXaxis().SetBinLabel(count,r)
    posY.GetXaxis().SetBinLabel(count,r)
    sigX.GetXaxis().SetBinLabel(count,r)
    sigY.GetXaxis().SetBinLabel(count,r)
    fileName=baseFolder+"/run_gbl_00"+r+"/ana_gbl00"+r+".root"
    if not os.path.isfile(fileName): continue
    rF=TFile(fileName)
    resX=rF.Get("res_X")
    fit=TF1("fitG","gaus",-200,200)
    resX.Fit(fit,"RE0")
    print fit.GetParameter(1)
    posX.SetBinContent(count,fit.GetParameter(1))
    posX.SetBinError(count,fit.GetParError(1))
    sigX.SetBinContent(count,fit.GetParameter(2))
    sigX.SetBinError(count,fit.GetParError(2))
    ##fit
    resY=rF.Get("res_Y")
    fit2=TF1("fitG2","gaus",-200,200)
    resY.Fit(fit2,"RE0")
    print fit2.GetParameter(1)
    posY.SetBinContent(count,fit2.GetParameter(1))
    posY.SetBinError(count,fit2.GetParError(1))
    sigY.SetBinContent(count,fit2.GetParameter(2))
    sigY.SetBinError(count,fit2.GetParError(2))
    
    rF.Close()
    fileName2=baseFolder2+"/run_00"+r+"/ana_00"+r+".root"
    if not os.path.isfile(fileName2): continue
    rF=TFile(fileName2)
    resX2=rF.Get("res_X")
    fit3=TF1("fitG3","gaus",-200,200)
    resX2.Fit(fit3,"RE0")
    posX2.SetBinContent(count,fit3.GetParameter(1))
    posX2.SetBinError(count,fit2.GetParError(1))
    sigX2.SetBinContent(count,fit2.GetParameter(2))
    sigX2.SetBinError(count,fit2.GetParError(2))
    resY2=rF.Get("res_Y")
    fit4=TF1("fitG4","gaus",-200,200)
    resY2.Fit(fit4,"RE0")
    posY2.SetBinContent(count,fit4.GetParameter(1))
    posY2.SetBinError(count,fit4.GetParError(1))
    sigY2.SetBinContent(count,fit4.GetParameter(2))
    sigY2.SetBinError(count,fit4.GetParError(2))
    rF.Close()

of=TFile("outStatility.root","RECREATE")

can=TCanvas("CRes","Residuals_Average",1500,600)
posX.SetMaximum( 150)
posX.SetMinimum(-150)
posX.SetMarkerSize(0.8)
posX.SetMarkerStyle(20)
posX.SetMarkerColor(2)
posX.SetLineColor(2)
posY.SetMarkerSize(0.8)
posY.SetMarkerStyle(20)
posY.SetMarkerColor(4)
posY.SetLineColor(4)
posX.Draw("E")
line.Draw("SAMEL")
line1.Draw("SAMEL")
line2.Draw("SAMEL")
posX.Draw("SAMEE")
posY.Draw("SAMEE")
can.Print("Residuals_Av.pdf")
os.system("evince Residuals_Av.pdf &")
of.WriteObject(posX,"posX")
of.WriteObject(posY,"posY")

###########################################################################################################
###########################################################################################################
###########################################################################################################
###########################################################################################################
###########################################################################################################
###########################################################################################################

can3=TCanvas("CRes2","Residuals_RMS",1500,600)
sigX.SetMaximum(50)
sigX.SetMinimum(  0)
sigX.SetMarkerSize(0.8)
sigX.SetMarkerStyle(20)
sigX.SetMarkerColor(2)
sigX.SetLineColor(2)
sigY.SetMarkerSize(0.8)
sigY.SetMarkerStyle(20)
sigY.SetMarkerColor(4)
sigY.SetLineColor(4)
sigX.Draw("E")
sigY.Draw("SAMEE")
line1.Draw("SAMEL")
can3.Print("Residuals_RMS.pdf")
os.system("evince Residuals_RMS.pdf &")
of.WriteObject(sigX,"sigX")
of.WriteObject(sigY,"sigY")

###########################################################################################################
###########################################################################################################
###########################################################################################################
###########################################################################################################
###########################################################################################################
###########################################################################################################


of.Close()
sys.exit()

