#include "TTree.h"
#include "TH1F.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TGraph.h"


void avg_pico_malta_desync(string run_number){
string fixPathSr90="";
TFile *_file0 = TFile::Open(("run_"+run_number+"/proc_"+run_number+"_0.root.root").c_str());
if(_file0==NULL) {
  _file0 = TFile::Open(("runs/run_"+run_number+"/proc_"+run_number+"_0.root.root").c_str());
  fixPathSr90="runs/";
} 
TFile *_file1 = TFile::Open(("run_"+run_number+"/proc_"+run_number+"_1.root.root").c_str());
if(_file1==NULL) _file1 = TFile::Open(("runs/run_"+run_number+"/proc_"+run_number+"_1.root.root").c_str()); 
TFile *_file2 = TFile::Open(("run_"+run_number+"/proc_"+run_number+"_2.root.root").c_str());
if(_file2==NULL) _file2 = TFile::Open(("runs/run_"+run_number+"/proc_"+run_number+"_2.root.root").c_str()); 


TTree *t0 = (TTree*) _file0->Get("Plane0/Hits");
TTree *t1 = (TTree*) _file1->Get("Plane0/Hits");
TTree *t2 = (TTree*) _file2->Get("Plane0/Hits");


int entries = t0->GetEntries();

TH1F *tmp0 = NULL;
TH1F *tmp1 = NULL;
TH1F *tmp2 = NULL;

TGraphErrors g0;
TGraphErrors g1;
TGraphErrors g2;
g0.SetNameTitle("g0","g0");
g1.SetNameTitle("g1","g1");
g2.SetNameTitle("g2","g2");
int events = 4096;

if (t0->GetEntries()<5000)  events = 50;

for (int i =0; i < (int)entries/events; ++i) {

   tmp0 = new TH1F(("tmp0_"+to_string(i)).c_str(), ("tmp0_"+to_string(i)).c_str(), 100, -10, 10);
   t0->Draw(("Timing-Timing_pico>>tmp0_"+to_string(i)).c_str(),"Timing_pico>0", "", events,events*i); 
   g0.SetPoint(i, i*events+25,tmp0->GetRMS());
   g0.SetPointError(i, 25,tmp0->GetRMS()/sqrt(events));

   tmp1 = new TH1F(("tmp1_"+to_string(i)).c_str(), ("tmp1_"+to_string(i)).c_str(), 100, -10, 10);
   t1->Draw(("Timing-Timing_pico>>tmp1_"+to_string(i)).c_str(),"Timing_pico>0", "", events,events*i); 
   g1.SetPoint(i, i*events+25,tmp1->GetRMS());
   g1.SetPointError(i, 25,tmp1->GetRMS()/sqrt(events));

   tmp2 = new TH1F(("tmp2_"+to_string(i)).c_str(), ("tmp2_"+to_string(i)).c_str(), 100, -10, 10);
   t2->Draw(("Timing-Timing_pico>>tmp2_"+to_string(i)).c_str(),"Timing_pico>0", "", events,events*i); 
   g2.SetPoint(i, i*events+25,tmp2->GetRMS());
   g2.SetPointError(i, 25,tmp2->GetRMS()/sqrt(events));


}

TCanvas c("c","c",800,800);
g0.SetLineColor(kRed);
g1.SetLineColor(kBlack);
g2.SetLineColor(kBlue);

g0.SetMarkerColor(kRed);
g1.SetMarkerColor(kBlack);
g2.SetMarkerColor(kBlue);

g0.SetMarkerStyle(8);
g1.SetMarkerStyle(8);
g2.SetMarkerStyle(8);

g0.Draw("apl");
g1.Draw("pl,same");
g2.Draw("pl,same");


bool bad = false;
bool warning = false;

if ( g0.GetHistogram()->GetMaximum() - g0.GetHistogram()->GetMinimum() > 2) { cout << "\033[1;31mdesync plane 0\033[0m\n" << endl; bad = true;}
if ( g1.GetHistogram()->GetMaximum() - g1.GetHistogram()->GetMinimum() > 2) { cout << "\033[1;31mdesync plane 1\033[0m\n" << endl; bad = true;}
if ( g2.GetHistogram()->GetMaximum() - g2.GetHistogram()->GetMinimum() > 2) { cout << "\033[1;31mdesync plane 2\033[0m\n" << endl; bad = true;}


g0.GetYaxis()->SetRangeUser(0,5);
c.SaveAs((fixPathSr90+"run_"+run_number+"/run_pico_malta.root").c_str());
c.SaveAs((fixPathSr90+"run_"+run_number+"/run_pico_malta.pdf").c_str());


TFile *malta = TFile::Open(("run_"+run_number+"/ana"+run_number+".root").c_str());
if(malta ==NULL) malta = TFile::Open(("runs/run_"+run_number+"/ana_gbl_"+run_number+".root").c_str());




TH1F *fram_A = (TH1F*)malta->Get("fram_A");

TH1F *fram_P = (TH1F*)malta->Get("fram_P");

for (int i =0; i< fram_A->GetNbinsX(); ++i){
  if ( fram_A->GetBinContent(i+1)/ (fram_A->Integral()/(fram_A->GetNbinsX())) < 0.8) { cout <<  "\033[1;35mWARNING, local desync around event: " << fram_A->GetBinCenter(i+1) << "\033[0m" << endl; warning = true;}
}
for (int i =0; i< fram_P->GetNbinsX(); ++i){
  if ( fram_P->GetBinContent(i+1)/ (fram_P->Integral()/(fram_P->GetNbinsX())) < 0.8) { cout <<  "\033[1;35mWARNING, local desync around event: " << fram_P->GetBinCenter(i+1) << "\033[0m" << endl; warning = true;}
}


if (bad == false and warning == false) cout << "first check: GOOD" << endl;
if (bad == true) cout << "first check: BAD" << endl;
if (bad == false and warning == true) cout << "first check: WARNING" << endl;
}
