#include "hit.h"

#include <cassert>
#include <ostream>

#include "cluster.h"

Storage::Hit::Hit(int col, int row, int timestamp, double timingMalta, double timingCorrected, double timingPico, double timingPico_corrected, double tot, int value, int bcid_long, int bcid_short, int time_fast, int word)
    : m_digitalCol(col)
    , m_digitalRow(row)
    , m_col(col)
    , m_row(row)
    , m_timestamp(timestamp)
    , m_timingMalta(timingMalta)
    , m_timingCorrected(timingCorrected)
    , m_timingPico(timingPico)
    , m_timingPico_corrected(timingPico_corrected)
    , m_timingTot(tot)
    , m_value(value)
    , m_bcid_long(bcid_long)
    , m_bcid_short(bcid_short)
    , m_time_fast(time_fast)
    , m_word(word)
    , m_region(kInvalidIndex)
    , m_cluster(kInvalidIndex)
{
}

void Storage::Hit::setPhysicalAddress(int col, int row)
{
  m_col = col;
  m_row = row;
}

void Storage::Hit::setCluster(Index cluster)
{
  assert((m_cluster == kInvalidIndex) && "hit can only be in one cluster");
  m_cluster = cluster;
}

std::ostream& Storage::operator<<(std::ostream& os, const Storage::Hit& hit)
{
  if ((hit.digitalCol() != hit.col()) || (hit.digitalRow() != hit.row())) {
    os << "addr0=" << hit.digitalCol();
    os << " addr1=" << hit.digitalRow();
    os << " ";
  }
  os << "col=" << hit.col();
  os << " row=" << hit.row();
  os << " ts=" << hit.timestamp();
  os << " tcorrected=" << hit.timingCorrected();
  os << " Tmalta=" << hit.timingMalta();
  os << " Tpico=" << hit.timingPico();
  os << " Tot=" << hit.timingTot();
  os << " value=" << hit.value();
  if (hit.hasRegion()) {
    os << " region=" << hit.region();
  }
  if (hit.isInCluster()) {
    os << " cluster=" << hit.cluster();
  }
  return os;
}
