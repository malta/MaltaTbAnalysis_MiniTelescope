#include "cluster.h"

#include <cassert>
#include <climits>
#include <limits>
#include <numeric>
#include <ostream>

#include "storage/hit.h"
#include "utils/logger.h"

PT_SETUP_LOCAL_LOGGER(Cluster)

Storage::Cluster::Cluster(Scalar col,
                          Scalar row,
                          Scalar timestamp,
                          Scalar time_malta,
                          Scalar time_malta_corrected,
                          Scalar time_pico,
                          Scalar time_pico_corrected,
                          Scalar time_tot,
                          Scalar value,
                          Scalar colVar,
                          Scalar rowVar,
                          Scalar timestampVar,
                          Scalar colRowCov)
    : m_col(col)
    , m_row(row)
    , m_timestamp(timestamp)
    , m_time_malta(time_malta)
    , m_time_malta_corrected(time_malta_corrected)
    , m_time_pico(time_pico)
    , m_time_pico_corrected(time_pico_corrected)
    , m_time_tot(time_tot)
    , m_value(value)
    , m_colVar(colVar)
    , m_rowVar(rowVar)
    , m_colRowCov(colRowCov)
    , m_timestampVar(timestampVar)
    , m_pos(Vector4::Constant(std::numeric_limits<Scalar>::quiet_NaN()))
    , m_posCov(SymMatrix4::Constant(std::numeric_limits<Scalar>::quiet_NaN()))
    , m_index(kInvalidIndex)
    , m_track(kInvalidIndex)
    , m_matchedState(kInvalidIndex)
{
}

bool Storage::Cluster::hasRegion() const
{
  return !m_hits.empty() && m_hits.front().get().hasRegion();
}

Index Storage::Cluster::region() const
{
  return m_hits.empty() ? kInvalidIndex : m_hits.front().get().region();
}

void Storage::Cluster::setTrack(Index track)
{
  assert((m_track == kInvalidIndex) && "cluster can only be in one track");
  m_track = track;
}

static Matrix<Scalar, 3, 4> projectionOntoPlane()
{
  Matrix<Scalar, 3, 4> proj = Matrix<Scalar, 3, 4>::Zero();
  proj(kLoc0 - kOnPlane, kU) = 1;
  proj(kLoc1 - kOnPlane, kV) = 1;
  proj(kTime - kOnPlane, kS) = 1;
  return proj;
}

Vector3 Storage::Cluster::onPlane() const
{
  return projectionOntoPlane() * m_pos;
}

SymMatrix3 Storage::Cluster::onPlaneCov() const
{
  return transformCovariance(projectionOntoPlane(), m_posCov);
}

Storage::Cluster::Area Storage::Cluster::areaPixel() const
{
  auto grow = [](Area a, const Hit& hit) {
    a.enclose(Area(Area::AxisInterval(hit.col(), hit.col() + 1),
                   Area::AxisInterval(hit.row(), hit.row() + 1)));
    return a;
  };
  return std::accumulate(m_hits.begin(), m_hits.end(), Area::Empty(), grow);
}

void Storage::Cluster::addHit(Storage::Hit& hit)
{
  hit.setCluster(m_index);
  m_hits.push_back(std::ref(hit));
}

std::ostream& Storage::operator<<(std::ostream& os, const Cluster& cluster)
{
  os << "col=" << cluster.col();
  os << " row=" << cluster.row();
  os << " u=" << cluster.u();
  os << " v=" << cluster.v();
  os << " time=" << cluster.time();
  os << " value=" << cluster.value();
  os << " size=" << cluster.size();
  if (cluster.isInTrack()) {
    os << " track=" << cluster.track();
  }
  if (cluster.isMatched()) {
    os << " matched=" << cluster.matchedState();
  }
  return os;
}
