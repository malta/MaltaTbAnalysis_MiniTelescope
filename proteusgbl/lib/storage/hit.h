#ifndef PT_HIT_H
#define PT_HIT_H

#include <iosfwd>

#include "utils/definitions.h"

namespace Storage {

/** A sensor hit identified by its address, timestamp, and value.
 *
 * To support devices where the recorded hit address does not directly
 * correspond to the pixel address in the physical pixel matrix, e.g. CCPDv4,
 * the Hit has separate digital (readout) and physical (pixel matrix)
 * addresses.
 */
class Hit {
public:
  Hit(int col, int row, int timestamp, double timingMalta, double timingCorrected, double timingPico, double timingPico_corrected, double tot, int value, int bcid_long, int bcid_short, int time_fast, int word);

  /** Set only the physical address leaving the digital address untouched. */
  void setPhysicalAddress(int col, int row);
  /** Set the region id. */
  void setRegion(Index region) { m_region = region; }
  /** Set the cluster index. */
  void setCluster(Index cluster);

  int digitalCol() const { return m_digitalCol; }
  int digitalRow() const { return m_digitalRow; }
  int col() const { return m_col; }
  int row() const { return m_row; }
  int timestamp() const { return m_timestamp; }
  double timingCorrected() const { return m_timingCorrected; }
  double timingMalta() const { return m_timingMalta; }
  double timingPico() const { return m_timingPico; }
  double timingPicoCorrected() const { return m_timingPico_corrected; }
  double timingTot() const { return m_timingTot; }
  int value() const { return m_value; }
  int bcid_long() const { return m_bcid_long; }
  int bcid_short() const { return m_bcid_short; }
  int time_fast() const { return m_time_fast; }
  int word() const { return m_word; }

  bool hasRegion() const { return m_region != kInvalidIndex; }
  Index region() const { return m_region; }

  bool isInCluster() const { return m_cluster != kInvalidIndex; }
  Index cluster() const { return m_cluster; }

private:
  int m_digitalCol;
  int m_digitalRow;
  int m_col;
  int m_row;
  int m_timestamp; // Level 1 accept, typically
  double m_timingCorrected;
  double m_timingMalta;
  double m_timingPico;
  double m_timingPico_corrected;
  double m_timingTot;
  int m_value;     // Time over threshold, typically
  int m_bcid_long;
  int m_bcid_short;
  int m_time_fast;
  int m_word;
  Index m_region;
  Index m_cluster;
};

std::ostream& operator<<(std::ostream& os, const Hit& hit);

} // namespace Storage

#endif // PT_HIT_H
