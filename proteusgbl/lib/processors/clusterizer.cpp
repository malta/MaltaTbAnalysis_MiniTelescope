/*
 * \file
 * \author Moritz Kiehn (msmk@cern.ch)
 * \date 2016-10
 */

#include "clusterizer.h"

#include <algorithm>
#include <functional>
#include <limits>

#include "loop/eventloop.h"
#include "mechanics/device.h"
#include "storage/event.h"
#include "utils/interval.h"
#include "utils/logger.h"

PT_SETUP_GLOBAL_LOGGER

using namespace Storage;
using namespace Utils;

using DigitalRange = Interval<int>;

// scaling from uniform with to equivalent Gaussian standard deviation
constexpr Scalar kVar = 1.0 / 12.0;

// return true if both hits are connected, i.e. share one edge
//
// WARNING: hits w/ the same position are counted as connected
static inline bool connected(const Hit& hit0, const Hit& hit1)
{
  auto dc = std::abs(hit1.col() - hit0.col());
  auto dr = std::abs(hit1.row() - hit0.row());
  return (hit0.region() == hit1.region()) and
         (((dc == 0) and (dr <= 1)) or ((dc <= 1) and (dr == 0)));
}

// return true if the hit is connected to any hit in the range
template <typename HitIterator>
static inline bool
connected(HitIterator clusterBegin, HitIterator clusterEnd, const Hit& hit)
{
  bool flag = false;
  for (; clusterBegin != clusterEnd; ++clusterBegin) {
    flag = (flag or connected(*clusterBegin->get(), hit));
  }
  return flag;
}

// rearange the input hit range so that pixels in a cluster are neighbours.
template <typename HitIterator, typename ClusterMaker>
static inline void clusterize(const DenseMask& mask,
                              SensorEvent& sensorEvent,
                              HitIterator hitsBegin,
                              HitIterator hitsEnd,
                              ClusterMaker makeCluster)
{
  // move masked pixels to the back of the hit vector
  hitsEnd = std::partition(hitsBegin, hitsEnd, [&](const auto& hit) {
    return not mask.isMasked(hit->col(), hit->row());
  });

  // group all connected hits starting from an arbitrary seed hit (first hit).
  auto clusterBegin = hitsBegin;
  while (clusterBegin != hitsEnd) {
    // every cluster has at least one member
    auto clusterEnd = std::next(clusterBegin);

    // each iteration can only pick up the nearest-neighboring pixels, so we
    // need to iterate until we find no more connected pixels.
    while (clusterEnd != hitsEnd) {
      // accumulate all connected hits to the beginning of the range
      auto moreHits = std::partition(clusterEnd, hitsEnd, [=](const auto& hit) {
        return connected(clusterBegin, clusterEnd, *hit);
      });
      // no connected hits were found -> cluster is complete
      if (moreHits == clusterEnd) {
        break;
      }
      // some connected hits were found -> extend cluster
      clusterEnd = moreHits;
    }

    // sort cluster hits by value and time
    // WARNING compare has to fullfil (from C++ standard)
    //   1. compare(a, a) == false
    //   2. compare(a, b) == true -> compare(b, a) == false
    //   3. compare(a, b) == true && compare(b, c) == true -> compare(a, c) ==
    //   true
    // if it does not, std::sort will corrupt the heap.
    // NOTE to future self:
    // do not try to be smart; the same problem broke the trackfinder.
    auto compare = [](const std::unique_ptr<Hit>& hptr0,
                      const std::unique_ptr<Hit>& hptr1) {
      const auto& hit0 = *hptr0;
      const auto& hit1 = *hptr1;
      // 1. sort by value, highest first
      if (hit0.value() > hit1.value())
        return true;
      if (hit1.value() > hit0.value())
        return false;
      // 2. sort by timestamp, lowest first
      if (hit0.timestamp() < hit1.timestamp())
        return true;
      if (hit1.timestamp() < hit0.timestamp())
        return false;
      // equivalent hits w/ respect to value and time
      return false;
    };
    std::sort(clusterBegin, clusterEnd, compare);

    // add cluster to event
    auto& cluster =
        sensorEvent.addCluster(makeCluster(clusterBegin, clusterEnd));
    for (auto hit = clusterBegin; hit != clusterEnd; ++hit) {
      cluster.addHit(*hit->get());
    }

    // only consider the remaining hits for the next cluster
    clusterBegin = clusterEnd;
  }
}

std::string Processors::BinaryClusterizer::name() const
{
  return "BinaryClusterizer(" + m_sensor.name() + ")";
}

void Processors::BinaryClusterizer::execute(Event& event) const
{
  auto makeCluster = [](auto h0, auto h1) {
    Scalar col = 0;
    Scalar row = 0;
    int ts = std::numeric_limits<int>::max();
    double t_corrected = std::numeric_limits<int>::max();
    double t_malta = std::numeric_limits<int>::max();
    double t_pico = std::numeric_limits<int>::max();
    double t_pico_corrected = std::numeric_limits<int>::max();
    double t_tot = std::numeric_limits<int>::max();
    int value = 0;
    int size = 0;
    DigitalRange rangeCol = DigitalRange::Empty();
    DigitalRange rangeRow = DigitalRange::Empty();

    for (; h0 != h1; ++h0) {
      const Hit& hit = *(h0->get());
      col += hit.col();
      row += hit.row();
      ts = std::min(ts, hit.timestamp());
      t_corrected = (hit.timingCorrected()<0) ? -99 : std::min(t_corrected, abs(hit.timingCorrected()));
      t_malta = (hit.timingMalta()<0) ? -99 : std::min(t_malta, abs(hit.timingMalta()));
      t_pico  = (hit.timingPico()<0)  ? -99 : std::min(t_pico, abs(hit.timingPico()));
      t_pico_corrected  = (hit.timingPicoCorrected()<0)  ? -99 : std::min(t_pico_corrected, abs(hit.timingPicoCorrected()));
      t_tot   = (hit.timingTot()<0)   ? -99 : std::min(t_tot, abs(hit.timingTot()));
      value += hit.value();
      size += 1;
      rangeCol.enclose(DigitalRange(hit.col(), hit.col() + 1));
      rangeRow.enclose(DigitalRange(hit.row(), hit.row() + 1));
    }
    col /= size;
    row /= size;

    auto colVar = kVar / rangeCol.length();
    auto rowVar = kVar / rangeRow.length();
    auto tsVar = kVar;
    return Cluster(col, row, ts, t_malta, t_corrected, t_pico, t_pico_corrected,t_tot, value, colVar, rowVar, tsVar);
  };
  auto& sensorEvent = event.getSensorEvent(m_sensor.id());
  clusterize(m_sensor.pixelMask(), sensorEvent, sensorEvent.m_hits.begin(),
             sensorEvent.m_hits.end(), makeCluster);
}

std::string Processors::ValueWeightedClusterizer::name() const
{
  return "ValueWeightedClusterizer(" + m_sensor.name() + ")";
}

void Processors::ValueWeightedClusterizer::execute(Event& event) const
{
  auto makeCluster = [](auto h0, auto h1) {
    Scalar col = 0;
    Scalar row = 0;
    double t_corrected = std::numeric_limits<int>::max();
    double t_malta = std::numeric_limits<int>::max();
    double t_pico = std::numeric_limits<int>::max();
    double t_pico_corrected = std::numeric_limits<int>::max();
    double t_tot = std::numeric_limits<int>::max();
    int ts = std::numeric_limits<int>::max();
    int value = 0;
    DigitalRange rangeCol = DigitalRange::Empty();
    DigitalRange rangeRow = DigitalRange::Empty();

    for (; h0 != h1; ++h0) {
      const Hit& hit = *(h0->get());
      // TODO how to handle zero value?
      col += hit.value() * hit.col();
      row += hit.value() * hit.row();
      t_corrected = (hit.timingCorrected()<0) ? -99 : std::min(t_corrected, abs(hit.timingCorrected()));
      t_malta = (hit.timingMalta()<0) ? -99 : std::min(t_malta, abs(hit.timingMalta()));
      t_pico  = (hit.timingPico()<0)  ? -99 : std::min(t_pico, abs(hit.timingPico()));
      t_pico_corrected  = (hit.timingPicoCorrected()<0)  ? -99 : std::min(t_pico_corrected, abs(hit.timingPicoCorrected()));
      t_tot   = (hit.timingTot()<0)   ? -99 : std::min(t_tot, abs(hit.timingTot()));
      ts = std::min(ts, hit.timestamp());
      value += hit.value();
      rangeCol.enclose(DigitalRange(hit.col(), hit.col() + 1));
      rangeRow.enclose(DigitalRange(hit.row(), hit.row() + 1));
    }
    col /= value;
    row /= value;

    auto colVar = kVar / rangeCol.length();
    auto rowVar = kVar / rangeRow.length();
    auto tsVar = kVar;
    return Cluster(col, row, ts, t_malta, t_corrected, t_pico, t_pico_corrected, t_tot, value, colVar, rowVar, tsVar);
  };
  auto& sensorEvent = event.getSensorEvent(m_sensor.id());
  clusterize(m_sensor.pixelMask(), sensorEvent, sensorEvent.m_hits.begin(),
             sensorEvent.m_hits.end(), makeCluster);
}

std::string Processors::FastestHitClusterizer::name() const
{
  return "FastestHitClusterizer(" + m_sensor.name() + ")";
}

void Processors::FastestHitClusterizer::execute(Event& event) const
{
  auto makeCluster = [](auto h0, auto h1) {
    Scalar col = 0;
    Scalar row = 0;
    int ts = std::numeric_limits<int>::max();
    double t_corrected = std::numeric_limits<int>::max();
    double t_malta = std::numeric_limits<int>::max();
    double t_pico = std::numeric_limits<int>::max();
    double t_pico_corrected = std::numeric_limits<int>::max();
    double t_tot = std::numeric_limits<int>::max();
    int value = 0;

    for (; h0 != h1; ++h0) {
      const Hit& hit = *(h0->get());
      if (hit.timestamp() < ts) {
        col = hit.col();
        row = hit.row();
        ts = hit.timestamp();
        t_corrected  = (hit.timingCorrected()<0) ? -99 : std::min(t_corrected, abs(hit.timingCorrected()));
        t_malta  = (hit.timingMalta()<0) ? -99 : std::min(t_malta, abs(hit.timingMalta()));
        t_pico   = (hit.timingPico()<0)  ? -99 : std::min(t_pico, abs(hit.timingPico()));
        t_pico_corrected   = (hit.timingPicoCorrected()<0)  ? -99 : std::min(t_pico_corrected, abs(hit.timingPicoCorrected()));
        t_tot    = (hit.timingTot()<0)   ? -99 : std::min(t_tot, abs(hit.timingTot()));
        value = hit.value();
      }
    }

    return Cluster(col, row, ts, t_malta, t_corrected, t_pico, t_pico_corrected, t_tot, value, kVar, kVar, kVar);
  };
  auto& sensorEvent = event.getSensorEvent(m_sensor.id());
  clusterize(m_sensor.pixelMask(), sensorEvent, sensorEvent.m_hits.begin(),
             sensorEvent.m_hits.end(), makeCluster);
}

std::string Processors::MaltaClusterizer::name() const
{
  return "MaltaClusterizer(" + m_sensor.name() + ")";
}

void Processors::MaltaClusterizer::execute(Event& event) const
{
  auto makeCluster = [](auto h0, auto h1) {
    Scalar col = 0;
    Scalar row = 0;
    double t_corrected = std::numeric_limits<int>::max();
    double t_malta = std::numeric_limits<int>::max();
    double t_pico = std::numeric_limits<int>::max();
    double t_pico_corrected = std::numeric_limits<int>::max();
    double t_tot = std::numeric_limits<int>::max();
    int ts = std::numeric_limits<int>::max();
    float value = 0.;
    int size = 0;
    float weight =1.;
    float time0 = 0.;
    DigitalRange rangeCol = DigitalRange::Empty();
    DigitalRange rangeRow = DigitalRange::Empty();

    for (; h0 != h1; ++h0) {
      const Hit& hit = *(h0->get());
      if (size==0){ 
         weight = 1.;
         time0 = hit.timestamp();
      }
      else {
         float time_diff = hit.timestamp() - time0;
         weight = exp(-1./2.7*0.073*time_diff);
      }
      col += weight * hit.col();
      row += weight * hit.row();
      ts = std::min(ts, hit.timestamp());
      t_corrected = (hit.timingCorrected()<0) ? -99 : std::min(t_corrected, abs(hit.timingCorrected()));
      t_malta = (hit.timingMalta()<0) ? -99 : std::min(t_malta, abs(hit.timingMalta()));
      t_pico  = (hit.timingPico()<0)  ? -99 : std::min(t_pico, abs(hit.timingPico()));
      t_pico_corrected  = (hit.timingPicoCorrected()<0)  ? -99 : std::min(t_pico_corrected, abs(hit.timingPicoCorrected()));
      t_tot   = (hit.timingTot()<0)   ? -99 : std::min(t_tot, abs(hit.timingTot()));
      value += weight;
      size += 1;
      rangeCol.enclose(DigitalRange(hit.col(), hit.col() + 1));
      rangeRow.enclose(DigitalRange(hit.row(), hit.row() + 1));
      //float tmp_time = hit.timestamp();
      //std::cout<<tmp_time<<std::endl;
    }
    //std::cout<<std::endl;
    col /= value;
    row /= value;

    auto colVar = kVar / rangeCol.length();
    auto rowVar = kVar / rangeRow.length();
    auto tsVar = kVar;
    return Cluster(col, row, ts, t_malta, t_corrected, t_pico, t_pico_corrected, t_tot, value, colVar, rowVar, tsVar);
  };
  auto& sensorEvent = event.getSensorEvent(m_sensor.id());
  clusterize(m_sensor.pixelMask(), sensorEvent, sensorEvent.m_hits.begin(),
             sensorEvent.m_hits.end(), makeCluster);
}

