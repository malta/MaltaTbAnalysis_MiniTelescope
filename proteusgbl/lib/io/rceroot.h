#ifndef PT_RCEROOT_H
#define PT_RCEROOT_H

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include <TTree.h>

#include "loop/reader.h"
#include "loop/writer.h"
#include "utils/definitions.h"
#include "utils/root.h"

namespace toml {
class Value;
}
namespace Io {

/** Common data for RceRoot{Reader,Writer}. */
class RceRootCommon {
protected:
  RceRootCommon(Utils::RootFilePtr&& file);
  ~RceRootCommon() = default;

  /* NOTE: these sizes are used to initialize arrays of track, cluster and
   * hit information. BUT these arrays are generated ONLY ONCE and re-used
   * to load events. Vectors could have been used in the ROOT file format, but
   * they would need to be constructed at each event reading step. */
  static constexpr Index kMaxHits = 1 << 14;
  static constexpr Index kMaxTracks = 1 << 14;

  struct SensorTrees {
    TTree* hits = nullptr;
    TTree* clusters = nullptr;
    TTree* intercepts = nullptr;
    int64_t entries = 0;
  };

  Utils::RootFilePtr m_file;
  int64_t m_entries;
  int64_t m_next;
  // Trees global to the entire event
  TTree* m_eventInfo;
  TTree* m_tracks;
  // Trees containing event-by-event data for each sensors
  std::vector<SensorTrees> m_sensors;

  // global event info
  ULong64_t timestamp;
  ULong64_t frameNumber;
  ULong64_t triggerTime;
  Bool_t invalid;
  // global track state and info
  Int_t numTracks;
  Double_t trackChi2[kMaxTracks];
  Int_t trackDof[kMaxTracks];
  Double_t trackX[kMaxTracks];
  Double_t trackY[kMaxTracks];
  Double_t trackSlopeX[kMaxTracks];
  Double_t trackSlopeY[kMaxTracks];
  Double_t trackCov[kMaxTracks][10];
  // local hits
  Int_t numHits;
  Int_t hitPixX[kMaxHits];
  Int_t hitPixY[kMaxHits];
  Double_t hitTiming[kMaxHits];
  Double_t hitTimingMalta[kMaxHits];
  Double_t hitTimingCorrected[kMaxHits];
  Double_t hitTimingPico[kMaxHits];
  Double_t hitTimingPico_corrected[kMaxHits];
  Double_t hitTimingTot[kMaxHits];
  Double_t hitValue[kMaxHits];
  Int_t hitInCluster[kMaxHits];
  Int_t hit_bcid_long[kMaxHits];
  Int_t hit_bcid_short[kMaxHits];
  Int_t hit_time_fast[kMaxHits];
  Int_t hit_word[kMaxHits];

  // local clusters
  Int_t numClusters;
  Double_t clusterCol[kMaxHits];
  Double_t clusterRow[kMaxHits];
  Double_t clusterVarCol[kMaxHits];
  Double_t clusterVarRow[kMaxHits];
  Double_t clusterCovColRow[kMaxHits];
  Double_t clusterTiming[kMaxHits];
  Double_t clusterTimingCorrected[kMaxHits];
  Double_t clusterTiming_malta[kMaxHits];
  Double_t clusterTiming_pico[kMaxHits];
  Double_t clusterTiming_pico_corrected[kMaxHits];
  Double_t clusterTiming_tot[kMaxHits];
  Double_t clusterValue[kMaxHits];
  Int_t clusterTrack[kMaxHits];
  // local track states
  Int_t numIntercepts;
  Double_t interceptU[kMaxTracks];
  Double_t interceptV[kMaxTracks];
  Double_t interceptSlopeU[kMaxTracks];
  Double_t interceptSlopeV[kMaxTracks];
  Double_t interceptCov[kMaxTracks][10];
  Int_t interceptTrack[kMaxTracks];
};

/** Read events from a RCE ROOT file. */
class RceRootReader : public RceRootCommon, public Loop::Reader {
public:
  /** Return a score of how likely the given path is an RCE Root file. */
  static int check(const std::string& path);
  /** Open the the file. */
  static std::shared_ptr<RceRootReader> open(const std::string& path,
                                             const toml::Value& /* unused */);

  /** Open an existing file and determine the number of sensors and events. */
  RceRootReader(const std::string& path);

  std::string name() const override final;
  uint64_t numEvents() const override final;
  size_t numSensors() const override final;

  void skip(uint64_t n) override final;
  bool read(Storage::Event& event) override final;

private:
  int64_t addSensor(TDirectory* dir);
};

/** Write event in the RCE ROOT file format. */
class RceRootWriter : public RceRootCommon, public Loop::Writer {
public:
  /** Open a new file and truncate existing content. */
  RceRootWriter(const std::string& path, size_t numSensors);
  ~RceRootWriter();

  std::string name() const;

  void append(const Storage::Event& event);

private:
  void addSensor(TDirectory* dir);
};

} // namespace Io

#endif // PT_RCEROOT_H
