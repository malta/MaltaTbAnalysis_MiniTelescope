/**
 * \author  Moritz Kiehn <msmk@cern.ch>
 * \date    2017-09
 */

#ifndef PT_MERGER_H
#define PT_MERGER_H

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include "loop/reader.h"

namespace Io {

/** Event merger that combines data from multiple readers.
 *
 * Assumes that the input streams are synchronized, i.e. the i-th event in each
 * data stream all belong to the same trigger or timestamp. The sensor events
 * from each reader are concatenated according to the order of the input
 * readers and to the order of the sensor events within each reader. They are
 * renumbered accordingly.
 *
 * Only sensor data, i.e. hits and clusters, are merged. Reconstructed data
 * is dropped.
 */
class EventMerger : public Loop::Reader {
public:
  EventMerger(std::vector<std::shared_ptr<Loop::Reader>> readers);

  std::string name() const override final;
  uint64_t numEvents() const override final { return m_events; }
  size_t numSensors() const override final { return m_sensors; }
  void skip(uint64_t n) override final;
  bool read(Storage::Event& event) override final;

private:
  std::vector<std::shared_ptr<Loop::Reader>> m_readers;
  uint64_t m_events;
  size_t m_sensors;
};

} // namespace Io

#endif // PT_MERGER_H
