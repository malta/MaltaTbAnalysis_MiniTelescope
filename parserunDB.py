import csv
import pandas as pd
from pandas_ods_reader import read_ods
import argparse
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("-r", "--run"     , help="Run number"              , type=int           , required=True)
parser.add_argument("-c", "--cosmics"     , help="cosmics"              , type=bool           , required=False, default=False)
parser.add_argument("-p", "--pathRun"     , help="pathRun"              , type=str           , required=True)
args=parser.parse_args()

run=args.run
pathRun= args.pathRun
cosmics=args.cosmics

data = read_ods("/home/sbmuser/MaltaSW/MaltaTbAnalysis/runDB.ods","Sheet1")
data=data[data['runNumber'] == run]
if (cosmics==True):
  _file = open((pathRun+"/ana%06i_config.txt")%(run), "w")
else:
  _file = open((pathRun+"/ana_gbl_%06i_config.txt")%(run), "w")


if (data.empty):
    print("\033[1;31mrun number not found in the spreadsheet!!! exit(0)\033[0m")
    exit(0)
if (data.iloc[0]["pixMask"] is not None):
    data["pixMask"]=[data.iloc[0]["pixMask"].split(';')]

    pixmasked = np.array([])
    for pix in data.iloc[0]["pixMask"]:
        addMask =pix.split('-')
        pixmasked = np.concatenate((pixmasked,addMask), axis=0)

    pixmasked=pixmasked.reshape(3,2)
    data["pixMask"]=[pixmasked]

for c in data.columns:
    if (c=="pixMask"):
        pixels = (data.iloc[0][c])
        if (pixels is not None):
            for x,y in pixels:
                print (c,int(x),int(y))
                _file.write("%s %s %s\n" % (c,str(int(x)),str(int(y))))
    elif (c=="chipID"):
    	_file.write("%s %s\n" % (c , (data.iloc[0][c]))  )
    else:
        if (np.isnan(data.iloc[0][c]) == False):
            print (c, int(data.iloc[0][c]))
            _file.write("%s %s\n" % (c , int(data.iloc[0][c]))  )

_file.close()

